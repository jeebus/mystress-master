﻿// XLSExporter.cs
//
// Written by Niklas Borglund and Jakob Hillerström
//

namespace SmartLocalization.Editor
{
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using ExcelLibrary.SpreadSheet;

public static class XLSExporter 
{
	public static void Write(string path, string sheetName, Dictionary<string,string> values)
	{
		var workbook = CreateWorkbookFromDictionary(sheetName, values);
		if(workbook != null)
		{
			workbook.Save(path);
		}
	}

	public static void Write(string path, string sheetName, List<string> keys, Dictionary<string, Dictionary<string, string>> languages)
	{
		var workbook = CreateWorkbookFromMultipleDictionaries(sheetName, keys, languages);
		if(workbook != null)
		{
			workbook.Save(path);
		}
	}

	internal static Workbook CreateWorkbookFromMultipleDictionaries(string sheetName, List<string> keys, Dictionary<string, Dictionary<string, string>> languages)
	{
		if(string.IsNullOrEmpty(sheetName))
		{
			throw new ArgumentNullException("sheetName");
		}
		if(keys == null)
		{
			throw new ArgumentNullException("keys");
		}
		if(languages == null)
		{
			throw new ArgumentNullException("languages");
		}

		keys.Sort();
		Workbook workbook = new Workbook();
		Worksheet worksheet = new Worksheet(sheetName);
		int column = 0;
		int row = 0;

		worksheet.Cells[row++, column] = new Cell(string.Empty);
		foreach(string key in keys)
		{
			worksheet.Cells[row++, column] = new Cell(key);
		}
		
		column++;
		foreach(var pair in languages)
		{
			row = 0;
			worksheet.Cells[row++, column] = new Cell(pair.Key);
			foreach(string key in keys)
			{
				if(pair.Value.ContainsKey(key))
				{
					worksheet.Cells[row++, column] = new Cell(pair.Value[key]);
				}
				else
				{
					worksheet.Cells[row++, column] = new Cell(string.Empty);
				}
			}

			column++;
		}

		CheckAndAddNumberOfRows(worksheet);

		workbook.Worksheets.Add(worksheet);

		return workbook;
	}

	internal static Workbook CreateWorkbookFromDictionary(string sheetName, Dictionary<string,string> values)
	{
		if(string.IsNullOrEmpty(sheetName))
		{
			throw new ArgumentNullException("sheetName");
		}
		if(values == null)
		{
			throw new ArgumentNullException("values");
		}

		Workbook workbook = new Workbook();
		Worksheet worksheet = new Worksheet(sheetName);
		int column = 0;
		int row = 0;
		foreach(var pair in values)
		{
			worksheet.Cells[row, column] = new Cell(pair.Key);
			worksheet.Cells[row, column +1] = new Cell(pair.Value);
			row++;
		}
		CheckAndAddNumberOfRows(worksheet);
		workbook.Worksheets.Add(worksheet);

		return workbook;
	}

	/// <summary>
	/// To make the xls spreadsheets openable with office 2010 - we need >= 100 cells
	/// Source: http://stackoverflow.com/questions/8107610/cant-open-excel-file-generated-with-excellibrary
	/// </summary>
	internal static void CheckAndAddNumberOfRows(Worksheet sheet)
	{
		for(int i = 0; i < 101; ++i)
		{
			var cell = sheet.Cells[i, 0];
			if(cell == null || cell == Cell.EmptyCell)
			{
				sheet.Cells[i, 0] = new Cell(string.Empty);
			}
		}
	}

	/// <summary>
	///	Read an xls file from the specified path
	/// </summary>
	/// <param name="path">The path to the xls file</param>
	public static List<List<string>> Read(string path)
	{
		var returnValues = new List<List<string>>();
		try
		{
			Workbook book = Workbook.Load(path);
			Worksheet sheet = book.Worksheets[0];
			int maxColumns = sheet.Cells.GetRow(0).LastColIndex;
			for (int rowIndex = sheet.Cells.FirstRowIndex; 
					rowIndex <= sheet.Cells.LastRowIndex; rowIndex++)
			 {
				 Row row = sheet.Cells.GetRow(rowIndex);
				 
				 List<string> currentRowList = new List<string>();

				 for(int columnIndex = 0; columnIndex <= maxColumns; columnIndex++)
				 {
					 Cell currentCell = row.GetCell(columnIndex);
					 if(currentCell != null)
					 {
						 currentRowList.Add(currentCell.StringValue);
					 }
					 else
					 { 
						 currentRowList.Add(string.Empty);
					 }
				 }

				 returnValues.Add(currentRowList);
			 }
		}
		catch(System.Exception ex)
		{
			UnityEngine.Debug.LogError("Failed to read xls file at path: " + path + ", Error: " + ex.Message);
		}

		return returnValues;
	}
}
}
