﻿using iTextSharp.text.pdf;
using iTextSharp.text;

using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace Thrive
{
    /// <summary>
    /// PDF document class, uses the iTextSharp .NET PDF library to generate PDF with.
    /// </summary>
    public class PdfDocument
    {
        static System.DateTime FromDate;
        static System.DateTime ToDate;
        System.DateTime DateInFocus;

        static string UserName;

        static int fieldHeight = 20;
        byte[] chartBarBytes;
        iTextSharp.text.Image logo;

        List<SmileyData> smileyList;

        bool imagesLoaded = false;
		float barMaxLength = 425f;

        /// <summary>
        /// Get if images are loaded
        /// </summary>
        /// <returns>True or false whether or not images are loaded</returns>
        public bool GetImageLoaded()
        {
            return  imagesLoaded;
        }

        /// <summary>
        /// Get localized string based on key
        /// </summary>
        /// <param name="key">key to get localized string for</param>
        /// <returns></returns>
		public string LocalizeKey(string key){
			return SmartLocalization.LanguageManager.Instance.GetTextValue (key);
		}

        /// <summary>
        /// PDFDocument constructor class
        /// </summary>
        /// <param name="fromDate">The System.DateTime from date</param>
        /// <param name="toDate">The System.DateTime to date</param>
        /// <param name="username">Name to put on PDF</param>
        public PdfDocument( System.DateTime fromDate, System.DateTime toDate, string username)
        {
           // LoadImages();
            UserName = username;
            FromDate = fromDate;
            ToDate = toDate;
#if UNITY_EDITOR
			ToDate.AddMilliseconds(1);
#endif
            DateInFocus = FromDate;
			int forMonths = (((toDate.ToLocalTime().Year - fromDate.ToLocalTime().Year) * 12) + toDate.ToLocalTime().Month - fromDate.ToLocalTime().Month) + 1;

			InitFonts ();
			// The resulting PDF file.
			pageHeaderFont =  new iTextSharp.text.Font(baseFont, 10, iTextSharp.text.Font.NORMAL, BaseColor.LIGHT_GRAY);
			pageFooterFont = new iTextSharp.text.Font(baseFont, 10, iTextSharp.text.Font.NORMAL);
			tableHeaderFont = new iTextSharp.text.Font(baseFont, 10, iTextSharp.text.Font.BOLD);
			tableCellFont = new iTextSharp.text.Font(baseFont, 9, iTextSharp.text.Font.NORMAL);

            TotalPageCount = forMonths;
        }

        class SmileyData
        {
            public iTextSharp.text.Image smileyImage;
            public string filePath;

            /// <summary>
            /// SmileyData constructor
            /// </summary>
            /// <param name="newFilePath">Filepath of image</param>
            /// <param name="newImage">Actual loaded image as iTextSharp.text.Image</param>
            public SmileyData(string newFilePath, iTextSharp.text.Image newImage = null)
            {
                smileyImage = newImage;
                filePath = newFilePath;
            }
        }

        /// <summary>
        /// Coroutine to load various smiley images
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator LoadImages()
        {
            string smileyGreenHappyFilePath;
            string smileyGreenNeutralFilePath;
            string smileyGreenSadFilePath;
            string smileyYellowHappyFilePath;
            string smileyYellowNeutralFilePath;
            string smileyYellowSadFilePath;
            string smileyRedHappyFilePath;
            string smileyRedNeutralFilePath;
            string smileyRedSadFilePath;

            string smileyGreyHappyFilePath;
            string smileyGreyNeutralFilePath;
            string smileyGreySadFilePath;

            string chartBarFilePath;

            string logoFilePath;

#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN ||  UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
            smileyGreenHappyFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_GREENSmiley_Happy.png";
            smileyGreenNeutralFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_GREENSmiley_Neutral.png";
            smileyGreenSadFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_GREENSmiley_Sad.png";
            smileyYellowHappyFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_YELLOWSmiley_Happy.png";
            smileyYellowNeutralFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_YELLOWSmiley_Neutral.png";
            smileyYellowSadFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_YELLOWSmiley_Sad.png";
            smileyRedHappyFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_REDSmiley_Happy.png";
            smileyRedNeutralFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_REDSmiley_Neutral.png";
            smileyRedSadFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_REDSmiley_Sad.png";

            smileyGreyHappyFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_GREYSmiley_Happy.png";
            smileyGreyNeutralFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_GREYSmiley_Neutral.png";
            smileyGreySadFilePath = "file://" + Application.streamingAssetsPath + "/Images/GUI_GREYSmiley_Sad.png";

            chartBarFilePath = "file://" + Application.streamingAssetsPath + "/Images/CopingProfilChartBar.png";

            logoFilePath = "file://" + Application.streamingAssetsPath + "/SiigerLogo.png";
#elif UNITY_IOS
			smileyGreenHappyFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_GREENSmiley_Happy.png";
            smileyGreenNeutralFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_GREENSmiley_Neutral.png";
            smileyGreenSadFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_GREENSmiley_Sad.png";
            smileyYellowHappyFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_YELLOWSmiley_Happy.png";
            smileyYellowNeutralFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_YELLOWSmiley_Neutral.png";
            smileyYellowSadFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_YELLOWSmiley_Sad.png";
            smileyRedHappyFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_REDSmiley_Happy.png";
            smileyRedNeutralFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_REDSmiley_Neutral.png";
            smileyRedSadFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_REDSmiley_Sad.png";

            smileyGreyHappyFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_GREYSmiley_Happy.png";
            smileyGreyNeutralFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_GREYSmiley_Neutral.png";
            smileyGreySadFilePath = "file://" + Application.dataPath + "/Raw/Images/GUI_GREYSmiley_Sad.png";

            chartBarFilePath = "file://" + Application.dataPath + "/Raw/Images/CopingProfilChartBar.png";

            logoFilePath = "file://" + Application.dataPath + "/Raw/SiigerLogo.png"; //"file:" + Path.Combine(Application.streamingAssetsPath, "SiigerLogo.png");
#elif UNITY_ANDROID
            smileyGreenHappyFilePath = Application.streamingAssetsPath + "/Images/GUI_GREENSmiley_Happy.png";
            smileyGreenNeutralFilePath = Application.streamingAssetsPath + "/Images/GUI_GREENSmiley_Neutral.png";
            smileyGreenSadFilePath = Application.streamingAssetsPath + "/Images/GUI_GREENSmiley_Sad.png";
            smileyYellowHappyFilePath =  Application.streamingAssetsPath + "/Images/GUI_YELLOWSmiley_Happy.png";
            smileyYellowNeutralFilePath = Application.streamingAssetsPath + "/Images/GUI_YELLOWSmiley_Neutral.png";
            smileyYellowSadFilePath =  Application.streamingAssetsPath + "/Images/GUI_YELLOWSmiley_Sad.png";
            smileyRedHappyFilePath = Application.streamingAssetsPath + "/Images/GUI_REDSmiley_Happy.png";
            smileyRedNeutralFilePath = Application.streamingAssetsPath + "/Images/GUI_REDSmiley_Neutral.png";
            smileyRedSadFilePath = Application.streamingAssetsPath + "/Images/GUI_REDSmiley_Sad.png";


            smileyGreyHappyFilePath = Application.streamingAssetsPath + "/Images/GUI_GREYSmiley_Happy.png";
            smileyGreyNeutralFilePath = Application.streamingAssetsPath + "/Images/GUI_GREYSmiley_Neutral.png";
            smileyGreySadFilePath = Application.streamingAssetsPath + "/Images/GUI_GREYSmiley_Sad.png";

            chartBarFilePath = Application.streamingAssetsPath + "/Images/CopingProfilChartBar.png";

            logoFilePath = Application.streamingAssetsPath + "/SiigerLogo.png";
#endif
			smileyList = new List<SmileyData>();


            smileyList.Add(new SmileyData( smileyGreenHappyFilePath));
            smileyList.Add(new SmileyData( smileyGreenNeutralFilePath));
            smileyList.Add(new SmileyData( smileyGreenSadFilePath));
            smileyList.Add(new SmileyData( smileyYellowHappyFilePath));
            smileyList.Add(new SmileyData( smileyYellowNeutralFilePath));
            smileyList.Add(new SmileyData( smileyYellowSadFilePath));
            smileyList.Add(new SmileyData( smileyRedHappyFilePath));
            smileyList.Add(new SmileyData( smileyRedNeutralFilePath));
            smileyList.Add(new SmileyData( smileyRedSadFilePath));
            smileyList.Add(new SmileyData( smileyGreyHappyFilePath));
            smileyList.Add(new SmileyData( smileyGreyNeutralFilePath));
            smileyList.Add(new SmileyData( smileyGreySadFilePath));

            for (int i = 0; i < smileyList.Count; i++)
            {
				if(smileyList.Count > i){
	                WWW www = new WWW(smileyList[i].filePath);
	                yield return www;
	                byte[] smileyImageBytes = www.bytes;

					if(smileyImageBytes.Length > 0){
		                smileyList[i].smileyImage = iTextSharp.text.Image.GetInstance(smileyImageBytes);
		                smileyList[i].smileyImage.Alignment = Element.ALIGN_CENTER;
		                smileyList[i].smileyImage.ScaleAbsoluteHeight(25);
		                smileyList[i].smileyImage.ScaleAbsoluteWidth(25);
					}
				}
            }

            WWW www1 = new WWW(logoFilePath);
            yield return www1;
            logo = iTextSharp.text.Image.GetInstance( www1.bytes);
            
            www1 = new WWW(chartBarFilePath);
            yield return www1;
            chartBarBytes = www1.bytes;
          
            imagesLoaded = true;
        }

        /// <summary>
        /// Initilizations fonts used in PDF generation
        /// </summary>
		public static void InitFonts()
        {
            string fontPath;
#if UNITY_IOS ||  UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
             fontPath = Path.Combine (Application.streamingAssetsPath + "/Fonts", "ProximaNova_light.otf");
             baseFont = BaseFont.CreateFont(fontPath, BaseFont.CP1252, BaseFont.EMBEDDED);
#elif UNITY_ANDROID
            fontPath = Application.streamingAssetsPath + "/Fonts/ProximaNova_light.otf";
            DateTime timeNow = DateTime.UtcNow;
            WWW www = new WWW( fontPath);
            while(!www.isDone){
                if ((timeNow - DateTime.UtcNow).TotalSeconds > 5)
                {
                    //timeout after 5 seconds
                    Debug.Log("WWW class timedout trying to find fontPath");
                    return;
                }
            }
            baseFont = BaseFont.CreateFont("ProximaNova_light.otf", BaseFont.CP1252, BaseFont.EMBEDDED, BaseFont.CACHED, www.bytes, null);
#endif
		}

		static BaseFont baseFont;
		static iTextSharp.text.Font pageHeaderFont ;
		static iTextSharp.text.Font pageFooterFont;
		static iTextSharp.text.Font tableHeaderFont;
		static iTextSharp.text.Font tableCellFont;
        static int TotalPageCount;

        /// <summary>
        /// Inner class to add a table as header and footer.
        /// </summary>
        class TableHeader : PdfPageEventHelper
        {
            System.DateTime DateInFocus = FromDate;
           
            // The header username text.
            String header = UserName;
            // The template with the total number of pages. 
           // PdfTemplate total;

            public string GetDateInFocus()
            {
				int month = DateInFocus.ToLocalTime().Month;
				int year = DateInFocus.ToLocalTime().Year;
                return  SmartLocalization.LanguageManager.Instance.GetTextValue("Generic.Month." + month) + " " + year;
            }

            public void SetMonth(DateTime newDate)
            {
                DateInFocus = newDate;
            }

            public void SetLogo(iTextSharp.text.Image logo)
            {
                this.logo = logo;
            }

            iTextSharp.text.Image logo;


            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
               // total = writer.DirectContent.CreateTemplate(30, 16);
            }

 
            public override void OnEndPage(PdfWriter writer, Document document)
            {
                PdfPTable table = new PdfPTable(3);
                
                
                //Page header

                logo.SetAbsolutePosition(430, 770);
                logo.ScaleAbsoluteHeight(30);
                logo.ScaleAbsoluteWidth(70);
                table.SetTotalWidth(new float[] {50, 350, 150 });
                table.DefaultCell.BackgroundColor = new BaseColor(100,0,0);
                 
                table.LockedWidth = true;
                table.DefaultCell.FixedHeight = fieldHeight;
                //table.DefaultCell.Border = Rectangle.NO_BORDER;
				table.DefaultCell.BorderColor = table.DefaultCell.BackgroundColor;
                PdfPCell logocell = table.DefaultCell;
                logocell.AddElement(logo);
                table.AddCell(logocell);

                table.AddCell(new Paragraph(String.Format("MyStress: {0}", header), pageHeaderFont));

                table.DefaultCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(new Paragraph(String.Format("{0}", GetDateInFocus()), pageHeaderFont));
                table.WriteSelectedRows(0, -1, 25, 803, writer.DirectContent);

                //Page footer
                   
                //Siiger logo
                PdfPTable siigerTbl = new PdfPTable(1);
                Paragraph siigerLogo = new Paragraph("Siiger Ledelsesudvikling", pageFooterFont);
                siigerLogo.Alignment = Element.ALIGN_LEFT;
                siigerTbl.TotalWidth = 300;
                siigerTbl.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell siigerCell = new PdfPCell(siigerLogo);
                siigerCell.Border = 0;
                siigerCell.PaddingLeft = 10;
                siigerTbl.AddCell(siigerCell);
                siigerTbl.WriteSelectedRows(0, -1, 15, 30, writer.DirectContent);

                //Page number
                PdfPTable pageNrTbl = new PdfPTable(1);
                Paragraph pageNumber = new Paragraph(String.Format("Side {0} / {1}", writer.PageNumber, TotalPageCount*2), pageFooterFont);
                pageNumber.Alignment = Element.ALIGN_RIGHT;
                pageNrTbl.TotalWidth = 300;
                pageNrTbl.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell pageNrCell = new PdfPCell(pageNumber);
                pageNrCell.Border = 0;
                pageNrCell.PaddingLeft = 10;
                pageNrTbl.AddCell(pageNrCell);
                pageNrTbl.WriteSelectedRows(0, -1, 515, 30, writer.DirectContent);
            }

			/*
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                
               // ColumnText.ShowTextAligned(total, Element.ALIGN_LEFT,                     new Phrase((writer.PageNumber - 1).ToString()),                    2, 2, 0);
              
                base.OnCloseDocument(writer, document);

                total.BeginText();
                total.SetFontAndSize(baseFont, 10.0f);
                total.SetTextMatrix(0, 0);
                total.ShowText("dfsdfsdfwef324234rewREWFGDFG" + (writer.PageNumber - 1));
                total.EndText();
            }
            */
        }

        /// <summary>
        /// An IPdfPCellEvent for adding images to table cells
        /// </summary>
        class WatermarkedCell : IPdfPCellEvent {
            Paragraph watermark;
			Paragraph trailingWatermark;

            public WatermarkedCell(Paragraph watermark)
            {
                this.watermark = watermark;
            }

			public WatermarkedCell(Paragraph watermark, Paragraph trailingWatermark){

				this.watermark = watermark;
				this.trailingWatermark = trailingWatermark;
			}

            public void CellLayout(PdfPCell cell, Rectangle position,
                PdfContentByte[] canvases) {
                PdfContentByte canvas = canvases[PdfPTable.TEXTCANVAS];
                ColumnText.ShowTextAligned(canvas, Element.ALIGN_MIDDLE,
                    watermark,
                    (position.Left ),
                    (position.Bottom + (position.Height / 3)), 0);

				if (trailingWatermark != null) {
					ColumnText.ShowTextAligned (canvas, Element.ALIGN_MIDDLE,
					                            trailingWatermark,
					                            (position.Right - trailingWatermark.Chunks[0].GetWidthPoint()),
				                           (position.Bottom + (position.Height / 3)), 0);
				}

            }
        }

        /// <summary>
        /// Creates new header table cell
        /// </summary>
        /// <param name="para">Paragraph to display in cell</param>
        /// <returns>PDFCell with Paragraph</returns>
        private PdfPCell GetNewHeaderCell(Paragraph para)
        {
            PdfPCell cell = new PdfPCell();
            PdfPTable headerTable = new PdfPTable(2);
            headerTable.WidthPercentage = 100;

			headerTable.AddCell(GetInnerCell(para, PdfPCell.ALIGN_LEFT, iTextSharp.text.BaseColor.GRAY));
			headerTable.AddCell(GetInnerCell(new Paragraph(LocalizeKey("Printable.AntalMarkeringer"), tableHeaderFont), PdfPCell.ALIGN_RIGHT, iTextSharp.text.BaseColor.GRAY));
            cell.AddElement(headerTable);

            cell.FixedHeight = fieldHeight;
            cell.Border = 0;
            cell.Padding = 0;
            
            return cell;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="alignment"></param>
        /// <param name="color"></param>
        /// <returns></returns>
		public PdfPCell GetInnerCell(Paragraph text, int alignment, iTextSharp.text.BaseColor color)
        {
            PdfPCell cell = new PdfPCell(text);
            cell.BackgroundColor = color;
			cell.BorderColor = color;
            cell.FixedHeight = fieldHeight;
            cell.HorizontalAlignment = alignment;
            //cell.Border = 0;
            return cell;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        private PdfPCell GetNewCalendarCell(Paragraph para)
        {
            PdfPCell cell = new PdfPCell();
            PdfPTable headerTable = new PdfPTable(1);
            headerTable.WidthPercentage = 100;
			headerTable.DefaultCell.BorderColor = iTextSharp.text.BaseColor.GRAY;
            headerTable.AddCell(GetInnerCell(para, PdfPCell.ALIGN_LEFT, iTextSharp.text.BaseColor.GRAY));

            cell.AddElement(headerTable);
            cell.FixedHeight = fieldHeight;
            cell.HorizontalAlignment = 2;
            cell.Border = 0;
            cell.Padding = 0;
            return cell;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="para"></param>
        /// <param name="scaledImageWidth"></param>
        /// <returns></returns>
        private PdfPCell GetCopingProfilCell(Paragraph para, float scaledImageWidth)
        {
            iTextSharp.text.Image barImage = iTextSharp.text.Image.GetInstance( chartBarBytes);

            barImage.ScaleAbsoluteWidth(scaledImageWidth);

			 if (float.IsNaN (scaledImageWidth)) {
				scaledImageWidth = 0;
			}

            PdfPCell imgCell = new PdfPCell(barImage);
		
			imgCell.CellEvent = new WatermarkedCell (para, new Paragraph ((scaledImageWidth / barMaxLength * 100).ToString ("0.00") + " %", tableCellFont));

			imgCell.FixedHeight = fieldHeight;
			imgCell.BackgroundColor = BaseColor.LIGHT_GRAY;
			imgCell.Border = 0;
			imgCell.VerticalAlignment = Element.ALIGN_MIDDLE;

			return imgCell;
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
		private PdfPCell GetNewHeaderCopingCell(Paragraph para)
        {
            PdfPCell cell = new PdfPCell();
            PdfPTable headerTable = new PdfPTable(1);
            headerTable.WidthPercentage = 100;
			headerTable.AddCell(GetInnerCell(para, PdfPCell.ALIGN_LEFT, iTextSharp.text.BaseColor.GRAY));
            cell.AddElement(headerTable);
            cell.FixedHeight = fieldHeight;
            cell.Border = 0;
            cell.Padding = 0;
            return cell;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columnSize"></param>
        /// <returns></returns>
        PdfPTable GetNewPageTable(int columnSize)
        {
            PdfPTable table = new PdfPTable(columnSize);
            table.HorizontalAlignment = Element.ALIGN_LEFT;

            float[] widths = new float[columnSize];
            for (int i = 0; i < widths.Length; i++)
            {
                widths[i] =  265.0f / columnSize;
            }

            table.DefaultCell.Border = 0;
            table.DefaultCell.Padding = 0;
            table.DefaultCell.FixedHeight = fieldHeight;

            table.SetTotalWidth(widths);
            table.LockedWidth = true;
            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copingString"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public PdfPTable GetCopingStringCellEven(string copingString, string amount)
        {
            PdfPTable combinedCellTable =  GetNewPageTable(2);
            combinedCellTable.SetWidthPercentage(new float[] { 90.0f, 10.0f }, PageSize.A4);
            PdfPCell leftCell = GetInnerCell(new Paragraph(copingString, tableCellFont), PdfPCell.ALIGN_LEFT, iTextSharp.text.BaseColor.WHITE);

            PdfPCell rightCell = GetInnerCell(new Paragraph(amount, tableCellFont), PdfPCell.ALIGN_RIGHT, iTextSharp.text.BaseColor.WHITE);
    
            combinedCellTable.AddCell(leftCell);
            combinedCellTable.AddCell(rightCell);

            return combinedCellTable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copingString"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public PdfPTable GetCopingStringCellOdd(string copingString, string amount)
        {
            PdfPTable combinedCellTable =  GetNewPageTable(2);
            combinedCellTable.SetWidthPercentage(new float[] { 90.0f, 10.0f }, PageSize.A4);
            PdfPCell leftCell = GetInnerCell(new Paragraph(copingString, tableCellFont), PdfPCell.ALIGN_LEFT, iTextSharp.text.BaseColor.LIGHT_GRAY);

            PdfPCell rightCell = GetInnerCell(new Paragraph(amount, tableCellFont), PdfPCell.ALIGN_RIGHT, iTextSharp.text.BaseColor.LIGHT_GRAY);

            combinedCellTable.AddCell(leftCell);
            combinedCellTable.AddCell(rightCell);

            return combinedCellTable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void createPdf(String filename)
        {
            Document document = new Document(PageSize.A4, 25, 25, 74, 76);
            var output = new FileStream(filename, FileMode.Create);
            PdfWriter writer = PdfWriter.GetInstance(document, output);
            TableHeader tableEvent = new TableHeader();
		
            writer.PageEvent = tableEvent;
            tableEvent.SetLogo(logo);
            document.Open();
            for (int i = 0; i < TotalPageCount; i++)
            {
                //Go to next month
                if (i > 0)
                {
                    DateInFocus = DateInFocus.AddMonths(1);
                    ((TableHeader)writer.PageEvent).SetMonth(DateInFocus);
                    ResetCopingAmount();
                }

                InitResetSymptomsAndTriggerStrings();
                UpdateTriggerStrings();
                UpdateCopingStringsAndChart();
                RefreshCalendar();
               

                PdfPTable rootPageTable =  new PdfPTable(3);
                rootPageTable.DefaultCell.Border = 0;
                rootPageTable.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                float[] widths = new float[] { 265f,18,265f };
                rootPageTable.SetTotalWidth(widths);
                rootPageTable.LockedWidth = true;

                PdfPTable rightPageTable = new PdfPTable(1);
                rightPageTable.DefaultCell.Border = 0;
                rightPageTable.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                rightPageTable.SetTotalWidth(new float[] { 265f });
                rightPageTable.LockedWidth = true;

                PdfPTable leftPageTable = new PdfPTable(1);
                leftPageTable.DefaultCell.Border = 0;
                leftPageTable.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                leftPageTable.SetTotalWidth(new float[] { 265f });
                leftPageTable.LockedWidth = true;



                //Create coping header
                PdfPTable table = GetNewPageTable(1);
				PdfPCell cell = GetNewHeaderCell(new Paragraph(LocalizeKey("Printable.Coping"), tableHeaderFont));
                table.AddCell(cell);
                table.SpacingAfter = fieldHeight;
                leftPageTable.AddCell(table);


                //Create calendar header
                table = GetNewPageTable(1);
				cell = GetNewHeaderCopingCell(new Paragraph(LocalizeKey("Printable.Calendar"), tableHeaderFont));
                table.AddCell(cell);
                table.SpacingAfter = fieldHeight;
                rightPageTable.AddCell(table);

                
                table = GetNewPageTable(1);
				cell = GetNewHeaderCopingCell(new Paragraph(LocalizeKey("Printable.AfledningHeader"), tableHeaderFont));
                table.AddCell(cell);
                PdfPTable combinedCellTable;
                for (int n = 0; n < afbrydeStrings.Length; n++)
                {
                    if (n % 2 == 0)
                    {
                        combinedCellTable = GetCopingStringCellEven(LocalizeKey(afbrydeStrings[n].copingString), afbrydeStrings[n].amount.ToString());
                    }
                    else
                    {
                        combinedCellTable = GetCopingStringCellOdd(LocalizeKey(afbrydeStrings[n].copingString), afbrydeStrings[n].amount.ToString());
                    }
                    table.AddCell(combinedCellTable);
                }
                table.AddCell(table.DefaultCell);
                leftPageTable.AddCell(table);

                PdfPCell innerCell;

                //Create calendar component
                table =  GetNewPageTable(7);

                table.SpacingAfter = 0;
				innerCell = GetNewCalendarCell(new Paragraph(LocalizeKey("Generic.Day.1.Short"), tableCellFont));
                table.AddCell(innerCell);
				innerCell = GetNewCalendarCell(new Paragraph(LocalizeKey("Generic.Day.2.Short"), tableCellFont));
                table.AddCell(innerCell);
				innerCell = GetNewCalendarCell(new Paragraph(LocalizeKey("Generic.Day.3.Short"), tableCellFont));
                table.AddCell(innerCell);
				innerCell = GetNewCalendarCell(new Paragraph(LocalizeKey("Generic.Day.4.Short"), tableCellFont));
                table.AddCell(innerCell);
				innerCell = GetNewCalendarCell(new Paragraph(LocalizeKey("Generic.Day.5.Short"), tableCellFont));
                table.AddCell(innerCell);
				innerCell = GetNewCalendarCell(new Paragraph(LocalizeKey("Generic.Day.6.Short"), tableCellFont));
                table.AddCell(innerCell);
				innerCell = GetNewCalendarCell(new Paragraph(LocalizeKey("Generic.Day.7.Short"), tableCellFont));
                table.AddCell(innerCell);
                

//				Debug.Log("Amount of smileys in array: " + CurrentVisibleMonth.Length);
                for (int j = 0; j < CurrentVisibleMonth.Length; j++)
                {
                    PdfPCell smileycell = new PdfPCell();
                    smileycell.BackgroundColor = BaseColor.LIGHT_GRAY;
				

					//Has to be done this way to completely avoid seeing borders on PDF viewers on OSX and iOS
					smileycell.BorderColorLeft = BaseColor.LIGHT_GRAY;
					smileycell.BorderColorRight = BaseColor.LIGHT_GRAY;
					smileycell.BorderColorTop = BaseColor.WHITE;
					smileycell.BorderColorBottom = BaseColor.LIGHT_GRAY;
					smileycell.BorderWidthLeft = 0.1f;
					smileycell.BorderWidthRight = 0.1f;
					smileycell.BorderWidthTop = 1f;
					smileycell.BorderWidthBottom = 1f;

					smileycell.PaddingTop = -4;
                    smileycell.FixedHeight = 34f;
                    if (CurrentVisibleMonth[j].date > 0)
                    {
                        smileycell.AddElement(new Paragraph(CurrentVisibleMonth[j].date.ToString(), tableCellFont));
                    }

                    
                    smileycell.HorizontalAlignment = 0;
                    if (CurrentVisibleMonth[j].smileyImage != null)
                    {
                        smileycell.AddElement(CurrentVisibleMonth[j].smileyImage);
                    }
                    table.AddCell(smileycell);
                }
                rightPageTable.AddCell(table);


                //Create copingprofil header
                table = GetNewPageTable(1);
				cell = GetNewHeaderCell(new Paragraph(LocalizeKey("Printable.CopingProfil"), tableHeaderFont));
                table.AddCell(cell);
                table.SpacingBefore = fieldHeight;
                table.SpacingAfter = fieldHeight;
                rightPageTable.AddCell(table);

				float totalAmount = afledningsAmount.Count + overblikAmount.Count + forhandleAmount.Count + bedeHjælpAmount.Count + bakkeUdAmount.Count;

                //Create copingprofil component
                table = GetNewPageTable(1);

				table.AddCell(GetCopingProfilCell(new Paragraph(LocalizeKey("Printable.Afledning"), tableCellFont), ((afledningsAmount.Count ) / totalAmount) * barMaxLength));
				table.AddCell(GetCopingProfilCell(new Paragraph(LocalizeKey("Printable.Overblik"), tableCellFont), ((overblikAmount.Count ) / totalAmount) * barMaxLength));
				table.AddCell(GetCopingProfilCell(new Paragraph(LocalizeKey("Printable.Forhandle"), tableCellFont), ((forhandleAmount.Count ) / totalAmount) * barMaxLength));
				table.AddCell(GetCopingProfilCell(new Paragraph(LocalizeKey("Printable.Hjælp"), tableCellFont), ((bedeHjælpAmount.Count ) / totalAmount) * barMaxLength));
				table.AddCell(GetCopingProfilCell(new Paragraph(LocalizeKey("Printable.Bakke"), tableCellFont), ((bakkeUdAmount.Count ) / totalAmount) * barMaxLength));
                rightPageTable.AddCell(table);


                //add empty cell
                rightPageTable.AddCell(rootPageTable.DefaultCell);

                table = GetNewPageTable(1);
				cell = GetNewHeaderCopingCell(new Paragraph(LocalizeKey("Printable.OverblikHeader"), tableHeaderFont));
                table.AddCell(cell);
                for (int n = 0; n < forhandleStrings.Length; n++)
                {
                    if (n % 2 == 0)
                    {
                        combinedCellTable = GetCopingStringCellEven(LocalizeKey(overblikStrings[n].copingString), overblikStrings[n].amount.ToString());
                    }
                    else
                    {
                        combinedCellTable = GetCopingStringCellOdd(LocalizeKey(overblikStrings[n].copingString), overblikStrings[n].amount.ToString());
                    }
                    table.AddCell(combinedCellTable);
                }
                table.AddCell(table.DefaultCell);
                leftPageTable.AddCell(table);

                 
                table = GetNewPageTable(1);
				cell = GetNewHeaderCopingCell(new Paragraph(LocalizeKey("Printable.ForhandleHeader"), tableHeaderFont));
                table.AddCell(cell);
                for (int n = 0; n < forhandleStrings.Length; n++)
                {
                    if (n % 2 == 0)
                    {
                        combinedCellTable = GetCopingStringCellEven(LocalizeKey(forhandleStrings[n].copingString), forhandleStrings[n].amount.ToString());
                    }
                    else
                    {
                        combinedCellTable = GetCopingStringCellOdd(LocalizeKey(forhandleStrings[n].copingString), forhandleStrings[n].amount.ToString());
                    }
                    table.AddCell(combinedCellTable);
                }
                table.AddCell(table.DefaultCell);
                leftPageTable.AddCell(table);


                table = GetNewPageTable(1);
				cell = GetNewHeaderCopingCell(new Paragraph(LocalizeKey("Printable.HjælpHeader"), tableHeaderFont));
                table.AddCell(cell);
                for (int n = 0; n < hjælpStrings.Length; n++)
                {
                    if (n % 2 == 0)
                    {
                        combinedCellTable = GetCopingStringCellEven(LocalizeKey(hjælpStrings[n].copingString), hjælpStrings[n].amount.ToString());
                    }
                    else
                    {
                        combinedCellTable = GetCopingStringCellOdd(LocalizeKey(hjælpStrings[n].copingString), hjælpStrings[n].amount.ToString());
                    }
                    table.AddCell(combinedCellTable);
                }
                table.AddCell(table.DefaultCell);
                leftPageTable.AddCell(table);


                table = GetNewPageTable(1);
				cell = GetNewHeaderCopingCell(new Paragraph(LocalizeKey("Printable.BakkeHeader"), tableHeaderFont));
                table.AddCell(cell);
                for (int n = 0; n < bakkeStrings.Length; n++)
                {
                    if (n % 2 == 0)
                    {
                        combinedCellTable = GetCopingStringCellEven(LocalizeKey(bakkeStrings[n].copingString), bakkeStrings[n].amount.ToString());
                    }
                    else
                    {
                        combinedCellTable = GetCopingStringCellOdd(LocalizeKey(bakkeStrings[n].copingString), bakkeStrings[n].amount.ToString());
                    }
                    table.AddCell(combinedCellTable);
                }
                table.AddCell(table.DefaultCell);
                leftPageTable.AddCell(table);

               
                rootPageTable.AddCell(leftPageTable);
                //add empty cell
                rootPageTable.AddCell(rootPageTable.DefaultCell);
                rootPageTable.AddCell(rightPageTable);

                document.Add(rootPageTable);
       
                //Second page
                document.NewPage();

                PdfPTable root2ndPageTable = new PdfPTable(3);
                root2ndPageTable.DefaultCell.Border = 0;
                root2ndPageTable.HorizontalAlignment = Element.ALIGN_LEFT;
                widths = new float[] { 265f, 18f, 265f };
                root2ndPageTable.SetTotalWidth(widths);
                root2ndPageTable.LockedWidth = true;

                //SYMPTOMER
                table = GetNewPageTable(1);
				cell = GetNewHeaderCell(new Paragraph(LocalizeKey("Printable.Symptoms"), tableHeaderFont));
                table.AddCell(cell);
              
                for (int l = 0; l < amountOfSymptomStrings; l++)
                {
                    if (l % 2 == 0)
                    {
                        combinedCellTable = GetCopingStringCellEven(LocalizeKey(symptomsStrings[l].copingString), symptomsStrings[l].amount.ToString());
                    }
                    else
                    {
                        combinedCellTable = GetCopingStringCellOdd(LocalizeKey(symptomsStrings[l].copingString), symptomsStrings[l].amount.ToString());
                    }
                    table.AddCell(combinedCellTable);
                }

                root2ndPageTable.AddCell(table);
                //add empty cell
                root2ndPageTable.AddCell(rootPageTable.DefaultCell);

                //TRIGGERS
                table = GetNewPageTable(1);
				cell = GetNewHeaderCell(new Paragraph(LocalizeKey("Printable.Triggere"), tableHeaderFont));
                table.AddCell(cell);
              
                for (int l = 0; l < amountOfTriggerStrings; l++)
                {
                    if (l % 2 == 0)
                    {
                        combinedCellTable = GetCopingStringCellEven(LocalizeKey(triggerStrings[l].copingString), triggerStrings[l].amount.ToString());
                    }
                    else
                    {
                        combinedCellTable = GetCopingStringCellOdd(LocalizeKey(triggerStrings[l].copingString), triggerStrings[l].amount.ToString());
                    }
                    table.AddCell(combinedCellTable);
                }

                root2ndPageTable.AddCell(table);
                document.Add(root2ndPageTable);


                root2ndPageTable = new PdfPTable(3);
                root2ndPageTable.DefaultCell.Border = 0;
                root2ndPageTable.HorizontalAlignment = Element.ALIGN_LEFT;
                widths = new float[] { 265f, 18f, 265f };
                root2ndPageTable.SetTotalWidth(widths);
                root2ndPageTable.LockedWidth = true;
                
                //EGNE SYMPTOMER
                table = GetNewPageTable(1);
                table.SpacingBefore = fieldHeight;
				cell = GetNewHeaderCell(new Paragraph(LocalizeKey("Printable.EgneSymptomer"), tableHeaderFont));
                table.AddCell(cell);

                for (int j = 0; j < userAddedSymptomsStrings.Count; j++)
                {
                    if (j % 2 == 0)
                    {
                        combinedCellTable = GetCopingStringCellEven(userAddedSymptomsStrings[j].copingString, userAddedSymptomsStrings[j].amount.ToString());
                    }
                    else
                    {
                        combinedCellTable = GetCopingStringCellOdd(userAddedSymptomsStrings[j].copingString, userAddedSymptomsStrings[j].amount.ToString());
                    }
                    table.AddCell(combinedCellTable);
                } 
                root2ndPageTable.AddCell(table);

                //add empty cell
                root2ndPageTable.AddCell(rootPageTable.DefaultCell);

                //EGNE TRIGGERE
                table = GetNewPageTable(1);
                table.SpacingBefore = fieldHeight;
				cell = GetNewHeaderCell(new Paragraph(LocalizeKey("Printable.EgneTriggere"), tableHeaderFont));
                table.AddCell(cell);

                for (int j = 0; j < userAddedTriggerStrings.Count; j++)
                {
                    if (j % 2 == 0)
                    {
                        combinedCellTable = GetCopingStringCellEven(userAddedTriggerStrings[j].copingString, userAddedTriggerStrings[j].amount.ToString());
                    }
                    else
                    {
                        combinedCellTable = GetCopingStringCellOdd(userAddedTriggerStrings[j].copingString, userAddedTriggerStrings[j].amount.ToString());
                    }
                    table.AddCell(combinedCellTable);
                } 
             
                root2ndPageTable.AddCell(table);
                document.Add(root2ndPageTable);
                document.NewPage();

            }
            document.Close();
        }

        /// <summary>
        /// 
        /// </summary>
		class CopingStringClass
		{
			public int amount;
			public string copingString;
			
			public CopingStringClass(int newAmount, string newString)
			{
				amount = newAmount;
				copingString = newString;
			}
			
			public void CountAmount1Up()
			{
				amount++;
			}
		}
		
		CopingStringClass[] afbrydeStrings = new CopingStringClass[] { new CopingStringClass(0, "Coping.1"), new CopingStringClass(0, "Coping.6"), new CopingStringClass(0, "Coping.14"), new CopingStringClass(0, "Coping.16") };
        CopingStringClass[] overblikStrings = new CopingStringClass[] { new CopingStringClass(0, "Coping.2"), new CopingStringClass(0, "Coping.3"), new CopingStringClass(0, "Coping.4"), new CopingStringClass(0, "Coping.7") };
        CopingStringClass[] forhandleStrings = new CopingStringClass[] { new CopingStringClass(0, "Coping.9"), new CopingStringClass(0, "Coping.11"), new CopingStringClass(0, "Coping.12"), new CopingStringClass(0, "Coping.17") };
        CopingStringClass[] hjælpStrings = new CopingStringClass[] { new CopingStringClass(0, "Coping.5"), new CopingStringClass(0, "Coping.8"), new CopingStringClass(0, "Coping.10"), new CopingStringClass(0, "Coping.15") };
        CopingStringClass[] bakkeStrings = new CopingStringClass[] { new CopingStringClass(0, "Coping.13"), new CopingStringClass(0, "Coping.18"), new CopingStringClass(0, "Coping.19"), new CopingStringClass(0, "Coping.20") };

        List<string> afledningsAmount;
        List<string> overblikAmount;
        List<string> forhandleAmount;
        List<string> bedeHjælpAmount;
        List<string> bakkeUdAmount;

        int amountOfSymptomStrings = 15;
        int amountOfTriggerStrings = 16;
        CopingStringClass[] symptomsStrings;
        CopingStringClass[] triggerStrings;

        List<CopingStringClass> userAddedTriggerStrings = new List<CopingStringClass>();
        List<CopingStringClass> userAddedSymptomsStrings = new List<CopingStringClass>();

        /// <summary>
        /// 
        /// </summary>
        public void InitResetSymptomsAndTriggerStrings()
        {
            symptomsStrings = new CopingStringClass[amountOfSymptomStrings];
            for (int i = 0; i < symptomsStrings.Length; i++)
            {
                symptomsStrings[i] = new CopingStringClass(0, "Symptoms." + (i+1));
            }
            triggerStrings = new CopingStringClass[amountOfTriggerStrings];
            for (int i = 0; i < triggerStrings.Length; i++)
            {
                triggerStrings[i] = new CopingStringClass(0, "Triggers." + (i + 1));
            }

            for (int i = 0; i < userAddedTriggerStrings.Count; i++)
            {
                userAddedTriggerStrings[i].amount = 0;
            }

            for (int i = 0; i < userAddedSymptomsStrings.Count; i++)
            {
                userAddedSymptomsStrings[i].amount = 0;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public void UpdateCopingStringsAndChart()
        {
            afledningsAmount = new List<string>();
            overblikAmount = new List<string>();
            forhandleAmount = new List<string>();
            bedeHjælpAmount = new List<string>();
            bakkeUdAmount = new List<string>();

            for (int i = 0; i < MyStressUserContent.sessionStats.Count; i++)
            {
				if (MyStressUserContent.sessionStats[i].regDate.ToLocalTime().Month == DateInFocus.ToLocalTime().Month && MyStressUserContent.sessionStats[i].regDate.ToLocalTime().Year == DateInFocus.ToLocalTime().Year)
                {
                    for (int j = 0; j < MyStressUserContent.sessionStats[i].coping.Count; j++)
                    {
                        if (MyStressUserContent.sessionStats[i].coping[j].selected)
                        {
                            switch (MyStressUserContent.sessionStats[i].coping[j].category)
                            {
                                case CopingCategory.AFLEDNING:
                                    for (int k = 0; k < afbrydeStrings.Length; k++)
                                    {
                                        if (afbrydeStrings[k].copingString == MyStressUserContent.sessionStats[i].coping[j].componentString)
                                        {
                                            afbrydeStrings[k].CountAmount1Up();
                                            afledningsAmount.Add(afbrydeStrings[k].copingString);
                                        }
                                    }
                                    break;
                               
                                case CopingCategory.OVERBLIK:
                                    for (int k = 0; k < overblikStrings.Length; k++)
                                    {
                                        if (overblikStrings[k].copingString == MyStressUserContent.sessionStats[i].coping[j].componentString)
                                        {
                                            overblikStrings[k].CountAmount1Up();
                                            overblikAmount.Add(overblikStrings[k].copingString);
                                        }
                                    }
                                    break;
                              
                                case CopingCategory.FORHANDLE:
                                    for (int k = 0; k < forhandleStrings.Length; k++)
                                    {
                                        if (forhandleStrings[k].copingString == MyStressUserContent.sessionStats[i].coping[j].componentString)
                                        {
                                            forhandleStrings[k].CountAmount1Up();
                                            forhandleAmount.Add(forhandleStrings[k].copingString);
                                        }
                                    }
                                    break;
                                               
                                case CopingCategory.BEDEOMHJÆLP:
                                    for (int k = 0; k < hjælpStrings.Length; k++)
                                    {
                                        if (hjælpStrings[k].copingString == MyStressUserContent.sessionStats[i].coping[j].componentString)
                                        {
                                            hjælpStrings[k].CountAmount1Up();
                                            bedeHjælpAmount.Add(hjælpStrings[k].copingString);
                                        }
                                    }
                                    break;
                                case CopingCategory.BAKKEUD:
                                    for (int k = 0; k < bakkeStrings.Length; k++)
                                    {
                                        if (bakkeStrings[k].copingString == MyStressUserContent.sessionStats[i].coping[j].componentString)
                                        {
                                            bakkeStrings[k].CountAmount1Up();
                                            bakkeUdAmount.Add(bakkeStrings[k].copingString);
                                        }
                                    }
                                    break;
                                     
                                default:
                                    break;
                            }
                        }
                    }
                }
            }


        }

        /// <summary>
        /// 
        /// </summary>
        public void UpdateTriggerStrings()
        {
            for (int i = 0; i < MyStressUserContent.sessionStats.Count; i++)
            {
				if (MyStressUserContent.sessionStats[i].regDate.ToLocalTime().Month == DateInFocus.ToLocalTime().Month && MyStressUserContent.sessionStats[i].regDate.ToLocalTime().Year == DateInFocus.ToLocalTime().Year)
                {
                    for (int j = 0; j < MyStressUserContent.sessionStats[i].triggers.Count; j++)
                    {
                        if (MyStressUserContent.sessionStats[i].triggers[j].selected)
                        {
                            if (MyStressUserContent.sessionStats[i].triggers[j].componentString.StartsWith("Triggers."))
                            {
                                triggerStrings[j].CountAmount1Up();
                            }
                            else
                            {
                                CopingStringClass userTrigger = new CopingStringClass(0, MyStressUserContent.sessionStats[i].triggers[j].componentString);
								CopingStringClass retrievedObject = userAddedTriggerStrings.Find(x => x.copingString == userTrigger.copingString);


								if (retrievedObject != null)
                                {
									retrievedObject.CountAmount1Up();
                                }
                                else
                                {
									userTrigger.CountAmount1Up();
									userAddedTriggerStrings.Add(userTrigger);
                                }
                            }
                        }
                    }

                    for (int j = 0; j < MyStressUserContent.sessionStats[i].symptoms.Count; j++)
                    {
                        if (MyStressUserContent.sessionStats[i].symptoms[j].selected)
                        {
                            if (MyStressUserContent.sessionStats[i].symptoms[j].componentString.StartsWith("Symptoms."))
                            {
                                symptomsStrings[j].CountAmount1Up();
                            }
                            else
                            {
                                CopingStringClass userSymtom = new CopingStringClass(0, MyStressUserContent.sessionStats[i].symptoms[j].componentString);

                                CopingStringClass retrievedObject = userAddedSymptomsStrings.Find(x => x.copingString == userSymtom.copingString);

                                if (retrievedObject != null)
                                {
                                    retrievedObject.CountAmount1Up();
                                }

                                else 
                                {
                                    userSymtom.CountAmount1Up();
                                    userAddedSymptomsStrings.Add(userSymtom);
                                }
                            
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        class CalendarElement
        {
            public int date;
            public iTextSharp.text.Image smileyImage;

            public CalendarElement(int newDate = 0, iTextSharp.text.Image newImage = null)
            {
                date = newDate;
                smileyImage = newImage;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        void ResetCopingAmount()
        {
            afbrydeStrings = new CopingStringClass[] { new CopingStringClass(0, "Coping.1"), new CopingStringClass(0, "Coping.6"), new CopingStringClass(0, "Coping.14"), new CopingStringClass(0, "Coping.16") };
            overblikStrings = new CopingStringClass[] { new CopingStringClass(0, "Coping.2"), new CopingStringClass(0, "Coping.3"), new CopingStringClass(0, "Coping.4"), new CopingStringClass(0, "Coping.7") };
            forhandleStrings = new CopingStringClass[] { new CopingStringClass(0, "Coping.9"), new CopingStringClass(0, "Coping.11"), new CopingStringClass(0, "Coping.12"), new CopingStringClass(0, "Coping.17") };
            hjælpStrings = new CopingStringClass[] { new CopingStringClass(0, "Coping.5"), new CopingStringClass(0, "Coping.8"), new CopingStringClass(0, "Coping.10"), new CopingStringClass(0, "Coping.15") };
            bakkeStrings = new CopingStringClass[] { new CopingStringClass(0, "Coping.13"), new CopingStringClass(0, "Coping.18"), new CopingStringClass(0, "Coping.19"), new CopingStringClass(0, "Coping.20") };
        }

        int daysInMonth = 42;
        CalendarElement[] CurrentVisibleMonth;

        List<MyStressData> statsForThisMonth;

        /// <summary>
        /// 
        /// </summary>
        public void RefreshCalendar()
        {
            CurrentVisibleMonth = new CalendarElement[daysInMonth];
            System.DateTime firstDayOfMonth = DateInFocus.AddDays(-(DateInFocus.Day));
            System.DateTime cachedFirstDayOfMonth = firstDayOfMonth;
            int daysInMonthCounter = 1;
            statsForThisMonth = new List<MyStressData>();

            for (int i = 0; i < MyStressUserContent.sessionStats.Count; i++)
            {
				if (MyStressUserContent.sessionStats[i].regDate.ToLocalTime().Month == DateInFocus.ToLocalTime().Month && MyStressUserContent.sessionStats[i].regDate.ToLocalTime().Year == DateInFocus.ToLocalTime().Year)
                {
                    MyStressData tempData = MyStressUserContent.sessionStats[i];
                    statsForThisMonth.Add(tempData);
                }
            }

            for (int i = 0; i < CurrentVisibleMonth.Length; i++)
            {
                CurrentVisibleMonth[i] = new CalendarElement();
                if ((int)cachedFirstDayOfMonth.DayOfWeek <= i)
                {
                    CurrentVisibleMonth[i].date = daysInMonthCounter;
                    daysInMonthCounter++;
                    firstDayOfMonth = firstDayOfMonth.AddDays(1);

                    List<MyStressData> temporaryList = new List<MyStressData>();
                    temporaryList = statsForThisMonth;
                    for (int k = 0; k < statsForThisMonth.Count; k++)
                    {

						if (firstDayOfMonth.ToLocalTime().Date == statsForThisMonth[k].regDate.ToLocalTime().Date)
                        {
                            CurrentVisibleMonth[i].smileyImage = GetSmiley(statsForThisMonth[k].overload, statsForThisMonth[k].effect);

                           // temporaryList.RemoveAt(k);
                          //  break;
                        }
                    }
                    statsForThisMonth = temporaryList;
                }
				if (firstDayOfMonth.ToLocalTime().Month > DateInFocus.ToLocalTime().Month || (firstDayOfMonth.ToLocalTime().Month <= DateInFocus.ToLocalTime().Month && firstDayOfMonth.Year != DateInFocus.Year))
                {
                    CurrentVisibleMonth[i].date = 0;
                }    
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loadLevel"></param>
        /// <param name="effectLevel"></param>
        /// <returns></returns>
        public iTextSharp.text.Image GetSmiley(OverloadLevel loadLevel, EffectLevel effectLevel)
        {
            iTextSharp.text.Image smiley = null;
            // 0 = Green Happy Smiley
            // 1 = Green Neutral Smiley
            // 2 = Green Sad Smiley
            // 3 = Yellow Happy Smiley
            // 4 = Yellow Neutral Smiley
            // 5 = Yellow Sad Smiley
            // 6 = Red Happy Smiley
            // 7 = Red Neutral Smiley
            // 8 = Red Sad Smiley
            // 9 = Grey Happy Smiley
            // 10 = Grey Neutral Smiley
            // 11 = Grey Sad Smiley

            switch (effectLevel)
            {
                case EffectLevel.NONE:
                    if (loadLevel == OverloadLevel.NORMAL)
                        smiley = smileyList[9].smileyImage;
                    else if (loadLevel == OverloadLevel.ABOVE_NORMAL)
                        smiley = smileyList[10].smileyImage;
                    else
                        smiley = smileyList[11].smileyImage;
                    break;
                case EffectLevel.GREEN:
                    if (loadLevel == OverloadLevel.NORMAL)
                        smiley = smileyList[0].smileyImage;
                    else if (loadLevel == OverloadLevel.ABOVE_NORMAL)
                        smiley = smileyList[1].smileyImage;
                    else
                        smiley = smileyList[2].smileyImage;
                    break;
                case EffectLevel.YELLOW:
                    if (loadLevel == OverloadLevel.NORMAL)
                        smiley = smileyList[3].smileyImage;
                    else if (loadLevel == OverloadLevel.ABOVE_NORMAL)
                        smiley = smileyList[4].smileyImage;
                    else
                        smiley = smileyList[5].smileyImage;
                    break;
                case EffectLevel.RED:
                    if (loadLevel == OverloadLevel.NORMAL)
                        smiley = smileyList[6].smileyImage;
                    else if (loadLevel == OverloadLevel.ABOVE_NORMAL)
                        smiley = smileyList[7].smileyImage;
                    else
                        smiley = smileyList[8].smileyImage;
                    break;
                default:
                    break;
            }
            return smiley;
        }
    }
}