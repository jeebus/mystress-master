﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Thrive
{
    /// <summary>
    /// Stress component for when user inputs and adds a new symptom or trigger to lists
    /// </summary>
	public class StressInputComponent : MonoBehaviour {

		public Image selectedButtonImage;
		public Button button;
		public InputField stressInput;
		public SelectableStressComponent selectableStressComponentPrefab;
        public StressComponentType componentType;

        /// <summary>
        /// Initilization
        /// </summary>
		void Start () {
			button.onClick.AddListener(() => { OnButtonClick(); });

		}
		
        /// <summary>
        /// On button click callback
        /// </summary>
		void OnButtonClick(){
			if (stressInput.text != "") {
				SelectableStressComponent newStressComponent = (SelectableStressComponent)Instantiate (selectableStressComponentPrefab);
				newStressComponent.GetStressTextComponent ().text = stressInput.text;
                newStressComponent.SetSelected(true);
				newStressComponent.transform.SetParent (transform.parent, false);
				newStressComponent.transform.SetSiblingIndex(transform.parent.childCount-3);
				
                switch (componentType)
                {
                    case StressComponentType.Symptom:
                        MyStressUserContent.sessionSymptomStrings.Add(new SelectableStressComponentData(stressInput.text, true));
                        MyStressUserContent.SaveSymptomStrings();
						MyStressCard.Instance.scrollAbleListSymptoms.Add(newStressComponent);
                        break;
                    case StressComponentType.Trigger:
                        MyStressUserContent.sessionTriggerStrings.Add(new SelectableStressComponentData (stressInput.text, true));
                        MyStressUserContent.SaveTriggerStrings();
					MyStressCard.Instance.scrollAbleListTriggers.Add(newStressComponent);
                        break;
                    case StressComponentType.Coping:
                  //      MyStressUserContent.sessionCopingStrings.Add(new SelectableStressComponentData(stressInput.text, false));
                    //    MyStressUserContent.SaveCopingStrings();

                        break;
                }
                MyStressUserContent.SaveCurrentSessionStats();
                stressInput.text = "";
			}
		}
	}
}