﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

namespace Thrive
{
    /// <summary>
    /// The effect "page" of the of the MyStress card
    /// This is the 5th and last "page" displayed after registering a days stress encounters
    /// </summary>
	public class EffectCard : MonoBehaviour, ICard {
		#region Unused components (Only here because of compile errors)
		[HideInInspector]
		public Image cardIcon;
		[HideInInspector]
		public Text cardNameTextComponent;
		#endregion
        [SerializeField]
		private Image smileyBackground;
        [SerializeField]
        private Image smileyForeground; // face
        [SerializeField]
        private Image sliderHandle;
        [SerializeField]
        private Text effectText;
        [SerializeField]
        private Text detailedText;
        [SerializeField]
        CanvasGroup effectInteracterableGroup;

		private bool first = true;
		bool runCountdown = false;
		bool cdThreadRunning = false;

        private string effectHigh = "MyStressCard1.Side5.Effect.High";
        private string effectMedium = "MyStressCard1.Side5.Effect.Medium";
        private string effectLow = "MyStressCard1.Side5.Effect.Low";
        private string effectKey;

		public bool canRegister = false;
        private IEnumerator countDownCoroutine;
        public Scrollbar effectScrollbar;
		float debugSpeedUpTime = 0; //-60 * 60 * 1.995f;

        /// <summary>
        /// Initilization
        /// </summary>
		void Start(){
			
			LanguageManager languageManagerInstance = LanguageManager.Instance;
			languageManagerInstance.OnChangeLanguage += OnChangeLanguage;
			OnChangeLanguage(languageManagerInstance);
			detailedText.resizeTextForBestFit = false;
			ResetCountDown ();
		}

        /// <summary>
        /// On language changed callback
        /// </summary>
        /// <param name="languageManager">SmartLocalization.LanguageManager instance</param>
		public void OnChangeLanguage(LanguageManager languageManager){
			
			effectText.text = LanguageManager.Instance.GetTextValue(effectKey);
		}

        /// <summary>
        /// Re-sets the displayed effect using a MyStressSmileyAttribute object
        /// </summary>
        /// <param name="smileyAttributes">MyStressSmileyAttribute to reset effect to</param>
		public void ResetEffect(MyStressSmileyAttribute smileyAttributes){
			smileyBackground.color = smileyAttributes.smileyColor;
			smileyForeground.sprite = smileyAttributes.smileySprite;
			sliderHandle.color = smileyAttributes.smileyColor;
            if (effectScrollbar != null)
            {
                UISetExtensions.Set(effectScrollbar, 0, false);
			}
		}

        /// <summary>
        /// Method for setting displayed effect when effect slider value is changed
        /// </summary>
        /// <param name="smileyAttributes">MyStressSmileyAttribute to set effect to</param>
		public void SetEffect(MyStressSmileyAttribute smileyAttributes)
		{
			Debug.Log ("smileyAttributes.effectLevel: " + smileyAttributes.effectLevel);
			detailedText.resizeTextForBestFit = false;
			smileyBackground.color = smileyAttributes.smileyColor;
			smileyForeground.sprite = smileyAttributes.smileySprite;
			sliderHandle.color = smileyAttributes.smileyColor;

			if (smileyAttributes.effectLevel == EffectLevel.NONE)
			{
				effectKey = effectHigh;
				if (first)
				{
					//first = true;
					Debug.Log("Effect set to none");
					sliderHandle.GetComponentInParent<Scrollbar>().value = 0f;
				}
			}
			else if (smileyAttributes.effectLevel == EffectLevel.GREEN)
			{
				effectKey = effectHigh;
				if (first)
				{
                    first = false;
                    Debug.Log("Effect set to green");
                    sliderHandle.GetComponentInParent<Scrollbar>().value = 0f;
                }
				MyStressUserContent.settings.hasEffectBeenSetToday = true;
            }
            else if (smileyAttributes.effectLevel == EffectLevel.YELLOW)
            {
				effectKey = effectMedium;
                if (first)
                {
                    first = false;
                    Debug.Log("Effect set to yellow");
                    sliderHandle.GetComponentInParent<Scrollbar>().value = 0.5f;
                }
				MyStressUserContent.settings.hasEffectBeenSetToday = true;
            }
            else if (smileyAttributes.effectLevel == EffectLevel.RED)
            {
				effectKey = effectLow;
                if (first)
                {
                    first = false;
                    Debug.Log("Effect set to red");
                    sliderHandle.GetComponentInParent<Scrollbar>().value = 1f;
                }
				MyStressUserContent.settings.hasEffectBeenSetToday = true;
            }

			System.DateTime tempTime = (System.DateTime.UtcNow.AddDays(MyStressUserContent.GetFakeDays()));
			if ((tempTime - MyStressUserContent.settings.lastNotificationDate.AddSeconds(debugSpeedUpTime)).Seconds >= MyStressUserContent.settings.notificationsTimer * 60.0f*60.0f )
            {
				canRegister = true;
				runCountdown = false;
				effectInteracterableGroup.blocksRaycasts = true;
				effectInteracterableGroup.alpha = 1.0f;


                Debug.Log("Effect has been set today");
				Debug.Log("last notification date: " + MyStressUserContent.settings.lastNotificationDate.ToLocalTime());

				int seconds = (System.DateTime.UtcNow.AddDays(MyStressUserContent.GetFakeDays()).ToLocalTime() - MyStressUserContent.settings.lastNotificationDate.ToLocalTime()).Seconds;

				Debug.Log("Notification seconds: " + seconds + " timer seconds: " + MyStressUserContent.settings.notificationsTimer * 60.0f*60.0f);
                MyStressUserContent.settings.hasEffectBeenSetToday = true;
			} else if(!MyStressUserContent.settings.hasEffectBeenSetToday) {
				effectKey = "MyStressCard1.Side5.CannotRegister";

				runCountdown = true;

				effectInteracterableGroup.blocksRaycasts = false;
				effectInteracterableGroup.alpha = 0.2f;

                countDownCoroutine = RunCountdown();
                StartCoroutine(countDownCoroutine);
			}

			effectText.text = LanguageManager.Instance.GetTextValue(effectKey);
		}

        /// <summary>
        /// Reset the countdown for when the user is able to register effect
        /// </summary>
        public void ResetCountDown()
        {
			countDownCoroutine = RunCountdown();
            StopCoroutine(countDownCoroutine);
			cdThreadRunning = false;
			runCountdown = true;
            
            StartCoroutine(countDownCoroutine);
        }

        /// <summary>
        /// Possibley ResetCountDown when OnEnable is called
        /// </summary>
		void OnEnable(){
			if(!MyStressUserContent.settings.hasEffectBeenSetToday){
				ResetCountDown();
			}
		}

        /// <summary>
        /// Countdown coroutine that displays time until the user can register effect.
        /// At this point the effect slider is activated.
        /// </summary>
        /// <returns>IEnumerator</returns>
		IEnumerator RunCountdown(){
			Debug.Log ("Try to execute RunCountdown() started");
			if (!cdThreadRunning) {
				cdThreadRunning = true;

				float timeToWaitInHours = 2.0f;

				if(runCountdown){
					Debug.Log ("RunCountdown() started");

				}
				while (runCountdown) {

					detailedText.resizeTextForBestFit = false;
			
					float timerInSeconds = MyStressUserContent.settings.notificationsTimer * 60.0f * 60.0f;
					System.TimeSpan twoHourSpan = System.TimeSpan.FromHours(2);
					var deltaTime = (System.DateTime.UtcNow.AddDays(MyStressUserContent.GetFakeDays()) - MyStressUserContent.settings.lastNotificationDate.AddSeconds(debugSpeedUpTime));
					var seconds= deltaTime.TotalSeconds ;
					deltaTime = twoHourSpan - deltaTime ;

					string hours = string.Format("{0:0}", Mathf.Max(0.0f, deltaTime.Hours));
					string minutes = string.Format("{0:00}", Mathf.Max(0.0f,deltaTime.Minutes));
					int parseleadingzero = 1;
					int.TryParse( minutes[0].ToString(), out parseleadingzero);
					if(parseleadingzero == 0){
						minutes = minutes[1].ToString();
					}

					string combinedString = LanguageManager.Instance.GetTextValue ("MyStressCard1.Side5.CannotRegister.Text.1");
					combinedString += " " + timeToWaitInHours + " ";
					combinedString += LanguageManager.Instance.GetTextValue ("MyStressCard1.Side5.CannotRegister.Text.2"); 
					combinedString += " " + hours + " ";
					combinedString += LanguageManager.Instance.GetTextValue ("MyStressCard1.Side5.CannotRegister.Text.3");
					combinedString += " " + minutes + " ";
					combinedString += LanguageManager.Instance.GetTextValue ("MyStressCard1.Side5.CannotRegister.Text.4");
					detailedText.text = combinedString;

					yield return new WaitForSeconds (1.0f);

					if (seconds > timerInSeconds) {
						Debug.Log("Seconds: " + seconds + "   timerSeconds: " + timerInSeconds);
						Debug.Log("Countdown stopped");
						runCountdown = false;
					}
				}
				detailedText.text = LanguageManager.Instance.GetTextValue ("MyStressCard1.Side5.ChangeText");
				effectText.text = LanguageManager.Instance.GetTextValue ("MyStressCard1.Side5.Effect.High");

				detailedText.resizeTextForBestFit = false;
				effectInteracterableGroup.blocksRaycasts = true;
				effectInteracterableGroup.alpha = 1.0f;

				/*
				bool fakeDayAdd = false;
				while (!fakeDayAdd) {
					yield return new WaitForSeconds (5.0f);
					fakeDayAdd = true;
				}
				MyStressUserContent.AddToFakeDays ();
				*/
				cdThreadRunning = false;
				Debug.Log("CountDown thread completed");
			}
			yield break;
		}

        /// <summary>
        /// ICard interface implemented methods
        /// </summary>
        #region ICard interface implementation
        public const string cardUID = "MIDT_000_INTRO";
		
		public string GetCardUID()
		{
			return cardUID;
		}
		
		public int GetCardOwnerID()
		{
			return 0;
			
		}
		
		public void SetCardOwnerID(int newCardOwner)
		{
			// cardOwnerID = newCardOwner;
		}
		
		public GameObject GetGameObject()
		{
			return gameObject;
		}
		
		public void MakeCardContentVisible()
		{
			gameObject.SetActive(true);
		}
		
		public void MakeCardContentInvisible()
		{
			gameObject.SetActive(false);
			// listenForAlertChanges = false;
		}
		
		public void ShowFrontSide()
		{
			// listenForAlertChanges = true;
			//StartCoroutine(UpdateAlertArea());
		}
		public void ShowBackSide()
		{
			
		}
		public bool GetRotateCardToReadMore()
		{
			return true;
		}
		public string GetCardName()
		{
			return "";
		}
		public void SetRotateCardToReadMore(bool newReadMoreValue)
		{
			// rotateCardToReadMore = newReadMoreValue;
		}
		public void SetCardMaterial(Material material)
		{
			
		}
		public void RotateCardToReadMore()
		{

		}
		
		public void RotateCardBackToOriginal()
		{
			
		}
		
		public Image GetCardIcon()
		{
			return cardIcon;
			//return new Image();
		}
		public Text GetCardNameTextComponent()
		{
			return cardNameTextComponent;
			//return new Text();
		}
		public Image GetCardImageTexture()
		{
			return null;
		}
		#endregion
	}
}
