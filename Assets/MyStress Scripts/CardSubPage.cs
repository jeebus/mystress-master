﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    /// <summary>
    /// Minimal card sub page, the least implemented amount of code for Thrive to ICard interface
    /// </summary>
    public class CardSubPage : MonoBehaviour, ICard
    {
        public Image cardIcon;
        public Text cardNameTextComponent;
        
        /// <summary>
        /// ICard interface implemented methods
        /// </summary>
        /// 
        #region ICard interface implementation	
        public const string cardUID = "MIDT_000_INTRO";

        public string GetCardUID()
        {
            return cardUID;
        }

        public int GetCardOwnerID()
        {
            return 0;

        }

        public void SetCardOwnerID(int newCardOwner)
        {
            // cardOwnerID = newCardOwner;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public void MakeCardContentVisible()
        {
            gameObject.SetActive(true);
        }

        public void MakeCardContentInvisible()
        {
            gameObject.SetActive(false);
            // listenForAlertChanges = false;
        }

        public void ShowFrontSide()
        {
            // listenForAlertChanges = true;
            //StartCoroutine(UpdateAlertArea());
        }
        public void ShowBackSide()
        {

        }
        public bool GetRotateCardToReadMore()
        {
            return true;
        }
        public string GetCardName()
        {
            return "NONAME";
        }
        public void SetRotateCardToReadMore(bool newReadMoreValue)
        {
            // rotateCardToReadMore = newReadMoreValue;
        }
        public void SetCardMaterial(Material material)
        {

        }
        public void RotateCardToReadMore()
        {


        }

        public void RotateCardBackToOriginal()
        {

        }

        public Image GetCardIcon()
        {
            return cardIcon;
        }
        public Text GetCardNameTextComponent()
        {
            return cardNameTextComponent;
        }
        public Image GetCardImageTexture()
        {
            return null;
        }
        #endregion
    }
}
