﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;
using UnityEngine.UI;
using Thrive;

/// <summary>
/// LanguageList is a component displayed on the Settings page that the user can use the shuffle thru available languages
/// </summary>
public class LanguageList : MonoBehaviour
{
	public Text currentLanguageText;
	
	List<SmartLocalization.SmartCultureInfo> availableLanguages;
	public Button forwardButton;
	public Button backwardsButton;
	
	int positionInListCounter = 0;

    /// <summary>
    /// Initialization
    /// </summary>
    void Start()
	{
		availableLanguages = SmartLocalization.LanguageManager.Instance.GetSupportedLanguages();
		
		forwardButton.onClick.AddListener(() => { OnForwardButtonClick(); });
		backwardsButton.onClick.AddListener(() => { OnBackwardButtonClick(); });
		
		currentLanguageText.text = SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.nativeName;
		
		//Set available language list counter too the default language index
		for (int i = 0; i < availableLanguages.Count; i++)
		{
			SmartLocalization.SmartCultureInfo item = availableLanguages[i];
			if (item.nativeName == SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.nativeName)
			{
				positionInListCounter = i;
			}
		}
	}

    /// <summary>
    /// Go forward in language list
    /// </summary>
    void OnForwardButtonClick()
	{
		positionInListCounter++;
		if (positionInListCounter > availableLanguages.Count-1)
		{
			positionInListCounter = 0;
		}
		currentLanguageText.text = availableLanguages[positionInListCounter].nativeName;
		
		
		if (SmartLocalization.LanguageManager.Instance.IsLanguageSupported(availableLanguages[positionInListCounter].languageCode))
		{
			SmartLocalization.LanguageManager.Instance.ChangeLanguage(availableLanguages[positionInListCounter].languageCode);
			MyStressUserContent.settings.language = availableLanguages[positionInListCounter].languageCode;
			MyStressUserContent.SaveCurrentSettings();
		}
		else
		{
			Debug.Log("The language: " + currentLanguageText.text + " is not supported");
		}
	}

    /// <summary>
    /// Go back in language list
    /// </summary>
    void OnBackwardButtonClick()
	{
		positionInListCounter--;
		if (positionInListCounter < 0)
		{
			positionInListCounter = availableLanguages.Count - 1;
		}
		currentLanguageText.text = availableLanguages[positionInListCounter].nativeName;
		
		if (SmartLocalization.LanguageManager.Instance.IsLanguageSupported(availableLanguages[positionInListCounter].languageCode))
		{
			SmartLocalization.LanguageManager.Instance.ChangeLanguage(availableLanguages[positionInListCounter].languageCode);
			MyStressUserContent.settings.language = availableLanguages[positionInListCounter].languageCode;
			MyStressUserContent.SaveCurrentSettings();
		}
		else
		{
			Debug.Log("The language: " + currentLanguageText.text + " is not supported");
		}
	}
}