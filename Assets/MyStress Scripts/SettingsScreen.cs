﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
using Thrive;

/// <summary>
/// MyStress app options screen, that is displayed when user clicks right top corner button
/// </summary>
public class SettingsScreen : MonoBehaviour, IScreen {
	[SerializeField]
	private Scrollbar notificationsToggle;
	[SerializeField]
	private Scrollbar notificationsTimeSlider;
	[SerializeField]
	private Button backButton;
    [SerializeField]
    private Text headerTitleText;


	private int hourAddition = 2;

    //This variable is used for controlling the upper left and right buttons in MyStress, to see which of the 2 have been activated so far
    private bool screenEntered = false;

    /// <summary>
    /// Initilization
    /// </summary>
	void Start()
	{
		notificationsToggle.onValueChanged.AddListener(OnNotificationsToggleChange);
		notificationsTimeSlider.onValueChanged.AddListener(OnNotificationTimeSliderChange);
		backButton.onClick.AddListener(OnGoBack);

		MyStressUserContent.LoadStoredSettings();
		Debug.Log(MyStressUserContent.settings);

		notificationsToggle.value = MyStressUserContent.settings.recieveNotifications == true ? 0 : 1;
		notificationsTimeSlider.value = ((float)(MyStressUserContent.settings.notificationsTimer - hourAddition) / 6f);
	}

    /// <summary>
    /// Gets whether or not the screen has been entered
    /// </summary>
    /// <returns>Returns true or false</returns>
    public bool GetSettingsScreenEntered()
    {
        return screenEntered;
    }

    /// <summary>
    /// Sets whether or not the screen has been entered
    /// </summary>
    /// <param name="value">True or false</param>
    public void SetSettingsScreenEntered(bool value)
    {
        screenEntered = value;
    }

    /// <summary>
    /// On notification toggle button  callback
    /// </summary>
    /// <param name="value">Toggle button value</param>
    void OnNotificationsToggleChange(float value)
	{
		bool recieveNotifications;
		if(value > 0.5f)
		{
			recieveNotifications = false;
		}
		else
		{
			recieveNotifications = true;
		}

		if(recieveNotifications != MyStressUserContent.settings.recieveNotifications)
		{
			// Value changed, save changes
			MyStressUserContent.settings.recieveNotifications = recieveNotifications;
			MyStressUserContent.SaveCurrentSettings();
		}

	}

    /// <summary>
    /// On notification slider changed callback
    /// </summary>
    /// <param name="value"></param>
	void OnNotificationTimeSliderChange(float value)
	{

		int selectedValue = (int)(value * 6.999f) + hourAddition; // range between 0 and 6 and add 2 hours to get 2 - 8
		if(selectedValue != MyStressUserContent.settings.notificationsTimer)
		{
			// Value changed, save changes
			MyStressUserContent.settings.notificationsTimer = selectedValue;
			MyStressUserContent.SaveCurrentSettings();
		}
	}
	
    /// <summary>
    /// On back button click callback
    /// </summary>
	void OnGoBack()
	{
		Debug.Log("Go Back");
		MyStressUserContent.settings.language = SmartLocalization.LanguageManager.Instance.LoadedLanguage;
		MyStressUserContent.SaveCurrentSettings();
		ShowPreviousScreen();
	}


    /// <summary>
    /// IScreen interface implemented methods
    /// </summary>
    #region IScreen implementation
    public string GetScreenHeaderTitle()
    {
        if (headerTitleText != null)
        {
            return headerTitleText.GetComponent<SmartLocalization.Editor.LocalizedText>().localizedKey;
        }
        else
        {
            return "";
        }
    }
	public void ShowThisScreen ()
	{
        if (!ViewIScreenController.IsTransitionRunning())
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.Fade));
            SetSettingsScreenEntered(true);
        }
    }
	
	public void ShowPreviousScreen ()
	{
        if (!ViewIScreenController.IsTransitionRunning())
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
            SetSettingsScreenEntered(false);
        }
    }
	
	public void DisableScreen ()
	{
        SetSettingsScreenEntered(false);
        gameObject.SetActive(false);
	}
	
	public void EnableScreen ()
	{
		gameObject.SetActive(true);
        SetSettingsScreenEntered(true);
    }
	
	public GameObject GetGameObject ()
	{
		return gameObject;
	}
	#endregion
}
