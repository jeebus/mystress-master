﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using SmartLocalization;


namespace Thrive
{
    public enum StressComponentType
    {
        Symptom,
        Trigger,
        Coping
    }

    /// <summary>
    /// Class defines a selected stress component, which are the interactive stress values on scrollable lists
    /// </summary>
	public class SelectableStressComponent : MonoBehaviour {

		public Sprite selectedButtonSprite;
		public Sprite unselectedButtonSprite;
		public Image selectedButtonImage;
		public Button button;
        public SelectableStressComponentData data;
        public StressComponentType componentType;
		LanguageManager languageManagerInstance;


		string localizedKey;
		Text stressText;

		bool selected = false;

        

		/// <summary>
        /// Pre-initilization
        /// </summary>
		void Awake () {
			button.onClick.AddListener(() => { OnButtonClick(); });
			stressText = transform.GetChild (0).GetComponent<Text>();

			//Subscribe to the change language event
			languageManagerInstance = LanguageManager.Instance;

		}

        /// <summary>
        /// Initilization
        /// </summary>
		void Start(){
			if(localizedKey != null)
			stressText.text = languageManagerInstance.GetTextValue(localizedKey);
		}

        /// <summary>
        /// On language changed callback
        /// </summary>
        /// <param name="languageManager"></param>
		public void OnChangeLanguage(LanguageManager languageManager)
		{
			stressText.text = languageManagerInstance.GetTextValue(localizedKey);
		}

        /// <summary>
        /// Sets this components localized key
        /// </summary>
        /// <param name="newKey"></param>
		public void SetLocalizedKey(string newKey){
			localizedKey = newKey;

		}

        /// <summary>
        /// Gets text of component
        /// </summary>
        /// <returns>Text component</returns>
		public Text GetStressTextComponent(){
			return stressText;
		}

        /// <summary>
        /// Sets if this component is selected or not
        /// </summary>
        /// <param name="hasBeenSelected">Boolean</param>
        public void SetSelected(bool hasBeenSelected)
        {
            if (!hasBeenSelected)
            {
                selected = false;
                selectedButtonImage.sprite = unselectedButtonSprite;

            }
            else
            {
                selected = true;
                selectedButtonImage.sprite = selectedButtonSprite;
            }
        }

        /// <summary>
        /// Component button callback
        /// </summary>
		void OnButtonClick(){
            if (selected)
            {
                selected = false;
                selectedButtonImage.sprite = unselectedButtonSprite;

            }
            else
            {
                selected = true;
                selectedButtonImage.sprite = selectedButtonSprite;
            }

            data.selected = selected;
            MyStressUserContent.SaveCurrentSessionStats();
            MyStressUserContent.settings.lastNotificationDate = DateTime.UtcNow.AddDays(MyStressUserContent.GetFakeDays());
            MyStressUserContent.SaveCurrentSettings();

           
                //reset notification
                MyStressUserContent.ResetNotification();
            
		}

        /// <summary>
        /// Gets selected
        /// </summary>
        /// <returns>Returns selected as boolean</returns>
        public bool IsSelected()
        {
            return selected;
        }
	}
}
