using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using SmartLocalization;

namespace Thrive
{
    /// <summary>
    /// Calendar component for showing months of registered MyStress data
    /// </summary>
	public class DateSelectorMyStress : MonoBehaviour
	{
		public Button previousWeekButton;
		public Image previousWeekButtonImage;
		public Button nextWeekButton;
		public System.DateTime[] currentWeek = new System.DateTime[7];
		System.DateTime now = System.DateTime.UtcNow.ToLocalTime().Date;
		public Text externalDateText;
		public Sprite currentDaySprite;
		public Sprite smileyBackgroundSprite;
		public Image currentDay;
		public List<CalenderDayComponent> week1;
		public List<CalenderDayComponent> week2;
		public List<CalenderDayComponent> week3;
		public List<CalenderDayComponent> week4;
		public List<CalenderDayComponent> week5;
		public List<CalenderDayComponent> week6;
		List<List<CalenderDayComponent>> currentVisibleMonth;
		MyStressCard mainCard;
		
        /// <summary>
        /// Initilization
        /// </summary>
		void Start()
		{
            SetUpEventListeners();
			currentVisibleMonth = new List<List<CalenderDayComponent>> ();
			currentVisibleMonth.Add (week1);
			currentVisibleMonth.Add (week2);
			currentVisibleMonth.Add (week3);
			currentVisibleMonth.Add (week4);
			currentVisibleMonth.Add (week5);
			currentVisibleMonth.Add (week6);
			mainCard = FindObjectOfType<MyStressCard> ();
			SmartLocalization.LanguageManager.Instance.OnChangeLanguage += OnChangeLanguage;
			OnChangeLanguage(SmartLocalization.LanguageManager.Instance);
		}

        /// <summary>
        /// On language changed callback method
        /// </summary>
        /// <param name="manager">SmartLocalization.LanguageManager Instance</param>
        void OnChangeLanguage(SmartLocalization.LanguageManager manager)
		{
			externalDateText.text = LanguageManager.Instance.GetTextValue("Generic.Month." + now.Month) + " " + now.Year.ToString();
		}

        /// <summary>
        /// For setting up button event listeners
        /// </summary>
		void SetUpEventListeners()
		{
			previousWeekButton.onClick.AddListener(() => { OnClickPrevious(); });
			nextWeekButton.onClick.AddListener(() => { OnClickNext(); });
		}

        /// <summary>
        /// A coroutine for refreshing displayed calendar content
        /// </summary>
        /// <returns>IEnumerator</returns>
		public IEnumerator Refresh()
		{
			System.DateTime dateInFocus = now;
			System.DateTime firstDayOfMonth = dateInFocus.AddDays (-(dateInFocus.Day));
            System.DateTime cachedFirstDayOfMonth = firstDayOfMonth;

			int daysInMonthCounter = 1;

			externalDateText.text = LanguageManager.Instance.GetTextValue("Generic.Month." + now.Month) + " " + now.Year.ToString();
		
			List<MyStressData> statsForThisMonth = new List<MyStressData>();

			for (int i = 0; i < MyStressUserContent.sessionStats.Count; i++) {
				if(MyStressUserContent.sessionStats[i].regDate.ToLocalTime().Month == now.Month && MyStressUserContent.sessionStats[i].regDate.ToLocalTime().Year == now.Year){
                    MyStressData tempData = MyStressUserContent.sessionStats[i];
					statsForThisMonth.Add(tempData);
				}	
			}

			for (int i = 0; i < currentVisibleMonth.Count; i++)
			{
				for (int j = 0; j < currentVisibleMonth[i].Count; j++) {
                    if (currentVisibleMonth[i][j].smileyBackground != null)
                    {
                        currentVisibleMonth[i][j].smileyBackground.enabled = false;
                    }
                    if (currentVisibleMonth[i][j].smileyForeground != null)
                    {
                        currentVisibleMonth[i][j].smileyForeground.enabled = false;
                    }
                    currentVisibleMonth[i][j].dateButton.enabled = false;

					if( i == 0){

						currentVisibleMonth[i][j].dateText.color = new Color(currentVisibleMonth[i][j].dateText.color.r, currentVisibleMonth[i][j].dateText.color.g, currentVisibleMonth[i][j].dateText.color.b, 0);

                        if ((int)cachedFirstDayOfMonth.DayOfWeek <= j)
                        {
							currentVisibleMonth[i][j].dateText.text = daysInMonthCounter.ToString();
                            daysInMonthCounter++;
                            firstDayOfMonth = firstDayOfMonth.AddDays(1);
							currentVisibleMonth[i][j].dateText.color = new Color(currentVisibleMonth[i][j].dateText.color.r, currentVisibleMonth[i][j].dateText.color.g, currentVisibleMonth[i][j].dateText.color.b, 1);
							//Could be optimized
							List<MyStressData> temporaryList = new List<MyStressData>();
							temporaryList = statsForThisMonth;
							for (int k = 0; k < statsForThisMonth.Count; k++) {
								if(firstDayOfMonth.Date == statsForThisMonth[k].regDate.ToLocalTime().Date){
									currentVisibleMonth[i][j].smileyBackground.enabled = true;
									currentVisibleMonth[i][j].smileyForeground.enabled = true;
									currentVisibleMonth[i][j].data = statsForThisMonth[k];
                                    currentVisibleMonth[i][j].dateButton.enabled = true;
                                    currentVisibleMonth[i][j].datePage = mainCard.dateStatsPage;
                                    currentVisibleMonth[i][j].calenderPage = mainCard.calenderCardPage;

									currentVisibleMonth[i][j].SetSmiley(mainCard.GetSmilieAtrributes(statsForThisMonth[k].overload, statsForThisMonth[k].effect));

									temporaryList.RemoveAt(k);
								}
							}
							statsForThisMonth = temporaryList;
						}
					} else {

						currentVisibleMonth[i][j].dateText.text = daysInMonthCounter.ToString();
                        daysInMonthCounter++;
                        firstDayOfMonth = firstDayOfMonth.AddDays(1);
						currentVisibleMonth[i][j].dateText.color = new Color(currentVisibleMonth[i][j].dateText.color.r, currentVisibleMonth[i][j].dateText.color.g, currentVisibleMonth[i][j].dateText.color.b, 1);
						//Could be optimized
						List<MyStressData> temporaryList = new List<MyStressData>();
						temporaryList = statsForThisMonth;
						for (int k = 0; k < statsForThisMonth.Count; k++) {
							if(firstDayOfMonth.Date == statsForThisMonth[k].regDate.ToLocalTime().Date){
								currentVisibleMonth[i][j].data = statsForThisMonth[k];
								currentVisibleMonth[i][j].smileyBackground.enabled = true;
								currentVisibleMonth[i][j].smileyForeground.enabled = true;
                                currentVisibleMonth[i][j].dateButton.enabled = true;
                                currentVisibleMonth[i][j].datePage = mainCard.dateStatsPage;
                                currentVisibleMonth[i][j].calenderPage = mainCard.calenderCardPage;

								currentVisibleMonth[i][j].SetSmiley(mainCard.GetSmilieAtrributes(statsForThisMonth[k].overload, statsForThisMonth[k].effect));

								temporaryList.RemoveAt(k);

							}
							
						}
						statsForThisMonth = temporaryList;
					}

                    if (firstDayOfMonth.Month > now.Month || (firstDayOfMonth.Month <= now.Month && firstDayOfMonth.Year != now.Year))
                    {
						currentVisibleMonth[i][j].dateText.color = new Color(currentVisibleMonth[i][j].dateText.color.r, currentVisibleMonth[i][j].dateText.color.g, currentVisibleMonth[i][j].dateText.color.b,0);
						currentVisibleMonth[i][j].smileyBackground.enabled = false;
					}
                   
				}
			}

			yield return null;
		}

        /// <summary>
        /// On next month button click
        /// </summary>
        public void OnClickNext()
        {
			Debug.Log("Clicked next");
			previousWeekButton.enabled = true;
			previousWeekButtonImage.enabled = true;
			now = now.AddMonths (1);
            StartCoroutine(Refresh());

		}

        /// <summary>
        /// On previous month button click
        /// </summary>
        public void OnClickPrevious()
		{
			Debug.Log("Clicked prev");
			now = now.AddMonths (-1);
            StartCoroutine(Refresh());
		}
	}
}