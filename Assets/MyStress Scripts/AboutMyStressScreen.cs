﻿using UnityEngine;
using System.Collections;
using Thrive;
using UnityEngine.UI;

/// <summary>
/// About MyStress screen that is shown when the user clicks on the MyStress logo in the top corner of the app
/// </summary>
public class AboutMyStressScreen : MonoBehaviour, IScreen {
	[SerializeField]
	private Button backButton;
	[SerializeField]
	private Button mystressAppButton;
    [SerializeField]
    private Button rapportButton;
	[SerializeField]
    private Button whatIsStressButton;
	[SerializeField]
	private Button stressCopingAndToolsButton;
	[SerializeField]
	private Button helpForAppButton;

	[SerializeField]
	private GameObject simpleTextScrollViewPrefab;
	[SerializeField]
	private GameObject myStressAppInfoPagePrefab;

    private SimpleTextScrollScreen instatiatedScrollScreen;
    SimpleTextScrollScreen instatiatedmyStressAppInfoPageScreen;
    [SerializeField]
    private Text headerTitleText;


    //This variable is used for controlling the upper left and right buttons in MyStress, to see which of the 2 have been activated so far
    private bool screenEntered;

    /// <summary>
    /// Initialize
    /// </summary>
	void Start()
	{
		SetupEventListeners();
    }

    /// <summary>
    /// Setup eventlisteners for the buttons on the AboutMyStress screen
    /// </summary>
    void SetupEventListeners()
	{
		backButton.onClick.AddListener(OnGoBack);
        mystressAppButton.onClick.AddListener(OnMyStressApp);
        rapportButton.onClick.AddListener(OnMyStressRapport);
        whatIsStressButton.onClick.AddListener(OnWhatIsStress);
        stressCopingAndToolsButton.onClick.AddListener(OnCopingAndTools);
		helpForAppButton.onClick.AddListener(OnOpenHelpForApp);
	}

    /// <summary>
    /// Gets whether or not the screen has been entered
    /// </summary>
    /// <returns>Returns true or false</returns>
    public bool GetAboutScreenEntered()
    {
        return screenEntered;
    }

    /// <summary>
    /// Sets whether or not the screen has been entered
    /// </summary>
    /// <param name="value">True or false</param>
    public void SetAboutScreenEntered(bool value)
    {
        screenEntered = value;
    }

    /// <summary>
    /// On MyStress app button click
    /// </summary>
    void OnMyStressApp()
	{
        if (instatiatedmyStressAppInfoPageScreen == null)
        {
            instatiatedmyStressAppInfoPageScreen = ((GameObject)Instantiate(myStressAppInfoPagePrefab, Vector3.zero, Quaternion.identity)).GetComponent<SimpleTextScrollScreen>();
        }
      
        
		SimpleTextScrollScreen screen = instatiatedmyStressAppInfoPageScreen.GetComponent<SimpleTextScrollScreen>();
        screen.bodyText.text = "\n" + SmartLocalization.LanguageManager.Instance.GetTextValue("AboutMyStressScreen.MyStressAppen.Text");
        screen.headerTitleKey = "AboutMyStressScreen.MyStressAppen";
        screen.ShowThisScreen();
    }

    /// <summary>
    /// On MyStress rapport button click
    /// </summary>
    void OnMyStressRapport()
	{
        SimpleTextScrollScreen screen = ShowPrefab(simpleTextScrollViewPrefab).GetComponent<SimpleTextScrollScreen>();
		screen.bodyText.text = GetBodyText(SmartLocalization.LanguageManager.Instance.GetTextValue("AboutMyStressScreen.MyStressRapporten.Text"));
        screen.headerTitleKey = "AboutMyStressScreen.MyStressRapporten";
        screen.ShowThisScreen();
    }

    /// <summary>
    /// On "What is stress" button click
    /// </summary>
    void OnWhatIsStress()
	{
        SimpleTextScrollScreen screen = ShowPrefab(simpleTextScrollViewPrefab).GetComponent<SimpleTextScrollScreen>();
		screen.bodyText.text = GetBodyText(SmartLocalization.LanguageManager.Instance.GetTextValue("AboutMyStressScreen.WhatIsStress.Text"));
        screen.headerTitleKey = "AboutMyStressScreen.WhatIsStress";
        screen.ShowThisScreen();
    }

    /// <summary>
    /// On "coping and tools" button click
    /// </summary>
    void OnCopingAndTools()
	{
        // This screen should be static
        SimpleTextScrollScreen screen = ShowPrefab(simpleTextScrollViewPrefab).GetComponent<SimpleTextScrollScreen>();
		screen.bodyText.text = GetBodyText(SmartLocalization.LanguageManager.Instance.GetTextValue("AboutMyStressScreen.CopingAndTools.Text"));
        screen.headerTitleKey = "AboutMyStressScreen.CopingAndTools";
        screen.ShowThisScreen();
    }

    /// <summary>
    /// On "Help for app" button click
    /// </summary>
	void OnOpenHelpForApp()
	{
        SimpleTextScrollScreen screen = ShowPrefab(simpleTextScrollViewPrefab).GetComponent<SimpleTextScrollScreen>();
		screen.bodyText.text = GetBodyText(SmartLocalization.LanguageManager.Instance.GetTextValue("AboutMyStressScreen.HelpWithApp.Text"));
        screen.headerTitleKey = "AboutMyStressScreen.HelpWithApp";
        screen.ShowThisScreen();

    }

    /// <summary>
    /// Show a screen prefab
    /// </summary>
    /// <param name="prefab">Gameobject with IScreen component to instantiate</param>
    /// <returns>Returns the instantiated screen</returns>
    SimpleTextScrollScreen ShowPrefab(GameObject prefab)
	{
        if(instatiatedScrollScreen == null)
            instatiatedScrollScreen = ((GameObject)Instantiate(prefab, Vector3.zero, Quaternion.identity)).GetComponent<SimpleTextScrollScreen>();
		//IScreen i = instatiatedScrollScreen.GetComponent(typeof(IScreen)) as IScreen;
     //   instatiatedScrollScreen.ShowThisScreen();

		return instatiatedScrollScreen;
	}

    /// <summary>
    /// Get body text from value, by adding some linebreaks
    /// </summary>
    /// <param name="value">value as a string</param>
    /// <returns>Formatted value as string</returns>
	string GetBodyText(string value)
	{
		return "\n" + value + "\n\n";
	}

    /// <summary>
    /// On back from this screen
    /// </summary>
	void OnGoBack()
	{
        EnableScreen();
		ShowPreviousScreen();
	}

    /// <summary>
    /// IScreen interface implemented methods
    /// </summary>
    #region IScreen implementation
    public string GetScreenHeaderTitle()
    {
        if (headerTitleText != null)
        {

            return headerTitleText.GetComponent<SmartLocalization.Editor.LocalizedText>().localizedKey;
    
        }
        else
        {
            return "";
        }
    }


	public void ShowThisScreen ()
	{
        if (!ViewIScreenController.IsTransitionRunning())
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.Fade));
            SetAboutScreenEntered(true);
          
        }
    }
	
	public void ShowPreviousScreen ()
	{
        if (!ViewIScreenController.IsTransitionRunning())
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
            SetAboutScreenEntered(false);
        }
    }
	
	public void DisableScreen ()
    {
        SetAboutScreenEntered(false);
        gameObject.SetActive(false);
	}
	
	public void EnableScreen ()
	{
		gameObject.SetActive(true);
        SetAboutScreenEntered(true);
    }
	
	public GameObject GetGameObject ()
	{
		return gameObject;
	}

	#endregion
}
