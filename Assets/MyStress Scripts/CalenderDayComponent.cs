﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    /// <summary>
    /// A calendar day component button, each day on visible calendar has one of these
    /// </summary>
	public class CalenderDayComponent : MonoBehaviour  {

		public Text dateText;
		public MyStressData data; 
		public Image smileyForeground;
		public Image smileyBackground;
        public Button dateButton;
        public Button backButton;
        public CardSubPage calenderPage;
        public MyStressCalendarDateInfo datePage;
        
        /// <summary>
        /// Initializing method
        /// </summary>
		void Awake(){
            //Debug.Log("CalenderDayComponent AWAKE");
			dateText = GetComponent<Text>();
			smileyBackground = new GameObject ("Smiley Background", typeof(RectTransform)).AddComponent<Image>();
			smileyBackground.transform.SetParent (gameObject.transform, false);
			smileyBackground.rectTransform.sizeDelta = Vector2.zero;
			smileyBackground.rectTransform.anchorMin = new Vector2 (0.2f,0);
			smileyBackground.rectTransform.anchorMax = new Vector2 (0.8f,1);
			smileyBackground.rectTransform.anchoredPosition = new Vector2(6,0);
			smileyBackground.preserveAspect = true;
			smileyBackground.enabled = false;
			smileyBackground.sprite = GetComponentInParent<DateSelectorMyStress>().smileyBackgroundSprite;

			smileyForeground = new GameObject ("Smiley Foreground", typeof(RectTransform)).AddComponent<Image>();
			smileyForeground.transform.SetParent (smileyBackground.transform, false);
			smileyForeground.rectTransform.sizeDelta = Vector2.zero;
			smileyForeground.rectTransform.anchorMin = new Vector2 (0,0);
			smileyForeground.rectTransform.anchorMax = new Vector2 (1,1);
			smileyForeground.preserveAspect = true;
			smileyForeground.enabled = false;

            dateButton = GetComponent<Button>();
            dateButton.onClick.AddListener(() => { OnShowThisDate(); });
            dateButton.enabled = false;
		}

        /// <summary>
        /// Sets visible smiley on calendar for this instance
        /// </summary>
        /// <param name="smileyAttribute"></param>
		public void SetSmiley(MyStressSmileyAttribute smileyAttribute){
			smileyForeground.sprite = smileyAttribute.smileySprite;
			smileyBackground.color = smileyAttribute.smileyColor;
		}

        /// <summary>
        /// Show date information for specific clicked date
        /// </summary>
        public void OnShowThisDate()
        {
            datePage.UpdateDateInfo(data);
            StartCoroutine(ViewIScreenController.ShowOtherCardPage(calenderPage, datePage, TransitionType.Fade,false));
        }
	}
}
