﻿using UnityEngine;
using System.Collections;
using Thrive;
using UnityEngine.UI;

/// <summary>
/// A basic screen to display scrollable text
/// </summary>
public class SimpleTextScrollScreen : MonoBehaviour, IScreen {
	[SerializeField]
	private Button backButton;

    [SerializeField]
    private Image leftCoverUpBar;
    [SerializeField]
    private Image rightCoverUpBar;
    [SerializeField]
    private ScrollRect scrollArea;

	public string headerTitleKey;
	public Text bodyText;
	

    CardHeaderPanelExtention headerExtension;

    /// <summary>
    /// Initilization
    /// </summary>
	void Awake()
	{
		backButton.onClick.AddListener(ShowPreviousScreen);
        headerExtension = FindObjectOfType<CardHeaderPanelExtention>();
        MainUI mainUI = FindObjectOfType<MainUI>();
        bodyText.fontSize = mainUI.GetBasicFontSize();

    }

    /// <summary>
    /// IScreen interface implemented methods
    /// </summary>
    #region IScreen implementation
    public string GetScreenHeaderTitle()
    {
        if (headerTitleKey != null)
        {
            return headerTitleKey;
        }
        else
        {
            return "";
        }
    }

	public void ShowThisScreen ()
	{
		EnableScreen();
        headerExtension.FadeOutLeft();
        headerExtension.FadeOutRight();
		StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.RightToLeft));
    }

	
	public void ShowPreviousScreen ()
	{
        headerExtension.FadeInLeft();
        headerExtension.FadeInRight();
		StartCoroutine(ViewIScreenController.ShowPreviousScreen());
	}
	
	public void DisableScreen ()
	{
        if (gameObject != null)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
	}
	
	public void EnableScreen ()
	{
		gameObject.SetActive(true);
	}
	
	public GameObject GetGameObject ()
	{
		return gameObject;
	}
	
	#endregion
}
