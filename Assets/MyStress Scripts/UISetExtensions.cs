﻿using System.Reflection;
using UnityEngine.UI;

/// <summary>
/// http://forum.unity3d.com/threads/change-the-value-of-a-toggle-without-triggering-onvaluechanged.275056/#post-2307765
/// Change the Value of a Toggle, Slider or Scrollbar without triggering OnValueChanged
/// </summary>
public static class UISetExtensions
{
	static readonly MethodInfo toggleSetMethod;
	static readonly MethodInfo sliderSetMethod;
	static readonly MethodInfo scrollbarSetMethod;
	
	static UISetExtensions()
	{
		// Find the Toggle's set method
		FindSetMethod(typeof(Toggle), out toggleSetMethod);
		
		// Find the Slider's set method
		FindSetMethod(typeof(Slider), out sliderSetMethod);
		
		// Find the Scrollbar's set method
		FindSetMethod(typeof(Scrollbar), out scrollbarSetMethod);
	}
	
	public static void Set(this Toggle instance, bool value, bool sendCallback = false)
	{
		InvokeSetMethod(toggleSetMethod, instance, value, sendCallback);
	}
	
	public static void Set(this Slider instance, float value, bool sendCallback = false)
	{
		InvokeSetMethod(sliderSetMethod, instance, value, sendCallback);
	}
	
	public static void Set(this Scrollbar instance, float value, bool sendCallback = false)
	{
		InvokeSetMethod(scrollbarSetMethod, instance, value, sendCallback);
	}
	
	private static void FindSetMethod(System.Type objectType, out MethodInfo setMethodInfo)
	{
		setMethodInfo = null;
		
		MethodInfo[] methods = objectType.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);
		for (var i = 0; i < methods.Length; i++)
		{
			if (methods[i].Name == "Set" && methods[i].GetParameters().Length == 2)
			{
				setMethodInfo = methods[i];
				break;
			}
		}
	}
	
	private static void InvokeSetMethod(MethodInfo setMethodInfo, System.Object instance, object value, bool sendCallback)
	{
		setMethodInfo.Invoke(instance, new [] { value, sendCallback });
	}
}
