﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


namespace Thrive
{
    /// <summary>
    /// Enum class that defines the levels of overload available
    /// </summary>
	public enum OverloadLevel
	{
	    NORMAL = 0,
	    ABOVE_NORMAL = 1,
	    HIGH = 2
	}

    /// <summary>
    /// Enum class that defines the levels of effect available
    /// </summary>
	public enum EffectLevel
	{
        NONE = 0,
	    GREEN = 1,
	    YELLOW = 2,
	    RED = 3,
		
	}
    /// <summary>
    /// Class that is used to display smileys on calendar page
    /// </summary>
	public class MyStressSmileyAttribute
	{
		public Sprite smileySprite;
		public Color smileyColor;
		public OverloadLevel overloadLevel;
		public EffectLevel effectLevel;
	}

    /// <summary>
    /// Class for seriaziling MyStress data, for saving and loading to disk
    /// </summary>
	[Serializable]
	public class MyStressData  {
	    
	    public System.DateTime regDate;
        public bool symptompsRegistered;
        public bool triggersRegistered;
        public bool copingRegistered;
	    public OverloadLevel overload;
        public List<SelectableStressComponentData> symptoms = new List<SelectableStressComponentData>();
        public List<SelectableStressComponentData> triggers = new List<SelectableStressComponentData>();
        public List<SelectableStressComponentData> coping = new List<SelectableStressComponentData>();
        public bool copingStepCompleted = false;
	    public EffectLevel effect = EffectLevel.NONE;

        public override string ToString()
        {
            string s = "";
            s += "regDate: " + regDate;
            return s;
        }
	}
}
