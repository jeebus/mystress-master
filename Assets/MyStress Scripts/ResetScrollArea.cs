﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Reset a scroll area to original position OnEnable
/// </summary>
public class ResetScrollArea : MonoBehaviour {

	public RectTransform scrollPanel;

	void OnEnable(){
		scrollPanel.anchoredPosition = new Vector2 (0,0);
	}

}
