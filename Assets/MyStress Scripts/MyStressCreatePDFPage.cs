﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Thrive
{
    /// <summary>
    /// Page that is displayed for the user to create PDFs from
    /// </summary>
    public class MyStressCreatePDFPage : MonoBehaviour
    {
        private string optionalName;
        public SimplePDF pdfCreator;
        public Button generatePDFButton;
        public Text fromDateText;
        public Text toDateText;

        public InputField optionalNameInput;

        System.DateTime startTime;
        System.DateTime endTime;

        /// <summary>
        /// Initialization
        /// </summary>
        void Start()
        {
			startTime = System.DateTime.UtcNow;
			endTime = System.DateTime.UtcNow;
            
			SmartLocalization.LanguageManager.Instance.OnChangeLanguage += OnChangeLanguage;
			OnChangeLanguage(SmartLocalization.LanguageManager.Instance);
        }

        /// <summary>
        /// On language changed callback
        /// </summary>
        /// <param name="manager">SmartLocalization.LanguageManager instance</param>
		void OnChangeLanguage(SmartLocalization.LanguageManager manager)
		{
			fromDateText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("Generic.Month." + startTime.Month) + " " + startTime.Year;
			toDateText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("Generic.Month." + endTime.Month) + " " + endTime.Year;
		}

        /// <summary>
        /// Method for advancing PDF stats from month, this has been set thru Unity Editor Inspector
        /// </summary>
        public void FromTimeStepForward()
        {
            startTime = startTime.AddMonths(1);
            fromDateText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("Generic.Month." + startTime.Month) + " " + startTime.Year;

            //Dont allow stepping forward longer than to-date
            if (startTime > endTime)
            {
                endTime = endTime.AddMonths(1);
                toDateText.text = fromDateText.text;
            }
        }

        /// <summary>
        /// Method for regressing PDF stats from month, this has been set thru Unity Editor Inspector
        /// </summary>
        public void FromTimeStepBack()
        {
            startTime = startTime.AddMonths(-1);
            fromDateText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("Generic.Month." + startTime.Month) + " " + startTime.Year;
        }

        /// <summary>
        /// Method for advancing PDF stats to month, this has been set thru Unity Editor Inspector
        /// </summary>
        public void ToTimeStepForward()
        {
            endTime = endTime.AddMonths(1);
            toDateText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("Generic.Month." + endTime.Month) + " " + endTime.Year;
        }

        /// <summary>
        /// Method for regressing PDF stats to month, this has been set thru Unity Editor Inspector
        /// </summary>
        public void ToTimeStepBack()
        {
            endTime = endTime.AddMonths(-1);
            toDateText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("Generic.Month." + endTime.Month) + " " + endTime.Year;

            //Dont allow stepping back longer than from date
            if (startTime > endTime)
            {
                startTime = startTime.AddMonths(-1);
                fromDateText.text = toDateText.text;
            }
        }

        /// <summary>
        /// Method call back for on create PDF button click, this has been set thru Unity Editor Inspector
        /// </summary>
        public void CreatePDFDocument()
        {
          
            if (!pdfCreator.IsGeneratingPDFNow())
            {
                string optionalName = "";
                if (optionalNameInput.text.Length > 0)
                {
                    optionalName = optionalNameInput.text;
                }

                pdfCreator.SetOptionalName(optionalName);
                Debug.Log("From date: " + startTime.ToString() + "   to date: " + endTime.ToString());
                pdfCreator.SetStatsPeriod(startTime, endTime);
                StartCoroutine( pdfCreator.CreatePDF());

            }
            else
            {
                Debug.Log("Already generating PDF, cant start it again.");
            }
        }
          
    }
}