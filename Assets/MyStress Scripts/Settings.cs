﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// MyStress Settings class to store app options
/// </summary>
[Serializable]
public class Settings 
{
	public bool recieveNotifications;
	public float notificationsTimer;
	public string language;
    public bool hasEffectBeenSetToday;
	public DateTime lastNotificationDate;

    /// <summary>
    /// An overridden version of ToString() method
    /// </summary>
    /// <returns>Customized ToString()</returns>
    public override string ToString ()
	{
		string s = "";
		s += "receive notifications: " + recieveNotifications + "\n";
		s += "notification timer: " + notificationsTimer + "\n";
		s += "language: " + language;
		return s;
	}
}
