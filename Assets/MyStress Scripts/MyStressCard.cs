﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using SmartLocalization;
using NativeAlert;

namespace Thrive
{
    /// <summary>
    /// In the MyStress app, this is the main card that controls any of the subsequent pages of the card that is displayed.
    /// ALl general interaction with the MyStress card pages are handled thru the ICard interface class, via Thrive classes. 
    /// Specific MyStress behaviour is implemented in this class and its under pages
    /// All public variables of this class is set the Unity Editor Inspector
    /// </summary>
    public class MyStressCard : MonoBehaviour, ICard
	{
		public static MyStressCard Instance;
		public Image cardIcon;
		public Text cardNameTextComponent;
        public GameObject cardFrontSide;
        public GameObject cardBackSide;
        public Image cardImageTexture;
        public CanvasGroup alertMessageOverlayGrp;
        public Button symptomsButton;

        public OverloadCard overloadCard;
        public EffectCard effectCard;
        public CardSubPage symptomsCardPage;
        public CardSubPage triggerCardPage;
        public CardSubPage copingCardPage;
        public CardSubPage calenderCardPage;
        public CardSubPage pdfCreationCardPage;

        public RectTransform symptomsComponentArea;
        public RectTransform triggerComponentArea;
        public RectTransform copingComponentArea;

        public GameObject selectableStressComponentPrefab;
        public MyStressCalendarDateInfo dateStatsPage;

		public Scrollbar overloadSlider;
        public Scrollbar effektSlider; 

        public Color effectColorGreen;
        public Color effectColorYellow;
        public Color effectColorRed;
		public Color effectColorNone;

		// These images are used by Calender
		public Sprite normalLoadSmileySprite;
		public Sprite overNormalLoadSprite;
		public Sprite highLoadSprite;
		public Sprite backgroundSmileySprite;

        public DateSelectorMyStress calender;

        bool rotateCardToReadMore = false;
        bool overloadValueWasChanged = false;
        OverloadLevel selectedOverloadLevel;
        EffectLevel selectedEffectLevel = EffectLevel.NONE;

        MyStressData todaysMyStressData = new MyStressData();

        public List<SelectableStressComponent> scrollAbleListSymptoms = new List<SelectableStressComponent>();
        public List<SelectableStressComponent> scrollAbleListTriggers = new List<SelectableStressComponent>();
        public List<SelectableStressComponent> scrollAbleListCoping = new List<SelectableStressComponent>();

		DateTime sessionDateTime;


        /// <summary>
        /// Initialization
        /// </summary>
        void Start()
		{
			//mainUI = FindObjectOfType<MainUI>();
			if(!Instance)
				Instance = this;

			sessionDateTime = System.DateTime.UtcNow;

#if UNITY_EDITOR
            // ONLY FOR DEBUGGING!
			sessionDateTime = sessionDateTime.AddDays(MyStressUserContent.GetFakeDays());
			Debug.Log("###### faked today is " + sessionDateTime);
#endif

			InitSettings();

			todaysMyStressData.regDate = sessionDateTime;
            Debug.Log("Todays data is from " + todaysMyStressData.regDate.ToLocalTime().ToString());


            if (MyStressUserContent.useTestData)
            {
                if (MyStressUserContent.LoadDummySessionStats() && MyStressUserContent.sessionStats.Count > 0)
                {
                    Debug.Log("Last registered overload level : " + MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].overload.ToString());
                }
            }
            else
            {
                if (MyStressUserContent.LoadStoredStats() && MyStressUserContent.sessionStats.Count > 0)
                {
                    Debug.Log("Last registered overload level : " + MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].overload.ToString());
                    Debug.Log("Last registered overload data: " + MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].regDate);
                }
            }
			bool statsAlreadyContainsToday = false;
			for (int i = 0; i < MyStressUserContent.sessionStats.Count; i++) {

				if (MyStressUserContent.sessionStats[i].regDate.ToLocalTime().Date == sessionDateTime.ToLocalTime().Date)
                {
					statsAlreadyContainsToday = true;
				}	

			}
            
			if (!statsAlreadyContainsToday) 
			{
                Debug.Log("##### MyStress data not logged for today!");
                //if (!statsAlreadyContainsToday)
                    MyStressUserContent.sessionStats.Add(todaysMyStressData); // After initialization we add an empty session for today
                symptomsButton.interactable = false;

			} else {
				Debug.Log("##### MyStress stats have already been logged for today");
                overloadValueWasChanged = true;
                symptomsButton.interactable = true;
				todaysMyStressData = MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count-1];



                switch (todaysMyStressData.overload)
	            {
                    case OverloadLevel.NORMAL:
                        overloadSlider.value = 0;
                        break;
                    case OverloadLevel.ABOVE_NORMAL:
                        overloadSlider.value = 0.4f;
                        break;
                    case OverloadLevel.HIGH:
                        overloadSlider.value = 0.8f;
                        break;
                    default:
                        break;
	            }

				lastRegisteredOverloadValue = overloadSlider.value;

			}
			overloadCard.SetOverload(GetSmilieAtrributes(todaysMyStressData.overload, todaysMyStressData.effect));

            InitSymptomsPage();
            InitTriggerPage();
            InitCopingPage();


            ViewIScreenController.SetCurrentCard(overloadCard, calenderCardPage);

			Debug.Log("LAST NOTE DATE:" + MyStressUserContent.settings.lastNotificationDate.ToLocalTime());
            if (MyStressUserContent.settings.lastNotificationDate != DateTime.MinValue)
            {
				if (MyStressUserContent.settings.lastNotificationDate.ToLocalTime().Date == sessionDateTime.ToLocalTime().Date && 
				    MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].regDate.ToLocalTime().Date == sessionDateTime.ToLocalTime().Date && 
				    MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].copingStepCompleted)
                {
                    OnEffektButtonClick();

                }
            }

			StartCoroutine (ListenForDateChanges());
        }

#if UNITY_EDITOR
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnApplicationPause(true);

                //	MyStressUserContent.AddToFakeDays ();
                Debug.Break();
            }

            if (Input.GetKeyDown(KeyCode.K))
            {
                ResetCardOnNewDate();
            }
        }
#endif


        int pauseCount = 0;

        void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                // we are in background
                Debug.Log("App paused" + " sessionStats count = " + MyStressUserContent.sessionStats.Count + " sesssion date : " + sessionDateTime.ToLocalTime().Date);
            }
            else
            {
                //was used for debugging purposes
                //pauseCount++;

                Debug.Log("App un-paused" + " sessionStats count = " + MyStressUserContent.sessionStats.Count + " sesssion date : " + sessionDateTime.ToLocalTime().Date);
                bool statsAlreadyContainsToday = false;
                sessionDateTime = System.DateTime.UtcNow.AddDays(pauseCount);

                if (MyStressUserContent.sessionStats != null)
                {
                    if (MyStressUserContent.sessionStats.Count > 0)
                    {
                        Debug.Log("Last registered date in stats: " + MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].regDate.ToLocalTime().Date);
                        if (MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].regDate.ToLocalTime().Date == sessionDateTime.ToLocalTime().Date)
                        {
                            statsAlreadyContainsToday = true;
                            Debug.Log("Stats already contain today, its not a new day yet");
                        }
                    }
                    if (!statsAlreadyContainsToday) // A day with unregistered data is present
                    {
                        Debug.Log("Resetting card on new date!");
                        ResetCardOnNewDate();

                    }
                }
                else {
                    Debug.Log("MyStressUserContent.sessionStats is null!");
                }
            }
        }

        /// <summary>
        /// Initiates the symptoms page of the MyStress card, loads stored symptom stats from all previous sessions, 
        /// or creates a new list of stats if none exists (i.e. on first run) 
        /// </summary>
        void InitSymptomsPage()
        {
            if (MyStressUserContent.useTestData)
            {
                for (int i = 1; i < 16; i++)
                {
                    MyStressUserContent.sessionSymptomStrings.Add(new SelectableStressComponentData("Symptoms." + i, false));
                }
            }
            else
            {
                if (!MyStressUserContent.LoadStoredSymptomStrings())
                {
                    // Ran only first time application starts
                    for (int i = 1; i < 16; i++)
                    {
                        MyStressUserContent.sessionSymptomStrings.Add(new SelectableStressComponentData("Symptoms." + i, false));
                    }
                }
            }


            if (MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].symptoms.Count > 0)
                MyStressUserContent.sessionSymptomStrings = MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].symptoms;

            todaysMyStressData.symptoms = MyStressUserContent.sessionSymptomStrings; // Pass by reference (NOT COPY)
                                                                                     //Always take the last added element, as a user is not alowed to edit anything but the current day
            for (int i = todaysMyStressData.symptoms.Count; i > 0; i--)
            {
                SelectableStressComponent newStressComponent = ((GameObject)Instantiate(selectableStressComponentPrefab)).GetComponent<SelectableStressComponent>();
                newStressComponent.transform.SetParent(symptomsComponentArea, false);
                newStressComponent.transform.SetAsFirstSibling();

                //Always take the last added element, as a user is not alowed to edit anything but the current day
                SelectableStressComponentData storedSymptom = todaysMyStressData.symptoms[i - 1];
                if (storedSymptom.componentString.StartsWith("Symptoms."))
                {

                    LanguageManager languageManagerInstance = LanguageManager.Instance;
                    languageManagerInstance.OnChangeLanguage += newStressComponent.OnChangeLanguage;
                    newStressComponent.SetLocalizedKey(storedSymptom.componentString);

                    //newStressComponent.GetStressTextComponent().text = SmartLocalization.LanguageManager.Instance.GetTextValue(storedSymptom.componentString);
                }
                else
                {
                    newStressComponent.GetStressTextComponent().text = storedSymptom.componentString;
                }
                newStressComponent.componentType = StressComponentType.Symptom;
                newStressComponent.data = storedSymptom;

                newStressComponent.SetSelected(newStressComponent.data.selected);
                if (newStressComponent.data.selected)
                {
                    //Debug.Log(newStressComponent.data.componentString);
                    newStressComponent.button.interactable = false;
                }
                scrollAbleListSymptoms.Add(newStressComponent);
            }


            symptomsCardPage.GetComponent<CanvasGroup>().blocksRaycasts = false;
            symptomsCardPage.gameObject.SetActive(false);
        }

        /// <summary>
        /// Initiates the triggers page of the MyStress card, loads stored trigger stats from all previous sessions, 
        /// or creates a new list of stats if none exists (i.e. on first run) 
        /// </summary>
        void InitTriggerPage()
        {
            if (MyStressUserContent.useTestData)
            {
                for (int i = 1; i < 16; i++)
                {
                    MyStressUserContent.sessionTriggerStrings.Add(new SelectableStressComponentData("Triggers." + i, false));
                }
            }
            else
            {
                if (!MyStressUserContent.LoadStoredTriggerStrings())
                {
                    for (int i = 1; i < 17; i++)
                    {
                        MyStressUserContent.sessionTriggerStrings.Add(new SelectableStressComponentData("Triggers." + i, false));
                    }
                }
            }

            if (MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].triggers.Count > 0) // If triggers are stored for today
                MyStressUserContent.sessionTriggerStrings = MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].triggers;

            todaysMyStressData.triggers = MyStressUserContent.sessionTriggerStrings;

            for (int i = todaysMyStressData.triggers.Count; i > 0; i--)
            {
                SelectableStressComponent newStressComponent = ((GameObject)Instantiate(selectableStressComponentPrefab)).GetComponent<SelectableStressComponent>();
                newStressComponent.transform.SetParent(triggerComponentArea.transform, false);
                newStressComponent.transform.SetAsFirstSibling();

                //Always take the last added element, as a user is not alowed to edit anything but the current day
                SelectableStressComponentData storedTrigger = todaysMyStressData.triggers[i - 1];
                if (storedTrigger.componentString.StartsWith("Triggers."))
                {
                    LanguageManager languageManagerInstance = LanguageManager.Instance;
                    languageManagerInstance.OnChangeLanguage += newStressComponent.OnChangeLanguage;
                    newStressComponent.SetLocalizedKey(storedTrigger.componentString);

                    //newStressComponent.GetStressTextComponent().text = SmartLocalization.LanguageManager.Instance.GetTextValue(storedTrigger.componentString);
                }
                else {
                    newStressComponent.GetStressTextComponent().text = storedTrigger.componentString;
                }

                newStressComponent.componentType = StressComponentType.Trigger;
                newStressComponent.data = storedTrigger;
                newStressComponent.SetSelected(newStressComponent.data.selected);
                if (newStressComponent.data.selected)
                {
                    newStressComponent.button.interactable = false;
                }
                scrollAbleListTriggers.Add(newStressComponent);
            }


            triggerCardPage.GetComponent<CanvasGroup>().blocksRaycasts = false;
            triggerCardPage.gameObject.SetActive(false);
        }

        /// <summary>
        /// Initiates the coping page of the MyStress card, loads stored coping stats from all previous sessions, 
        /// or creates a new list of stats if none exists (i.e. on first run) 
        /// </summary>
        void InitCopingPage()
        {
            //Hardcoded coping strings category order  
            int[] copingCategoryOrder = new int[20] { 1, 2, 2, 2, 4, 1, 2, 4, 3, 4, 3, 3, 5, 1, 4, 1, 3, 5, 5, 5 };

            if (!MyStressUserContent.LoadStoredCopingStrings())
            {
                for (int i = 1; i < 20; i++)
                {
                    MyStressUserContent.sessionCopingStrings.Add(new SelectableStressComponentData("Coping." + i, false, (CopingCategory)copingCategoryOrder[i]));
                }

            }
            if (MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].coping.Count > 0) // If coping are stored for today
                MyStressUserContent.sessionCopingStrings = MyStressUserContent.sessionStats[MyStressUserContent.sessionStats.Count - 1].coping;

            todaysMyStressData.coping = MyStressUserContent.sessionCopingStrings;

            for (int i = todaysMyStressData.coping.Count; i > 0; i--)
            {
                SelectableStressComponent newStressComponent = ((GameObject)Instantiate(selectableStressComponentPrefab)).GetComponent<SelectableStressComponent>();
                newStressComponent.transform.SetParent(copingComponentArea.transform, false);
                newStressComponent.transform.SetAsFirstSibling();

                //Always take the last added element, as a user is not alowed to edit anything but the current day
                SelectableStressComponentData storedCope = todaysMyStressData.coping[i - 1];
                LanguageManager languageManagerInstance = LanguageManager.Instance;
                languageManagerInstance.OnChangeLanguage += newStressComponent.OnChangeLanguage;
                newStressComponent.SetLocalizedKey(storedCope.componentString);

                //newStressComponent.GetStressTextComponent().text = SmartLocalization.LanguageManager.Instance.GetTextValue(storedCope.componentString);
                newStressComponent.componentType = StressComponentType.Coping;
                newStressComponent.data = storedCope;
                newStressComponent.SetSelected(newStressComponent.data.selected);
                if (newStressComponent.data.selected)
                {
                    //Debug.Log("Selected" + storedCope.componentString);
                    newStressComponent.button.interactable = false;
                }
                scrollAbleListCoping.Add(newStressComponent);
            }

            copingCardPage.GetComponent<CanvasGroup>().blocksRaycasts = false;
            copingCardPage.gameObject.SetActive(false);
        }

        /// <summary>
        /// Initiates app settings, by first checking if there are any previous sessions settings to load
        /// </summary>
        void InitSettings()
        {
            if (MyStressUserContent.LoadStoredSettings() == false)
            {
                Debug.Log("InitSettings() call");
                MyStressUserContent.settings.lastNotificationDate = DateTime.UtcNow.AddDays(MyStressUserContent.GetFakeDays());
                MyStressUserContent.settings.notificationsTimer = 2.0f;//2;
                MyStressUserContent.settings.recieveNotifications = true;
                MyStressUserContent.settings.language = "da";
                MyStressUserContent.settings.hasEffectBeenSetToday = false;
                MyStressUserContent.SaveCurrentSettings();
            }
        }

        /// <summary>
        /// Coroutine for listening for date changes when app is running, only useful when app is open at midnight
        /// </summary>
        /// <returns></returns>
        IEnumerator ListenForDateChanges(){
			yield return new WaitForSeconds(1.0f);

			if (sessionDateTime.Date != System.DateTime.UtcNow.AddDays(pauseCount).Date) {
				Debug.Log ("Date has advanced!");
				ResetCardOnNewDate();
			}
		}

        /// <summary>
        /// Method for resetting MyStress content on date changes
        /// On method call, shows the inital page, the overload card, and resets components (scroll lists, sliders,...) on the other pages
        /// </summary>
		void ResetCardOnNewDate(){

			sessionDateTime = System.DateTime.UtcNow.AddDays(pauseCount);

			if (ViewIScreenController.GetCurrentFrontSideCard() != overloadCard) {
                Debug.Log(ViewIScreenController.GetCurrentFrontSideCard() + " is not equal to " + overloadCard);

                StartCoroutine(ViewIScreenController.ShowOtherCardPage(ViewIScreenController.GetCurrentFrontSideCard(), overloadCard, TransitionType.Fade,true));
			} else {
				Debug.Log ("Already showing overloadCard, no need to attempt to show this card again");

			}
			todaysMyStressData = new MyStressData ();

			List<SelectableStressComponentData> symptomsStringCopy = new List<SelectableStressComponentData> ();
			for (int i = 0; i <  MyStressUserContent.sessionSymptomStrings.Count; i++) {
				SelectableStressComponentData newData = new SelectableStressComponentData(MyStressUserContent.sessionSymptomStrings[i].componentString,false,MyStressUserContent.sessionSymptomStrings[i].category);
				symptomsStringCopy.Add(newData);
			}
			MyStressUserContent.sessionSymptomStrings = symptomsStringCopy;
			todaysMyStressData.symptoms = MyStressUserContent.sessionSymptomStrings;

			List<SelectableStressComponentData> copingStringCopy = new List<SelectableStressComponentData> ();
			for (int i = 0; i <  MyStressUserContent.sessionCopingStrings.Count; i++) {
				SelectableStressComponentData newData = new SelectableStressComponentData(MyStressUserContent.sessionCopingStrings[i].componentString,false,MyStressUserContent.sessionCopingStrings[i].category);
				copingStringCopy.Add(newData);
			}
			MyStressUserContent.sessionCopingStrings = copingStringCopy;
			todaysMyStressData.coping = MyStressUserContent.sessionCopingStrings;


			List<SelectableStressComponentData> triggerStringCopy = new List<SelectableStressComponentData> ();
			for (int i = 0; i <  MyStressUserContent.sessionTriggerStrings.Count; i++) {
				SelectableStressComponentData newData = new SelectableStressComponentData(MyStressUserContent.sessionTriggerStrings[i].componentString,false,MyStressUserContent.sessionTriggerStrings[i].category);
				triggerStringCopy.Add(newData);
			}
			MyStressUserContent.sessionTriggerStrings = triggerStringCopy;
			todaysMyStressData.triggers = MyStressUserContent.sessionTriggerStrings;


			lastRegisteredOverloadValue = 0;
			overloadSlider.value = 0;
			MyStressUserContent.settings.hasEffectBeenSetToday = false;

		//	sessionDateTime = System.DateTime.UtcNow.AddDays(MyStressUserContent.GetFakeDays());
		//	Debug.Log("###### faked today is " + sessionDateTime);

			todaysMyStressData.regDate = sessionDateTime;
			Debug.Log("Todays data is from " + todaysMyStressData.regDate.ToLocalTime().ToString());

			MyStressUserContent.sessionStats.Add(todaysMyStressData); // After initialization we add an empty session for today
			symptomsButton.interactable = false;
			MyStressSmileyAttribute resetSmiley = GetSmilieAtrributes (OverloadLevel.NORMAL, EffectLevel.NONE);
			
			overloadCard.SetOverload(resetSmiley);
			effectCard.ResetEffect (resetSmiley);

			for (int i = 0; i < scrollAbleListSymptoms.Count; i++)
			{
				if (scrollAbleListSymptoms[i].IsSelected())
				{
					scrollAbleListSymptoms[i].SetSelected(false);
					scrollAbleListSymptoms[i].button.interactable = true;
				}
			}

			for (int i = 0; i < scrollAbleListCoping.Count; i++)
			{
				if (scrollAbleListCoping[i].IsSelected())
				{
					scrollAbleListCoping[i].SetSelected(false);
					scrollAbleListSymptoms[i].button.interactable = true;
				}
			}

			for (int i = 0; i < scrollAbleListTriggers.Count; i++)
			{
				if (scrollAbleListTriggers[i].IsSelected())
				{
					scrollAbleListTriggers[i].SetSelected(false);
					scrollAbleListSymptoms[i].button.interactable = true;
				}
			}
			Debug.Log("LAST NOTE DATE:" + MyStressUserContent.settings.lastNotificationDate.ToLocalTime());
		}


        /// <summary>
        /// Returns a smiley element, based on a given OverloadLevel and EffectLevel
        /// </summary>
        /// <param name="loadLevel">Overload level</param>
        /// <param name="effectLevel">Effect level</param>
        /// <returns>Returns a MyStressSmileyAttribute object</returns>
        public MyStressSmileyAttribute GetSmilieAtrributes(OverloadLevel loadLevel, EffectLevel effectLevel)
		{
			MyStressSmileyAttribute smiley = new MyStressSmileyAttribute();

			smiley.overloadLevel = loadLevel;

			if(loadLevel == OverloadLevel.NORMAL)
				smiley.smileySprite = normalLoadSmileySprite;
			else if(loadLevel == OverloadLevel.ABOVE_NORMAL)
				smiley.smileySprite = overNormalLoadSprite;
			else
				smiley.smileySprite = highLoadSprite;

	
			smiley.effectLevel = effectLevel;
			if(effectLevel == EffectLevel.GREEN)
				smiley.smileyColor = effectColorGreen;
			else if(effectLevel == EffectLevel.YELLOW)
				smiley.smileyColor = effectColorYellow;
			else if (effectLevel == EffectLevel.RED)
				smiley.smileyColor = effectColorRed;
			else // NONE
				smiley.smileyColor = effectColorNone;

			return smiley;
		}

		float lastRegisteredOverloadValue = 0;

        /// <summary>
        /// Method that is called when the value of the Overload slider is changed, this method has been set on slider thru Unity Editor Inspector
        /// </summary>
		public void OnChangedOverloadValue(){
			if (lastRegisteredOverloadValue <= overloadSlider.value) {
				if (overloadValueWasChanged)
					symptomsButton.interactable = true;

				overloadValueWasChanged = true;

				if (overloadSlider.value < 0.333f) {
					//Normal belastet
					selectedOverloadLevel = OverloadLevel.NORMAL;
				}
				if (overloadSlider.value > 0.333f && overloadSlider.value < 0.666f) {
					//Over normal belastet
					selectedOverloadLevel = OverloadLevel.ABOVE_NORMAL;
				}
				if (overloadSlider.value > 0.666f) {
					//Hårdt belastet
					selectedOverloadLevel = OverloadLevel.HIGH;
				}

				overloadCard.SetOverload (GetSmilieAtrributes (selectedOverloadLevel, selectedEffectLevel));
				todaysMyStressData.overload = selectedOverloadLevel;


			} else {
				 overloadSlider.value = lastRegisteredOverloadValue;
			}
			
		}


        /// <summary>
        /// Method that is called when the value of the Effect slider is changed, this method has been set on slider thru Unity Editor Inspector
        /// </summary>
        public void OnChangedEffektValue()
        {

			Debug.Log ("Effekt value changed");
			if (effektSlider.value < 0.333f)
            {
                selectedEffectLevel = EffectLevel.GREEN;
            }
            if (effektSlider.value > 0.333f && effektSlider.value < 0.666f)
            {
               selectedEffectLevel = EffectLevel.YELLOW;
            }
            if (effektSlider.value > 0.666f)
            {
               selectedEffectLevel = EffectLevel.RED;
        	}
			if (!MyStressUserContent.settings.hasEffectBeenSetToday)
			{
				//selectedEffectLevel = EffectLevel.NONE;
			}

			if(effectCard.isActiveAndEnabled)
			{

				effectCard.SetEffect(GetSmilieAtrributes(selectedOverloadLevel, selectedEffectLevel));
				//Debug.Log(GetSmilieAtrributes(selectedOverloadLevel, selectedEffectLevel).effectLevel);
            	todaysMyStressData.effect = selectedEffectLevel;
			}

        }
        

        /// <summary>
        /// A public method for saving current session stats that can be called from components thru Unity Editor Inspector
        /// </summary>
        public void SaveStats()
        {
            MyStressUserContent.SaveCurrentSessionStats();
        }

        /// <summary>
        /// Method that is called when the Symptoms button on the Overload page is clicked, this method has been set on button thru Unity Editor Inspector
        /// </summary>
        public void OnSymptomerButtonClick(){
            if (overloadValueWasChanged)
            {
                StartCoroutine(ViewIScreenController.ShowOtherCardPage(overloadCard, symptomsCardPage, TransitionType.Fade, true));
                Debug.Log("Go to symptons page");

				//store last registered overload value when going to next "page", used to restrict the user from reducing overload value later on a day with overload value was alaready registered. Can only be used to increase overload 
				lastRegisteredOverloadValue = overloadSlider.value;
            }
            else
            {
                //Joan doesnt like this alert, but maybe we should still show some kind of alert
                //StartCoroutine(ShowAlertTask());
            }
		}


        /// <summary>
        /// Method for when the Triggers button on the Symptoms page is clicked, this method has been set on button thru Unity Editor Inspector
        /// </summary>
        public void OnTriggerButtonClick()
        {
            StartCoroutine(ViewIScreenController.ShowOtherCardPage(symptomsCardPage, triggerCardPage, TransitionType.Fade, true));
        }

        /// <summary>
        /// Method that is called when the Coping button on the Triggers page is clicked, this method has been set on button thru Unity Editor Inspector
        /// </summary>
        public void OnCopingButtonClick()
        {
            StartCoroutine(ViewIScreenController.ShowOtherCardPage(triggerCardPage, copingCardPage, TransitionType.Fade, true));

        }


        /// <summary>
        /// Method for when the Effect button on the Coping page is clicked, this method has been set on button thru Unity Editor Inspector
        /// </summary>
        public void OnEffektButtonClick()
        {
            if (copingCardPage.gameObject.activeSelf)
                StartCoroutine(ViewIScreenController.ShowOtherCardPage(copingCardPage, effectCard, TransitionType.Fade, true));
            else // If starting directly on effect page, we jump from the first page: overloadCard
                StartCoroutine(ViewIScreenController.ShowOtherCardPage(overloadCard, effectCard, TransitionType.Fade,true));
            
           
            todaysMyStressData.copingStepCompleted = true;
            SaveStats();
			SetLocalNotification();

           
			//OnChangedEffektValue();
			effectCard.SetEffect(GetSmilieAtrributes(todaysMyStressData.overload, todaysMyStressData.effect));
        }

        /// <summary>
        /// Method for when the "Create PDF" button on the calendar page is clicked, this method has been set on button thru Unity Editor Inspector
        /// </summary>
        public void OnCreatePDFStartPage()
        {
            if (PlayerPrefs.GetInt("access") == 1)
            {
                StartCoroutine(ViewIScreenController.ShowOtherCardPage(calenderCardPage, pdfCreationCardPage, TransitionType.Fade, false));
            }
            else {
                Debug.Log("No access");

                string header = SmartLocalization.LanguageManager.Instance.GetTextValue("DeniedAccessModal.Header");
                string message = SmartLocalization.LanguageManager.Instance.GetTextValue("DeniedAccessModal.Content");

#if UNITY_ANDROID
                AndroidNativeAlert.ShowAlert(header, message, "Ok");
#elif UNITY_IPHONE
				IOSNativeAlert.ShowAlert(header,message, "Ok");
#endif

            }
        }

        /// <summary>
        /// Starts a new notification on a timer, when Effekt button on Coping page is clicked
        /// </summary>
        void SetLocalNotification()
        {
			bool isNotificationSet = false;
			Debug.Log ("SetLocalNotification() call");

			if (MyStressUserContent.settings.lastNotificationDate.ToLocalTime().Date == sessionDateTime.ToLocalTime().Date)
			{
				isNotificationSet = true;
			}
			else
			{
				MyStressUserContent.settings.lastNotificationDate = DateTime.UtcNow.AddDays(MyStressUserContent.GetFakeDays());
				MyStressUserContent.SaveCurrentSettings();
			}

            if(!isNotificationSet && MyStressUserContent.settings.recieveNotifications)
            {
                float secondsToPush = MyStressUserContent.settings.notificationsTimer * 60 * 60; // settings.notifcationTimer is stored in hours
                //secondsToPush = 60; // DEBUG
				// Don't set notification if it will be next day (set after 22:00), since it is not possible to register effect at that time
                if(DateTime.Now.AddSeconds(secondsToPush).Day != DateTime.Now.Day)
				{

#if UNITY_ANDROID && !UNITY_EDITOR
	              	//AndroidJavaObject ajc = new AndroidJavaObject("com.zeljkosassets.notifications.Notifier");
	                //ajc.CallStatic("sendNotification", "MyStress", "Husk effekt", "Husk at registrere din effekt", secondsToPush);
					LocalNotificationAndroid.SendNotification(1,(long) secondsToPush, "MyStress", SmartLocalization.LanguageManager.Instance.GetTextValue("Notification.Main"), new Color32(0xff, 0x44, 0x44, 255),true,true,true,"");
                
#elif UNITY_IOS
					NotificationServices.RegisterForLocalNotificationTypes(LocalNotificationType.Alert);
					LocalNotification notification = new LocalNotification();
					notification.alertBody = SmartLocalization.LanguageManager.Instance.GetTextValue("Notification.Main");
					notification.fireDate = DateTime.Now.AddSeconds(secondsToPush);
					//Debug.Log("Fire notification! " + DateTime.Now.ToString("HH:mm:ss") + " at " + notification.fireDate.ToString("HH:mm:ss"));
					
					NotificationServices.ScheduleLocalNotification(notification);
#endif
				}
            }
        }

        //Functions for top-corner back button actions, depending on what page is currently being displayed.
        //Functions are set thru Unity Editor Inspector.
        #region MyStressCard back-buttons functionality
        public void OnBackToCalenderPage()
        {
			if(calenderCardPage == null)
			{
				#if UNITY_ANDROID
				AndroidNativeAlert.ShowAlert("Error", "calendar page is null", "Ok");
				#elif UNITY_IPHONE
				IOSNativeAlert.ShowAlert("Error","calendar page is null", "Ok");
				#endif
			}

            StartCoroutine(ViewIScreenController.ShowOtherCardPage(dateStatsPage, calenderCardPage, TransitionType.Fade, false));
            Debug.Log("Go to Calendar page");
        }

        public void OnBackToOverloadStartPage()
        {
            StartCoroutine(ViewIScreenController.ShowOtherCardPage(symptomsCardPage, overloadCard, TransitionType.Fade, true));
            Debug.Log("Go to overload page");
        }

        public void OnBackToSymptomsStartPage()
        {
            for (int i = 0; i < scrollAbleListSymptoms.Count; i++)
            {
                if (scrollAbleListSymptoms[i].IsSelected())
                {
                    scrollAbleListSymptoms[i].button.interactable = false;
                }
            }
            StartCoroutine(ViewIScreenController.ShowOtherCardPage(triggerCardPage, symptomsCardPage, TransitionType.Fade, true));
            Debug.Log("Go to symptoms page");
        }

        public void OnBackToTriggersStartPage()
        {
            for (int i = 0; i < scrollAbleListTriggers.Count; i++)
            {
                if (scrollAbleListTriggers[i].IsSelected())
                {
                    scrollAbleListTriggers[i].button.interactable = false;
                }
            }
            StartCoroutine(ViewIScreenController.ShowOtherCardPage(copingCardPage, triggerCardPage, TransitionType.Fade, true));
            Debug.Log("Go back to Triggers page");
        }
        public void OnBackToCopingStartPage()
        {
			for (int i = 0; i < scrollAbleListCoping.Count; i++)
            {
                if (scrollAbleListCoping[i].IsSelected())
                {
                    scrollAbleListCoping[i].button.interactable = false;
                }
            }
            StartCoroutine(ViewIScreenController.ShowOtherCardPage(effectCard, copingCardPage, TransitionType.Fade, true));
            Debug.Log("Go back to Coping page");
        }


        public void OnBackToCalendarFromPDFStartPage()
        {
            StartCoroutine(ViewIScreenController.ShowOtherCardPage(pdfCreationCardPage, calenderCardPage, TransitionType.Fade, false));
            Debug.Log("Go to Calendar page");
        }
        #endregion

        /// <summary>
        /// ICard interface implemented methods
        /// </summary>
        /// 
        #region ICard interface implementation		
        public const string cardUID = "MYSTRESS_001_STRESS";
		
		public string GetCardUID()
		{
			return cardUID;
		}
		
		public int GetCardOwnerID()
		{
			return 0;
		}
		
		public void SetCardOwnerID(int newCardOwner)
		{
			// cardOwnerID = newCardOwner;
		}
		
		public GameObject GetGameObject()
		{
           // Debug.Log("Try to get gameobject for " + cardUID);
            if (gameObject == null)
            {
                Debug.Log("GameObject is null");
                return null;
            }

			return gameObject;
		}
		
		public void MakeCardContentVisible()
		{
			gameObject.SetActive(true);
		}
		
		public void MakeCardContentInvisible()
		{
			gameObject.SetActive(false);
			// listenForAlertChanges = false;
		}
		
		public void ShowFrontSide()
		{
            cardBackSide.GetComponent<Canvas>().enabled = false;
            cardFrontSide.GetComponent<Canvas>().enabled = true;
            cardBackSide.GetComponent<CanvasGroup>().blocksRaycasts = false;
            cardFrontSide.GetComponent<CanvasGroup>().blocksRaycasts = true;
		}
		public void ShowBackSide()
		{
            cardBackSide.GetComponent<Canvas>().enabled = true;
            cardFrontSide.GetComponent<Canvas>().enabled = false;
            cardBackSide.GetComponent<CanvasGroup>().blocksRaycasts = true;
            cardFrontSide.GetComponent<CanvasGroup>().blocksRaycasts = false;

            StartCoroutine(calender.Refresh());
		}
		public bool GetRotateCardToReadMore()
		{
            return rotateCardToReadMore;
		}
		public string GetCardName()
		{
			return "MyStress";
		}
		public void SetRotateCardToReadMore(bool newReadMoreValue)
		{
			rotateCardToReadMore = newReadMoreValue;
		}
		public void SetCardMaterial(Material material)
		{
			
		}


		public void RotateCardToReadMore()
		{            
            rotateCardToReadMore = true;
            CardController.Instance.RotateCardToReadMore(this);
		}
		
		public void RotateCardBackToOriginal()
		{
            rotateCardToReadMore = false;
            CardController.Instance.RotateCardBackToOriginal(this);
		}
		
		public Image GetCardIcon()
		{
			return cardIcon;
		}
		public Text GetCardNameTextComponent()
		{
			return cardNameTextComponent;
		}
        public Image GetCardImageTexture()
        {
            return cardImageTexture;
        }
        #endregion
    }
}