﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class TestScript : MonoBehaviour,ICard
    {
        bool rotateCardToReadMore;
        Image cardIcon;
        public GameObject cardBackSide;
        public GameObject cardFrontSide;
        Text cardNameTextComponent;
        Image cardImageTexture;

        public void RotateCardToReadMore()
        {
            rotateCardToReadMore = true;
            CardController.Instance.RotateCardToReadMore(this);
        }

        public void RotateCardBackToOriginal()
        {
            rotateCardToReadMore = false;
            CardController.Instance.RotateCardBackToOriginal(this);
        }
        /// <summary>
        /// ICard interface implemented methods
        /// </summary>
        /// 
        #region ICard interface implementation		
        public const string cardUID = "MYSTRESS_001_STRESS";

        public string GetCardUID()
        {
            return cardUID;
        }

        public int GetCardOwnerID()
        {
            return 0;
        }

        public void SetCardOwnerID(int newCardOwner)
        {
            // cardOwnerID = newCardOwner;
        }

        public GameObject GetGameObject()
        {
            // Debug.Log("Try to get gameobject for " + cardUID);
            if (gameObject == null)
            {
                Debug.Log("GameObject is null");
                return null;
            }

            return gameObject;
        }

        public void MakeCardContentVisible()
        {
            gameObject.SetActive(true);
        }

        public void MakeCardContentInvisible()
        {
            gameObject.SetActive(false);
            // listenForAlertChanges = false;
        }

        public void ShowFrontSide()
        {
            cardBackSide.GetComponent<Canvas>().enabled = false;
            cardFrontSide.GetComponent<Canvas>().enabled = true;
            cardBackSide.GetComponent<CanvasGroup>().blocksRaycasts = false;
            cardFrontSide.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
        public void ShowBackSide()
        {
            cardBackSide.GetComponent<Canvas>().enabled = true;
            cardFrontSide.GetComponent<Canvas>().enabled = false;
            cardBackSide.GetComponent<CanvasGroup>().blocksRaycasts = true;
            cardFrontSide.GetComponent<CanvasGroup>().blocksRaycasts = false;

          //  StartCoroutine(calender.Refresh());
        }
        public bool GetRotateCardToReadMore()
        {
            return rotateCardToReadMore;
        }
        public string GetCardName()
        {
            return "MyStress";
        }
        public void SetRotateCardToReadMore(bool newReadMoreValue)
        {
            rotateCardToReadMore = newReadMoreValue;
        }
        public void SetCardMaterial(Material material)
        {

        }

        public Image GetCardIcon()
        {
            return cardIcon;
        }
        public Text GetCardNameTextComponent()
        {
            return cardNameTextComponent;
        }
        public Image GetCardImageTexture()
        {
            return cardImageTexture;
        }
        #endregion
    }
}