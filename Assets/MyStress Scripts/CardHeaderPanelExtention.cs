﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Thrive;
using UnityEngine.UI;

/// <summary>
/// A component that adds MyStress behaviour to header panel (added by Jacob)
/// </summary>
public class CardHeaderPanelExtention : MonoBehaviour {
	[SerializeField]
	private Button headerLeftButton;
    [SerializeField]
    private Button headerRightButton;
	[SerializeField]
	private GameObject AboutMyStressPrefab;
    [SerializeField]
    private GameObject SettingsScreenPrefab;
	[SerializeField]
	private Sprite leftActive;
	[SerializeField]
	private Sprite leftInactive;
    [SerializeField]
    private Sprite rightActive;
    [SerializeField]
    private Sprite rightInactive;
    [SerializeField]
    private Image leftButtonImage;
    [SerializeField]
    private Image rightButtonImage;

    public Text debugText;

    AboutMyStressScreen instancedAboutScreen;
    SettingsScreen instancedSettingsScreen;

    /// <summary>
    /// Initilization
    /// </summary>
	void Start()
	{
		headerLeftButton.onClick.AddListener(OnGoToAboutMyStress);
	}


    /// <summary>
    /// For debugging purposes
    /// </summary>
    #if UNITY_EDITOR
    void Update()
    {
        debugText.text = ViewIScreenController.GetCurrentScreen().screen.ToString();
    }
    #endif

    /// <summary>
    /// Go to options screen callback function, for right corner button clicks on, this hasa beenn set thru Unity Editor Inspector
    /// </summary>
    public void OnGoToOptions()
    {
        ToggleOptions();
    }

    /// <summary>
    /// Go to about mystress screen callback function, for left corner button clicks on
    /// </summary>
    public void OnGoToAboutMyStress()
    {
        ToggleAbout();
    }

    /// <summary>
    /// Go to about mystress screen callback function, for left corner button clicks on
    /// </summary>
    void ToggleAbout(){
        if (instancedAboutScreen == null)
        {
            instancedAboutScreen = ((GameObject)Instantiate(AboutMyStressPrefab, Vector3.zero, Quaternion.identity)).GetComponent<AboutMyStressScreen>();
            //instancedScreen = go.GetComponent(typeof(IScreen)) as IScreen;
        }
        if (!instancedAboutScreen.GetAboutScreenEntered())
        {
            Debug.Log("Entering about screen");
            instancedAboutScreen.ShowThisScreen();
            leftButtonImage.sprite = leftActive;
        }
        else
        {
            Debug.Log("Exitting about screen");
            instancedAboutScreen.EnableScreen();
            instancedAboutScreen.ShowPreviousScreen();
            leftButtonImage.sprite = leftInactive;
        }
        Debug.Log("Current screen is: " + ViewIScreenController.GetCurrentScreen().screen);
    }

    /// <summary>
    /// Go to options screen callback function, for right corner button clicks on, this hasa beenn set thru Unity Editor Inspector
    /// </summary>
    void ToggleOptions()
    {
        if (instancedSettingsScreen == null)
        {
            instancedSettingsScreen = ((GameObject)Instantiate(SettingsScreenPrefab, Vector3.zero, Quaternion.identity)).GetComponent<SettingsScreen>();
            //instancedScreen = go.GetComponent(typeof(IScreen)) as IScreen;
        }
        if (!instancedSettingsScreen.GetSettingsScreenEntered())
        {
            Debug.Log("Entering options screen");
            instancedSettingsScreen.ShowThisScreen();
            rightButtonImage.sprite = rightActive;
          //  instancedSettingsScreen.SetSettingsScreenEntered(true);
        }
        else
        {
            Debug.Log("Exitting options screen");
            //instancedSettingsScreen.SetSettingsScreenEntered(false);
            instancedSettingsScreen.EnableScreen();
            instancedSettingsScreen.ShowPreviousScreen();
            rightButtonImage.sprite = rightInactive;
        }

        Debug.Log("Current screen is: " + ViewIScreenController.GetCurrentScreen().screen);
    }

    public void FadeOutLeft()
    {
        if (!ViewIScreenController.IsTransitionRunning())
        {
            StartCoroutine(FadeOutButton(headerLeftButton));
        }
    }
    public void FadeInLeft()
    {
        if (!ViewIScreenController.IsTransitionRunning())
        {
            StartCoroutine(FadeInButton(headerLeftButton));
        }
    }
    public void FadeOutRight()
    {
        if (!ViewIScreenController.IsTransitionRunning())
        {
            StartCoroutine(FadeOutButton(headerRightButton));
        }
    }
    public void FadeInRight()
    {
        if (!ViewIScreenController.IsTransitionRunning())
        {
            StartCoroutine(FadeInButton(headerRightButton));
        }
    }

    /// <summary>
    /// Fade out button coroutine, for animationn
    /// </summary>
    /// <param name="button">Button to perform fade on</param>
    /// <returns>IEnumerator</returns>
    IEnumerator FadeOutButton(Button button)
    {
        float counter = 1;
        Image[] childImage = new Image[0];
        if (button.transform.childCount > 0)
        {
            childImage = button.transform.GetChild(0).GetComponentsInChildren<Image>();
        }
        

        button.interactable = false;
        while(counter > 0){
            if (childImage.Length > 0)
            {
                for (int i = 0; i < childImage.Length; i++)
                {
                    childImage[i].color = new Color(childImage[i].color.r, childImage[i].color.g, childImage[i].color.b, counter);
                }
            }
            else
            {
                button.image.color = new Color(button.image.color.r, button.image.color.g, button.image.color.b, counter);
            }
            counter -= Time.deltaTime;
            yield return null;
        }
        counter = 0;
        if (childImage.Length > 0)
        {
            for (int i = 0; i < childImage.Length; i++)
            {
                childImage[i].color = new Color(childImage[i].color.r, childImage[i].color.g, childImage[i].color.b, counter);
            }
        }
        else
        {
            button.image.color = new Color(button.image.color.r, button.image.color.g, button.image.color.b, counter);
        }
    }

    /// <summary>
    /// Fade in button coroutine, for animationn
    /// </summary>
    /// <param name="button">Button to perform fade on</param>
    /// <returns>IEnumerator</returns>
    IEnumerator FadeInButton(Button button)
    {
        float counter = 0;
        Image[] childImage = new Image[0];
        if (button.transform.childCount > 0)
        {
            childImage = button.transform.GetChild(0).GetComponentsInChildren<Image>();
        }
        while (counter < 1)
        {
            if (childImage.Length > 0)
            {
                for (int i = 0; i < childImage.Length; i++)
                {
                    childImage[i].color = new Color(childImage[i].color.r, childImage[i].color.g, childImage[i].color.b, counter);
                }
            }
            else
            {
                button.image.color = new Color(button.image.color.r, button.image.color.g, button.image.color.b, counter);
            }
            counter += Time.deltaTime;
            yield return null;
        }
        counter = 1;
        if (childImage.Length > 0)
        {
            for (int i = 0; i < childImage.Length; i++)
            {
                childImage[i].color = new Color(childImage[i].color.r, childImage[i].color.g, childImage[i].color.b, counter);
            }
        }
        else
        {
            button.image.color = new Color(button.image.color.r, button.image.color.g, button.image.color.b, counter);
        }
        button.interactable = true;
    }

}
