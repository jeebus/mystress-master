﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using SmartLocalization;

namespace Thrive
{
    /// <summary>
    /// Card component that is displayed when a particular date is inspected from the calendar side
    /// </summary>
	public class MyStressCalendarDateInfo : MonoBehaviour, ICard
	{
		[HideInInspector]
		public Image cardIcon;
		[HideInInspector]
		public Text cardNameTextComponent;

		public Text symptomsTextComponent;
		public Text triggersTextComponent;
		public Text copingEffectTextComponent;

		public Text dateTextComponent;

		public Image overloadSmileyForeground;
		public Image effectSmileyForeground;
		public Image effectSmileyBackground;
		public GameObject attributeTemplate;

        public Button backButton;
        public CardSubPage calenderPage;

		private List<GameObject> attributes = new List<GameObject>();

        /// <summary>
        /// Initialization
        /// </summary>
		void Awake()
		{
            backButton.onClick.AddListener(() => { OnClickBackButton(); });
		}
        
        /// <summary>
        /// Method for creating a new attribute that is used to display stress data
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
		Transform CreateAttribute(string text)
		{
			GameObject go = GameObject.Instantiate(attributeTemplate) as GameObject;
			
			Text t = go.GetComponent<Text>();
			t.text = "- " + text;
			go.transform.SetParent(symptomsTextComponent.transform.parent, false);
			go.transform.localScale = Vector3.one;
			attributes.Add(go);
			go.SetActive(true);
			return go.transform;
		}


        /// <summary>
        /// Method for setting current date of the current MyStressCalendarDateInfo from a System.DateTime object
        /// </summary>
        /// <param name="newDate">System.DateTime object to set date from</param>
		void SetMyStressDate(System.DateTime newDate){
            //its sunday!
            if ((int)newDate.DayOfWeek == 0)
            {
                dateTextComponent.text = LanguageManager.Instance.GetTextValue("Generic.Day." + 7 + ".Short").ToUpper();
            }
            else
            {
                dateTextComponent.text = LanguageManager.Instance.GetTextValue("Generic.Day." + (int)newDate.DayOfWeek + ".Short").ToUpper(); // Sat
            }
            dateTextComponent.text += ". " + newDate.Day + ". " + LanguageManager.Instance.GetTextValue("Generic.Month." + newDate.Month).ToUpper(); // . 7. MARCH
            dateTextComponent.text += " " + newDate.Year.ToString();
		}

        /// <summary>
        /// On back button click, used to return to calendar side
        /// </summary>
        public void OnClickBackButton()
        {
			Debug.Log("GOING BACK");
            StartCoroutine(ViewIScreenController.ShowOtherCardPage(this, calenderPage, TransitionType.Fade, false));

			for(int i = 0; i < attributes.Count; i++)
			{
				Destroy(attributes[i]);
			}
			attributes.Clear();
        }

        /// <summary>
        /// Method for updating the displayed information on the MyStressCalendarDateInfo instance  from a MyStressData object
        /// </summary>
        /// <param name="data"></param>
        public void UpdateDateInfo(MyStressData data)
        {
			MyStressSmileyAttribute smileyAttributes = GetComponentInParent<MyStressCard>().GetSmilieAtrributes(data.overload, data.effect);

			overloadSmileyForeground.sprite = smileyAttributes.smileySprite;
			effectSmileyForeground.sprite = smileyAttributes.smileySprite;
			effectSmileyBackground.color = smileyAttributes.smileyColor;
			Debug.Log("Updated date " + data.regDate);
			SetMyStressDate (data.regDate);

            if (data.symptoms != null)
            {
				int siblingCounter = 0;
                //symptomsTextComponent.text = "<b>Symptomer</b> \n";
                for (int i = 0; i < data.symptoms.Count; i++)
                {
                    if (data.symptoms[i].selected)
                    {
                        if (data.symptoms[i].componentString.StartsWith("Symptoms."))
                        {
                            CreateAttribute(SmartLocalization.LanguageManager.Instance.GetTextValue(data.symptoms[i].componentString)).SetSiblingIndex(symptomsTextComponent.transform.GetSiblingIndex() + ++siblingCounter);
                        }
                        else
                        {
                            CreateAttribute(data.symptoms[i].componentString).SetSiblingIndex(symptomsTextComponent.transform.GetSiblingIndex() + ++siblingCounter);
                        }
                    }
                    
                }
                if (siblingCounter == 0)
                {
					string tempString = SmartLocalization.LanguageManager.Instance.GetTextValue("MyStressCard1.NoRegistrationForDatePart1") +" " + SmartLocalization.LanguageManager.Instance.GetTextValue("MyStressCard1.Side1.Symptoms").ToLower() +" " + SmartLocalization.LanguageManager.Instance.GetTextValue("MyStressCard1.NoRegistrationForDatePart2");

					CreateAttribute("- " + tempString).SetSiblingIndex(symptomsTextComponent.transform.GetSiblingIndex() + ++siblingCounter);
                }
				siblingCounter = 0;
                //symptomsTextComponent.text += "\n<b>Triggers</b> \n";
                for (int i = 0; i < data.triggers.Count; i++)
                {
                    if (data.triggers[i].selected)
                    {
                        if (data.triggers[i].componentString.StartsWith("Triggers."))
                        {
                            CreateAttribute(SmartLocalization.LanguageManager.Instance.GetTextValue(data.triggers[i].componentString)).SetSiblingIndex(triggersTextComponent.transform.GetSiblingIndex() + ++siblingCounter);
                        }
                        else
                        {
                            CreateAttribute(data.triggers[i].componentString).SetSiblingIndex(triggersTextComponent.transform.GetSiblingIndex() + ++siblingCounter);
                        }
                        
                    }
                }
                if (siblingCounter == 0)
                {
					string tempString = SmartLocalization.LanguageManager.Instance.GetTextValue("MyStressCard1.NoRegistrationForDatePart1") +" " + SmartLocalization.LanguageManager.Instance.GetTextValue("MyStressCard1.Side2.Triggers").ToLower() +" " + SmartLocalization.LanguageManager.Instance.GetTextValue("MyStressCard1.NoRegistrationForDatePart2");

					CreateAttribute("- " + tempString).SetSiblingIndex(triggersTextComponent.transform.GetSiblingIndex() + ++siblingCounter);
                }
				siblingCounter = 0;
                //symptomsTextComponent.text += "\n<b>Coping</b> \n";
                for (int i = 0; i < data.coping.Count; i++)
                {


                    if (data.coping[i].selected)
                    {
						CreateAttribute(SmartLocalization.LanguageManager.Instance.GetTextValue(data.coping[i].componentString)).SetSiblingIndex(copingEffectTextComponent.transform.GetSiblingIndex() + ++siblingCounter);
                        //Debug.Log("Add a component string!");
                        //symptomsTextComponent.text += data.coping[i].componentString + "\n";
                    }
                    
                }
                if (siblingCounter == 0)
                {
					string tempString = SmartLocalization.LanguageManager.Instance.GetTextValue("MyStressCard1.NoRegistrationForDatePart1") +" " + SmartLocalization.LanguageManager.Instance.GetTextValue("MyStressCard1.Side3.Coping").ToLower() +" " + SmartLocalization.LanguageManager.Instance.GetTextValue("MyStressCard1.NoRegistrationForDatePart2");

					CreateAttribute("- " + tempString).SetSiblingIndex(copingEffectTextComponent.transform.GetSiblingIndex() + ++siblingCounter);
                }
            }
            else
            {
                //symptomsTextComponent.text = "NULL";
            }

			attributeTemplate.SetActive(false);
        }


        /// <summary>
        /// ICard interface implemented methods
        /// </summary>
        #region ICard interface implemented methods
        public const string cardUID = "MyStressDateCard";
		
		public string GetCardUID()
		{
			return cardUID;
		}
		
		public int GetCardOwnerID()
		{
			return 0;
			
		}
		
		public void SetCardOwnerID(int newCardOwner)
		{
			// cardOwnerID = newCardOwner;
		}
		
		public GameObject GetGameObject()
		{
			return gameObject;
		}
		
		public void MakeCardContentVisible()
		{
			gameObject.SetActive(true);
		}
		
		public void MakeCardContentInvisible()
		{
			gameObject.SetActive(false);
			// listenForAlertChanges = false;
		}
		
		public void ShowFrontSide()
		{
			// listenForAlertChanges = true;
			//StartCoroutine(UpdateAlertArea());
		}
		public void ShowBackSide()
		{
			
		}
		public bool GetRotateCardToReadMore()
		{
			return true;
		}
		public string GetCardName()
		{
			return "";
		}
		public void SetRotateCardToReadMore(bool newReadMoreValue)
		{
			// rotateCardToReadMore = newReadMoreValue;
		}
		public void SetCardMaterial(Material material)
		{
			
		}
		public void RotateCardToReadMore()
		{
			
			
		}
		
		public void RotateCardBackToOriginal()
		{
			
		}
		
		public Image GetCardIcon()
		{
			return cardIcon;
		}
		public Text GetCardNameTextComponent()
		{
			return cardNameTextComponent;
		}
        public Image GetCardImageTexture()
        {
            return null;
        }
		#endregion
	}
}
