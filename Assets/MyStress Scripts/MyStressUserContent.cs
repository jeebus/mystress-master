﻿//#define Delete_Stuff_On_Start

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
//using SimpleJSON;



namespace Thrive
{

    /// <summary>
    /// Class for storing and loading stats from local disk
    /// </summary>
    public static class MyStressUserContent
    {
        public static List< MyStressData> sessionStats = new List<MyStressData>();
        public static List<SelectableStressComponentData> sessionSymptomStrings = new List<SelectableStressComponentData>();
        public static List<SelectableStressComponentData> sessionTriggerStrings = new List<SelectableStressComponentData>();
        public static List<SelectableStressComponentData> sessionCopingStrings = new List<SelectableStressComponentData>();
		public static Settings settings = new Settings();


		static string userName = "user";
        static string fileLocation = Application.persistentDataPath + "/" + userName + "/";
        static string storedStatsFileName = "storedContent.dat";
        static string storedSymptomsFileName = "storedSymptomStrings.dat";
        static string storedTriggersFileName = "storedTriggerStrings.dat";
        static string storedCopingFileName = "storedCopingStrings.dat";
		static string settingsFileName = "settings.dat";

        static public readonly bool useTestData = false;

		public static int fakeDaysAdd = 0;

        /// <summary>
        /// Get fake days for debugging purposes
        /// </summary>
        /// <returns>Amount of fake days as integer</returns>
		public static int GetFakeDays()
		{
			return fakeDaysAdd;
		}

        /// <summary>
        /// Add one to fake days for debugging purposes
        /// </summary>
		public static void AddToFakeDays(){
			fakeDaysAdd++;
			Debug.Log ("Added to fakedays, now is: " + fakeDaysAdd);
		}
			
	    /// <summary>
        /// Load some dummy stats for debugging purposes
        /// </summary>
        /// <returns>Returns true when done</returns>
        public static bool LoadDummySessionStats()
        {
            List<MyStressData> alreadyStoredStats = new List<MyStressData>();
            System.DateTime now = System.DateTime.UtcNow.ToLocalTime();

            int amountOfDaysToCreateDummyDataFor = 30;

            for (int i = 0; i < amountOfDaysToCreateDummyDataFor; i++)
            {
                MyStressData newData = new MyStressData();
                newData.overload = (OverloadLevel)(i % 3);

                List<SelectableStressComponentData> dummySymptomStrings = new List<SelectableStressComponentData>();

                for (int j  = 1; j < 16; j++)
                {
                    dummySymptomStrings.Add(new SelectableStressComponentData("Symptoms." + j, UnityEngine.Random.Range(0, 2) == 0 ? true : false));
                }
                List<SelectableStressComponentData> dummyTriggerStrings = new List<SelectableStressComponentData>();
                for (int k = 1; k < 17; k++)
                {
                    dummyTriggerStrings.Add(new SelectableStressComponentData("Triggers." + k, UnityEngine.Random.Range(0, 2) == 0 ? true : false));
                }

                List<SelectableStressComponentData> dummyCopingStrings= new List<SelectableStressComponentData>();
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 1,  UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)1));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 2, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)2));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 3, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)2));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 4, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)2));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 5, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)4));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 6, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)1));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 7, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)2));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 8, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)4));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 9, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)3));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 10, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)4));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 11, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)3));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 12, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)3));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 13, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)5));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 14, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)1));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 15, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)4));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 16, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)1));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 17, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)3));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 18, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)5));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 19, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)5));
                dummyCopingStrings.Add(new SelectableStressComponentData("Coping." + 20, UnityEngine.Random.Range(0, 2) == 0 ? true : false, (CopingCategory)5));


                newData.symptoms = dummySymptomStrings;
                newData.triggers = dummyTriggerStrings;
                newData.coping = dummyCopingStrings;
                newData.effect = (EffectLevel)(i % 3);
                newData.regDate = now.AddDays(-i-2);
                alreadyStoredStats.Add(newData);
            }


            sessionStats = alreadyStoredStats;
            return true;
        }

        /// <summary>
        /// Save current app settings locally to disk
        /// </summary>
		public static void SaveCurrentSettings()
		{
			string filePath = fileLocation + settingsFileName;
			BinaryFormatter bf = new BinaryFormatter();
            Debug.Log("Saving to path: " + filePath);
			if (File.Exists(filePath))
			{
				Debug.Log("Settings file exists, overwriting");
				FileStream createFileStream = File.Create(filePath);
				bf.Serialize(createFileStream, MyStressUserContent.settings);
				createFileStream.Close();
			}
			else
			{
				Debug.Log("Settings file does not exist, create new");
				if (!Directory.Exists(fileLocation))
				{
					Directory.CreateDirectory(fileLocation);
				}
				FileStream fileStream = File.Create(filePath);
				bf.Serialize(fileStream, MyStressUserContent.settings);
				fileStream.Close();
			}
		}

        /// <summary>
        /// Delete all stored data for debugging purposes
        /// </summary>
		public static void DeleteAllData()
		{
			Debug.Log("Deleting all data");
			File.Delete(fileLocation + settingsFileName);
			File.Delete(fileLocation + storedCopingFileName);
			File.Delete(fileLocation + storedStatsFileName);
			File.Delete(fileLocation + storedSymptomsFileName);
			File.Delete(fileLocation + storedTriggersFileName);

			ClearStrings();
			settings = new Settings();

		}

        /// <summary>
        /// Clear current loaded session stats
        /// </summary>
		public static void ClearStrings()
		{
			sessionStats = new List<MyStressData>();
			sessionSymptomStrings = new List<SelectableStressComponentData>();
			sessionTriggerStrings = new List<SelectableStressComponentData>();
			sessionCopingStrings = new List<SelectableStressComponentData>();
		}
		
        /// <summary>
        /// Load stored settings
        /// </summary>
        /// <returns></returns>
		public static bool LoadStoredSettings()
		{
			string filePath = fileLocation + settingsFileName;
            Debug.Log("Loading from path: " + filePath);

			if (File.Exists(filePath))
			{
#if UNITY_EDITOR && Delete_Stuff_On_Start //For debugging purposes
				Debug.LogWarning("DELETING STUFF ON START");
				File.Delete(fileLocation + settingsFileName);
				File.Delete(fileLocation + storedCopingFileName);
				File.Delete(fileLocation + storedStatsFileName);
				File.Delete(fileLocation + storedSymptomsFileName);
				File.Delete(fileLocation + storedTriggersFileName);
				return false;
#else
				Debug.Log("Settings have been loaded");
				BinaryFormatter bf = new BinaryFormatter();
				FileStream fileStream = File.Open(filePath, FileMode.Open);
				Settings alreadyStoredSettings = (Settings)bf.Deserialize(fileStream);
				fileStream.Close();
				settings = alreadyStoredSettings;
				return true;
#endif
			}
			else
			{
				Debug.Log("Theres no stored settings to load");
				return false;
			}
		}

        /// <summary>
        /// Save current sessions stats to local disk
        /// </summary>
        public static void SaveCurrentSessionStats()
        {
            string filePath = fileLocation + storedStatsFileName;
            BinaryFormatter bf = new BinaryFormatter();
            //Debug.Log("### Attempting to save children content ###");

            sessionStats[sessionStats.Count - 1].symptoms = sessionSymptomStrings; // Store symptomStrings
            sessionStats[sessionStats.Count - 1].triggers = sessionTriggerStrings;
            sessionStats[sessionStats.Count - 1].coping = sessionCopingStrings;
            
            if (File.Exists(filePath))
            {
                //Debug.Log("StoredContent file exists, overwriting");
                FileStream createFileStream = File.Create(filePath);
                bf.Serialize(createFileStream, MyStressUserContent.sessionStats);
                createFileStream.Close();
            }
            else
            {
                //Debug.Log("StoredContent file does not exist, create new");
                if (!Directory.Exists(fileLocation))
                {
                    Directory.CreateDirectory(fileLocation);
                }
                FileStream fileStream = File.Create(filePath);
                bf.Serialize(fileStream, MyStressUserContent.sessionStats);
                fileStream.Close();
            }

        }

        /// <summary>
        /// Load stored stats from disk
        /// </summary>
        /// <returns>Returns true if there was something to load</returns>
        public static bool LoadStoredStats()
        {
            string filePath = fileLocation + storedStatsFileName;
            if (File.Exists(filePath))
            {
				Debug.Log("Stats have been loaded");
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fileStream = File.Open(filePath, FileMode.Open);
                List<MyStressData> alreadyStoredStats = (List<MyStressData>)bf.Deserialize(fileStream);
                fileStream.Close();
                sessionStats = alreadyStoredStats;
                return true;
            }
            else
            {
                Debug.Log("Theres no stored content to load");
                return false;
            }
        }



        /// <summary>
        /// Loading stored stress component strings
        /// </summary>
        /// <returns>Returns true if there was something to load</returns>
        public static bool LoadStoredSymptomStrings()
        {
            // All strings loaded have selected = false
            string filePath = fileLocation + storedSymptomsFileName;
            if (File.Exists(filePath))
            {
                Debug.Log("Symptom strings have been loaded");
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fileStream = File.Open(filePath, FileMode.Open);
                sessionSymptomStrings = (List<SelectableStressComponentData>)bf.Deserialize(fileStream);
                fileStream.Close();
                return true;
            }
            else
            {
                Debug.Log("Theres no stored symptoms strings to load");
                return false;
            }
        }

        /// <summary>
        /// Loading stored trigger component strings
        /// </summary>
        /// <returns>Returns true if there was something to load</returns>
        public static bool LoadStoredTriggerStrings()
        {
            string filePath = fileLocation + storedTriggersFileName;
            if (File.Exists(filePath))
            {
                Debug.Log("Trigger strings have been loaded");
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fileStream = File.Open(filePath, FileMode.Open);
                sessionTriggerStrings = (List<SelectableStressComponentData>)bf.Deserialize(fileStream);
                fileStream.Close();
                return true;
            }
            else
            {
                Debug.Log("Theres no stored triggers strings to load");
                return false;
            }
        }

        /// <summary>
        /// Loading stored coping component strings
        /// </summary>
        /// <returns>Returns true if there was something to load</returns>
        public static bool LoadStoredCopingStrings()
        {
            string filePath = fileLocation + storedCopingFileName;
            if (File.Exists(filePath))
            {
                Debug.Log("Coping strings have been loaded");
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fileStream = File.Open(filePath, FileMode.Open);
                sessionCopingStrings = (List<SelectableStressComponentData>)bf.Deserialize(fileStream);
                fileStream.Close();
                return true;
            }
            else
            {
                Debug.Log("Theres no stored coping strings to load");
                return false;
            }
        }


        /// <summary>
        /// Save symptoms strings to disk
        /// </summary>
        public static void SaveSymptomStrings()
        {
            string filePath = fileLocation + storedSymptomsFileName;
            BinaryFormatter bf = new BinaryFormatter();
            Debug.Log("Attempting to save symptoms strings");

            List<SelectableStressComponentData> stringsToSave = GenericCopier<List<SelectableStressComponentData>>.DeepCopy(sessionSymptomStrings); // Copy 

            for (int i = 0; i < stringsToSave.Count; i++)
            {
                stringsToSave[i].selected = false;
            }

            if (File.Exists(filePath))
            {
                Debug.Log("Symptoms strings file exists, overwriting");
                FileStream createFileStream = File.Create(filePath);
                bf.Serialize(createFileStream, stringsToSave);
                //Debug.Log("COunt: " + sessionSymptomStrings[0].selected);
                createFileStream.Close();
            }
            else
            {
                Debug.Log("Symptoms strings file does not exist, create new");
                if (!Directory.Exists(fileLocation))
                {
                    Directory.CreateDirectory(fileLocation);
                }
                FileStream fileStream = File.Create(filePath);
                bf.Serialize(fileStream, stringsToSave);
                fileStream.Close();
            }
        }

        /// <summary>
        /// Save trigger strings to disk
        /// </summary>
        public static void SaveTriggerStrings()
        {
            string filePath = fileLocation + storedTriggersFileName;
            BinaryFormatter bf = new BinaryFormatter();
            Debug.Log("Attempting to save Trigger strings");

            List<SelectableStressComponentData> stringsToSave = GenericCopier<List<SelectableStressComponentData>>.DeepCopy(sessionTriggerStrings); // Copy 
            for (int i = 0; i < stringsToSave.Count; i++)
            {
                stringsToSave[i].selected = false;
            }

            if (File.Exists(filePath))
            {
                Debug.Log("Trigger strings file exists, overwriting");
                FileStream createFileStream = File.Create(filePath);
                bf.Serialize(createFileStream, stringsToSave);
                createFileStream.Close();
            }
            else
            {
                Debug.Log("Trigger strings file does not exist, create new");
                if (!Directory.Exists(fileLocation))
                {
                    Directory.CreateDirectory(fileLocation);
                }
                FileStream fileStream = File.Create(filePath);
                bf.Serialize(fileStream, stringsToSave);
                fileStream.Close();
            }

        }


        /// <summary>
        /// Save coping strings to disk
        /// </summary>
        public static void SaveCopingStrings()
        {
            string filePath = fileLocation + storedCopingFileName;
            BinaryFormatter bf = new BinaryFormatter();
            Debug.Log("Attempting to save coping strings");

            List<SelectableStressComponentData> stringsToSave = GenericCopier<List<SelectableStressComponentData>>.DeepCopy(sessionCopingStrings); // Copy 
            for (int i = 0; i < stringsToSave.Count; i++)
            {
                stringsToSave[i].selected = false;
            }

            if (File.Exists(filePath))
            {
                
                Debug.Log("Coping strings file exists, overwriting");
                FileStream createFileStream = File.Create(filePath);
                bf.Serialize(createFileStream, stringsToSave);
                createFileStream.Close();
            }
            else
            {
                Debug.Log("Coping strings file does not exist, create new");
                if (!Directory.Exists(fileLocation))
                {
                    Directory.CreateDirectory(fileLocation);
                }
                FileStream fileStream = File.Create(filePath);
                bf.Serialize(fileStream, stringsToSave);
                fileStream.Close();
            }
        }

        /// <summary>
        /// Reset notification, that alerts user when effect is avaible to register
        /// </summary>
        public static void ResetNotification()
        {
            float secondsToPush =  MyStressUserContent.settings.notificationsTimer * 60 * 60; // notifcationTimer stored in hours
            //secondsToPush = 60; // DEBUG
#if UNITY_EDITOR
			secondsToPush = secondsToPush* 2 / 2;
            
#elif UNITY_ANDROID 

               // AndroidJavaObject ajc = new AndroidJavaObject("com.zeljkosassets.notifications.Notifier");
              //  ajc.CallStatic("sendNotification", "MyStress", "Husk effekt", "Husk at registrere din effekt", secondsToPush);
              //  LocalNotification.CancelNotification(1);
                LocalNotificationAndroid.SendNotification(1, (long)secondsToPush, "MyStress", "Husk at registrere din effekt", new Color32(0xff, 0x44, 0x44, 255),true,true,true,"");
#elif UNITY_IOS  
                NotificationServices.CancelAllLocalNotifications();
				NotificationServices.RegisterForLocalNotificationTypes(LocalNotificationType.Alert);
				LocalNotification notification = new LocalNotification();
				notification.alertBody = "Husk at registere din effekt";
				notification.fireDate = DateTime.Now.AddSeconds(secondsToPush);
				//Debug.Log("Fire notification! " + DateTime.Now.ToString("HH:mm:ss") + " at " + notification.fireDate.ToString("HH:mm:ss"));
				
				NotificationServices.ScheduleLocalNotification(notification);
#endif
        }

    }

    /// <summary>
    /// Used for copying session lists
    /// </summary>
    /// <typeparam name="T">Generic type to perform deep copy on</typeparam>
    public static class GenericCopier<T>
    {
        public static T DeepCopy(object objectToCopy)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, objectToCopy);
                memoryStream.Seek(0, SeekOrigin.Begin);
                return (T)binaryFormatter.Deserialize(memoryStream);
            }
        }
    }
}

