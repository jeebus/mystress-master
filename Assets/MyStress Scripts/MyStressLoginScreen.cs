﻿//#define ClearLoginInfoOnStart

using UnityEngine;
using System.Collections;
using Thrive;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// MyStress login screen, currently also handles actual login behaviour 
/// </summary>
public class MyStressLoginScreen : MonoBehaviour, IScreen
{

    MainUI mainUI;
    [SerializeField]
    private GameObject simpleTextScrollViewPrefab;
    private SimpleTextScrollScreen instatiatedScrollScreen;

    public InputField usernameInputField;
    public InputField passwordInputField;
    public Button HowToButton;
    public Button privacyButton;
	public Button freeUserButton;

    public Button loginButton;
    public Image loginButtonIcon;
    public Text loginButtonText;

    private string loginURL = "http://mystress.dk/MyStress/RegisterMSUser.php"; //should be HTTPS, if Joan pays for it
    private bool isLoggingIn = false;

    Image inputfieldBackgroundUsername;
    Image inputfieldBackgroundPassword;
    public Image fakeSplashScreenImage;
    [SerializeField]
    private Text headerTitleText;

    /// <summary>
    /// Pre-initilization
    /// </summary>
    void Awake()
    {
		MyStressUserContent.LoadStoredSettings();
        string language;
        if (MyStressUserContent.settings.language != null)
        {
            language = MyStressUserContent.settings.language;
        }
        else
        {
            language = "da";
        }

        SmartLocalization.LanguageManager.Instance.ChangeLanguage(language);

#if UNITY_ANDROID
        usernameInputField.shouldHideMobileInput = false;
        passwordInputField.shouldHideMobileInput = false;
#endif
#if UNITY_IPHONE
		usernameInputField.shouldHideMobileInput = true;
		passwordInputField.shouldHideMobileInput = true;
#endif
    }


    /// <summary>
    /// Initilization
    /// </summary>
    void Start()
    {
		#if ClearLoginInfoOnStart && UNITY_EDITOR
		Debug.LogWarning("Resetting login info");
		PlayerPrefs.DeleteAll();
#endif

        mainUI = GameObject.FindObjectOfType<MainUI>();
        if (mainUI.GetHeaderPanel() != null)
        {
            mainUI.GetHeaderPanel().TransitionHeaderTitle(GetScreenHeaderTitle());
        }
#if UNITY_IPHONE
		NotificationServices.RegisterForLocalNotificationTypes(LocalNotificationType.Alert);
		NotificationServices.RegisterForLocalNotificationTypes(LocalNotificationType.Badge);
		NotificationServices.RegisterForLocalNotificationTypes(LocalNotificationType.Sound);
#endif
        inputfieldBackgroundUsername = usernameInputField.GetComponent<Image>();
        inputfieldBackgroundPassword = passwordInputField.GetComponent<Image>();


        ((RectTransform)transform).sizeDelta = Vector2.zero;
        ((RectTransform)transform).anchorMin = new Vector2(0, 0);
        ((RectTransform)transform).anchorMax = new Vector2(1, 1);

        CheckPlayerPrefsForExisitingData();

        SetUpEventListeners();
        loginButtonIcon.enabled = false;
        if (mainUI != null)
        {
            loginButton.image.color = CustomUtilities.HexToColor(mainUI.greenValidButtonColor);
        }
        if (usernameInputField.text.Length > 0 && passwordInputField.text.Length > 0)
        {
            Login();
        }
        else
        {
            fakeSplashScreenImage.enabled = false;
        }

        ViewIScreenController.SetCurrentScreen(new VisitedScreens(this, TransitionType.RightToLeft));
    }

    /// <summary>
    /// Setup button event listeners
    /// </summary>
    void SetUpEventListeners()
    {
        HowToButton.onClick.AddListener(() => { OnClickAccountHowTo(); });
        privacyButton.onClick.AddListener(() => { OnClickPrivacyStatement(); });
        loginButton.onClick.AddListener(() => { OnNextButtonClick(); });
		freeUserButton.onClick.AddListener(OnFreeUserClick);
    }

    /// <summary>
    /// Function for logging in, after user has inputted username and password
    /// </summary>
    void Login()
    {
        loginButton.image.color = CustomUtilities.HexToColor(mainUI.yellowPendingButtonColor);
        loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.AttemptingLogin");
        loginButtonIcon.enabled = true;

        StartCoroutine(AttemptToContactServerForLoginFromLoginScreen());
    }

    /// <summary>
    /// Coroutine for attempting to contact server, and perform according login behaviour
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator AttemptToContactServerForLoginFromLoginScreen()
	{
		//yield return null;
		if(isLoggingIn)
			yield break;

		isLoggingIn = true;
		WWWForm form = new WWWForm();
		form.AddField("email", usernameInputField.text.Trim());
		form.AddField("password", WWW.EscapeURL(passwordInputField.text.Trim()));

		WWW www = new WWW(loginURL, form);
		yield return www;

		if(string.IsNullOrEmpty(www.error))
		{
			JSONObject json = JSONObject.Create(www.text);
			if(json && json.type != JSONObject.Type.NULL)
			{
				int responseCode = (int)json.GetField("code").f;
				string response = json.GetField("text").str;

				Debug.Log("Response: " + response + " code: " + responseCode);

				if(responseCode < 0) // Some error occured
				{
					loginButtonIcon.sprite = SpriteResources.inputFieldSelectedInvalidSpriteBlack;
					loginButton.image.color = CustomUtilities.HexToColor(mainUI.redInvalidButtonColor);
					loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.AccessDenied");
					
					loginButton.interactable = true;
					fakeSplashScreenImage.enabled = false;
					PlayerPrefs.SetInt("access", 0);
				}
                    
				else // Login succesfull
				{
					PlayerPrefs.SetInt("access", 1);
					loginButtonIcon.enabled = false;
					
					mainUI.OnLogin();

					loginButton.image.color = CustomUtilities.HexToColor(mainUI.greenValidButtonColor);
					loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.LoginSucceeded");
                    mainUI.GetHeaderPanel().TransitionHeaderTitle("");
                    StoreInPlayerPrefs(); // Only store them on successful login as they are used for checking offline access
				}
                     
			}
		}
		else // An error occured
		{
            if (PlayerPrefs.GetInt("access") == 1) // bypass login for offline access
            {
                Debug.Log("STARTING IN OFFLINE MODE!");
                loginButtonIcon.enabled = false;
					
				mainUI.OnLogin();
						
				loginButton.image.color = CustomUtilities.HexToColor(mainUI.greenValidButtonColor);
				loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.LoginSucceeded");
                mainUI.GetHeaderPanel().TransitionHeaderTitle("");
                //StoreInPlayerPrefs();
            }
            else
            {
			    loginButtonIcon.sprite = SpriteResources.inputFieldSelectedInvalidSpriteBlack;
			    loginButton.image.color = CustomUtilities.HexToColor(mainUI.redInvalidButtonColor);
			
			    loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.NotResponding");
			
			
			    loginButton.interactable = true;
			    fakeSplashScreenImage.enabled = false;
            }
			
		}
		loginButtonIcon.enabled = false;
        isLoggingIn = false;
	}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    void OnInputValueChanged(string value)
    {
        if (!isLoggingIn)
        {
            loginButton.image.color = CustomUtilities.HexToColor(mainUI.greenValidButtonColor);
            loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.LoginButton");
            loginButtonIcon.enabled = false;
        }
    }

    /// <summary>
    /// Check  player prefs for any existing login information from previous sessions
    /// </summary>
    void CheckPlayerPrefsForExisitingData()
    {
        //Check if username and password has already been stored from a previous login
        if (PlayerPrefs.GetString("username").Length > 0 && PlayerPrefs.GetString("username") != null && PlayerPrefs.GetString("username") != "")
        {
            //Debug.Log("Username exists in player prefs.");

            usernameInputField.text = PlayerPrefs.GetString("username");
        }
        else
        {
            Debug.Log("No username found in player prefs.");
        }


        if (PlayerPrefs.GetString("password").Length > 0 && PlayerPrefs.GetString("password") != null && PlayerPrefs.GetString("password") != "")
        {
            //Debug.Log("Password exists in player prefs.");
            passwordInputField.text = PlayerPrefs.GetString("password");
        }
        else
        {
            Debug.Log("No password found in player prefs.");
        }
    }

    /// <summary>
    /// Stores inputted username and password locally in player prefs
    /// </summary>
    void StoreInPlayerPrefs()
    {
        //Store username in playerprefs
        PlayerPrefs.SetString("username", usernameInputField.text);

        //Store passwordd in playerprefs
        PlayerPrefs.SetString("password", passwordInputField.text);

    }

    /// <summary>
    /// On login button click callback
    /// </summary>
    public void OnNextButtonClick()
    {
        if (usernameInputField.text != "" && passwordInputField.text != "")
        {
            loginButton.interactable = false;
            Login();
            Debug.Log("Login and then store prefs!!!!!!");

        }
    }

    /// <summary>
    /// On offline login button click callback, for non-registered users  PDF generating is disabled
    /// </summary>
	void OnFreeUserClick()
	{
		PlayerPrefs.SetInt("access", 0);
		loginButtonIcon.enabled = false;	
		mainUI.OnLogin();
		loginButton.image.color = CustomUtilities.HexToColor(mainUI.greenValidButtonColor);
		loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.LoginSucceeded");
		//UserStats.userName = usernameInputField.text;
		mainUI.GetHeaderPanel().TransitionHeaderTitle("");
		StoreInPlayerPrefs(); // Only store them on successful login as they are used for checking offline access
	}

    /// <summary>
    /// On privacy statement button click callback
    /// </summary>
    void OnClickPrivacyStatement()
    {
        if (instatiatedScrollScreen == null)
            instatiatedScrollScreen = ((GameObject)Instantiate(simpleTextScrollViewPrefab, Vector3.zero, Quaternion.identity)).GetComponent<SimpleTextScrollScreen>();

		instatiatedScrollScreen.bodyText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("PrivacyStatement.Statement");
		instatiatedScrollScreen.headerTitleKey = "PrivacyStatement";
		instatiatedScrollScreen.ShowThisScreen();
    }

    /// <summary>
    /// On how to setup account button click callback
    /// </summary>
    void OnClickAccountHowTo()
    {
        if (instatiatedScrollScreen == null)
            instatiatedScrollScreen = ((GameObject)Instantiate(simpleTextScrollViewPrefab, Vector3.zero, Quaternion.identity)).GetComponent<SimpleTextScrollScreen>();

		instatiatedScrollScreen.bodyText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.HowToLogin.Text");
		instatiatedScrollScreen.headerTitleKey = "LoginScreen.HowToLogin";
        instatiatedScrollScreen.ShowThisScreen();

    }

    /// <summary>
    /// On username input field  de-selected callback,this has been set thru Unity Editor Inspector
    /// </summary>
    void OnUsernameFieldUnSelected()
    {
        inputfieldBackgroundUsername.sprite = SpriteResources.inputFieldSprite;
    }

    /// <summary>
    /// On password input field  de-selected callback, this has been set thru Unity Editor Inspector
    /// </summary>
    void OnPasswordFieldUnSelected()
    {
        inputfieldBackgroundPassword.sprite = SpriteResources.inputFieldSprite;
    }

    /// <summary>
    /// IScreen interface implemented methods
    /// </summary>
    #region IScreen implementation
    public string GetScreenHeaderTitle()
    {
        if (headerTitleText != null)
        {
            return headerTitleText.GetComponent<SmartLocalization.Editor.LocalizedText>().localizedKey;
        }
        else
        {
            return "";
        }
    }
    public void DisableScreen()
    {
        gameObject.SetActive(false);
    }
    public void EnableScreen()
    {
        gameObject.SetActive(true);
    }
    public void ShowThisScreen()
    {
        EnableScreen();
        StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.RightToLeft));
    }

    public void ShowPreviousScreen()
    {
        StartCoroutine(ViewIScreenController.ShowPreviousScreen());

    }
    public GameObject GetGameObject()
    {
        return gameObject;
    }

    #endregion
}
