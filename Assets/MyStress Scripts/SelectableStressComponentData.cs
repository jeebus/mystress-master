﻿using UnityEngine;
using System.Collections;
using System;

namespace Thrive
{
    /// <summary>
    /// Enum defining the possible coping categories
    /// </summary>
    public enum CopingCategory
    {
        NOT_APPLICABLE = 0,
        AFLEDNING = 1,
        OVERBLIK = 2,
        FORHANDLE = 3,
        BEDEOMHJÆLP = 4,
        BAKKEUD = 5

    }

    /// <summary>
    /// Data part of the interactive stress components on the scrollable lists
    /// </summary>
    [Serializable]
    public class SelectableStressComponentData
    {

        public string componentString;
        public bool selected;
        public CopingCategory category;

        /// <summary>
        /// A SelectableStressComponentData contructor class
        /// </summary>
        /// <param name="newComponentString"></param>
        /// <param name="selectedState"></param>
        public SelectableStressComponentData(string newComponentString, bool selectedState)
        {
            componentString = newComponentString;
            selected = selectedState;
            category = CopingCategory.NOT_APPLICABLE;
        }

        /// <summary>
        /// A SelectableStressComponentData contructor class
        /// </summary>
        /// <param name="newComponentString"></param>
        /// <param name="selectedState"></param>
        /// <param name="newCategory"></param>
        public SelectableStressComponentData(string newComponentString, bool selectedState, CopingCategory newCategory)
        {
            componentString = newComponentString;
            selected = selectedState;
            category = newCategory;
        }


        /// <summary>
        /// An overridden version of ToString() method
        /// </summary>
        /// <returns>Customized ToString()</returns>
        public override string ToString()
        {
            string s = "";
            s += componentString + "\n";
            s += "selected: " + selected + "\n";
            return s;
        }
    }
}