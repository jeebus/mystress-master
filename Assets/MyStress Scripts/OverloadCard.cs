﻿using UnityEngine;
using System.Collections;
using Thrive;
using UnityEngine.UI;
using SmartLocalization;

/// <summary>
/// The overload page of the of the MyStress card
/// This is the first page displayed when starting a new MyStress daily registration
/// </summary>
public class OverloadCard : MonoBehaviour, ICard {
	#region Unused components (Only here because of compile errors)
	[HideInInspector]
	public Image cardIcon;
	[HideInInspector]
	public Text cardNameTextComponent;
	#endregion

	[SerializeField]
	private Image smileyBackground;
	[SerializeField]
	private Image smileyForeground;
	[SerializeField]
	private Text selectedLoadText;
	[SerializeField]
	private Text loadExplanationText;


	private string selectedLoadTextKey;
	private string loadExplanationTextKey;

    /// <summary>
    /// Initilization
    /// </summary>
	void Start () 
	{
		smileyBackground.color = GetComponentInParent<MyStressCard>().effectColorNone;

		LanguageManager languageManagerInstance = LanguageManager.Instance;
		languageManagerInstance.OnChangeLanguage += this.OnChangeLanguage;
		OnChangeLanguage(languageManagerInstance);

	}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="smileyAttributes"></param>
	public void SetOverload(MyStressSmileyAttribute smileyAttributes)
	{
		if(smileyAttributes.overloadLevel == OverloadLevel.NORMAL) // LOW
		{
			selectedLoadTextKey = "MyStressCard1.Side1.OverloadDegree.Low";
			loadExplanationTextKey = "MyStressCard1.Side1.OverloadDegreeExplanation.Low";
		}
		else if(smileyAttributes.overloadLevel == OverloadLevel.ABOVE_NORMAL) // MEDIUM
		{
			selectedLoadTextKey = "MyStressCard1.Side1.OverloadDegree.Medium";
			loadExplanationTextKey = "MyStressCard1.Side1.OverloadDegreeExplanation.Medium";
		}
		else // HIGH
		{
			selectedLoadTextKey = "MyStressCard1.Side1.OverloadDegree.High";
			loadExplanationTextKey = "MyStressCard1.Side1.OverloadDegreeExplanation.High";
		}

		smileyForeground.sprite = smileyAttributes.smileySprite;
		OnChangeLanguage(LanguageManager.Instance);
	}

    /// <summary>
    /// On changed language callback
    /// </summary>
    /// <param name="languageManager">SmartLocalization.LanguageManager instance</param>
	public void OnChangeLanguage(LanguageManager languageManager){
		selectedLoadText.text = languageManager.GetTextValue(selectedLoadTextKey);
		loadExplanationText.text = languageManager.GetTextValue(loadExplanationTextKey);
	}

    /// <summary>
    /// ICard interface implemented methods
    /// </summary>
    #region ICard interface implementation
    public const string cardUID = "MIDT_000_INTRO";
	
	public string GetCardUID()
	{
		return cardUID;
	}
	
	public int GetCardOwnerID()
	{
		return 0;
		
	}
	
	public void SetCardOwnerID(int newCardOwner)
	{
		// cardOwnerID = newCardOwner;
	}
	
	public GameObject GetGameObject()
	{
		return gameObject;
	}
	
	public void MakeCardContentVisible()
	{
		gameObject.SetActive(true);
	}
	
	public void MakeCardContentInvisible()
	{
		gameObject.SetActive(false);
		// listenForAlertChanges = false;
	}
	
	public void ShowFrontSide()
	{
		// listenForAlertChanges = true;
		//StartCoroutine(UpdateAlertArea());
	}
	public void ShowBackSide()
	{
		
	}
	public bool GetRotateCardToReadMore()
	{
		return true;
	}
	public string GetCardName()
	{
		return "";
	}
	public void SetRotateCardToReadMore(bool newReadMoreValue)
	{
		// rotateCardToReadMore = newReadMoreValue;
	}
	public void SetCardMaterial(Material material)
	{
		
	}
	public void RotateCardToReadMore()
	{
		
	}
	
	public void RotateCardBackToOriginal()
	{
		
	}
	
	public Image GetCardIcon()
	{
		return cardIcon;
		//return new Image();
	}
	public Text GetCardNameTextComponent()
	{
		return cardNameTextComponent;
		//return new Text();
	}
	public Image GetCardImageTexture()
	{
		return null;
	}
	#endregion
}
