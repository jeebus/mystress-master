using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
//A simple PDF reader, for opening PDFs
using IndieYP;
//iTextSharp for generating PDFs
using iTextSharp.text.pdf;
using iTextSharp.text;


namespace Thrive
{
    /// <summary>
    /// Class used to create PDF documents of MyStress registered periods
    /// </summary>
    public class SimplePDF : MonoBehaviour
    {
        string attacName;
        string fileName;
        string folderPath;
        string optionalUserName = "";
        System.DateTime fromDate;
        System.DateTime toDate;


        public GameObject printablePagePrefab;
        List<string> screenShotFilePaths;
        bool generatingPDFNow = false;

        /// <summary>
        /// Define the filename of the pdf, based on month and year
        /// </summary>
        public void DefineFilePath()
        {

			#if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
			    folderPath = Application.streamingAssetsPath + "/PDFs/";
			#elif UNITY_IOS || UNITY_ANDROID
			    folderPath = Application.persistentDataPath +"/PDFs/";
			#endif


			if (!Directory.Exists(folderPath))
			{
				Directory.CreateDirectory(folderPath);
			}
            fileName = "MyStress_rapport_" + SmartLocalization.LanguageManager.Instance.GetTextValue("Generic.Month." + fromDate.Month)+ fromDate.Year + "-" + SmartLocalization.LanguageManager.Instance.GetTextValue("Generic.Month." + toDate.Month)+ toDate.Year  ;


            int fileNr = 0;
            bool validFileNameFound = false;
            DateTime starTime = DateTime.Now;
            while (!validFileNameFound)
            {
                if ((DateTime.Now - starTime).TotalSeconds > 5)
                {
                    Debug.Log("Timeout, returning from code");
                    return;
                }

                FileInfo file = new FileInfo(folderPath+fileName + fileNr + ".pdf");
                if (file.Exists)
                {
                    fileNr++;
                }
                else
                {
                    validFileNameFound = true;
                    fileName = fileName + fileNr;
                }
            }

            attacName = folderPath + fileName  + ".pdf";
        }
        PdfDocument pdfdoc;

        /// <summary>
        /// Create PDF coroutine
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator CreatePDF()
        {
            generatingPDFNow = true;

            BlankBlackScreen blankScreen = Screens.GetBlankScreen();
            blankScreen.SetAlphaAmount(0.7f);
            blankScreen.ShowThisScreen();
            while (blankScreen.IsTransitionRunning())
            {
                yield return null;
            }

			#if UNITY_IPHONE
			Handheld.SetActivityIndicatorStyle(iOSActivityIndicatorStyle.WhiteLarge);
			#elif UNITY_ANDROID
			Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Large);
			#endif
			Handheld.StartActivityIndicator();
			yield return new WaitForSeconds(0);


            pdfdoc = new PdfDocument(fromDate, toDate, optionalUserName);
            StartCoroutine(pdfdoc.LoadImages());
            while (!pdfdoc.GetImageLoaded())
            {
                //wait and do nothinng
                yield return null;
            }
		
			Debug.Log("Images loaded");
            Main();
        }

        
        /// <summary>
        /// Get if pdf is generating now
        /// </summary>
        /// <returns>Returns true or false</returns>
        public bool IsGeneratingPDFNow()
        {
            return generatingPDFNow;
        }

        /// <summary>
        /// Main function for creating PDF
        /// </summary>
        void Main()
        {
            DefineFilePath();
            BlankBlackScreen blankScreen = Screens.GetBlankScreen();
            
            //Load images via WWW class in coroutine 
            pdfdoc.createPdf(attacName);

#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            Application.OpenURL(attacName);
#elif UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX 
            Application.OpenURL("file://"+ attacName);
#elif UNITY_IOS
            PDFReader.OpenDocInMenu ("file://localhost/"+attacName,false);
#elif UNITY_ANDROID
            StartCoroutine(PDFReader.OpenDocLocal("/PDFs/" + fileName));
#endif
            generatingPDFNow = false;
            
            Handheld.StopActivityIndicator();
            blankScreen.ShowPreviousScreen();
        }


        /// <summary>
        /// Sets name on PDF
        /// </summary>
        /// <param name="newOptionalName">The name on the PDF as a string</param>
        public void SetOptionalName(string newOptionalName)
        {
            optionalUserName = newOptionalName;
        }

        /// <summary>
        /// Sets the from and to period as dates
        /// </summary>
        /// <param name="newFromDate">From date</param>
        /// <param name="newToDate">To date</param>
        public void SetStatsPeriod(System.DateTime newFromDate, System.DateTime newToDate)
        {
            fromDate = newFromDate;
            toDate = newToDate;
        }

        /// <summary>
        /// Get period for which to generate PDF over
        /// </summary>
        /// <returns>An array with 2 elemts, first is from date, second is to date</returns>
        public System.DateTime[] GetStatsPeriod()
        {
            System.DateTime[] statsPeriod = new System.DateTime[2];
            statsPeriod[0] = fromDate;
            statsPeriod[1] = toDate;
            return statsPeriod;
        }

        /// <summary>
        /// Delete PDFs stored internally in app folders
        /// </summary>
		public void DeletePDF(){
			string path;
			#if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
			path = Application.streamingAssetsPath + "/PDFs/";
			#elif UNITY_IOS || UNITY_ANDROID
			path = Application.persistentDataPath +"/PDFs/";
			#endif
			Debug.Log ("Trying to delete: " + path);
			if (Directory.Exists (path)) {
				var info = new DirectoryInfo (path);
				var fileInfo = info.GetFiles ();
                Debug.Log("Amount of files to delete : " + fileInfo.Length);
				foreach (var file in fileInfo) {
                   if(!IsFileLocked(file)) 
                    {
                        file.Delete();
                    }
                   else
                   {
                       Debug.Log("Cant delete file: " + file.FullName + " because it is locked.");
                   }
				}
			}
		}


        /// <summary>
        /// Load images coroutine
        /// </summary>
        /// <param name="filePath">File path to load from, as string</param>
        /// <returns></returns>
        IEnumerator LoadPDFImagesTask(string filePath)
        {
            WWW www = new WWW(filePath);
            yield return www;
        }

        /// <summary>
        /// When application shuts down, do cleanup
        /// </summary>
        void OnApplicationQuit()
        {
            DeletePDF();
        }

        /// <summary>
        /// Check if current PDF file (filename to be written to) is already open
        /// </summary>
        /// <param name="file">System.IO.File.FileInfo object</param>
        /// <returns>Return true or false</returns>
        bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
    }
}