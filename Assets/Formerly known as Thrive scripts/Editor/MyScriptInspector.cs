﻿#if  UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
using UnityEngine;
using UnityEditor;
namespace Thrive
{
    [CustomEditor(typeof(CardUIPlaceHolder))]
    public class CardUIPlaceholderInspector : Editor
    {
        
        public override void OnInspectorGUI()
        {
            CardUIPlaceHolder script = (CardUIPlaceHolder)target;

            Object obj = EditorGUILayout.ObjectField("Actual Card", (Object)script.actualCard, typeof(ICard), true);
            script.actualCard = obj as ICard;
            DrawDefaultInspector();
        }
         
    }
}
#endif