﻿
#if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Thrive {
    [InitializeOnLoad]
    public class BundleVersionChecker
    {
        /// <summary>
        /// Class name to use when referencing from code.
        /// </summary>
        const string ClassName = "CurrentBundleVersion";

        const string TargetCodeFile = "Assets/" + ClassName + ".cs";

        static BundleVersionChecker()
        {
            string bundleVersion = PlayerSettings.bundleVersion;
            string lastVersion = CurrentBundleVersion.version;
            if (lastVersion != bundleVersion)
            {
                Debug.Log("Found new bundle version " + bundleVersion + " replacing code from previous version " + lastVersion + " in file \"" + TargetCodeFile + "\"");
                CreateNewBuildVersionClassFile(bundleVersion);
            }
        }

        static string CreateNewBuildVersionClassFile(string bundleVersion)
        {
            using (StreamWriter writer = new StreamWriter(TargetCodeFile, false))
            {
                try
                {
                    string code = GenerateCode(bundleVersion);
                    writer.WriteLine("{0}", code);
                }
                catch (System.Exception ex)
                {
                    string msg = " threw:\n" + ex.ToString();
                    Debug.LogError(msg);
                    EditorUtility.DisplayDialog("Error when trying to regenerate class", msg, "OK");
                }
            }
            return TargetCodeFile;
        }

        /// <summary>
        /// Regenerates (and replaces) the code for ClassName with new bundle version id.
        /// </summary>
        /// <returns>
        /// Code to write to file.
        /// </returns>
        /// <param name='bundleVersion'>
        /// New bundle version.
        /// </param>
        static string GenerateCode(string bundleVersion)
        {
            string code = "namespace Thrive {\n public static class " + ClassName + "\n{\n";
            code += System.String.Format("\tpublic static readonly string version = \"{0}\";", bundleVersion);
            code += "\n}\n}\n";
            return code;
        }
    }

}
#endif