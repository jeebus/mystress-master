﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Thrive
{
    public class BirthDatePicker : MonoBehaviour
    {
        public InputField day;
        public InputField month;
        public InputField year;

        string birthdateString;

        bool yearIsValid = false;
        bool dayIsValid = false;
        bool monthIsValid = false;

        public WarningField birthdateWarning;
        void Awake()
        {
#if UNITY_ANDROID
            day.shouldHideMobileInput = false;
            month.shouldHideMobileInput = false;
            year.shouldHideMobileInput = false;
#endif
#if UNITY_IPHONE
            day.shouldHideMobileInput = true;
            month.shouldHideMobileInput = true;
            year.shouldHideMobileInput = true;
#endif
        }

		void Start(){

			SetupEventlisteners ();
		}

		void SetupEventlisteners(){

			day.gameObject.AddComponent<DisableInputfieldPlaceholderText>();
			month.gameObject.AddComponent<DisableInputfieldPlaceholderText>();
			year.gameObject.AddComponent<DisableInputfieldPlaceholderText>();
		}


		void OnEnable(){
			day.text = "";
			month.text = "";
			year.text = "";

			yearIsValid = false;
			dayIsValid = false;
			monthIsValid = false;

		}

        public bool IsBirthdateValid()
        {
            yearIsValid = false;
            dayIsValid = false;
            monthIsValid = false;
            if (year.text.Length == 4)
            {
                //year must be after 1900 and must not be later then this year
                if (int.Parse(year.text) > 1900 && int.Parse(year.text) <= DateTime.UtcNow.Year)
                {
                    yearIsValid = true;
                }
            }


            if (day.text.Length>0)
            {
                if (int.Parse(day.text) >= 1 && int.Parse(day.text) <= 31)
                {
                    dayIsValid = true;
                }
            }

            if (month.text.Length > 0)
            {
                if (int.Parse(month.text) >= 1 && int.Parse(month.text) <= 12)
                {
                    monthIsValid = true;
                }
            }



            if (yearIsValid && dayIsValid && monthIsValid)
            {
                return true;
            }
            else
            {
                birthdateWarning.ShowText();
                return false;
            }
        }

        public string GetBirthdateInput()
        {
            string[] birthDateArray = new string[3];

          


            birthDateArray[0] = year.text + "-";
            birthDateArray[1] = month.text + "-";
            birthDateArray[2] = day.text + "T00:00:00";
           
            birthdateString = String.Concat(birthDateArray);
            Debug.Log("Birthdate: " + birthdateString);
            return birthdateString;
        }

    }
}