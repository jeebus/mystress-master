﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class FoldingPlanes : MonoBehaviour
    {

        public Image topImage;
        public Image bottomImage;
        public Transform topImageHinge;
        public Transform bottomImageHinge;

        float foldLerpCounter = 0;
      //  float fadeLerpCounter = 0;
        float fadeContentLerpCounter = 0;
        float foldOutMultiplier = 2.0f;

        float topFoldedInRotation = -90;
        float topFoldedOutRotation = 0;
        float bottomFoldedInRotation = 90;
        float bottomFoldedOutRotation = 0;

        public bool foldIn;
        public bool foldOut;
        bool currentlyFolding = false;

        public Button activateButton;

        CanvasGroup foldingMainCanvasGrp;
        public CanvasGroup topImageContentCanvasGrp;
        public CanvasGroup bottomImageContentCanvasGrp;
        public RectTransform mainContentPanel;

        public Button acceptButton;
        public Button rejectButton;

        public Text whatSortaInvitation;
        public Text inviteeName;


        MainUI mainUI;

        float mainContentAnchorMin = 0.6453957f;


        // Use this for initialization
        void Awake()
        {
            activateButton.onClick.AddListener(() => { OnButtonClick(); });

            acceptButton.onClick.AddListener(() => { OnAcceptedButtonClick(); });
            rejectButton.onClick.AddListener(() => { OnRejectButtonClick(); });
            foldingMainCanvasGrp = GetComponent<CanvasGroup>();
            mainUI = FindObjectOfType<MainUI>();
            gameObject.SetActive(false);
            
        }
        void Start()
        {
           // invitationManager = FindObjectOfType<InvitationManager>();
        }


        void OnAcceptedButtonClick()
        {
        //    invitationManager.AcceptCurrenInFocusInvitation();
            FoldIn();
        }

        void OnRejectButtonClick()
        {
        //    invitationManager.RejectCurrenInFocusInvitation();
            FoldIn();
        }




        void OnButtonClick()
        {
            if (!currentlyFolding)
            {
                if (foldOut)
                {
                    FoldIn();
                }
                else if (foldIn)
                {
                    FoldOut();
                }
            }
        }


        public void FoldOut()
        {
            mainUI.GetTutorialScreen().HideThisScreen();
            foldOut = true;
            foldIn = false;
            currentlyFolding = true;
            gameObject.SetActive(true);
            StartCoroutine(FadeIn());
        }

        public void FoldIn()
        {
            foldOut = false;
            foldIn = true;
            currentlyFolding = true;
            HideContent();
            mainUI.GetTutorialScreen().ShowThisScreen();
        }


        IEnumerator FoldOutTask()
        {
            while (foldLerpCounter <= 1 && !foldIn)
            {
                topImageHinge.rotation = Quaternion.Euler(Mathf.Lerp(topFoldedInRotation, topFoldedOutRotation, foldLerpCounter), 0, 0);
                bottomImageHinge.rotation = Quaternion.Euler(Mathf.Lerp(bottomFoldedInRotation, bottomFoldedOutRotation, foldLerpCounter), 0, 0);

                mainContentPanel.anchorMax = new Vector2(mainContentPanel.anchorMax.x, Mathf.Lerp(mainUI.mainContentAnchorMax, mainContentAnchorMin, foldLerpCounter));

                foldLerpCounter += Time.deltaTime * foldOutMultiplier;
                yield return null;
            }
            if (!foldIn)
            {
                topImageHinge.rotation = Quaternion.Euler(new Vector3(topFoldedOutRotation, 0, 0));
                bottomImageHinge.rotation = Quaternion.Euler(new Vector3(bottomFoldedOutRotation, 0, 0));
                mainContentPanel.anchorMax = new Vector2(mainContentPanel.anchorMax.x, mainContentAnchorMin);
                foldLerpCounter = 1;
                ShowContent();
            }
        }

        IEnumerator FoldInTask()
        {
            while (foldLerpCounter >= 0 && !foldOut)
            {
                topImageHinge.rotation = Quaternion.Euler(Mathf.Lerp(topFoldedInRotation, topFoldedOutRotation, foldLerpCounter), 0, 0);
                bottomImageHinge.rotation = Quaternion.Euler(Mathf.Lerp(bottomFoldedInRotation, bottomFoldedOutRotation, foldLerpCounter), 0, 0);
                mainContentPanel.anchorMax = new Vector2(mainContentPanel.anchorMax.x, Mathf.Lerp(mainUI.mainContentAnchorMax, mainContentAnchorMin, foldLerpCounter));

                foldLerpCounter -= Time.deltaTime * foldOutMultiplier;

                yield return null;
            }
            if (!foldOut)
            {
                topImageHinge.rotation = Quaternion.Euler(new Vector3(topFoldedInRotation, 0, 0));
                bottomImageHinge.rotation = Quaternion.Euler(new Vector3(bottomFoldedInRotation, 0, 0));
                mainContentPanel.anchorMax = new Vector2(mainContentPanel.anchorMax.x, mainUI.mainContentAnchorMax);
                foldLerpCounter = 0;
                StartCoroutine(FadeOut());
            }
        }

        IEnumerator FadeOut()
        {
            /*
            while(fadeLerpCounter >= 0){
                foldingMainCanvasGrp.alpha = fadeLerpCounter;
                fadeLerpCounter -= Time.deltaTime;
                yield return null;

            }
               fadeLerpCounter = 0;
             */
           
            foldingMainCanvasGrp.alpha = 0;
           
            currentlyFolding = false;
            gameObject.SetActive(false);
            yield return null;
        }

        IEnumerator FadeIn()
        {
            /*
            while (fadeLerpCounter <= 1)
            {
                foldingMainCanvasGrp.alpha = fadeLerpCounter;
                fadeLerpCounter += Time.deltaTime;
                yield return null;

            }
             fadeLerpCounter = 1;
             */

            foldingMainCanvasGrp.alpha = 1;
            StartCoroutine(FoldOutTask());
            yield return null;
        }

        void ShowContent()
        {
            
            StartCoroutine(FadeInContent());
        }

        void HideContent()
        {
            StartCoroutine(FadeOutContent());
        }

        IEnumerator FadeInContent()
        {
            while (fadeContentLerpCounter <= 1 && !foldIn)
            {
                topImageContentCanvasGrp.alpha = fadeContentLerpCounter;
                bottomImageContentCanvasGrp.alpha = fadeContentLerpCounter;
                fadeContentLerpCounter += Time.deltaTime;
                yield return null;

            }
            if (!foldIn)
            {
                fadeContentLerpCounter = 1;
                topImageContentCanvasGrp.alpha = 1;
                bottomImageContentCanvasGrp.alpha = 1;
                currentlyFolding = false;
            }
        }
        IEnumerator FadeOutContent()
        {
            while (fadeContentLerpCounter >= 0 && !foldOut)
            {
                topImageContentCanvasGrp.alpha = fadeContentLerpCounter;
                bottomImageContentCanvasGrp.alpha = fadeContentLerpCounter;
                fadeContentLerpCounter -= Time.deltaTime;
                yield return null;

            }
            if (!foldOut)
            {
                fadeContentLerpCounter = 0;
                topImageContentCanvasGrp.alpha = 0;
                bottomImageContentCanvasGrp.alpha = 0;
                StartCoroutine(FoldInTask());
            }
        }
    }
}
