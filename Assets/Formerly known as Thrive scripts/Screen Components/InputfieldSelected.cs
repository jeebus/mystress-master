﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

namespace Thrive
{
    public class InputfieldSelected : MonoBehaviour
    {
        EventTrigger eventTrigger = null;

        Image inputfieldImage;

        // Use this for initialization
        void Start()
        {
            eventTrigger = gameObject.GetComponent<EventTrigger>();

            AddEventTrigger(OnPointerClick, EventTriggerType.PointerClick);
//            AddEventTrigger(OnPointerEnter, EventTriggerType.PointerEnter);

            inputfieldImage = transform.GetComponent<Image>();
            if (inputfieldImage == null)
            {
                inputfieldImage = transform.parent.GetComponent<Image>();
            }
        }


        private void AddEventTrigger(UnityAction action, EventTriggerType triggerType)
        {
            // Create a nee TriggerEvent and add a listener
            EventTrigger.TriggerEvent trigger = new EventTrigger.TriggerEvent();
            trigger.AddListener((eventData) => action()); // you can capture and pass the event data to the listener

            // Create and initialise EventTrigger.Entry using the created TriggerEvent
            EventTrigger.Entry entry = new EventTrigger.Entry() { callback = trigger, eventID = triggerType };

            // Add the EventTrigger.Entry to delegates list on the EventTrigger
            eventTrigger.triggers.Add(entry);
        }

        private void AddEventTrigger(UnityAction<BaseEventData> action, EventTriggerType triggerType)
        {
            // Create a nee TriggerEvent and add a listener
            EventTrigger.TriggerEvent trigger = new EventTrigger.TriggerEvent();
            trigger.AddListener((eventData) => action(eventData)); // you can capture and pass the event data to the listener

            // Create and initialise EventTrigger.Entry using the created TriggerEvent
            EventTrigger.Entry entry = new EventTrigger.Entry() { callback = trigger, eventID = triggerType };

            // Add the EventTrigger.Entry to delegates list on the EventTrigger
            eventTrigger.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();// 
            eventTrigger.triggers.Add(entry);
        }


        private void OnPointerClick(BaseEventData data)
        {
            if (inputfieldImage.sprite != SpriteResources.inputFieldSelectedInvalidSprite)
            {
                inputfieldImage.sprite = SpriteResources.inputFieldSelectedSprite;
            }
        }

    }
}