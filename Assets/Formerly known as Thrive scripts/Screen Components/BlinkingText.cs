﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class BlinkingText : MonoBehaviour
    {
        Text blinkingText;
        public bool isBlinking = true;
        public float blinkingOffset;
        Button blinkingButton;

        // Use this for initialization
        void Start()
        {
            blinkingText = GetComponent<Text>();
            StartCoroutine(BlinkingAction());
        }



        public IEnumerator BlinkingAction()
        {
            float time = 0;
            while (isBlinking)
            {
                time += Time.deltaTime;
                blinkingText.color = new Color(blinkingText.color.r, blinkingText.color.g, blinkingText.color.b, pulse(time, blinkingOffset));
                yield return null;
            }
        }

        float pulse(float time, float offset)
        {
            const float pi = 3.14f;
            const float frequency = 1.0f; // Frequency in Hz
            return 0.5f * (1 + Mathf.Sin(2 * pi * frequency * time + offset));
        }
    }
}
