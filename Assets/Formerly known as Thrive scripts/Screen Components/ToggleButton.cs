﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    public class ToggleButton : MonoBehaviour {
        MainUI mainUI;

        Scrollbar toggleButton;
        public Text toggleHandleText;
        public Image toggleHandleImage;

	    // Use this for initialization
	    void Awake () {
            mainUI = FindObjectOfType<MainUI>();
            toggleButton = GetComponent<Scrollbar>();
            toggleButton.onValueChanged.AddListener(delegate
            {
                OnValueChanged();
            });
            OnValueChanged();

     
            SmartLocalization.LanguageManager.Instance.OnChangeLanguage += OnChangeLanguage;
	    }

        void OnChangeLanguage(SmartLocalization.LanguageManager thisLanguageManager)
        {
            OnValueChanged();
        }

        public void OnValueChanged()
        {
            if(toggleButton.value > 0.5f){
                //toggleHandleText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("Button.Off");
                StartCoroutine(CustomUtilities.UpdateTextOnComponent(toggleHandleText, SmartLocalization.LanguageManager.Instance.GetTextValue("Button.Off")));
                toggleHandleImage.color = CustomUtilities.HexToColor(mainUI.toggleButtonOffColor);
            }
            else
            {
          //     toggleHandleText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("Button.On");
                StartCoroutine(CustomUtilities.UpdateTextOnComponent(toggleHandleText, SmartLocalization.LanguageManager.Instance.GetTextValue("Button.On")));
                toggleHandleImage.color = CustomUtilities.HexToColor(mainUI.prettyPurpleColor);
            }
        }
    }
}
