﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class CellSize : MonoBehaviour
    {

        private LayoutElement LayOut;
        public int listItemsOnScreen = 1;


        // Use this for initialization
        void Start()
        {
            MainUI mainUI = FindObjectOfType<MainUI>();
            LayOut = GetComponent<LayoutElement>();
            LayOut.preferredWidth = Screen.width;
			LayOut.preferredHeight = mainUI.cardMinimizedPreferredHeight;
        }

    }
}
