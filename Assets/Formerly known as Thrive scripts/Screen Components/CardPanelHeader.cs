﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class CardPanelHeader : MonoBehaviour
    {
        public Text userNameText;
        public Button optionsButton;
        public Image userIcon;
        public FoldingPlanes foldingPanels;

        MainUI mainUI;
        float optionsLerpCounter = 0;
        float fadeMultiplier = 3;
        CanvasGroup headerTitleCanvasGroup;
        int headerTitleFontSize;
        string headerKey;

        // Use this for initialization
        void Awake()
        {
          //  optionsButton.onClick.AddListener(() => { OnOptionsButtonClick(); });
            mainUI = FindObjectOfType<MainUI>();
            if (userNameText != null)
            {
                userNameText.text = "";
            }

            headerTitleCanvasGroup = userNameText.GetOrAddComponent<CanvasGroup>();
            headerTitleCanvasGroup.alpha = 0;

            SmartLocalization.LanguageManager.Instance.OnChangeLanguage += OnLanguageChanged;
            StartCoroutine(GetHeaderTitleFontSize());
        }

        IEnumerator GetHeaderTitleFontSize()
        {
            //Font size is first generated after initial frame
            yield return new WaitForEndOfFrame();
            headerTitleFontSize = userNameText.cachedTextGenerator.fontSizeUsedForBestFit;
            userNameText.resizeTextForBestFit = false;
            userNameText.fontSize = headerTitleFontSize;
        }

        void OnLanguageChanged(SmartLocalization.LanguageManager languageManager)
        {
            StartCoroutine(HeaderTransitionText(languageManager.GetTextValue(headerKey)));
        }

        public void TransitionHeaderTitle(string newHeaderKey)
        {

            if (headerTitleCanvasGroup != null)
            {
                Debug.Log("Transition header, new header key: " + newHeaderKey);
                headerKey = newHeaderKey;
                StartCoroutine(HeaderTransitionText(SmartLocalization.LanguageManager.Instance.GetTextValue(headerKey)));
            }
        }


        IEnumerator HeaderTransitionText(string newHeader)
        {
            float fadeCounter = 1;
            if (headerTitleCanvasGroup.alpha > 0)
            {
                //Fade out current header
                while(fadeCounter > 0){
                    fadeCounter -= Time.deltaTime*fadeMultiplier;
                    headerTitleCanvasGroup.alpha = fadeCounter;
                    yield return null;
                }
                
            }
            fadeCounter = 0;
            headerTitleCanvasGroup.alpha = fadeCounter;
            userNameText.text = newHeader;
            while (fadeCounter < 1)
            {
                fadeCounter += Time.deltaTime * fadeMultiplier;
                headerTitleCanvasGroup.alpha = fadeCounter;
                yield return null;
            }
        }

        void OnOptionsButtonClick()
        {
            if (optionsLerpCounter == 0)
            {
                //main user deck 
                
                if (mainUI.GetCurrentDeckID() == 0)
                {
                   mainUI.GetOptionsScreen().ShowThisScreen();
                }
                /*
                //a childs deck
                if (mainUI.GetCurrentDeckID() > 0)
                {
                    ProfileScreen profileScreen = mainUI.GetProfileScreen();
                    profileScreen.UpdateProfileScreen();
                    //((RectTransform)profileScreen.transform).anchoredPosition3D = new Vector3(0, ((RectTransform)profileScreen.transform).anchoredPosition3D.y, ((RectTransform)profileScreen.transform).anchoredPosition3D.z);
                    profileScreen.ShowThisScreen();
                }
                 * */
            }
        }
    }
}