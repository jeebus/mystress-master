﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace Thrive
{
    public class InviteStatusComponent : MonoBehaviour
    {

        private LayoutElement LayOut;

        public Text inviteeName;
        public Button cancelOrUnlinkButton;
        public Button resendButton;
        public Text resendButtonText;
		public UGUITextLocalization cancelOrUnlinkButtonText;
        public Image invitationStatusIcon;
        public Invitation actualInvitation;

        MainUI mainUI;
        Animator pendingIconAnimator;

        // Use this for initialization
        void Awake()
        {
            mainUI = FindObjectOfType<MainUI>();
            LayOut = GetComponent<LayoutElement>();
            LayOut.preferredWidth = ((RectTransform)mainUI.GetLoginScreen().loginButton.transform).rect.width;
            LayOut.preferredHeight = mainUI.cardMinimizedPreferredHeight/5;

            cancelOrUnlinkButton.onClick.AddListener(() => { OnCancelOrUnlinkButtonClick(); });
            resendButton.onClick.AddListener(() => { OnResendButtonClick(); });

            pendingIconAnimator = invitationStatusIcon.GetComponent<Animator>();
        }

        public void InvitationStatusAccepted()
        {
            pendingIconAnimator.enabled = false;

            invitationStatusIcon.sprite = SpriteResources.verifiedInputPassedIcon;
            resendButton.interactable = false;
            resendButtonText.color = new Color(resendButtonText.color.r, resendButtonText.color.g, resendButtonText.color.b, 0.3f);
			cancelOrUnlinkButtonText.localizedKey= "InvitationStatusComponent.Unlink";

        }

        public void InvitationStatusRejected()
        {
            pendingIconAnimator.enabled = false;

            invitationStatusIcon.sprite = SpriteResources.verifiedInputFailedIcon;
            resendButton.interactable = true;
            resendButtonText.color = new Color(resendButtonText.color.r, resendButtonText.color.g, resendButtonText.color.b, 1.0f);
			cancelOrUnlinkButtonText.localizedKey = "InvitationStatusComponent.Remove";
        }

        public void InvitationStatusIncommingPending()
        {
            invitationStatusIcon.sprite = SpriteResources.verifiedInputFailedIcon;
            resendButton.interactable = true;
            resendButtonText.color = new Color(resendButtonText.color.r, resendButtonText.color.g, resendButtonText.color.b, 1.0f);
			cancelOrUnlinkButtonText.localizedKey = "InvitationStatusComponent.Reject";
        }



        void OnCancelOrUnlinkButtonClick()
        {
			if (cancelOrUnlinkButtonText.localizedKey == "InvitationStatusComponent.Reject")
            {
                Debug.Log("Send reject account message");
         //       mainUI.GetInvitationManager().RejectCurrenInFocusInvitation(actualInvitation.GetInvitationHash());
                Destroy(gameObject);
            }
			if(cancelOrUnlinkButtonText.localizedKey == "InvitationStatusComponent.Unlink"){
                Debug.Log("Send unlink account message");
            }
			if (cancelOrUnlinkButtonText.localizedKey == "InvitationStatusComponent.Cancel"|| cancelOrUnlinkButtonText.localizedKey == "InvitationStatusComponent.Remove")
            {
                Debug.Log("Cancel invitation message");
                /*
                Thrive.RPC.DeleteInvite(actualInvitation.GetInvitationHash(), (res) =>
                {

                    Debug.Log("Invitation delete requested, res.raw = " + res.raw);
                    // Debug.Log(invitationFromUserID + " " + invitationToUserName + " " + invitationClickedDate);
                    // if child added do something
                    if (res.code == "INVITE_CANCELED")
                    {
                        // StartCoroutine(OnClickBackButton());
                        Destroy(gameObject);
                    }

                    if (res.code == "INVITE_ERROR")
                    {
                        Debug.Log("An invite error happened");
                    }

                });
                 * */
            }
        }

        void OnResendButtonClick()
        {
            Debug.Log("Resend invitation");
            /*
            Thrive.RPC.ResendInvite(actualInvitation.GetInvitationHash() , (res) =>
                {

                    Debug.Log("New invitation sent, res.raw = " + res.raw);
                   // Debug.Log(invitationFromUserID + " " + invitationToUserName + " " + invitationClickedDate);
                    // if child added do something
                    if (res.code == "INVITE_RECIEVED")
                    {
                       // StartCoroutine(OnClickBackButton());
                    }

                    if (res.code == "INVITE_ERROR")
                    {
                        Debug.Log("An invite error happened");
                    }

                });
             * */
        
        }
    }
}
