﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Thrive
{
    /// Tests an E-Mail address.
    public static class InputfieldChecker
    {
        /// Regular expression, which is used to validate an E-Mail address.
        public const string MatchEmailPattern =
            @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
              + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";


        /// Regular expression, which is used to validate a username.
        public const string MatchUsernamePattern =
            @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{2}|[\w-]{2,}))";

        /// Regular expression, which is used to validate a password.
        public const string MatchPasswordPattern =
            @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{8}|[\w-]{2,}))";


        /// Checks whether the given Email-Parameter is a valid E-Mail address.
        public static bool IsEmail(string email)
        {
            if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
            else return false;
        }

        public static bool IsUsername(string username)
        {
            if (username != null) return Regex.IsMatch(username, MatchUsernamePattern);
            else return false;
        }

        public static bool IsPassword(string password)
        {
            if (password != null) return Regex.IsMatch(password, MatchPasswordPattern);
            else return false;
        }
    }
}