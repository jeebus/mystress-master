﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Thrive
{

    public class MetricButton : MonoBehaviour
    {
        public Text lengthUnitText;
        public Text weightUnitText;
        public InputField lenghtValueText;
        public InputField weightValueText;

        Scrollbar button;

        // Use this for initialization
        void Start()
        {
            button = GetComponent<Scrollbar>();
            button.onValueChanged.AddListener(delegate
            {
                OnMetricChanged();
            });
        }

        void OnMetricChanged()
        {
            //1 inch = 25.4 millimeters
            //1 ounce = 28.35 grams
            if (button.value <= 0.5f)
            {
                if (lenghtValueText.text != "" && lengthUnitText.text != "in")
                {
                    lenghtValueText.text = ((int)(float.Parse(lenghtValueText.text) / 2.54f)).ToString();
                    Debug.Log("Lenght change");
                }
                if (weightValueText.text != "" && weightUnitText.text != "oz")
                {
                    weightValueText.text = ((int)(float.Parse(weightValueText.text) / 28.35f)).ToString();
                }
                lengthUnitText.text = "in";
                weightUnitText.text = "oz";
               
            }
            else
            {
               

                if (lenghtValueText.text != "" && lengthUnitText.text != "cm")
                {
                    lenghtValueText.text = ((float.Parse(lenghtValueText.text) * 2.54f)).ToString();
                    Debug.Log("Lenght change");
                }
                if (weightValueText.text != "" && weightUnitText.text != "g")
                {
                    weightValueText.text = ((float.Parse(weightValueText.text) * 28.35f)).ToString();
                }

                lengthUnitText.text = "cm";
                weightUnitText.text = "g";
            }
        }
    }
}