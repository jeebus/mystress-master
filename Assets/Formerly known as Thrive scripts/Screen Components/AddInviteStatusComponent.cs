﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Thrive {
public class AddInviteStatusComponent : MonoBehaviour {


        public GameObject inviteStatusComponentPrefab;
        public GameObject inviteStatusDateGrouperPrefab;

        List<GameObject> inviteStatusList = new List<GameObject>();
        MainUI mainUI;

        public Button addInviteStatusButton;
        public Button addNewDateButton;

        GameObject latestDateGrouper;

        // Use this for initialization
        void Start()
        {
            mainUI = FindObjectOfType<MainUI>();

            addInviteStatusButton.onClick.AddListener(() => { OnAddInviteStatusButtonClick(); });
            addNewDateButton.onClick.AddListener(() => { OnAddNewDateButtonClick(); });
        }


        public void AddNewInviteStatusComponent()
        {
            if (latestDateGrouper == null)
            {
                latestDateGrouper = Instantiate(inviteStatusDateGrouperPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            }

            GameObject newInviteStatusComponent = Instantiate(inviteStatusComponentPrefab, Vector3.zero, Quaternion.identity) as GameObject;


            inviteStatusList.Add(newInviteStatusComponent);

            newInviteStatusComponent.transform.position = new Vector3(0, newInviteStatusComponent.transform.position.y, newInviteStatusComponent.transform.position.z);
            latestDateGrouper.transform.SetParent(transform, false);
            newInviteStatusComponent.transform.SetParent(latestDateGrouper.transform, false);

        }

        public void AddNewDateComponent()
        {
            latestDateGrouper = Instantiate(inviteStatusDateGrouperPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            VerticalLayoutGroup layout = latestDateGrouper.GetComponent<VerticalLayoutGroup>();
            layout.spacing = (mainUI.cardMinimizedPreferredHeight / 5) / 4;
            layout.padding.bottom = (int)(mainUI.cardMinimizedPreferredHeight / 5) / 2;
            layout.padding.top = (int)(mainUI.cardMinimizedPreferredHeight / 5) / 2;
            latestDateGrouper.transform.SetParent(transform, false);

        }


        void OnAddInviteStatusButtonClick()
        {
            AddNewInviteStatusComponent();
        }

        void OnAddNewDateButtonClick()
        {
            AddNewDateComponent();
        }
    }
}
