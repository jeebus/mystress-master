﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    public class ToggleStatsButton : MonoBehaviour
    {

        Scrollbar toggleButton;
        public Text toggleHandleText;
        public Image toggleHandleImage;

        float previousValue;

        // Use this for initialization
        void Awake()
        {
            toggleButton = GetComponent<Scrollbar>();
            toggleButton.onValueChanged.AddListener(delegate
            {
				StartCoroutine( OnValueChanged());
            });
            


            SmartLocalization.LanguageManager.Instance.OnChangeLanguage += OnChangeLanguage;
        }

		void Start(){

			StartCoroutine(OnValueChanged());
		}

        void OnChangeLanguage(SmartLocalization.LanguageManager thisLanguageManager)
        {
            StartCoroutine( OnValueChanged());
        }

        public IEnumerator OnValueChanged()
        {
            if (gameObject.activeInHierarchy)
            {
                if (toggleButton.value >= 0.5f && previousValue < 0.5f)
                {
                     toggleHandleText.text = "Tid";
                    //StartCoroutine(CustomUtilities.UpdateTextOnComponent(toggleHandleText, "Tid"));//SmartLocalization.LanguageManager.Instance.GetTextValue("Button.Off")));
                    //    toggleHandleImage.color = CustomUtilities.HexToColor(mainUI.toggleButtonOffColor);
                    previousValue = toggleButton.value;
					yield return null;
                }
                if (toggleButton.value < 0.5f && previousValue >= 0.5f)
                {

                    toggleHandleText.text = "Mængde";
                    //StartCoroutine(CustomUtilities.UpdateTextOnComponent(toggleHandleText, "Mængde"));//SmartLocalization.LanguageManager.Instance.GetTextValue("Button.On")));
                    //  toggleHandleImage.color = CustomUtilities.HexToColor(mainUI.prettyPurpleColor);
                    previousValue = toggleButton.value;
					yield return null;
                }
            }
        }
    }
}
