﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class FamilyScreenUserComponent : MonoBehaviour {


        public Text userNameText;

        private LayoutElement LayOut;

        public Button mainButton;
        MainUI mainUI;

        // Use this for initialization
        void Start()
        {
            mainUI = FindObjectOfType<MainUI>();
            LayOut = GetComponent<LayoutElement>();
          
            LayOut.preferredWidth = ((RectTransform)mainUI.GetLoginScreen().loginButton.transform).rect.width;
            LayOut.preferredHeight = mainUI.cardMinimizedPreferredHeight/3;
        }

        /*
        public void SetupEventlistener(ProfileScreen profileScreen,Child childData)
        {
            mainButton.GetComponent<Button>().onClick.AddListener(() =>
            {
               // profileScreen.UpdateProfileScreen(childData);
               // profileScreen.ShowThisScreen();
               // ViewIScreenController.SetPreviousScreen(ViewIScreenController.GetCurrentScreen());
                mainUI.LerpToSpecificDeckPanel(transform.GetSiblingIndex()+2);
                mainUI.StepBackInPreviousScreens(2);

             
            });

        }
         * */
    }

}
