﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive {
	public class CardPanel : MonoBehaviour {


		public GameObject panelInstance;
        public Canvas thisCanvas;

        public ScrollRect panelScrollRect;
        public bool scrollVerticalAllowed = true;

        MainUI mainUI;

        public User user;

		// Use this for initialization
		void Start () {
            mainUI = FindObjectOfType<MainUI>();
            panelInstance.GetComponent<VerticalLayoutGroup>().spacing = mainUI.cardMinimizedPreferredHeight * -0.3f;
		}


        public void ActivateVerticalScrolling()
        {
            panelScrollRect.vertical = true;
            scrollVerticalAllowed = true;
        }

        public void DeactivateVerticalScrolling()
        {
            panelScrollRect.vertical = false;
            scrollVerticalAllowed = false;
        }
	}
}
