﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public interface ICard
{
    GameObject GetGameObject();

    void ShowFrontSide();
    
    void ShowBackSide();

    void MakeCardContentVisible();

    void MakeCardContentInvisible();

    //void SetRotateCardToReadMore(bool newReadMoreValue);

    bool GetRotateCardToReadMore();

    string GetCardName();

    string GetCardUID();

    int GetCardOwnerID();

    void SetCardOwnerID(int newCardOwnerID);

    void RotateCardToReadMore();

    void RotateCardBackToOriginal();

    Image GetCardIcon();

    Text GetCardNameTextComponent();

    Image GetCardImageTexture();
}
