﻿using UnityEngine;
using System.Collections;

public interface IScreen {

    void ShowThisScreen();

    void ShowPreviousScreen();

    void DisableScreen();
    void EnableScreen();

    GameObject GetGameObject();

    string GetScreenHeaderTitle();
}
