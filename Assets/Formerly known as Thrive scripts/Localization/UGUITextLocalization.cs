﻿using UnityEngine;
using System.Collections;
using SmartLocalization;
using UnityEngine.UI;

namespace Thrive
{
    public class UGUITextLocalization : MonoBehaviour
    {

        public string localizedKey = "INSERT_KEY_HERE";
        [SerializeField]
        [HideInInspector]
        Text textComponent = null;

        float fadeMultiplier = 3;

        void Awake()
        {
            if (!textComponent)
                textComponent = GetComponent<Text>();
        }

        void Start()
        {
            //Subscribe to the change language event
            LanguageManager thisLanguageManager = LanguageManager.Instance;
            thisLanguageManager.OnChangeLanguage += OnChangeLanguage;

            //   OnChangeLanguage(thisLanguageManager);
            //   textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, 1);
            textComponent.text = LanguageManager.Instance.GetTextValue(localizedKey);
        }



        void OnEnable()
        {
            textComponent.text = LanguageManager.Instance.GetTextValue(localizedKey);
        }

        void OnDestroy()
        {
            if (LanguageManager.HasInstance)
                LanguageManager.Instance.OnChangeLanguage -= OnChangeLanguage;
        }

        //void SetDefaultLanguage)()

        void OnChangeLanguage(LanguageManager thisLanguageManager)
        {
            if (enabled && gameObject.activeInHierarchy)
            {
                StartCoroutine(ShowThisText(LanguageManager.Instance.GetTextValue(localizedKey)));
            }
            //textComponent.text = LanguageManager.Instance.GetTextValue(localizedKey);
        }
        IEnumerator ShowThisText(string newLanguage)
        {
            float lerpCounter = 0;
            while (lerpCounter < 1)
            {
                lerpCounter += Time.deltaTime * fadeMultiplier;
                textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, 1 - lerpCounter);
                yield return null;
            }
            lerpCounter = 0;
            textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, 0);
            textComponent.text = newLanguage;
            while (lerpCounter < 1)
            {
                lerpCounter += Time.deltaTime * fadeMultiplier;
                textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, lerpCounter);
                yield return null;
            }
            textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, 1);
        }
    }
}