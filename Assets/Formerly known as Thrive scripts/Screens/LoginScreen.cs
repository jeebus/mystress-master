﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using SimpleJSON;

namespace Thrive{
    public class LoginScreen : MonoBehaviour, IScreen
    {
        MainUI mainUI;

        public InputField usernameInputField;
        public InputField passwordInputField;
        public Button accountCreateButton;
        public Button forgotPasswordButton;
        public Button privacyButton;

        public Button loginButton;
        public Image loginButtonIcon;
        public Text loginButtonText;
		public Image fakeSplashScreenImage;
        Animator loginButtonIconAnimator;

        Toggle passwordSaveToggle;
        Image inputfieldBackgroundUsername;
        Image inputfieldBackgroundPassword;

        public Text headerTitleText;

        public Text loginServerMessages;

        void Awake()
        {
#if UNITY_ANDROID
            usernameInputField.shouldHideMobileInput = false;
            passwordInputField.shouldHideMobileInput = false;
#endif
#if UNITY_IPHONE
            usernameInputField.shouldHideMobileInput = true;
            passwordInputField.shouldHideMobileInput = true;
#endif
        }

        // Use this for initialization
        void Start()
        {
            inputfieldBackgroundUsername = usernameInputField.GetComponent<Image>();
            inputfieldBackgroundPassword = passwordInputField.GetComponent<Image>();

            mainUI = GameObject.FindObjectOfType<MainUI>();
            ((RectTransform)transform).sizeDelta = Vector2.zero;
            ((RectTransform)transform).anchorMin = new Vector2(0, 0);
            ((RectTransform)transform).anchorMax = new Vector2(1, 1);
          
          // CheckPlayerPrefsForExisitingData();

            SetUpEventListeners();
            loginButtonIconAnimator = loginButtonIcon.GetComponent<Animator>();
            loginButtonIcon.enabled = false;
			if (mainUI != null) {
				loginButton.image.color = CustomUtilities.HexToColor (mainUI.greenValidButtonColor);
			}
            fakeSplashScreenImage.enabled = false;

            ViewIScreenController.SetCurrentScreen(new VisitedScreens( this,TransitionType.RightToLeft));
			
            if (usernameInputField.text.Length > 0  && passwordInputField.text.Length > 0 ) {
				Login ();
			} else {

				fakeSplashScreenImage.enabled = false;
			}
             
        }

        

        void SetUpEventListeners()
        {
            accountCreateButton.onClick.AddListener(() => { OnClickAccountHowTo(); });
            forgotPasswordButton.onClick.AddListener(() => { OnClickForgotPassword(); });
            privacyButton.onClick.AddListener(() => { OnClickPrivacyStatement(); });
            loginButton.onClick.AddListener(() => { OnNextButtonClick(); });
            usernameInputField.gameObject.AddComponent<EventTrigger>();
            usernameInputField.gameObject.AddComponent<InputfieldSelected>();
			usernameInputField.gameObject.AddComponent<DisableInputfieldPlaceholderText>();
            usernameInputField.onEndEdit.AddListener(delegate
            {
                OnUsernameFieldUnSelected();
            });
            passwordInputField.gameObject.AddComponent<EventTrigger>();
            passwordInputField.gameObject.AddComponent<InputfieldSelected>();
			passwordInputField.gameObject.AddComponent<DisableInputfieldPlaceholderText>();

            passwordInputField.onEndEdit.AddListener(delegate
            {
                OnPasswordFieldUnSelected();
            });
        }


        void Login()
        {
            // Should be loaded from plyerPrefs
  //          Thrive.Session.SetUser(usernameInputField.text, passwordInputField.text);

            loginButton.image.color = CustomUtilities.HexToColor(mainUI.yellowPendingButtonColor);
//            loginButtonIcon.sprite = mainUI.loadingIcon;
			loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.AttemptingLogin");//"Attempting to login...";
			loginServerMessages.text += SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.AttemptingLogin");
            loginButtonIcon.enabled = true;
            loginButtonIconAnimator.enabled = true;

 

            if (mainUI.serverSynchronization)
            {
                StartCoroutine(AttemptToContactServerForLoginFromLoginScreen());
            }
            else
            {
                loginButtonIcon.enabled = false;

                mainUI.OnLogin();

                //ViewIScreenController.SetCurrentScreen(this);
                ViewIScreenController.SetPreviousScreen(new VisitedScreens( this,TransitionType.RightToLeft));

                loginButton.image.color = CustomUtilities.HexToColor(mainUI.greenValidButtonColor);
				loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.LoginSucceeded");
				loginServerMessages.text += SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.LoginSucceeded");
                //UserStats.userName = usernameInputField.text;
            }
      
             
        }
        IEnumerator AttemptToContactServerForLoginFromLoginScreen()
        {
            yield return null;
            /*
            StartCoroutine( LoginManager.AttemptToContactServerForLogin());
            while (!LoginManager.GetLoginReplyRecieved() )//|| LoginManager.GetLoginResponse() != null)
            {
                yield return null;
            }
                
            APIResponse res = LoginManager.GetLoginResponse();

            if (!res.error)
            {
                loginServerMessages.text += "Response: " + res.code + "\n";
                if (res.code == "CREDENTIALS_VALID")
                {
                    loginButtonIcon.enabled = false;
                    ViewIScreenController.SetPreviousScreen(new VisitedScreens(this, TransitionType.RightToLeft));

                    mainUI.GetLoadingScreen().ShowThisScreen();

                    mainUI.OnLogin();

                    loginButton.image.color = CustomUtilities.HexToColor(mainUI.greenValidButtonColor);
					loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.LoginSucceeded");
					loginServerMessages.text += SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.LoginSucceeded");
                    UserStats.userName = usernameInputField.text;

                }
                if (res.code == "ACCESS_DENIED")
                {
					fakeSplashScreenImage.enabled = false;
                    loginButtonIconAnimator.enabled = false;
                    loginButtonIcon.sprite = SpriteResources.inputFieldSelectedInvalidSpriteBlack;
                    loginButton.image.color = CustomUtilities.HexToColor(mainUI.redInvalidButtonColor);
					loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.AccessDenied");
					loginServerMessages.text += SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.AccessDenied");

                    loginButton.interactable = true;
                }
                if (res.code == "ACCOUNT_RESET_PENDING")
                {
                    loginButtonIconAnimator.enabled = false;
					fakeSplashScreenImage.enabled = false;
                    loginButtonIcon.sprite = SpriteResources.inputFieldSelectedInvalidSpriteBlack;
                    loginButton.image.color = CustomUtilities.HexToColor(mainUI.redInvalidButtonColor);
					loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.ResetPending");
					loginServerMessages.text += SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.ResetPending");

                    loginButton.interactable = true;
                }
                if (res.code == "ACCOUNT_RESET")
                {
                    Debug.Log(res.raw);
					fakeSplashScreenImage.enabled = false;
                    JSONNode json = JSON.Parse(res.raw);
                    string hash = json["message"];
                    loginButtonIconAnimator.enabled = false;
                    loginButtonIcon.sprite = SpriteResources.inputFieldSelectedInvalidSpriteBlack;
                    loginButton.image.color = CustomUtilities.HexToColor(mainUI.redInvalidButtonColor);
					loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.ResetSuccesful");
					loginServerMessages.text += SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.ResetSuccesful");
                    yield return new WaitForSeconds(1);
                    mainUI.GetPasswordResetScreen().ShowThisScreen();

                    mainUI.GetPasswordResetScreen().SetRecievedPasswordHash(hash);

                    loginButton.interactable = true;
                }
            }
            else
            {
                Debug.LogWarning("Err " + res.code);
				fakeSplashScreenImage.enabled = false;
                loginButtonIconAnimator.enabled = false;
                loginButtonIcon.sprite = SpriteResources.inputFieldSelectedInvalidSpriteBlack;
                loginButton.image.color = CustomUtilities.HexToColor(mainUI.redInvalidButtonColor);
                            
				loginButtonText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("LoginScreen.NotResponding");

                loginServerMessages.text += "Server not responding!!\n";
                loginServerMessages.text += " " + res.code;

                loginButton.interactable = true;


                mainUI.GetLoadingScreen().ShowThisScreen();

                //Cant contact server, attempt to restore saved content
                UserContent.RestoreSavedContent();
            }
             * */
        }

        void CheckPlayerPrefsForExisitingData()
        {
            //Check if username and password has already been stored from a previous login
            if (PlayerPrefs.GetString("username").Length > 0 && PlayerPrefs.GetString("username") != null && PlayerPrefs.GetString("username") != "")
            {
//                Debug.Log("Username exists in player prefs.");

                usernameInputField.text = PlayerPrefs.GetString("username");
            }
            else
            {
                Debug.Log("No username found in player prefs.");
            }


            if (PlayerPrefs.GetString("password").Length > 0 && PlayerPrefs.GetString("password") != null && PlayerPrefs.GetString("password") != "")
            {
               // Debug.Log("Password exists in player prefs.");
                passwordInputField.text = PlayerPrefs.GetString("password");
            }
            else
            {
                Debug.Log("No password found in player prefs.");
            }
        }

        void StoreInPlayerPrefs()
        {
            //Store username in playerprefs
            PlayerPrefs.SetString("username", usernameInputField.text);

            //Store passwordd in playerprefs
            PlayerPrefs.SetString("password", passwordInputField.text);

        }

        //On button click delegates
        public void OnNextButtonClick()
        {
            if (usernameInputField.text != "" && passwordInputField.text != "")
            {
                loginButton.interactable = false;
                Login();
				Debug.Log("Login and then store prefs!!!!!!");
                StoreInPlayerPrefs();
            }
        }
        void OnClickPrivacyStatement()
        {
            mainUI.GetPrivacyStatementScreen().ShowThisScreen();
        }

        void OnClickForgotPassword()
        {
            mainUI.GetRecoverAccountScreen().ShowThisScreen();
        }

        void OnClickAccountHowTo()
        {
            if (mainUI.GetCreateAccountScreen() != null)
            {
                mainUI.GetCreateAccountScreen().ShowThisScreen();
            }
        }

        void OnUsernameFieldUnSelected()
        {
            inputfieldBackgroundUsername.sprite = SpriteResources.inputFieldSprite;
        }
        void OnPasswordFieldUnSelected()
        {
            inputfieldBackgroundPassword.sprite = SpriteResources.inputFieldSprite;
        }


        public InputField GetUsernameInputField()
        {
            return usernameInputField;
        }
        public InputField GetPasswordInputField()
        {
            return passwordInputField;
        }

        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
        public void DisableScreen()
        {
            gameObject.SetActive(false);
        }
        public void EnableScreen()
        {
            gameObject.SetActive(true);
        }
        public void ShowThisScreen()
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.RightToLeft));
        }

        public void ShowPreviousScreen()
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
          
        }
        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}
