﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using SimpleJSON;

namespace Thrive
{
    public class RecoverAccountScreen : MonoBehaviour, IScreen
    {
        MainUI mainUI;
        GameObject mainContent;
        public Button headerBackButton;
        public Button recoverAccountButton;
        public Text recoverButtonText;
        public Image recoverButtonIcon;
        public Text recoverEmailHasBeenSentText;
        public Text headerTitleText;

        public InputField usernameInputField;
        public InputField emailInputField;
        Color recoverButtonColor;

        void Awake()
        {
            mainUI = FindObjectOfType<MainUI>();
            // CreatePrivacyStatement();
            DisableScreen();
            SetUpEventListeners();
            recoverAccountButton.image.color = CustomUtilities.HexToColor(mainUI.greenValidButtonColor);
            recoverButtonIcon.enabled = false;
        }
        void SetUpEventListeners()
        {
            headerBackButton.onClick.AddListener(() => { ShowPreviousScreen(); });
            recoverAccountButton.onClick.AddListener(() => { Recover(); });
        }
        void Recover()
        {
            recoverButtonIcon.enabled = true;
            if (usernameInputField.text != "" || emailInputField.text != "")
            {
                /*
                Thrive.RPC.RequestAccountRecover(usernameInputField.text, emailInputField.text, (res) =>
                {
                    Debug.Log(res.raw);
                    
                    JSONNode json = JSON.Parse(res.raw);
                    if(res.code == "RESET_REQUESTED"){
                        StartCoroutine(ShowRecoverEmailSentText());
                    }
                    //StartCoroutine(UpdateAndDisplayInvitesListTask(json));
                    
                });
                 * */
            }
            else
            {
                recoverAccountButton.image.color = CustomUtilities.HexToColor(mainUI.redInvalidButtonColor);
                recoverButtonText.text = "Invalid username and/or email";
                recoverButtonIcon.sprite = SpriteResources.inputFieldSelectedInvalidSpriteBlack;
            }
        }

        IEnumerator ShowRecoverEmailSentText()
        {
            float counter = 0;
            float fadeTextMultiplier = 3;
            while (counter < 1)
            {
                counter += Time.deltaTime * fadeTextMultiplier;
                recoverEmailHasBeenSentText.color = new Color(recoverEmailHasBeenSentText.color.r, recoverEmailHasBeenSentText.color.g, recoverEmailHasBeenSentText.color.b,counter);
                yield return null;
            }

        }

        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
        public void DisableScreen()
        {
            gameObject.SetActive(false);
        }
        public void EnableScreen()
        {
            gameObject.SetActive(true);
        }
        public void ShowThisScreen()
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.RightToLeft));

        }

        public void ShowPreviousScreen()
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

    }

}