﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive{
	
	public class TutorialScreen : MonoBehaviour, IScreen {
		
		
		MainUI mainUI;

		float lerpCounter = 0;
		float lerpCounterMultiplier = 3;
		bool showLoadingScreen = false;
		bool hideLoadingScreen = false;
        public Text headerTitleText;
        public CanvasGroup canvasGrp;
		
		void Awake()
		{
			mainUI = FindObjectOfType<MainUI>();
			SetUpEventListeners();
            transform.SetParent(mainUI.GetCameraTarget().transform, false);
		}

        void Start()
        {
           
        }
		void SetUpEventListeners()
		{
			//headerBackButton.onClick.AddListener(() => { ShowPreviousScreen(); });
		}
		

		
		IEnumerator ShowTutorialScreenTask()
		{
			
			((RectTransform)gameObject.transform).anchoredPosition3D = new Vector3(0, 0, 0);
			CanvasGroup canvasGrp = gameObject.GetComponent<CanvasGroup>();
			while (lerpCounter < 1 && showLoadingScreen)
			{
				lerpCounter += Time.deltaTime * lerpCounterMultiplier;
				
				canvasGrp.alpha = lerpCounter;
				yield return null;
			}
			
			canvasGrp.alpha = 1;
			lerpCounter = 0;
		}
		
		IEnumerator HideTutorialScreenTask()
		{
			
			((RectTransform)gameObject.transform).anchoredPosition3D = new Vector3(0, 0, 0);
			CanvasGroup canvasGrp = gameObject.GetComponent<CanvasGroup>();
			lerpCounter = 0;
			while (lerpCounter < 1 && hideLoadingScreen)
			{
				lerpCounter += Time.deltaTime * lerpCounterMultiplier;
				
				canvasGrp.alpha = 1 - lerpCounter;
				yield return null;
			}
			
			canvasGrp.alpha = 0;
			lerpCounter = 0;
			
			DisableScreen ();
		}

        public void HideThisScreen()
        {
            showLoadingScreen = false;
            hideLoadingScreen = true;
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine(HideTutorialScreenTask());
            }
        }

		/////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
		public void DisableScreen()
		{
			gameObject.SetActive(false);
		}
		public void EnableScreen()
		{
			gameObject.SetActive(true);
			
		}
		public void ShowThisScreen()
		{
			EnableScreen();
			showLoadingScreen = true;
			hideLoadingScreen = false;
        //    SmartLocalization.LanguageManager.Instance.ChangeLanguage(SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode);
			StartCoroutine(ShowTutorialScreenTask());
			
		}
		
		public void ShowPreviousScreen()
		{
            HideThisScreen();
		}
		
		public GameObject GetGameObject()
		{
			return gameObject;
		}
	}
}
