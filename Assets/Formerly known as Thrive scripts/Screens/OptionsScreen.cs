﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class OptionsScreen : MonoBehaviour, IScreen
    {
        MainUI mainUI;
        public Button backButton;
        public Button familyButton;
        public Button inviteStatusButton;
        public Button privacyButton;
        public Button updateAccountButton;
        public Button aboutButton;
        public Text headerTitleText;

  //      public FamilyScreen familyScreenPrefab;
        public CreateKidProfileScreen createKidProfilePrefab;
//        public InviteExistingUserScreen inviteExistingUserPrefab;
 //       public InviteStatusScreen inviteStatusScreenPrefab;
  //      public UpdateAccountScreen updateAccountScreenPrefab;
        public AboutScreen aboutScreenPrefab;
        
//        FamilyScreen instantiatedFamilyScreen;
        CreateKidProfileScreen instatiatedCreateKidProfile;
//        InviteExistingUserScreen instantiatedInviteExistingUser;
//        InviteStatusScreen instantiatedInviteStatusScreen;
   //     UpdateAccountScreen updateAccountScreen;
        AboutScreen aboutScreen;

        // Use this for initialization
        void Awake()
        {
            mainUI = FindObjectOfType<MainUI>();
            SetupEventListeners();
        }

        void SetupEventListeners()
        {
            backButton.onClick.AddListener(() => { OnClickBackButton(); });
            familyButton.onClick.AddListener(() => { OnClickFamilyButton(); });
            inviteStatusButton.onClick.AddListener(() => {OnClickInviteStatusButton(); });
            privacyButton.onClick.AddListener(() => { mainUI.GetPrivacyStatementScreen().OnPrivacyStatementClick(); });
            updateAccountButton.onClick.AddListener(() => { OnAccountUpdateButton(); });
            aboutButton.onClick.AddListener(() => { OnAboutButton(); });
        }




        void OnClickBackButton()
        {
            ShowPreviousScreen();
        }

        void OnClickFamilyButton()
        {
       //     GetFamilyScreen().ShowThisScreen();
        }

        void OnClickInviteStatusButton()
        {
         //   GetInvitationStatusScreen().ShowThisScreen();
        }

        void OnAccountUpdateButton()
        {
        //    GetUpdateAccountScreen().ShowThisScreen();
        }

        void OnAboutButton()
        {
            GetAboutScreen().ShowThisScreen();
        }
        /*
        public FamilyScreen GetFamilyScreen()
        {
            if (instantiatedFamilyScreen == null)
            {
                instantiatedFamilyScreen = (FamilyScreen)Instantiate(familyScreenPrefab);

                instantiatedFamilyScreen.transform.SetParent(mainUI.mainCanvas.transform, false);
                ((RectTransform)instantiatedFamilyScreen.transform).anchoredPosition3D = new Vector3(mainUI.mainCanvas.pixelRect.width, ((RectTransform)instantiatedFamilyScreen.transform).anchoredPosition3D.y, ((RectTransform)instantiatedFamilyScreen.transform).anchoredPosition3D.z);

            }
            return instantiatedFamilyScreen;
        }
         * */

        public CreateKidProfileScreen GetInstatiatedCreateKidProfile()
        {
            if (instatiatedCreateKidProfile == null)
            {
                instatiatedCreateKidProfile = (CreateKidProfileScreen)Instantiate(createKidProfilePrefab, Vector3.zero, Quaternion.identity);

                instatiatedCreateKidProfile.transform.SetParent(mainUI.mainCanvas.transform, false);
                ((RectTransform)instatiatedCreateKidProfile.transform).anchoredPosition3D = new Vector3(Screen.width, 0, 0);

            }

            return instatiatedCreateKidProfile;
        }
        public AboutScreen GetAboutScreen()
        {
            if (aboutScreen == null)
            {
                aboutScreen = (AboutScreen)Instantiate(aboutScreenPrefab,Vector3.zero,Quaternion.identity);

                aboutScreen.transform.SetParent(mainUI.mainCanvas.transform, false);
                ((RectTransform)aboutScreen.transform).anchoredPosition3D = new Vector3(Screen.width, 0, 0);

            }
            return aboutScreen;
        }
        /*
        public UpdateAccountScreen GetUpdateAccountScreen()
        {
            if (updateAccountScreen == null)
            {
                updateAccountScreen = (UpdateAccountScreen)Instantiate(updateAccountScreenPrefab);

                updateAccountScreen.transform.SetParent(mainUI.mainCanvas.transform, false);
                ((RectTransform)updateAccountScreen.transform).anchoredPosition3D = new Vector3(Screen.width, 0, 0);

            }
            return updateAccountScreen;
        }
         * */
        /*
        public InviteStatusScreen GetInvitationStatusScreen()
        {
            if (instantiatedInviteStatusScreen == null)
            {
                instantiatedInviteStatusScreen = (InviteStatusScreen)Instantiate(inviteStatusScreenPrefab);

                instantiatedInviteStatusScreen.transform.SetParent(mainUI.mainCanvas.transform, false);
                ((RectTransform)instantiatedInviteStatusScreen.transform).anchoredPosition3D = new Vector3(Screen.width, 0,0);

            }
            return instantiatedInviteStatusScreen;
        }


        public InviteExistingUserScreen GetInstatiatedInviteExistingUser()
        {
            if (instantiatedInviteExistingUser == null)
            {
                instantiatedInviteExistingUser = (InviteExistingUserScreen)Instantiate(inviteExistingUserPrefab);

                instantiatedInviteExistingUser.transform.SetParent(mainUI.mainCanvas.transform, false);
                ((RectTransform)instantiatedInviteExistingUser.transform).anchoredPosition3D = new Vector3(Screen.width, 0, 0);
            }
            return instantiatedInviteExistingUser;
        }

        public InviteStatusScreen GetInstantiatedInviteStatusScreen()
        {
            if (instantiatedInviteStatusScreen == null)
            {
                instantiatedInviteStatusScreen = (InviteStatusScreen)Instantiate(inviteStatusScreenPrefab);

                instantiatedInviteStatusScreen.transform.SetParent(mainUI.mainCanvas.transform, false);
                ((RectTransform)instantiatedInviteStatusScreen.transform).anchoredPosition3D = new Vector3(mainUI.mainCanvas.pixelRect.width, ((RectTransform)instantiatedInviteStatusScreen.transform).anchoredPosition3D.y, ((RectTransform)instantiatedInviteStatusScreen.transform).anchoredPosition3D.z);

            }
            return instantiatedInviteStatusScreen;
        }
         * */
        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
        public void DisableScreen()
        {
            gameObject.SetActive(false);
        }
        public void EnableScreen()
        {
            gameObject.SetActive(true);
        }
        public void ShowThisScreen()
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.Fade));
        }

        public void ShowPreviousScreen()
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
        }
        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}
