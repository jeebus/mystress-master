﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{



    public class FadeInOutScreen : MonoBehaviour, IScreen
    {


        MainUI mainUI;
        //public Button headerBackButton;
        public Text headerTitleText;

        float lerpCounter = 0;
        float lerpCounterMultiplier = 0.5f;
        bool showLoadingScreen = false;
        bool hideLoadingScreen = false;
        float timeToWaitBeforeInit = 0.0f;
        bool waitingDone = false;
        bool sceneStarting = false;
        bool sceneEnding = false;
        public bool fadeCycleRunning= false;

        CanvasGroup canvasGrp;

        void Awake()
        {
            mainUI = FindObjectOfType<MainUI>();
            SetUpEventListeners();
            canvasGrp = gameObject.GetComponent<CanvasGroup>();
        }

        public CanvasGroup GetCanvasGroup()
        {
            return canvasGrp;
        }

        void SetUpEventListeners()
        {
            //headerBackButton.onClick.AddListener(() => { ShowPreviousScreen(); });
        }


        IEnumerator ShowBlankScreenTask()
        {

            ((RectTransform)gameObject.transform).anchoredPosition3D = new Vector3(0, 0, 0);

            while (lerpCounter < 1 && showLoadingScreen)
            {
                lerpCounter += Time.deltaTime * lerpCounterMultiplier;

                canvasGrp.alpha = lerpCounter;
                yield return null;
            }

            canvasGrp.alpha = 1;
            lerpCounter = 0;

            sceneStarting = false;
            sceneEnding = true;
        }

        IEnumerator HideBlankScreenTask()
        {

            ((RectTransform)gameObject.transform).anchoredPosition3D = new Vector3(0, 0, 0);
            CanvasGroup canvasGrp = gameObject.GetComponent<CanvasGroup>();
            lerpCounter = 0;
            while (lerpCounter < 1 && hideLoadingScreen)
            {
                lerpCounter += Time.deltaTime * lerpCounterMultiplier;

                canvasGrp.alpha = 1 - lerpCounter;
                yield return null;
            }

            canvasGrp.alpha = 0;
            lerpCounter = 0;

            DisableScreen();
            sceneEnding = false;
        }


        public void RunFadeCycle()
        {
            StartCoroutine(RunFadeCycleTask());
        }

        IEnumerator RunFadeCycleTask()
        {
            sceneStarting = true;
            fadeCycleRunning = true;

            while (timeToWaitBeforeInit > 0)
            {
                timeToWaitBeforeInit -= Time.deltaTime;
                yield return null;
            }
            if (timeToWaitBeforeInit <= 0)
            {
                waitingDone = true;
            }

            if (waitingDone)
            {
                if (sceneStarting)
                {
                    ShowThisScreen();
                }
                while (!sceneEnding)
                {
                    yield return null;
                }

                mainUI.GetLoginScreen().DisableScreen();

                ShowPreviousScreen();

                while (sceneEnding)
                {
                    yield return null;
                }
                
            }
            fadeCycleRunning = false;
        }

        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
        public void DisableScreen()
        {
            gameObject.SetActive(false);
        }
        public void EnableScreen()
        {
            gameObject.SetActive(true);
            // transform.SetParent(mainUI.mainCanvas.transform, false);

        }
        public void ShowThisScreen()
        {
            EnableScreen();
            showLoadingScreen = true;
            hideLoadingScreen = false;

            StartCoroutine(ShowBlankScreenTask());
        }

        public void ShowPreviousScreen()
        {
            showLoadingScreen = false;
            hideLoadingScreen = true;
            EnableScreen();
            StartCoroutine(HideBlankScreenTask());

    
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}