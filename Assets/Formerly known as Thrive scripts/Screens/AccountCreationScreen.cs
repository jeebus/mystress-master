﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    public class AccountCreationScreen : MonoBehaviour, IScreen
    {
        MainUI mainUI;
        public Text headerTitleText;

        GameObject header;
        GameObject mainContent;
        public Button createAccountButton;
        public Button acceptTermsButton;
        public Button privacyButton;

        public Button headerBackButton;

        IScreen previousScreen;

        public InputField accountCreateUsernameField;
        public InputField accountCreateRealnameField;
        public InputField accountCreatePasswordField;
        public InputField accountCreateRepeatPasswordField;
        public InputField accountCreateEmailField;



        Image nameInputfieldImage;
		Image realNameInputfieldImage;
        Image passwordInputfieldImage;
        Image repeatPasswordInputfieldImage;
        Image emailInputfieldImage;
        Image createAccountButtonImage;


        public Image verifiedUsernameIconImage;
        public Image verifiedPasswordIconImage;
        public Image verifiedRepeatPasswordIconImage;
        public Image verifiedEmailIconImage;
        public Image verifiedAcceptedTermsIconImage;

        string createAccountCompleteText = "Account created";

        int previousUsernameLength = 0;
//        int previousEmailLength = 0;
        public bool conditionAndTermsAreAccepted = false;


        bool isUserNameAvailable = false;
        bool isPasswordValid = false;
        bool isEmailValid = false;
		CanvasGroup createAccountButtonCanvasGrp;


        // Use this for initialization
        void Awake()
        {
            mainUI = FindObjectOfType<MainUI>();
            nameInputfieldImage = accountCreateUsernameField.GetComponent<Image>();
			realNameInputfieldImage = accountCreateRealnameField.GetComponent<Image>();
            passwordInputfieldImage = accountCreatePasswordField.GetComponent<Image>();
            repeatPasswordInputfieldImage = accountCreateRepeatPasswordField.GetComponent<Image>();
            emailInputfieldImage = accountCreateEmailField.GetComponent<Image>();
            createAccountButtonImage = createAccountButton.GetComponent<Image>();

            SetUpEventListeners();

			createAccountButtonCanvasGrp = createAccountButton.gameObject.AddComponent<CanvasGroup>();


#if UNITY_ANDROID
            accountCreateUsernameField.shouldHideMobileInput = false;
            accountCreateRealnameField.shouldHideMobileInput = false;
            accountCreatePasswordField.shouldHideMobileInput = false;
            accountCreateRepeatPasswordField.shouldHideMobileInput = false;
            accountCreateEmailField.shouldHideMobileInput = false;
#endif
#if UNITY_IPHONE
            accountCreateUsernameField.shouldHideMobileInput = true;
            accountCreateRealnameField.shouldHideMobileInput = true;
            accountCreatePasswordField.shouldHideMobileInput = true;
            accountCreateRepeatPasswordField.shouldHideMobileInput = true;
            accountCreateEmailField.shouldHideMobileInput = true;
#endif
        }

        void SetUpEventListeners()
        {
            
            headerBackButton.onClick.AddListener(() => { ShowPreviousScreen(); });

            accountCreateUsernameField.onValueChange.AddListener(delegate
            {
                OnChangedInputUsername();
            });
            accountCreateUsernameField.onEndEdit.AddListener(delegate
            {
                OnUsernameFieldUnSelected();
            });

            accountCreateUsernameField.gameObject.AddComponent<EventTrigger>();
            accountCreateUsernameField.gameObject.AddComponent<InputfieldSelected>();
			accountCreateUsernameField.gameObject.AddComponent<DisableInputfieldPlaceholderText>();



			accountCreateRealnameField.onEndEdit.AddListener(delegate
			                                                 {
				OnRealnameFieldUnSelected();
			});
			accountCreateRealnameField.gameObject.AddComponent<EventTrigger>();
			accountCreateRealnameField.gameObject.AddComponent<InputfieldSelected>();
			accountCreateRealnameField.gameObject.AddComponent<DisableInputfieldPlaceholderText>();




            accountCreatePasswordField.onValueChange.AddListener(delegate
            {
                OnChangedInputPassword();
            });
            accountCreatePasswordField.onEndEdit.AddListener(delegate
            {
                OnPasswordFieldUnSelected();
            });
            accountCreatePasswordField.gameObject.AddComponent<EventTrigger>();
            accountCreatePasswordField.gameObject.AddComponent<InputfieldSelected>();
			accountCreatePasswordField.gameObject.AddComponent<DisableInputfieldPlaceholderText>();



            accountCreateRepeatPasswordField.gameObject.AddComponent<EventTrigger>();
            accountCreateRepeatPasswordField.gameObject.AddComponent<InputfieldSelected>();
			accountCreateRepeatPasswordField.gameObject.AddComponent<DisableInputfieldPlaceholderText>();
			accountCreateRepeatPasswordField.onValueChange.AddListener(delegate
            {
                OnChangedInputRepeatPassword();
            });

            accountCreateRepeatPasswordField.onEndEdit.AddListener(delegate
            {
                OnRepeatPasswordFieldUnSelected();
            });

            accountCreateEmailField.onValueChange.AddListener(delegate
            {
                OnChangedInputEmail();
            });
            accountCreateEmailField.onEndEdit.AddListener(delegate
            {
                OnEmailFieldUnSelected();
            });
            accountCreateEmailField.gameObject.AddComponent<EventTrigger>();
            accountCreateEmailField.gameObject.AddComponent<InputfieldSelected>();
			accountCreateEmailField.gameObject.AddComponent<DisableInputfieldPlaceholderText>();

            createAccountButton.onClick.AddListener(() => { OnCreateNewAccount(); });
            acceptTermsButton.onClick.AddListener(() => { OnAgreementLinkClick(); });
            privacyButton.onClick.AddListener(() => { mainUI.GetPrivacyStatementScreen().OnPrivacyStatementClick(); });

        }

        /*
        IEnumerator ShowAccountCreateScreenAfterFade()
        {
            while (mainUI.GetFaderScreen().fadeCounter > 0)
            {
                yield return null;
            }
            mainUI.GetFaderScreen().sceneStarting = true;
            gameObject.SetActive(false);
        }
        */
        // Update is called once per frame
        void Update()
        {
            if (isUserNameAvailable && isPasswordValid && isEmailValid && conditionAndTermsAreAccepted)
            {
                createAccountButtonImage.color = CustomUtilities.HexToColor(mainUI.greenValidButtonColor);
				createAccountButton.interactable = true;
				createAccountButtonCanvasGrp.alpha = 1.0f;

            } else {
                createAccountButtonImage.color = Color.clear;
				createAccountButton.interactable = false;
				createAccountButtonCanvasGrp.alpha = 0.2f;
            }
        }

        void StoreInPlayerPrefs()
        {
            //Store username in playerprefs
            PlayerPrefs.SetString("username", accountCreateUsernameField.text);

            //Store passwordd in playerprefs
            PlayerPrefs.SetString("password", accountCreatePasswordField.text);

        }

        void OnCreateNewAccount()
        {
            if (isUserNameAvailable && isPasswordValid && isEmailValid && conditionAndTermsAreAccepted)
            {
                Debug.Log("Everything seems good, create account.");
                /*
                RPC.CreateUser(accountCreateUsernameField.text,accountCreateRealnameField.text,  Thrive.Crypto.Utils.MD5(accountCreatePasswordField.text), accountCreateEmailField.text, (res2) =>
                {
                    if (!res2.error)
                    {
                        Debug.Log("Try create account");
                      // mainUI.GetFaderScreen().sceneEnding = true;
                        mainUI.GetLoginScreen().GetUsernameInputField().text = accountCreateUsernameField.text;
                        mainUI.GetLoginScreen().GetPasswordInputField().text = accountCreatePasswordField.text;
                        StoreInPlayerPrefs();

                       CompletedFlowScreen completedFlowScreen = mainUI.GetCompletedFlowScreen();
						completedFlowScreen.SetFlowSpecificText(SmartLocalization.LanguageManager.Instance.GetTextValue("CompletedFlowScreen.AccountCreated"));
                       completedFlowScreen.accountSuccessfullyCreated = true;

                       completedFlowScreen.ShowThisScreen();


                    }
                    else
                    {
                        Debug.LogWarning("Err " + res2.message);
                    }

                });
                 * */
            }
        }

     void OnChangedInputUsername()
     {
         if (accountCreateUsernameField.text.Length != previousUsernameLength && accountCreateUsernameField.text != "Required")
         {
             
             if (verifiedUsernameIconImage.enabled == false)
             {
                 verifiedUsernameIconImage.enabled = true;
                 UsernameFieldSelected();
             }
             if (InputfieldChecker.IsUsername(accountCreateUsernameField.text))
             {
                 

                 bool includeToken = false;
                 /*
                 Thrive.API.JSONRequest("GET", "/username/" + accountCreateUsernameField.text, includeToken, (res) =>
                 {
                     if (!res.error)
                     {
                         if (res.code == "USERNAME_TAKEN")
                         {
                             verifiedUsernameIconImage.sprite = SpriteResources.verifiedInputFailedIcon;
                             UsernameFieldSelectedInvalidInput();
                             isUserNameAvailable = false;
                         }

                         if (res.code == "USERNAME_AVAILABLE")
                         {
                             verifiedUsernameIconImage.sprite = SpriteResources.verifiedInputPassedIcon;
                             isUserNameAvailable = true;
                             UsernameFieldSelected();
                         }
                     }
                     else
                     {
                         Debug.LogWarning("Err " + res.message + " code " + res.code);
                     }
                 });
                  * */
             }
         }
         if ((!InputfieldChecker.IsUsername(accountCreateUsernameField.text) && accountCreateUsernameField.text.Length > 0) || accountCreateUsernameField.text.Length == 0)
         {
             UsernameFieldSelectedInvalidInput();
             verifiedUsernameIconImage.sprite = SpriteResources.verifiedInputFailedIcon;
             isUserNameAvailable = false;
         }
         previousUsernameLength = accountCreateUsernameField.text.Length;
     }

     void OnChangedInputEmail()
     {

         if (accountCreateEmailField.text.Length > 0 && accountCreateEmailField.text != "Required" && accountCreateEmailField.text != "")
         {
             if (verifiedEmailIconImage.enabled == false)
             {
                 verifiedEmailIconImage.enabled = true;
                 EmailFieldSelected();
             }
             if (InputfieldChecker.IsEmail(accountCreateEmailField.text))
             {
                 //previousEmailLength = accountCreateEmailField.text.Length;
                // verifiedEmailIconImage.sprite = mainUI.verifiedInputPassedIcon;
                 EmailFieldSelected();
                // isEmailValid = true;

					bool includeToken = false;
                 /*
					Thrive.API.JSONRequest("GET", "/email/" + accountCreateEmailField.text, includeToken, (res) =>
					                       {
						if (!res.error)
						{
							if (res.code == "EMAIL_TAKEN")
							{
                                verifiedEmailIconImage.sprite = SpriteResources.verifiedInputFailedIcon;
								UsernameFieldSelectedInvalidInput();
								isEmailValid = false;
							}
							
							if (res.code == "EMAIL_AVAILABLE")
							{
                                verifiedEmailIconImage.sprite = SpriteResources.verifiedInputPassedIcon;
								isEmailValid = true;
								UsernameFieldSelected();
							}
						}
						else
						{
							Debug.LogWarning("Err " + res.message + " code " + res.code);
						}
					});
                 */

             }
             else
             {
                 verifiedEmailIconImage.sprite = SpriteResources.verifiedInputFailedIcon;
                 EmailFieldSelectedInvalidInput();
                 isEmailValid = false;
             }
         }
     }

     void OnChangedInputPassword()
     {
         if (accountCreatePasswordField.text == "" || accountCreateRepeatPasswordField.text == "" )
         {
             verifiedPasswordIconImage.sprite = SpriteResources.verifiedInputFailedIcon;
             PasswordFieldSelectedInvalidInput();
             isPasswordValid = false;
         }
         else
         {
             if (verifiedPasswordIconImage.enabled == false)
             {
                 verifiedPasswordIconImage.enabled = true;
                 PasswordFieldSelected();
					Debug.Log("Enable verify password icon image");
             }
            if (accountCreatePasswordField.text != accountCreateRepeatPasswordField.text || !InputfieldChecker.IsPassword(accountCreatePasswordField.text))
            {
                verifiedPasswordIconImage.sprite = SpriteResources.verifiedInputFailedIcon;
                PasswordFieldSelectedInvalidInput();
                isPasswordValid = false;
					Debug.Log("Password invalid, set icon to reflect this. Password: " + accountCreatePasswordField.text + " repeat password: " + accountCreateRepeatPasswordField.text );
                return;
            }

            if (string.Compare( accountCreatePasswordField.text, accountCreateRepeatPasswordField.text) == 0 && InputfieldChecker.IsPassword(accountCreatePasswordField.text))
            {
                verifiedPasswordIconImage.sprite = SpriteResources.verifiedInputPassedIcon;
                PasswordFieldSelected();
                isPasswordValid = true;
					Debug.Log("Password valid! Password: " + accountCreatePasswordField.text + " repeat password: " + accountCreateRepeatPasswordField.text );
            }
         }
     }

		void OnChangedInputRepeatPassword()
		{
			if (accountCreatePasswordField.text == "" || accountCreateRepeatPasswordField.text == "" )
			{
                verifiedPasswordIconImage.sprite = SpriteResources.verifiedInputFailedIcon;
				RepeatPasswordFieldSelectedInvalidInput();
				isPasswordValid = false;
			}
			else
			{
				if (verifiedPasswordIconImage.enabled == false)
				{
					verifiedPasswordIconImage.enabled = true;
					RepeatPasswordFieldSelected();
					Debug.Log("Enable verify password icon image");

				}
				if (accountCreatePasswordField.text != accountCreateRepeatPasswordField.text || !InputfieldChecker.IsPassword(accountCreateRepeatPasswordField.text))
				{
                    verifiedPasswordIconImage.sprite = SpriteResources.verifiedInputFailedIcon;
					RepeatPasswordFieldSelectedInvalidInput();
					isPasswordValid = false;
					Debug.Log("Password invalid, set icon to reflect this. Password: " + accountCreatePasswordField.text + " repeat password: " + accountCreateRepeatPasswordField.text );
					return;
				}
				
				if (string.Compare( accountCreatePasswordField.text, accountCreateRepeatPasswordField.text) == 0 && InputfieldChecker.IsPassword(accountCreateRepeatPasswordField.text))
				{
                    verifiedPasswordIconImage.sprite = SpriteResources.verifiedInputPassedIcon;

					RepeatPasswordFieldSelected();
					isPasswordValid = true;
					OnPasswordFieldUnSelected();
					Debug.Log("Password valid! Password: " + accountCreatePasswordField.text + " repeat password: " + accountCreateRepeatPasswordField.text );
				}
			}
		}
		
		
		
		
		
		void OnAgreementLinkClick()
		{
			mainUI.GetTermsAgreementScreen().ShowThisScreen();
		}
		
		void OnBackToLoginScreen()
		{
			
			ShowLoginScreen();
		}
		
		void ShowLoginScreen()
     {
         mainUI.GetFaderScreen().RunFadeCycle();
         mainUI.GetCreateAccountScreen().DisableScreen();
     }

     void OnUsernameFieldUnSelected()
     {
         if (isUserNameAvailable || accountCreateUsernameField.text == "")
         {
             nameInputfieldImage.sprite = SpriteResources.inputFieldSprite;
             accountCreateUsernameField.text = accountCreateUsernameField.text;
         }
     }

	void OnRealnameFieldUnSelected()
     {

         realNameInputfieldImage.sprite = SpriteResources.inputFieldSprite;
         
     }
     void UsernameFieldSelected()
     {
         nameInputfieldImage.sprite = SpriteResources.inputFieldSelectedSprite;
     }
     void UsernameFieldSelectedInvalidInput()
     {
         nameInputfieldImage.sprite = SpriteResources.inputFieldSelectedInvalidSprite;
     }

     void OnPasswordFieldUnSelected()
     {
         if (isPasswordValid || accountCreatePasswordField.text == "")
         {
             passwordInputfieldImage.sprite = SpriteResources.inputFieldSprite;
            //repeatPasswordInputfieldImage.sprite = mainUI.inputFieldSprite;
         }
     }
		void OnRepeatPasswordFieldUnSelected()
		{
			if (isPasswordValid || accountCreateRepeatPasswordField.text == "")
			{
				//passwordInputfieldImage.sprite = mainUI.inputFieldSprite;
                repeatPasswordInputfieldImage.sprite = SpriteResources.inputFieldSprite;
			}
		}
		void PasswordFieldSelected()
		{
            passwordInputfieldImage.sprite = SpriteResources.inputFieldSelectedSprite;
        
     }
     void PasswordFieldSelectedInvalidInput()
     {
         passwordInputfieldImage.sprite = SpriteResources.inputFieldSelectedInvalidSprite;
         
     }

		void RepeatPasswordFieldSelected()
		{
            repeatPasswordInputfieldImage.sprite = SpriteResources.inputFieldSelectedSprite;
		}
		void RepeatPasswordFieldSelectedInvalidInput()
		{
            repeatPasswordInputfieldImage.sprite = SpriteResources.inputFieldSelectedInvalidSprite;
		}
		
	 void OnEmailFieldUnSelected()
	 {
         if (isEmailValid || accountCreateEmailField.text == "")
         {
             emailInputfieldImage.sprite = SpriteResources.inputFieldSprite;
         }
     }
     void EmailFieldSelected()
     {
         emailInputfieldImage.sprite = SpriteResources.inputFieldSelectedSprite;
     }
     void EmailFieldSelectedInvalidInput()
     {
         emailInputfieldImage.sprite = SpriteResources.inputFieldSelectedInvalidSprite;
     }

     /////////////////////IScreen interface implementation////////////////////////////////
     public string GetScreenHeaderTitle()
     {
         if (headerTitleText != null)
         {
             return headerTitleText.text;
         }
         else
         {
             return "";
         }
     }
     public void DisableScreen()
     {
         /*
         Destroy(this);
         if (gameObject != null)
         {
             Destroy(gameObject);
         }
         */
         gameObject.SetActive(false);
     }
     public void EnableScreen()
     {
         gameObject.SetActive(true);
     }

     public void ShowThisScreen()
     {
         EnableScreen();
         StartCoroutine(ViewIScreenController.ShowScreen(this,TransitionType.RightToLeft));
     }

     public void ShowPreviousScreen()
     {
         StartCoroutine(ViewIScreenController.ShowPreviousScreen());
     }
     public GameObject GetGameObject()
     {
         return gameObject;
     }

    }
}
