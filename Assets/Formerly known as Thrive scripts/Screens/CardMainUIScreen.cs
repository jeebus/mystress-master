﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class CardMainUIScreen : MonoBehaviour, IScreen
    {
        public Text headerTitleText;

        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
        public void DisableScreen()
        {
            //gameObject.SetActive(false);
        }
        public void EnableScreen()
        {
            //gameObject.SetActive(true);
        }
        public void ShowThisScreen()
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.RightToLeft));
        }

        public void ShowPreviousScreen()
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
        }
        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }

}