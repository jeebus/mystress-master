﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class PrivacyStatementScreen : MonoBehaviour, IScreen
    {
        MainUI mainUI;
        GameObject mainContent;
        public Button headerBackButton;
        public Text headerTitleText;
        IScreen previousScreen;

        // Use this for initialization
        void Awake()
        {
            mainUI = FindObjectOfType<MainUI>();
            SetUpEventListeners();
            
        }

        void Start()
        {

          //  GetComponentInChildren<Scrollbar>().value = 1;
        }

        void SetUpEventListeners()
        {
            headerBackButton.onClick.AddListener(() => {  ShowPreviousScreen(); });
        }


        public void OnPrivacyStatementClick()
        {
            EnableScreen();
            ShowThisScreen();
        }

        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }

        public void DisableScreen()
        {
          //  Destroy(gameObject);
            gameObject.SetActive(false);
        }
        public void EnableScreen()
        {
            gameObject.SetActive(true);
           // transform.SetParent(mainUI.mainCanvas.transform);
        }

        public void ShowThisScreen()
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.RightToLeft));
 
        }

        public void ShowPreviousScreen()
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
        }
        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}