﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive{
    public class AboutScreen : MonoBehaviour, IScreen {

            MainUI mainUI;
            public Button headerBackButton;
            public Text headerTitleText;

            void Awake()
            {
                mainUI = FindObjectOfType<MainUI>();
                DisableScreen();
                SetUpEventListeners();
            }
            void SetUpEventListeners()
            {
                headerBackButton.onClick.AddListener(() => { ShowPreviousScreen(); });
            }
            /////////////////////IScreen interface implementation////////////////////////////////
            public string GetScreenHeaderTitle()
            {
                if (headerTitleText != null)
                {
                    return headerTitleText.text;
                }
                else
                {
                    return "";
                }
            }

            public void DisableScreen()
            {
                gameObject.SetActive(false);
            }
            public void EnableScreen()
            {
                gameObject.SetActive(true);
                transform.SetParent(mainUI.mainCanvas.transform, false);
            }
            public void ShowThisScreen()
            {
                EnableScreen();
                StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.RightToLeft));
            }

            public void ShowPreviousScreen()
            {
                StartCoroutine(ViewIScreenController.ShowPreviousScreen());
            }

            public GameObject GetGameObject()
            {
                return gameObject;
            }
        }
}
