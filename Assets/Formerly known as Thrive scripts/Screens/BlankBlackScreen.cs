﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{

    public class BlankBlackScreen : MonoBehaviour, IScreen
    {


        MainUI mainUI;
        //public Button headerBackButton;
        public Text loadingDotsText;
        public Text headerTitleText;

        float lerpCounter = 0;
        float lerpCounterMultiplier = 3.0f;
        bool showLoadingScreen = false;
        bool hideLoadingScreen = false;
        float maxFadedAlpha = 1.0f;
        CanvasGroup canvasGrp;
        bool transitionIsRunning = false;

        void Awake()
        {
            mainUI = FindObjectOfType<MainUI>();
            SetUpEventListeners();
            canvasGrp = gameObject.GetComponent<CanvasGroup>();
        }

        public CanvasGroup GetCanvasGroup()
        {
            return canvasGrp;
        }

        void SetUpEventListeners()
        {
            //headerBackButton.onClick.AddListener(() => { ShowPreviousScreen(); });
        }

        public bool IsTransitionRunning()
        {
            return transitionIsRunning;
        }

        IEnumerator ShowBlankScreenTask()
        {
            transitionIsRunning = true;
            ((RectTransform)gameObject.transform).anchoredPosition3D = new Vector3(0, 0, 0);
            
            while (lerpCounter < 1 && showLoadingScreen)
            {
               
                lerpCounter += Time.deltaTime * lerpCounterMultiplier;

                canvasGrp.alpha = Mathf.Min(maxFadedAlpha, lerpCounter);
                Debug.Log("Show blank screen, alpha: " + canvasGrp.alpha );
                Debug.Log("Show blank screen, lerpcounter: " + lerpCounter);
                yield return null;
            }

            canvasGrp.alpha = maxFadedAlpha;
            lerpCounter = 0;
            transitionIsRunning = false;
        }

        IEnumerator HideBlankScreenTask()
        {
            transitionIsRunning = true;
            ((RectTransform)gameObject.transform).anchoredPosition3D = new Vector3(0, 0, 0);
            CanvasGroup canvasGrp = gameObject.GetComponent<CanvasGroup>();
            lerpCounter = 0;
            while (lerpCounter < 1 && hideLoadingScreen)
            {
                lerpCounter += Time.deltaTime * lerpCounterMultiplier;

                canvasGrp.alpha = Mathf.Min(maxFadedAlpha, 1 - lerpCounter);
                yield return null;
            }

            canvasGrp.alpha = 0;
            lerpCounter = 0;
            transitionIsRunning = false;
            DisableScreen();
        }


        public void SetAlphaAmount(float newMaxFadedAlpha)
        {
            maxFadedAlpha = newMaxFadedAlpha;
        }
        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
        public void DisableScreen()
        {
            gameObject.SetActive(false);
        }
        public void EnableScreen()
        {
            gameObject.SetActive(true);
           // transform.SetParent(mainUI.mainCanvas.transform, false);

        }
        public void ShowThisScreen()
        {
            EnableScreen();
            showLoadingScreen = true;
            hideLoadingScreen = false;

            StartCoroutine(ShowBlankScreenTask());
        }

        public void ShowPreviousScreen()
        {
            showLoadingScreen = false;
            hideLoadingScreen = true;

            StartCoroutine(HideBlankScreenTask());
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}
