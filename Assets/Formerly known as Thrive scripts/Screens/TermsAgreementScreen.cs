﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    public class TermsAgreementScreen : MonoBehaviour, IScreen
    {
        MainUI mainUI;
        public Scrollbar acceptTermsScrollBar;

        public Button headerBackButton;
        public Button faqButton;
        public Button acceptTermsButton;
        public Button privacyButton;
        public Text headerTitleText;
        public Image verifiedAcceptedTermsIconImage;

        public Text fontSizeCalculation;
        public Text mainTextArea;

        // Use this for initialization
        void Awake()
        {
            mainUI = FindObjectOfType<MainUI>();
            SetUpEventListeners();
           
        }

        IEnumerator Start()
        {

           yield return new WaitForSeconds(0.1f);
           
            mainTextArea.resizeTextMinSize = fontSizeCalculation.cachedTextGenerator.fontSizeUsedForBestFit;
            mainTextArea.resizeTextMaxSize = fontSizeCalculation.cachedTextGenerator.fontSizeUsedForBestFit;
            mainTextArea.rectTransform.anchoredPosition = new Vector2(mainTextArea.rectTransform.anchoredPosition.x, -mainTextArea.rectTransform.rect.height / 2);

        }

        void OnEnable()
        {
            mainTextArea.rectTransform.anchoredPosition = new Vector2(mainTextArea.rectTransform.anchoredPosition.x, -mainTextArea.rectTransform.rect.height/2);

        }

        void SetUpEventListeners()
        {
            headerBackButton.onClick.AddListener(() => { ShowPreviousScreen(); });
            faqButton.onClick.AddListener(() => { OnClickFAQ(); });
            acceptTermsButton.onClick.AddListener(() => { OnAcceptTermsClick(); });
            privacyButton.onClick.AddListener(() => { mainUI.GetPrivacyStatementScreen().OnPrivacyStatementClick(); });
        }
     

        void OnAcceptTermsClick()
        {
            mainUI.GetCreateAccountScreen().conditionAndTermsAreAccepted = true;
            mainUI.GetCreateAccountScreen().verifiedAcceptedTermsIconImage.sprite = SpriteResources.verifiedInputPassedIcon;
            verifiedAcceptedTermsIconImage.sprite = SpriteResources.verifiedInputPassedIcon;
            ShowPreviousScreen();
        }
        void OnClickFAQ()
        {
            mainUI.GetFAQScreen().ShowThisScreen();
        }


        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
        public void DisableScreen()
        {
            /*
            Destroy(this);
            if (gameObject != null)
            {
                Destroy(gameObject);
            }
             * */
            gameObject.SetActive(false);
        }
        public void EnableScreen()
        {
            gameObject.SetActive(true);
        }

        public void ShowThisScreen()
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.RightToLeft));
        }

        public void ShowPreviousScreen()
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}