﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
//using SimpleJSON;

namespace Thrive
{
    public class CreateKidProfileScreen : MonoBehaviour, IScreen
    {
        MainUI mainUI;
        public Button backButton;
        public Button saveButton;
        public Text headerTitleText;

        public InputField childRealNameField;
        public Text childIDText;
        public BirthDatePicker childBirthDateField;
        public InputField childWeightField;
        public InputField childHeightField;

        public Scrollbar childGenderButton;
        public Scrollbar childMetricButton;

        //Child profile data
        string childRealName;
        string birthDate;//System.DateTime.UtcNow.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
        string childSex;
        string metricUnit;
        int childHeight;
        int childWeight;



        void Awake()
        {
#if UNITY_ANDROID
            childRealNameField.shouldHideMobileInput = false;
            childWeightField.shouldHideMobileInput = false;
            childHeightField.shouldHideMobileInput = false;
#endif
#if UNITY_IPHONE
            childRealNameField.shouldHideMobileInput = true;
            childWeightField.shouldHideMobileInput = true;
            childHeightField.shouldHideMobileInput = true;
#endif
        }

        // Use this for initialization
        void Start()
        {
            mainUI = FindObjectOfType<MainUI>();
            SetupEventListeners();
       //     childIDText.text = UserStats.userName + ".";
        }
        void SetupEventListeners()
        {
            backButton.onClick.AddListener(() => { OnClickBackButton(); });
            saveButton.onClick.AddListener(() => { OnSaveButtonClick(); });

            childRealNameField.onValueChange.AddListener(delegate
            {
                OnChangedInputUsername();
            });


			childRealNameField.gameObject.AddComponent<DisableInputfieldPlaceholderText>();
			childWeightField.gameObject.AddComponent<DisableInputfieldPlaceholderText>();
			childHeightField.gameObject.AddComponent<DisableInputfieldPlaceholderText>();
        }

        void OnClickBackButton()
        {
            ShowPreviousScreen();
        }

        void OnChangedInputUsername()
        {
         //   childIDText.text = UserStats.userName + "." + childRealNameField.text;
           
        }

		void OnEnable(){
			childRealNameField.text = "";
			childWeightField.text = "";
			childHeightField.text = "";

			childGenderButton.value = 1;
		}

        void OnSaveButtonClick()
        {
            childRealName = childRealNameField.text;
           // childIDField = childIDField.text;
            if (childBirthDateField.IsBirthdateValid())
            {
                birthDate = childBirthDateField.GetBirthdateInput();
            }
            else
            {
                Debug.Log("Birthdate not valid");
            }

            childSex = childGenderButton.value <= 0.3f ? "F" : "M";
            childSex = childGenderButton.value <= 0.6f ? childSex : "-";

            metricUnit = childMetricButton.value <= 0.5f ? "Imperial" : "Metric";
            if (childWeightField.text != "")
            {
                childWeight = Mathf.RoundToInt(float.Parse( childWeightField.text));
            }
            else
            {
                childWeight = -1;
            }

             if (childHeightField.text != "")
            {
                childHeight = int.Parse(childHeightField.text);
            }
            else
            {
                childHeight = -1;
            }

            Debug.Log(metricUnit);
            /*
            Thrive.RPC.CreateChild(childRealName, birthDate, childSex, metricUnit, childHeight, childWeight, (res) =>
            {

                Debug.Log("New child profile pushed to server, res.raw = " + res.raw);
                Debug.Log(childRealName + " " + birthDate + " " + childSex + " " + metricUnit + " " + childHeight + " " + childWeight);

                JSONNode json = JSON.Parse(res.raw);
                string newlyAddedChildID = json["message"];
                // if child added do something
                if(res.code == "CHILD_ADDED"){
				//	mainUI.GetOptionsScreen().GetFamilyScreen().UpdateAndDisplaySocialGraph();
                 //   OnClickBackButton();

					mainUI.GetLoadingScreen().ShowThisScreen();
                    UserStats.userContentRecieved = false;
                    UserStats.GetSpecificUserContent(newlyAddedChildID);
                    
                 //   StartCoroutine(mainUI.GetInvitationManager().ListenForInvitationAcceptedContentResponse());

                    //StartCoroutine(mainUI.CreateDeckOnServerResponse());
                }

                // if child not already
                if (res.code == "CHILD_NOT_ADDED")
                {
                    //UserStats.GetSpecificUserContent(string userID)
                }

            });
             */
        }


        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
        public void DisableScreen()
        {
            gameObject.SetActive(false);
        }
        public void EnableScreen()
        {
            gameObject.SetActive(true);
        }
        public void ShowThisScreen()
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.RightToLeft));
        }

        public void ShowPreviousScreen()
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
        }
        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}
