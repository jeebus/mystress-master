﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    public class PasswordResetScreen : MonoBehaviour, IScreen
    {
        MainUI mainUI;
        public Button nextButton;
        CanvasGroup nextButtonGrp;
        public InputField newPasswordField;
        public InputField newRepeatPasswordField;
        public Image verifiedPasswordIconImage;
        Image passwordInputfieldImage;
        Image repeatPasswordInputfieldImage;

        string recievedPasswordResetHash;
        bool hideNextButton = false;
        bool showNextButton = false;
        bool isPasswordValid = false;
        public Text headerTitleText;

        // Use this for initialization
        void Start()
        {
            mainUI = FindObjectOfType<MainUI>();
            SetUpEventListeners();
            nextButtonGrp = nextButton.gameObject.AddComponent<CanvasGroup>();
            nextButtonGrp.alpha = 0;
            passwordInputfieldImage = newPasswordField.GetComponent<Image>();
            repeatPasswordInputfieldImage = newRepeatPasswordField.GetComponent<Image>();


        }

        void SetUpEventListeners()
        {

            nextButton.onClick.AddListener(() => { OnPasswordResetButton(); });


            newPasswordField.onValueChange.AddListener(delegate
            {
                OnChangedInputPassword();
            });
            newPasswordField.onEndEdit.AddListener(delegate
            {
                OnPasswordFieldUnSelected();
            });
            newPasswordField.gameObject.AddComponent<EventTrigger>();
            newPasswordField.gameObject.AddComponent<InputfieldSelected>();

            newRepeatPasswordField.gameObject.AddComponent<EventTrigger>();
            newRepeatPasswordField.gameObject.AddComponent<InputfieldSelected>();
            newRepeatPasswordField.onValueChange.AddListener(delegate
            {
                OnChangedInputPassword();
            });
            newRepeatPasswordField.onEndEdit.AddListener(delegate
            {
                OnPasswordFieldUnSelected();
            });

        }


        public void SetRecievedPasswordHash(string passwordResetHash)
        {
            recievedPasswordResetHash = passwordResetHash;
        }
        void OnPasswordFieldUnSelected()
        {
            if (isPasswordValid || newPasswordField.text == "")
            {
                passwordInputfieldImage.sprite = SpriteResources.inputFieldSprite;
                repeatPasswordInputfieldImage.sprite = SpriteResources.inputFieldSprite;
            }
        }

        void OnChangedInputPassword()
        {
            if (newPasswordField.text == "" || newRepeatPasswordField.text == "")
            {
                verifiedPasswordIconImage.sprite = SpriteResources.verifiedInputFailedIcon;
                PasswordFieldSelectedInvalidInput();
                isPasswordValid = false;
                StartCoroutine(HideNextButton());
            }
            else
            {
                if (verifiedPasswordIconImage.enabled == false)
                {
                    verifiedPasswordIconImage.enabled = true;
                    PasswordFieldSelected();
                }

                if (newPasswordField.text == newRepeatPasswordField.text && InputfieldChecker.IsPassword(newPasswordField.text))
                {
                    verifiedPasswordIconImage.sprite = SpriteResources.verifiedInputPassedIcon;
                    PasswordFieldSelected();
                    isPasswordValid = true;

                    hideNextButton = false;
                    showNextButton = true;
                    StartCoroutine(ShowNextButton());

                }
                if (newPasswordField.text != newRepeatPasswordField.text || !InputfieldChecker.IsPassword(newPasswordField.text))
                {
                    verifiedPasswordIconImage.sprite = SpriteResources.verifiedInputFailedIcon;
                    PasswordFieldSelectedInvalidInput();
                    isPasswordValid = false;
                    if (showNextButton)
                    {
                        hideNextButton = true;
                        showNextButton = false;
                        StartCoroutine(HideNextButton());
                    }
                }
            }
        }

        void PasswordFieldSelected()
        {
            passwordInputfieldImage.sprite = SpriteResources.inputFieldSelectedSprite;
            repeatPasswordInputfieldImage.sprite = SpriteResources.inputFieldSelectedSprite;
        }
        void PasswordFieldSelectedInvalidInput()
        {
            passwordInputfieldImage.sprite = SpriteResources.inputFieldSelectedInvalidSprite;
            repeatPasswordInputfieldImage.sprite = SpriteResources.inputFieldSelectedInvalidSprite;
        }

        void OnPasswordResetButton()
        {
            Debug.Log("Hash: " + recievedPasswordResetHash + "  new password: " + newPasswordField.text);
            /*
            Thrive.RPC.PasswordReset(recievedPasswordResetHash, Thrive.Crypto.Utils.MD5(newPasswordField.text), (res) =>
            {
                if (!res.error)
                {
                    //Store passwordd in playerprefs
                    PlayerPrefs.SetString("password",  newPasswordField.text);

                    StartCoroutine( LoginAfterPasswordReset());
                }
                Debug.Log(res.raw);
            });
             * */
        }

        IEnumerator LoginAfterPasswordReset()
        {
            yield return null;
            /*
            string storedUserName = "";
            if (PlayerPrefs.GetString("username").Length > 0 && PlayerPrefs.GetString("username") != null && PlayerPrefs.GetString("username") != "")
            {
                //                Debug.Log("Username exists in player prefs.");

                storedUserName = PlayerPrefs.GetString("username");


                Thrive.Session.SetUser(storedUserName,newPasswordField.text);
            
            StartCoroutine(LoginManager.AttemptToContactServerForLogin());
            while (!LoginManager.GetLoginReplyRecieved())//|| LoginManager.GetLoginResponse() != null)
            {
                yield return null;
            }

                 
            APIResponse res = LoginManager.GetLoginResponse();

            if (!res.error)
            {
               
                if (res.code == "CREDENTIALS_VALID")
                {
                  
                    mainUI.OnLogin();


                }
                if (res.code == "ACCESS_DENIED")
                {
                  
                }
                if (res.code == "ACCOUNT_RESET_PENDING")
                {
                   
                }
                if (res.code == "ACCOUNT_RESET")
                {

                }
            }
            }
            else
            {
                Debug.Log("No username stored in prefs");
            }
             * */
        }

        IEnumerator ShowNextButton()
        {
            float counter = 0;
            float fadeMultiplier = 3;
            while (counter < 1 && showNextButton)
            {
                counter += Time.deltaTime * fadeMultiplier;
                nextButtonGrp.alpha = counter;
                yield return null;
            }
            nextButton.interactable = true;
          //  nextButtonGrp.alpha = 1;
        }

        IEnumerator HideNextButton()
        {
            float counter = 0;
            float fadeMultiplier = 3;
            nextButton.interactable = false;
            while (counter < 1 && hideNextButton)
            {
                counter += Time.deltaTime * fadeMultiplier;
                nextButtonGrp.alpha = 1-counter;
                yield return null;
            }
            
            //nextButtonGrp.alpha = 0;
        }


        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
        public void DisableScreen()
        {
            gameObject.SetActive(false);
        }
        public void EnableScreen()
        {
            gameObject.SetActive(true);
        }
        public void ShowThisScreen()
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.Fade));
        }

        public void ShowPreviousScreen()
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}
