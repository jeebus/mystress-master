﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    public class CompletedFlowScreen : MonoBehaviour, IScreen
    {
        MainUI mainUI;
        public Button nextButton;
        public Text flowSpecificText;
		CanvasGroup nextButtonCanvasGrp;
        public Text headerTitleText;

        //The different kinds of completed flow screens, result from different contexts, do something different depending on what context
        public bool accountSuccessfullyCreated = false;

	    // Use this for initialization
	    void Awake () {
            SetUpEventListeners();
            mainUI = FindObjectOfType<MainUI>();
			nextButtonCanvasGrp = nextButton.gameObject.AddComponent<CanvasGroup>();
	    }
        void SetUpEventListeners()
        {
            if (nextButton != null)
            {
                nextButton.onClick.AddListener(() => { OnNextButtonClick(); });
            }
        }

        public void SetFlowSpecificText(string newText)
        {
            flowSpecificText.text = newText;
        }

        void OnNextButtonClick()
        {
            if (!accountSuccessfullyCreated)
            {
                Debug.Log("Show previous screen from completed screen");
                ShowPreviousScreen();
            }
            else
            {
                Debug.Log("Show logging in info");
				StartCoroutine(FadeButtonOut());
                StartCoroutine(ShowLoggingInInfo());
            }
        }

        IEnumerator ShowLoggingInInfo()
        {
			flowSpecificText.text = SmartLocalization.LanguageManager.Instance.GetTextValue("CompletedFlowScreen.AttemptLogin");// "Attempting to log-in...";
            /*
            Thrive.Session.SetUser(mainUI.GetLoginScreen().usernameInputField.text, mainUI.GetLoginScreen().passwordInputField.text);
            StartCoroutine(LoginManager.AttemptToContactServerForLogin());
            while (!LoginManager.GetLoginReplyRecieved())
            {
                Debug.Log("Not yet logged in.");
                yield return null;
            }
            APIResponse res = LoginManager.GetLoginResponse();

            if (!res.error)
            {
                if (res.code == "CREDENTIALS_VALID")
                {

                    mainUI.OnLogin();


                    UserStats.userName = mainUI.GetLoginScreen().usernameInputField.text;
                    while(!mainUI.GetIsLoggedIn()){
                        yield return null;
                    }
                   // StartCoroutine(ShowPreviousScreen());
                }
                if (res.code == "ACCESS_DENIED")
                {
                    SetFlowSpecificText(res.code);
                }
                if (res.code == "ACCOUNT_RESET_PENDING")
                {
                    SetFlowSpecificText(res.code);


                }

                Debug.Log( res.raw);
            }
            else
            {
                Debug.LogWarning("Err " + res.code);
            }

            */
            yield return null;
        }

		float fadeMultiplier = 3.0f;


		IEnumerator FadeButtonOut(){
			float fadeCounter = 1;
			while (fadeCounter > 0) {
				fadeCounter -= Time.deltaTime * fadeMultiplier;
				nextButtonCanvasGrp.alpha = fadeCounter;
				yield return null;
			}
		}
        /////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
        public void DisableScreen()
        {
            Destroy(this);
            if (gameObject != null)
            {
                Destroy(gameObject);
            }
        }
        public void EnableScreen()
        {
            gameObject.SetActive(true);
        }

        public void ShowThisScreen()
        {
            EnableScreen();
            StartCoroutine(ViewIScreenController.ShowScreen(this, TransitionType.RightToLeft));

        }

        public void ShowPreviousScreen()
        {
            StartCoroutine(ViewIScreenController.ShowPreviousScreen());
        }
        public GameObject GetGameObject()
        {
            return gameObject;
        }
    }
}