﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive{

	public class LoadingScreen : MonoBehaviour, IScreen {

		
		MainUI mainUI;
		//public Button headerBackButton;
		public Text loadingDotsText;

		bool loading = false;
        float lerpCounter = 0;
        float lerpCounterMultiplier = 3;
        bool showLoadingScreen = false;
        bool hideLoadingScreen = false;
        public Text headerTitleText;
		void Awake()
		{
			mainUI = FindObjectOfType<MainUI>();
			SetUpEventListeners();
		}


		void SetUpEventListeners()
		{
			//headerBackButton.onClick.AddListener(() => { ShowPreviousScreen(); });
		}

		void ActivateLoadingBlinkies(){
			loading = true;
			StartCoroutine (ActivateLoadingBlinkiesTask());
		}

		IEnumerator ActivateLoadingBlinkiesTask(){
			int amountOfBlinkies = 0;
			while (loading) {
				if(amountOfBlinkies > 3){
					amountOfBlinkies = 0;
				}
				string blinkyString = "";
				for (int i = 0; i < amountOfBlinkies; i++) {
					blinkyString += ".";
				}
				loadingDotsText.text = blinkyString;
				amountOfBlinkies++;
				yield return new WaitForSeconds(0.2f);
			}
		}

		void DeactivateLoadingBlinkies(){
			loading = false;
		}

        IEnumerator ShowLoadingScreenTask()
        {
            
            ((RectTransform)gameObject.transform).anchoredPosition3D = new Vector3(0, 0, 0);
            CanvasGroup canvasGrp = gameObject.GetComponent<CanvasGroup>();
            while (lerpCounter < 1 && showLoadingScreen)
            {
                lerpCounter += Time.deltaTime * lerpCounterMultiplier;

                canvasGrp.alpha = lerpCounter;
                yield return null;
            }

            canvasGrp.alpha = 1;
            lerpCounter = 0;
        }

        IEnumerator HideLoadingScreenTask()
        {

            ((RectTransform)gameObject.transform).anchoredPosition3D = new Vector3(0, 0, 0);
            CanvasGroup canvasGrp = gameObject.GetComponent<CanvasGroup>();
			lerpCounter = 0;
            while (lerpCounter < 1 && hideLoadingScreen)
            {
                lerpCounter += Time.deltaTime * lerpCounterMultiplier;

                canvasGrp.alpha = 1 - lerpCounter;
				yield return null;
            }
            
            canvasGrp.alpha = 0;
            lerpCounter = 0;

			DisableScreen ();
        }

		/////////////////////IScreen interface implementation////////////////////////////////
        public string GetScreenHeaderTitle()
        {
            if (headerTitleText != null)
            {
                return headerTitleText.text;
            }
            else
            {
                return "";
            }
        }
		public void DisableScreen()
		{
			gameObject.SetActive(false);
		}
		public void EnableScreen()
		{
			gameObject.SetActive(true);
			transform.SetParent(mainUI.currentScreenCanvas.transform, false);
            ActivateLoadingBlinkies();
		}
		public void ShowThisScreen()
		{
			EnableScreen();
            showLoadingScreen = true;
            hideLoadingScreen = false;
            StartCoroutine(ShowLoadingScreenTask());

		}
		
		public void ShowPreviousScreen()
		{
            DeactivateLoadingBlinkies();
            showLoadingScreen = false;
            hideLoadingScreen = true;
            StartCoroutine(HideLoadingScreenTask());
		}
		
		public GameObject GetGameObject()
		{
			return gameObject;
		}
	}
}
