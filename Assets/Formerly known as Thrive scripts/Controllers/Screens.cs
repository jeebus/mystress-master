﻿using UnityEngine;
using System.Collections;

namespace Thrive
{
    public static class Screens {

        //Instantiated objects
        static LoginScreen instantiatedLoginScreen;
        static AccountCreationScreen instantiatedAccountCreationScreen;
        static PrivacyStatementScreen instantiatedPrivacyStatementScreen;
        static TermsAgreementScreen instantiatedTermsAndAgreementScreen;
        static RecoverAccountScreen instantiatedRecoverAccountScreen;
        static FAQScreen instantiatedFaqScreen;
        static FadeInOutScreen instantiatedFadeScreen;
        static CompletedFlowScreen instantiatedFlowScreen;
        static OptionsScreen instantiatedOptions;
        static PasswordResetScreen instantiatedPasswordResetScreen;
        static LoadingScreen instantiatedLoadingScreen;
        static BlankBlackScreen blankScreen;

        static GameObject presentCardCanvas;

        public static void Init()
        {
           presentCardCanvas = GameObject.FindGameObjectWithTag("ScreenPresentCanvas");
           Debug.Log("Screens class instantiated, found canvas: " + presentCardCanvas);
        }

        public static BlankBlackScreen GetBlankScreen()
        {
            if (blankScreen == null)
            {
                blankScreen = (BlankBlackScreen)GameObject.Instantiate(ThrivePrefabResources.blankBlackScreenPrefab, Vector3.zero, Quaternion.identity);
                blankScreen.transform.SetParent(presentCardCanvas.transform, false);
            }
            return blankScreen;
        }

        
    }
}