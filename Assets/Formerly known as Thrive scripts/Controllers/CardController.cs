﻿using UnityEngine;
using System.Collections;

namespace Thrive
{
    public class CardController : Singleton<CardController>
    {

        // Use this for initialization
        void Start()
        {

        }


        bool doOnce = true;
        public bool rotateCardToReadMore = false;
     //   float rotationLerpCounter = 0;
        Quaternion readMoreRotation = Quaternion.Euler(new Vector3(0, 180, 0));

        Quaternion originalRotation = Quaternion.Euler(new Vector3(0, 0, 0));


        public void RotateCardBackToOriginal(ICard cardInstance)
        {
            rotateCardToReadMore = false;
            StartCoroutine(RotateCardBackToOriginalTask(cardInstance));
        }

        public void RotateCardToReadMore(ICard cardInstance)
        {
            rotateCardToReadMore = true;
            StartCoroutine(RotateCardToReadMoreTask(cardInstance));
        }

        IEnumerator RotateCardToReadMoreTask(ICard cardInstance)
        {

            /*
          // Depth position
            float depthPosLerpCounter = 0;
           Vector3 startPostion = transform.position;
            
           while (depthPosLerpCounter <= 1 && rotateCardToReadMore)
           {
               depthPosLerpCounter += Time.deltaTime * depthMoveMultiplier;
               transform.position = Vector3.Slerp(startPostion, startPostion + new Vector3(0, 0, depthOffset), depthPosLerpCounter);
               yield return null;
           }
          
            depthPosLerpCounter = 1;
              */
            float rotationLerpCounter = 0;
            Transform trans = cardInstance.GetGameObject().transform;
            Debug.Log("Card RotateCardToReadMoreTask started");
            while (rotationLerpCounter <= 1 && rotateCardToReadMore)
            {
                rotationLerpCounter += Time.deltaTime;
                trans.rotation = Quaternion.Lerp(originalRotation, readMoreRotation, rotationLerpCounter);
                if (rotationLerpCounter >= 0.5f && doOnce)
                {
                    doOnce = false;
                    Debug.Log("ShowBackSide activated");
                    cardInstance.ShowBackSide();
                }
                yield return null;
            }
            Debug.Log("Card RotateCardToReadMoreTask completed");
            /*
            while (depthPosLerpCounter >= 0 && rotateCardToReadMore)
            {
                depthPosLerpCounter -= Time.deltaTime * depthMoveMultiplier;
                transform.position = Vector3.Slerp(startPostion, startPostion + new Vector3(0, 0, depthOffset), depthPosLerpCounter);
                yield return null;
            }
            */
            if (rotationLerpCounter > 1)
            {
                rotationLerpCounter = 1;
            }

        }

        IEnumerator RotateCardBackToOriginalTask(ICard cardInstance)
        {
            /*
           float depthPosLerpCounter = 0;
           Vector3 startPostion = transform.position;
           
           while (depthPosLerpCounter <= 1 )
           {
               depthPosLerpCounter += Time.deltaTime * depthMoveMultiplier;
               transform.position = Vector3.Slerp(startPostion, startPostion + new Vector3(0, 0, depthOffset), depthPosLerpCounter);
               yield return null;
           }
              depthPosLerpCounter = 1;
            */
  //          Debug.Log("Start rotating back");
            float rotationLerpCounter = 1;
            Transform trans = cardInstance.GetGameObject().transform;
            Debug.Log("Card RotateCardBackToOriginalTask started");
            while (rotationLerpCounter > 0)
            {
//                Debug.Log("Rotating back");
                rotationLerpCounter -= Time.deltaTime;
                trans.rotation = Quaternion.Lerp(originalRotation, readMoreRotation, rotationLerpCounter);
                if (rotationLerpCounter <= 0.5f && !doOnce)
                {
                    cardInstance.ShowFrontSide();
                    doOnce = true;
                    Debug.Log("ShowFrontSide activated");
                }
                yield return null;
            }
            Debug.Log("Card RotateCardBackToOriginalTask completed");
            /*
            while (depthPosLerpCounter >= 0 )
            {
                depthPosLerpCounter -= Time.deltaTime * depthMoveMultiplier;
                transform.position = Vector3.Slerp(startPostion, startPostion + new Vector3(0, 0, depthOffset), depthPosLerpCounter);
                yield return null;
            }
            */
            if (rotationLerpCounter < 0)
            {
                rotationLerpCounter = 0;
            }
            rotateCardToReadMore = false;

        }
    }
}