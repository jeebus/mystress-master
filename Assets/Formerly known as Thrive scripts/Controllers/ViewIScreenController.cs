﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Thrive
{
    public enum TransitionType
    {
        RightToLeft,
        Fade
    }

    public class VisitedScreens
    {
        public IScreen screen;
        public TransitionType transition;
        public VisitedScreens(IScreen newScreen, TransitionType newTransition)
        {
            screen = newScreen;
            transition = newTransition;
        }
    }

    public static class ViewIScreenController
    {
        static float lerpCounter = 0;
        static float lerpCounterMultiplier = 3;
		static float lerpRightToLeftCounterMultiplier = 1.5f;

        static List<VisitedScreens> previousScreens = new List<VisitedScreens>();
        static VisitedScreens currentScreen;

        static ICard currentCardFrontSide;

        static ICard currentCardBackSide;

        static MainUI mainUI;
        public static bool cardTransitionRunning = false;
        public static bool screenTransitionRunning = false;


        public static void SetMainUI(MainUI mainui)
        {
            mainUI = mainui;
        }


        public static bool IsCardTransitionRunning()
        {
            return cardTransitionRunning;
        }

        public static bool IsTransitionRunning()
        {
            return screenTransitionRunning;
        }

        public static void SetPreviousScreen(VisitedScreens newPreviousScreen)
        {
            if (lerpCounter == 0)
            {
                if (newPreviousScreen != null)
                {
                    previousScreens.Add(newPreviousScreen);
                } else
                {
                    Debug.Log("Previous screen is null, not allowed!");
                }
            }
            else
            {
                Debug.Log("A screen transition hasnt finished yet");
            }
        }

        public static void SetCurrentScreen(VisitedScreens newCurrentScreen)
        {
            
            if (lerpCounter == 0)
            {
                PreviousScreenHandling(currentScreen);


                int index = previousScreens.FindIndex(ByScreen(newCurrentScreen));
                if (index >= 0)
                {
                    previousScreens.RemoveAt(index);
                }
                currentScreen = newCurrentScreen;
            }
            else
            {
                Debug.Log("A screen transition hasnt finished yet");
            }
        }

        public static VisitedScreens GetCurrentScreen()
        {
            return currentScreen;
        }

        public static ICard GetCurrentFrontSideCard()
        {
            return currentCardFrontSide;
        }

        public static ICard GetCurrentBackSideCard()
        {
            return currentCardBackSide;
        }

        public static void SetCurrentCard(ICard newCurrentFrontSideCard = null, ICard newCurrentBackSideCard = null)
        {
            if (newCurrentFrontSideCard != null)
            {
                currentCardFrontSide = newCurrentFrontSideCard;
                Debug.Log("Set currentCardFrontSide to: " + newCurrentFrontSideCard);
            }
            if (newCurrentBackSideCard != null)
            {
                currentCardBackSide = newCurrentBackSideCard;
                Debug.Log("Set currentCardBackSide to: " + newCurrentBackSideCard);
            }
        }

        public static VisitedScreens GetPreviousScreen()
        {
            return previousScreens[previousScreens.Count-1];
        }

        public static List<VisitedScreens> GetPreviousScreens()
        {
            return previousScreens;
        }

        public static VisitedScreens RemovePreviousScreen()
        {
            VisitedScreens previous= previousScreens[previousScreens.Count-1];
            previousScreens.RemoveAt(previousScreens.Count - 1);
            return previous;
        }

        public static IEnumerator ShowOtherCardPage(ICard cardPageToStopShowing, ICard cardPageToShow, TransitionType transitionType, bool isFrontSide)
        {
            if (cardPageToShow == null){
                Debug.Log("ShowOtherCardPage with cardToShow is null");
                yield break;
            }
            if (cardPageToStopShowing == null){
                Debug.Log("ShowOtherCardPage with cardPageToStopShowing is null");
                yield break;
            }
            Debug.Log("Stop showing card page " + cardPageToStopShowing + " that is on " + (isFrontSide ? "FRONT SIDE" : "BACK SIDE"));
            Debug.Log("Show card page " + cardPageToShow + " with transition type " + transitionType);



            if (lerpCounter == 0)
            {
                cardPageToStopShowing.GetGameObject().GetComponent<CanvasGroup>().blocksRaycasts = false;
                cardPageToShow.GetGameObject().GetComponent<CanvasGroup>().blocksRaycasts = true;

                cardPageToShow.MakeCardContentVisible();

                switch (transitionType)
                {
                    case TransitionType.RightToLeft:
                        while (lerpCounter < 1)
                        {
                            lerpCounter += Time.deltaTime * lerpRightToLeftCounterMultiplier;
                            // ((RectTransform)cardPageToShow.GetGameObject().transform).anchoredPosition3D = new Vector3(Mathf.Lerp(mainUI.mainCanvas.pixelRect.width, 0, mainUI.screenRightToLeftPresentationCurve.Evaluate(lerpCounter)), ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D.y, ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D.z);
                            yield return null;
                        }

                        break;
                    case TransitionType.Fade:
                        CanvasGroup cardPageToStopShowingCanvasGrp = cardPageToStopShowing.GetGameObject().GetComponent<CanvasGroup>();
                        CanvasGroup cardPageToShowCanvasGrp = cardPageToShow.GetGameObject().GetComponent<CanvasGroup>();

                        while (lerpCounter < 1)
                        {
                            lerpCounter += Time.deltaTime * lerpCounterMultiplier;
                            cardPageToStopShowingCanvasGrp.alpha = 1 - lerpCounter;
                            cardPageToShowCanvasGrp.alpha = lerpCounter;
                            yield return null;
                        }

                        cardPageToShowCanvasGrp.alpha = 1;
                        lerpCounter = 0;
                        break;
                    default:
                        break;
                }

                cardPageToStopShowing.MakeCardContentInvisible();
                lerpCounter = 0;
                if (isFrontSide)
                {
                    currentCardFrontSide = cardPageToShow;
                }
                else
                {
                    currentCardBackSide = cardPageToShow;
                }
            }
            else
            {
                Debug.Log("Transition still running, wait till its done");
            }
        }

        public static IEnumerator ShowScreen(IScreen screenToShow, TransitionType transitionType)
        {
            Debug.Log("Show screen " + screenToShow + " with transition type " + transitionType + ". Previous screen is "+ currentScreen.screen);
           
            if (lerpCounter == 0)
            {
                screenTransitionRunning = true;
                screenToShow.EnableScreen();

                SetCurrentScreen(new VisitedScreens(screenToShow, transitionType));
              
				((RectTransform)currentScreen.screen.GetGameObject().transform).SetParent(mainUI.currentScreenCanvas.transform,false);
                ((RectTransform)currentScreen.screen.GetGameObject().transform).SetAsLastSibling();

                mainUI.GetHeaderPanel().TransitionHeaderTitle(screenToShow.GetScreenHeaderTitle());
                switch (transitionType)
                {
                    case TransitionType.RightToLeft:
                        while (lerpCounter < 1)
                        {
						lerpCounter += Time.deltaTime * lerpRightToLeftCounterMultiplier;
                        ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D = new Vector3(Mathf.Lerp(Screen.width*2, 0, mainUI.screenRightToLeftPresentationCurve.Evaluate(lerpCounter)), ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D.y, ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D.z);
                            yield return null;
                        }

                        break;
                    case TransitionType.Fade:

                        ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D = new Vector3(0, 0, 0);
                        CanvasGroup canvasGrp = currentScreen.screen.GetGameObject().GetComponent<CanvasGroup>();
                        while (lerpCounter < 1)
                        {
                            lerpCounter += Time.deltaTime * lerpCounterMultiplier;

                            canvasGrp.alpha = lerpCounter;
                            yield return null;
                        }

                        canvasGrp.alpha = 1;
                        lerpCounter = 0;
                        break;
                    default:
                        break;
                }
                previousScreens[previousScreens.Count - 1].screen.DisableScreen();

				if(mainUI.presentCard){
					mainUI.currentEnlargedCard.actualCard.MakeCardContentInvisible();
				}
                lerpCounter = 0;
                screenTransitionRunning = false;
            }
            else
            {
                Debug.Log("Transition still running, wait till its done");
            }
           
        }


        static System.Predicate<VisitedScreens> ByScreen(VisitedScreens screenToCompare)
        {
            return delegate (VisitedScreens screen)
            {
                if (screen != null)
                {
                    return screen.screen == screenToCompare.screen;
                } else
                {
                    Debug.Log("Screen is null!!");
                    return false;
                }
            };
        }

        /// <summary>
        /// Method for handling when previous screens already contains the screenInQuestion
        /// </summary>
        /// <param name="screenInQuestion"></param>
        private static void PreviousScreenHandling(VisitedScreens screenInQuestion)
        {
            int index = previousScreens.FindIndex(ByScreen(screenInQuestion));
            if (index >= 0)
            {
               // int index = previousScreens.FindIndex(ByScreen(screenInQuestion));
                Debug.Log("Cnntains screen in question");
                VisitedScreens tempObj = previousScreens[index];
                previousScreens.RemoveAt(index);
                previousScreens.Add(tempObj);
            } else
            {
                previousScreens.Add(screenInQuestion);
            }
           
        }

        public static IEnumerator ShowPreviousScreen()
        {

            VisitedScreens tempScreen = currentScreen;
            Debug.Log("Show screen " + previousScreens[previousScreens.Count - 1].screen + " with transition type " + previousScreens[previousScreens.Count - 1].transition + ". PreviousScreen count is : " + previousScreens.Count + ". Previous screen is " + currentScreen.screen);
           
            if (previousScreens.Count > 0 && lerpCounter == 0)
            {
                screenTransitionRunning = true;
				if(mainUI.presentCard){
					mainUI.currentEnlargedCard.actualCard.MakeCardContentVisible();
				}

                previousScreens[previousScreens.Count - 1].screen.EnableScreen();
                while (!previousScreens[previousScreens.Count - 1].screen.GetGameObject().activeInHierarchy)
                {
                    yield return null;
                }
         //       yield return new WaitForSeconds(0.1f);// WaitForEndOfFrame();

                mainUI.GetHeaderPanel().TransitionHeaderTitle(previousScreens[previousScreens.Count - 1].screen.GetScreenHeaderTitle());

                switch (currentScreen.transition)
                {
                    case TransitionType.RightToLeft:
                        while (lerpCounter < 0.9f)
                        {
						    lerpCounter += Time.deltaTime * lerpRightToLeftCounterMultiplier;
                            ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D = new Vector3(Mathf.Lerp(0, Screen.width*2, mainUI.screenRightToLeftPresentationCurve.Evaluate(lerpCounter)), ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D.y, ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D.z);
                            yield return null;
                        }
                        break;
                    case TransitionType.Fade:
                        lerpCounter = 0;
                        CanvasGroup canvasGrp = currentScreen.screen.GetGameObject().GetComponent<CanvasGroup>();
                        while (lerpCounter < 0.9f)
                        {
                            float time = Time.deltaTime;
                            lerpCounter += time * lerpCounterMultiplier;
                            canvasGrp.alpha = 1-lerpCounter;
                            yield return null;
                        }
                        ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D = new Vector3(Screen.width*2, ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D.y, ((RectTransform)currentScreen.screen.GetGameObject().transform).anchoredPosition3D.z);
                        canvasGrp.alpha = 0;
                        lerpCounter = 0;
                        break;
                    default:
                        break;
                }
                lerpCounter = 0;
                currentScreen.screen.DisableScreen();
                currentScreen = previousScreens[previousScreens.Count - 1];
                previousScreens.RemoveAt(previousScreens.Count - 1);
                screenTransitionRunning = false;
            }
            else
            {
                Debug.Log("Reached end of previous screen list, or transition still running");
            }
        }




    }
}
