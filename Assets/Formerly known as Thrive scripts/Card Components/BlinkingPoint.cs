﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class BlinkingPoint : MonoBehaviour
    {

        Image blinkingImage;
        public bool isBlinking = true;
        public float blinkingOffset;
        Button blinkingButton;

        // Use this for initialization
        void Awake()
        {
            blinkingImage = GetComponent<Image>();
            blinkingButton = GetComponent<Button>();
            StartCoroutine(BlinkingAction());
            SetupEventlisteners();
        }

        void SetupEventlisteners()
        {
            blinkingButton.onClick.AddListener(() => { OnBlinkingButtonClick(); });
        }

        public IEnumerator BlinkingAction()
        {
            float time = 0;
			while (isBlinking && blinkingImage != null)
            {
                time = Time.time;
                blinkingImage.color = new Color(blinkingImage.color.r, blinkingImage.color.g, blinkingImage.color.b,  pulse(time, blinkingOffset));
                yield return null;
            }
        }

        float pulse(float time,float offset)
        {
            const float pi = 3.14f;
            const float frequency = 1.0f; // Frequency in Hz
            return 0.5f * (1 + Mathf.Sin(2 * pi * frequency * time+offset));
        }

        void OnBlinkingButtonClick()
        {
            isBlinking = false;
            blinkingImage.color = new Color(blinkingImage.color.r, blinkingImage.color.g, blinkingImage.color.b, 1);
        }

    }
}