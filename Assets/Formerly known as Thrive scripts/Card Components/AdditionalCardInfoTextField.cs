﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class AdditionalCardInfoTextField : MonoBehaviour
    {
        public GameObject mainContent;

        // Use this for initialization
        void Start() {
	
           LayoutElement layout =  gameObject.GetComponent<LayoutElement>();
			if(layout!= null && mainContent != null && layout.minHeight < ((RectTransform)mainContent.transform).rect.height){
           		layout.minHeight = ((RectTransform)mainContent.transform).rect.height;
			}
	    }

    }
}