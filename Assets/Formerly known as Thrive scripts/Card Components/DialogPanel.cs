﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    public class DialogPanel : MonoBehaviour {

        public Button acceptButton;
        public Button cancelButton;

        CanvasGroup canvasGrp;
        float fadeMultiplier = 3;

        public delegate void ClickAction();

        public static event ClickAction OnClickedAccepted;

        void Awake()
        {
            canvasGrp = GetComponent<CanvasGroup>();
            SetUpEventListeners();
        }

        void ResetClickActionDelegate()
        {
            OnClickedAccepted = null;
        }

        void SetUpEventListeners()
        {

            acceptButton.onClick.AddListener(() => { OnAcceptButtonClick(); });
            cancelButton.onClick.AddListener(() => { OnCancelButtonClick(); });
        }


        void OnAcceptButtonClick()
        {
            if (OnClickedAccepted !=null)
            {
                OnClickedAccepted();
            }
            HideDialogBox();
            ResetClickActionDelegate();
        }
        void OnCancelButtonClick()
        {
            HideDialogBox();
            ResetClickActionDelegate();
        }
        IEnumerator ShowDialogBoxTask()
        {
            float counter = 0;
            while (counter < 1)
            {
                counter += Time.deltaTime*fadeMultiplier;
                canvasGrp.alpha = counter;
                yield return null;
            }
        }

        IEnumerator HideDialogBoxTask()
        {
            float counter = 1;
            while (counter > 0)
            {
                counter -= Time.deltaTime * fadeMultiplier;
                canvasGrp.alpha = counter;
                yield return null;
            }
            gameObject.SetActive(false);
        }

        public void ShowDialogBox(){
            gameObject.SetActive(true);
            StartCoroutine(ShowDialogBoxTask());
        }

        public void HideDialogBox()
        {
            StartCoroutine(HideDialogBoxTask());
        }
    }
}