﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    public class StatsDataPoint
    {
        public Image dataPointImage;
        public DateTime creationDate;

    }
}