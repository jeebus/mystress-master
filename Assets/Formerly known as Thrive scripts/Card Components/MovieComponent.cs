﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive{


public class MovieComponent : MonoBehaviour {

    public Button playAndPauseButton;
    public string moviePath;
    MainUI mainUI;
    

	// Use this for initialization
	void Start () {
        SetupEventlisteners();
        mainUI = FindObjectOfType<MainUI>();
	}
    void SetupEventlisteners()
    {
        playAndPauseButton.onClick.AddListener(() => { StartCoroutine(PlayVideo()); });
    }


    IEnumerator PrepareToPlayVideo()
    {
        mainUI.GetBlankScreen().ShowThisScreen();
        while (mainUI.GetBlankScreen().GetCanvasGroup().alpha < 1)
        {
            yield return null;
        }
        PlayVideoSimple();
    }

    void PlayVideoSimple()
    {
        Screen.orientation = ScreenOrientation.Landscape;
        Handheld.PlayFullScreenMovie(moviePath, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFit);
        Screen.orientation = ScreenOrientation.Portrait;
        mainUI.GetBlankScreen().ShowPreviousScreen();
    }


    IEnumerator PlayVideo()
    {
			yield return null;
#if UNITY_EDITOR
            mainUI.GetBlankScreen().ShowThisScreen();
#endif

        #if !UNITY_EDITOR 
            #if  UNITY_ANDROID ||  UNITY_IPHONE
			mainUI.GetBlankScreen().ShowThisScreen();
			while(mainUI.GetBlankScreen().GetCanvasGroup().alpha < 1){
				yield return null;
			}
                Screen.orientation = ScreenOrientation.Landscape;
                
                yield return new WaitForSeconds(.1f);
                yield return new WaitForEndOfFrame();
                Handheld.PlayFullScreenMovie(moviePath, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFit);

                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
                Screen.orientation = ScreenOrientation.Portrait;
                yield return new WaitForEndOfFrame();
                mainUI.GetBlankScreen().ShowPreviousScreen();
            #endif
        #endif
        //
    }
}
}