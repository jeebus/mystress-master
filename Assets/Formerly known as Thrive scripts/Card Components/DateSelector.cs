﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    public class DateSelector : MonoBehaviour
    {
        public Button previousWeekButton;
		public Image previousWeekButtonImage;
        public Button nextWeekButton;

        public System.DateTime[] currentWeek = new System.DateTime[7];

        public List<Text> visibleWeekDays;

        System.DateTime now = System.DateTime.UtcNow.Date;

      //  public List<CardStatistics> dateSelectorConnectedStats = new List<CardStatistics>();
       // public BreastCardStatistics dateSelectorConnectedBreastStats;
       // public BottleCardStatistics dateSelectorConnectedBottleStats;

        public Text externalMonthText;
        public Text externalYearText;

        public Sprite currentDaySprite;

        public Image currentDay;

        MainUI mainUI;

        // Use this for initialization
        void Start()
        {
            SetUpEventListeners();
            currentDay = new GameObject("Current day").AddComponent<Image>();
            currentDay.sprite = currentDaySprite;
            currentDay.color = new Color(1,1,1,1);
			currentDay.rectTransform.sizeDelta = Vector2.zero;
			currentDay.rectTransform.anchorMin = new Vector2 (-0.1f,0);
			currentDay.rectTransform.anchorMax = new Vector2 (1.1f,1);
			currentDay.preserveAspect = true;
            StartCoroutine(Refresh());
           
        }

        void Awake()
        {
            mainUI = FindObjectOfType<MainUI>();
        }

        void SetUpEventListeners()
        {
            previousWeekButton.onClick.AddListener(() => { OnClickPrevious(); });
            nextWeekButton.onClick.AddListener(() => { OnClickNext(); });
        }

      
        public IEnumerator Refresh()
        {
            System.DateTime dateNow = now;
            int weekOverflowDays = 0;
            currentDay.enabled = false;
            for (int i = 0; i < visibleWeekDays.Count; i++)
            {
              
                dateNow = now.AddDays(i - weekOverflowDays);
                
                int index = (((int)dateNow.DayOfWeek) + 6) % 7;

                if (dateNow == System.DateTime.UtcNow.Date)
                {
                    currentDay.enabled = true;
                    currentDay.transform.SetParent(visibleWeekDays[index].transform.parent, false);
                    currentDay.transform.SetAsFirstSibling();
                    currentDay.rectTransform.anchoredPosition = Vector2.zero;
                }

                if (index  == visibleWeekDays.Count-1 )
                {
                    weekOverflowDays = 7;
                }
                currentWeek[index] = dateNow;
                if (mainUI.GetCurrentDeckID() - 1 >= 0)
                {
                  //  System.DateTime childBirthTime = System.DateTime.ParseExact(UserStats.connectedChildren[mainUI.GetCurrentDeckID() - 1].GetBirthDate(), "yyyy-MM-dd", null);
                    System.DateTime calendarNowTime = dateNow;// currentWeek[index].Date.ToUniversalTime();
                    double days = 1.0;//(calendarNowTime - childBirthTime).Days;

                    string tempString = currentWeek[index].Date.ToUniversalTime().ToString("dd");//ToString("yyyy'-'MM'-'dd");

                    if (tempString.Substring(0, 1) == "0")
                    {
                        visibleWeekDays[index].text = tempString.Substring(1, tempString.Length - 1);
                    }
                    else
                    {
                        visibleWeekDays[index].text = tempString;
                    }
                    visibleWeekDays[index].text = days.ToString();
                    if (days < 0)
                    {
                        visibleWeekDays[index].text = "Ufødt";
						previousWeekButton.enabled = false;
						previousWeekButtonImage.enabled = false;

                    }
                }

//                externalMonthText.text = dateNow.ToString("MMMM");
 //               externalYearText.text = dateNow.Year.ToString();
            }
            yield return null;
        }


        void OnClickNext () {
            Debug.Log("Clicked next");
			previousWeekButton.enabled = true;
			previousWeekButtonImage.enabled = true;
            now = now.AddDays(7);
            UpdateAllStats();
        }

        public void UpdateAllStats()
        {
            /*
        StartCoroutine(Refresh());
            
            for (int i = 0; i < dateSelectorConnectedStats.Count; i++)
            {
                if (dateSelectorConnectedStats[i] != null)
                {
                    dateSelectorConnectedStats[i].UpdateVisibleCardStats();
                }
            }
        
      if (dateSelectorConnectedColorStats)
      {
          StartCoroutine(dateSelectorConnectedColorStats.UpdateVisiblePoopColorStats());
      }
          
      if (dateSelectorConnectedBreastStats)
      {
          StartCoroutine( dateSelectorConnectedBreastStats.RefreshBreastFeedingStats());
      }
      if (dateSelectorConnectedBottleStats)
      {
          StartCoroutine(dateSelectorConnectedBottleStats.RefreshBottleFeedingStats());
      }
       * */
        }

        void OnClickPrevious()
        {
            Debug.Log("Clicked prev");
            now = now.AddDays(-7);
            UpdateAllStats();
        }
    }
}