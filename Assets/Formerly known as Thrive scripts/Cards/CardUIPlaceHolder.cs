﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Thrive
{
    public class CardUIPlaceHolder : MonoBehaviour
    {
        MainUI mainUI;
        public int cardID;
        public string cardName = " Ble Skift";

        public Button cardButton;
        public Image placeholderGraphicsContent;
        public Image placeholderIcon;
        public Text placeholderText;
        public bool cardIsFocused = false;



        
        public Quaternion originalRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        public Quaternion readMoreRotation = Quaternion.Euler(new Vector3(0, 180, 0));

        public ICard actualCard;

        float presentCardLerpPositionCounter = 0;
        float presentCardLerpSizeCounter = 0;
        float presentLerpMultiplier = 3.5f;
		float presentLerpSizeMultiplier = 4.0f;
        float placeholderFadeSpeedMultiplier = 2.5f;

        float smallCardIconPositionOffset;
        float largeIconPositionOffset;

        float smallCardNamePositionOffset;
        float largeCardNamePositionOffset;

		CanvasGroup placeholderIconCanvasGrp;


        void Awake()
        {

            ((RectTransform)placeholderIcon.transform).sizeDelta = Vector2.zero;
            ((RectTransform)placeholderIcon.transform).anchorMin = new Vector2(0.5f, 0.5f);
            ((RectTransform)placeholderIcon.transform).anchorMax = new Vector2(0.5f, 0.5f);

            ((RectTransform)placeholderText.transform).sizeDelta = Vector2.zero;
            ((RectTransform)placeholderText.transform).anchorMin = new Vector2(0.5f, 0.5f);
            ((RectTransform)placeholderText.transform).anchorMax = new Vector2(0.5f, 0.5f);


        }

        void Start()
        {
            cardButton = GetComponent<Button>();
           // cardButton.onClick.AddListener(() => { OnInternClick(); });
            mainUI = GameObject.FindObjectOfType<MainUI>();
            if (actualCard != null) {
                Debug.Log("Actual card is not null, in placeholder start()");
            }
            else
            {
                Debug.Log("Actual card is null");
            }
           
            placeholderText.color = actualCard.GetCardNameTextComponent().color;
        
       ((RectTransform)placeholderIcon.transform).sizeDelta = new Vector2(((RectTransform)actualCard.GetCardIcon().transform).rect.width, ((RectTransform)actualCard.GetCardIcon().transform).rect.height);
       ((RectTransform)placeholderText.transform).sizeDelta = new Vector2(((RectTransform)actualCard.GetCardNameTextComponent().transform).rect.width, ((RectTransform)actualCard.GetCardNameTextComponent().transform).rect.height);

            
           ((RectTransform)placeholderIcon.transform).anchoredPosition = new Vector2(((RectTransform)actualCard.GetCardIcon().transform).localPosition.x, ((RectTransform)placeholderIcon.transform).localPosition.y + ((RectTransform)actualCard.GetCardIcon().transform).rect.height);
           ((RectTransform)placeholderText.transform).anchoredPosition = new Vector2(((RectTransform)actualCard.GetCardNameTextComponent().transform).localPosition.x + ((RectTransform)actualCard.GetCardIcon().transform).rect.width, ((RectTransform)placeholderText.transform).localPosition.y + ((RectTransform)actualCard.GetCardIcon().transform).rect.height);
       

          smallCardIconPositionOffset = ((RectTransform)actualCard.GetCardIcon().transform).anchoredPosition.y + ((RectTransform)actualCard.GetCardIcon().transform).rect.height;
          largeIconPositionOffset = ((RectTransform)actualCard.GetCardIcon().transform).localPosition.y;

          smallCardNamePositionOffset = ((RectTransform)actualCard.GetCardNameTextComponent().transform).anchoredPosition.y + ((RectTransform)actualCard.GetCardIcon().transform).rect.height;
          largeCardNamePositionOffset = ((RectTransform)actualCard.GetCardNameTextComponent().transform).localPosition.y;

          placeholderIconCanvasGrp = placeholderIcon.GetOrAddComponent<CanvasGroup> ();
          placeholderIconCanvasGrp.blocksRaycasts = false;
          placeholderIconCanvasGrp.interactable = false;

        }


        public void DisableCardMainButton()
        {
            cardButton.interactable = false;
        }

        public void EnableCardMainButton()
        {
            cardButton.interactable = true;
        }


        public void ArrangeInOriginalOrder()
        {
            if (mainUI.currentEnlargedCard.cardID == cardID)
            {
                SortCardsByID();
//                mainUI.GetScrollAreaRect().enabled = true;
            }
        }


        public void MinimizeRestOfDeck()
        {
            mainUI.minimizingDeckNow = true;
            mainUI.maximizingDeckNow = false;
//            Debug.Log("Minimize deck");
            StartCoroutine(mainUI.MinimizeDeck());

        }


        public void MinimizeHeader()
        {
            mainUI.minimizeHeaderNow = true;
            mainUI.maximizeHeaderNow = false;
        }

        public void MaximizeHeader()
        {
            mainUI.minimizeHeaderNow = false;
            mainUI.maximizeHeaderNow = true;
        }

        public void MinimizeFooter()
        {
           
            mainUI.minimizeFooterNow = true;
            mainUI.maximizeFooterNow = false;

            StartCoroutine( mainUI.MinimizeFooter());
        }

        public void MaximizeFooter()
        {
            mainUI.minimizeFooterNow = false;
            mainUI.maximizeFooterNow = true;
            StartCoroutine(mainUI.MaximizeFooter());
        }

       

        public void SortCardsByID()
        {
            for (int i = 0; i < mainUI.cards.Count; i++)
            {
                ((CardUIPlaceHolder)mainUI.cards[i]).transform.parent = mainUI.GetCardPanel().transform;
            }
        }



        public void DisablePlaceholderContent()
        {
            placeholderGraphicsContent.gameObject.SetActive(false);
        }

        public void EnablePlaceholderContent()
        {
            placeholderGraphicsContent.gameObject.SetActive(true);
        }

        
        float placeholderFadeCounter = 0;

		bool fadeIn = false;
		bool fadeOut = false;

        public IEnumerator FadePlaceHolderOut()
        {
			fadeOut = true;
			fadeIn = false;

            while (placeholderFadeCounter <= 1 && fadeOut)
            {
                placeholderFadeCounter += Time.deltaTime * placeholderFadeSpeedMultiplier;
				float alphaValue = Mathf.Lerp(1, 0, mainUI.cardPresentationCurve.Evaluate(placeholderFadeCounter));
                placeholderGraphicsContent.color = new Color(placeholderGraphicsContent.color.r, placeholderGraphicsContent.color.g, placeholderGraphicsContent.color.b, alphaValue);
                placeholderText.color = new Color(placeholderText.color.r, placeholderText.color.g, placeholderText.color.b, alphaValue);
                placeholderIcon.color = new Color(1, 1, 1, alphaValue);
                yield return new WaitForEndOfFrame();
            }
			if(fadeOut){
	            placeholderFadeCounter = 1;
	            placeholderGraphicsContent.color = new Color(placeholderGraphicsContent.color.r, placeholderGraphicsContent.color.g, placeholderGraphicsContent.color.b,0);
	            placeholderText.color = new Color(placeholderText.color.r, placeholderText.color.g, placeholderText.color.b, 0);
	            placeholderIcon.color = new Color(1, 1, 1, 0);

	            DisablePlaceholderContent();
			}
        }

        public IEnumerator FadePlaceHolderIn()
        {
			fadeOut = false;
			fadeIn = true;
            EnablePlaceholderContent();
            
            while (placeholderFadeCounter >= 0 && fadeIn)
            {
//                Debug.Log(placeholderFadeCounter);
                placeholderFadeCounter -= Time.deltaTime * placeholderFadeSpeedMultiplier;
				float alphaValue = Mathf.Lerp(1, 0, mainUI.cardPresentationCurve.Evaluate(placeholderFadeCounter));
                placeholderGraphicsContent.color = new Color(placeholderGraphicsContent.color.r, placeholderGraphicsContent.color.g, placeholderGraphicsContent.color.b, alphaValue);
                placeholderText.color = new Color(placeholderText.color.r, placeholderText.color.g, placeholderText.color.b, alphaValue);
                placeholderIcon.color = new Color(1, 1, 1, alphaValue);
                yield return new WaitForEndOfFrame();
            }
             if (fadeIn) {
				placeholderFadeCounter = 0;
				placeholderGraphicsContent.color = new Color (placeholderGraphicsContent.color.r, placeholderGraphicsContent.color.g, placeholderGraphicsContent.color.b, 1);
				placeholderText.color = new Color (placeholderText.color.r, placeholderText.color.g, placeholderText.color.b, 1);
				placeholderIcon.color = new Color (1, 1, 1, 1);
			}
        }

        int taskCount = 0;


        public void StopClickTask()
        {
            StopCoroutine(OnPlaceHolderClickWaitTask());
        }
        public void OnPlaceHolderClick(){
           // StartCoroutine(OnPlaceHolderClickWaitTask());
            if (mainUI.presentCard == false && mainUI.unPresentCard == false && !mainUI.horizontalSwipe && !mainUI.verticalSwipe && !mainUI.lerpingCardPanelsSideways)
            {
                /*
                taskCount++;
                Debug.Log("Current amount of tasks: " + taskCount);
                StopCoroutine(OnPlaceHolderClickWaitTask());
                StartCoroutine(OnPlaceHolderClickWaitTask());
                */
                cardIsFocused = true;
                mainUI.presentCard = true;
                mainUI.StoreCardsPositionInDeckBeforeAnimate();
                mainUI.currentEnlargedCard = this;
                StartCoroutine(PresentEnlargedCard());
                mainUI.DisableAllCardPanelVerticalScrolling();
                mainUI.unPresentCard = false;
                MinimizeRestOfDeck();
                MinimizeHeader();
                MinimizeFooter();
                DisableCardMainButton();
            }
            
        }
        public IEnumerator OnPlaceHolderClickWaitTask()
        {
            if (mainUI.presentCard == false && mainUI.unPresentCard == false && !mainUI.horizontalSwipe && !mainUI.verticalSwipe && !mainUI.lerpingCardPanelsSideways)
            {
                yield return new WaitForSeconds(0.2f);
            }
            if (mainUI.presentCard == false && mainUI.unPresentCard == false && !mainUI.horizontalSwipe && !mainUI.verticalSwipe && !mainUI.lerpingCardPanelsSideways)
            {
                /*
                taskCount++;
                Debug.Log("Current amount of tasks: " + taskCount);
                StopCoroutine(OnPlaceHolderClickWaitTask());
                StartCoroutine(OnPlaceHolderClickWaitTask());
                */
                cardIsFocused = true;
                mainUI.presentCard = true;
                mainUI.StoreCardsPositionInDeckBeforeAnimate();
                mainUI.currentEnlargedCard = this;
                StartCoroutine(PresentEnlargedCard());
                mainUI.DisableAllCardPanelVerticalScrolling();
                mainUI.unPresentCard = false;
                MinimizeRestOfDeck();
                MinimizeHeader();
                MinimizeFooter();
                DisableCardMainButton();
            }
            yield break;
        }


        public IEnumerator PresentEnlargedCard()
        {
        //    mainUI.DisableAllOtherPanels();
            ViewIScreenController.cardTransitionRunning = true;
            this.transform.parent.GetComponent<VerticalLayoutGroup>().enabled = false;

         
            while ((presentCardLerpPositionCounter < 1 || presentCardLerpSizeCounter < 1) && mainUI.presentCard && !mainUI.unPresentCard)
            {
                if (presentCardLerpPositionCounter < 1)
                {
					presentCardLerpPositionCounter += Time.deltaTime *presentLerpMultiplier*1.2f;// presentTimeLerpMultiplier;
                    ((RectTransform)this.transform).anchoredPosition = Vector2.Lerp(new Vector2(0, mainUI.cardsPositionInMaximizedDeck[this.cardID].y), new Vector2(0, mainUI.mainContentCardYPosition), mainUI.cardPresentationCurve.Evaluate(presentCardLerpPositionCounter) );
                    //   ((RectTransform)currentEnlargedCard.transform).anchoredPosition = new Vector2(10*Mathf.Sin(amplitude*Mathf.PI * presentCardLerpPositionCounter * frequency + phase) , ((RectTransform)currentEnlargedCard.transform).anchoredPosition.y);
                }

                if (presentCardLerpSizeCounter < 1)
                {
					presentCardLerpSizeCounter += Time.deltaTime*presentLerpSizeMultiplier;;
					((RectTransform)this.placeholderIcon.gameObject.transform).anchoredPosition = new Vector2(((RectTransform)this.placeholderIcon.gameObject.transform).anchoredPosition.x, Mathf.Lerp(smallCardIconPositionOffset, largeIconPositionOffset, mainUI.cardPresentationCurve.Evaluate(presentCardLerpSizeCounter) ));
					((RectTransform)this.placeholderText.gameObject.transform).anchoredPosition = new Vector2(((RectTransform)this.placeholderText.gameObject.transform).anchoredPosition.x, Mathf.Lerp(smallCardIconPositionOffset, largeIconPositionOffset, mainUI.cardPresentationCurve.Evaluate(presentCardLerpSizeCounter) ));
					((RectTransform)this.transform).sizeDelta = new Vector2(((RectTransform)this.transform).sizeDelta.x, Mathf.Lerp(mainUI.cardMinimizedPreferredHeight, ((RectTransform)actualCard.GetGameObject().transform).rect.height, mainUI.cardPresentationCurve.Evaluate(presentCardLerpSizeCounter) ));

                }
                yield return null;

            }
            this.actualCard.MakeCardContentVisible();
            presentCardLerpSizeCounter = 1;
            presentCardLerpPositionCounter = 1;

            ((RectTransform)this.transform).anchoredPosition = new Vector2(0, mainUI.mainContentCardYPosition);
            ((RectTransform)this.transform).sizeDelta = new Vector2(((RectTransform)this.transform).sizeDelta.x, ((RectTransform)actualCard.GetGameObject().transform).rect.height);
           
            


         //   ((RectTransform)this.actualCard.GetGameObject().transform).position = new Vector3(0, ((RectTransform)this.transform).position.y, ((RectTransform)this.actualCard.GetGameObject().transform).position.z);

            this.actualCard.ShowFrontSide();


            StartCoroutine(this.FadePlaceHolderOut());

            originalCardPanelParent = transform.parent;
            transform.parent.parent.parent.GetComponent<CanvasGroup>().blocksRaycasts = false;
            cardPanelChildNr = transform.GetSiblingIndex();
			mainUI.presentCardCanvas.enabled = true;
            this.transform.SetParent(mainUI.presentCardCanvas.transform, true);
            ViewIScreenController.cardTransitionRunning = false;
        }

        Transform originalCardPanelParent;
        int cardPanelChildNr;

        public IEnumerator UnPresentEnlargedCard()
        {
            ViewIScreenController.cardTransitionRunning = true;

            if (this.actualCard.GetRotateCardToReadMore())
            {
                Debug.Log("Card is rotated, need to be rotated back");

                this.actualCard.RotateCardBackToOriginal();
                while (this.actualCard.GetRotateCardToReadMore())
                {
                    yield return null;
                }
            }

            

            StartCoroutine(this.FadePlaceHolderIn());
            while (this.placeholderGraphicsContent.color.a < 1)
            {
                yield return null;
            }
            this.transform.SetParent(originalCardPanelParent, true);
            this.transform.SetSiblingIndex(cardPanelChildNr);
            transform.parent.parent.parent.GetComponent<CanvasGroup>().blocksRaycasts = true;

			for (int i = 0; i < transform.childCount; i++) {
				ICard child = (ICard) transform.GetChild(i).GetComponent(typeof(ICard));
				
				if(child != null){
					child.GetGameObject().SetActive(false);
				}
			}
         
			this.actualCard.MakeCardContentInvisible();

            while (presentCardLerpSizeCounter >= 0 || presentCardLerpPositionCounter >= 0 && mainUI.unPresentCard && !mainUI.presentCard)
            {
                if (presentCardLerpPositionCounter > 0 && presentCardLerpSizeCounter < 0.95)
                {
					presentCardLerpPositionCounter -= Time.deltaTime * presentLerpMultiplier;
					((RectTransform)this.transform).anchoredPosition = Vector2.Lerp(new Vector2(0, mainUI.cardsPositionInMaximizedDeck[this.cardID].y), new Vector2(0, mainUI.mainContentCardYPosition), mainUI.cardPresentationCurve.Evaluate(presentCardLerpPositionCounter) );

                }

                if (presentCardLerpSizeCounter > 0 )
                {
					presentCardLerpSizeCounter -= Time.deltaTime*(presentLerpSizeMultiplier-1.0f);
					((RectTransform)this.transform).sizeDelta = new Vector2(((RectTransform)this.transform).sizeDelta.x, Mathf.Lerp(((RectTransform)actualCard.GetGameObject().transform).rect.height, mainUI.cardMinimizedPreferredHeight, 1 - mainUI.cardPresentationCurve.Evaluate(presentCardLerpSizeCounter) ));
					((RectTransform)this.placeholderIcon.gameObject.transform).anchoredPosition = new Vector2(((RectTransform)this.placeholderIcon.gameObject.transform).anchoredPosition.x, Mathf.Lerp(smallCardIconPositionOffset, largeIconPositionOffset, mainUI.cardPresentationCurve.Evaluate(presentCardLerpSizeCounter)));
					((RectTransform)this.placeholderText.gameObject.transform).anchoredPosition = new Vector2(((RectTransform)this.placeholderText.gameObject.transform).anchoredPosition.x, Mathf.Lerp(smallCardIconPositionOffset, largeIconPositionOffset, mainUI.cardPresentationCurve.Evaluate(presentCardLerpSizeCounter) ));
                                    
                }
                yield return null;
            }
           // mainUI.EnableOnlyClosestsPanels();
            mainUI.unPresentCard = false;
            presentCardLerpPositionCounter = 0;
            presentCardLerpSizeCounter = 0;
            ViewIScreenController.cardTransitionRunning = false;
        }
    }
}