﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Thrive
{
    public class AdditionalCardInfo : MonoBehaviour, ICard
    {
        public Button backButton;
        float lerpCounter = 0;
        MainUI mainUI;
        public Text mainTextArea;
        public Image cardBackground;
        public Scrollbar scrollBar;
        public ScrollRect scrollRect;
		public RectTransform mainContentArea;
		public DynamicScrollbar scrollBarController;
		public ICard belongsToCard;
        bool couldBeSwipe;
        bool verticalSwipe;
        bool horizontalSwipe;
        Vector3 startPos;
        Vector3 cardPanelPositionOnSwipeStart;
        float startTime;
        float swipeDist;
        float minSwipeDist = 40.0f;
        float minSwipeTime = 0.0f;

        public Text fontSizeCalculation;

        // Use this for initialization
        void Awake()
        {

            mainUI = FindObjectOfType<MainUI>();
            mainTextArea.rectTransform.pivot = new Vector2(0.5f, 1.0f);
            if (scrollBar != null)
            {
               // ShouldScrollbarBeShown();
                scrollBar.value = 1;
            }
			cardBackground.type = Image.Type.Sliced;
        }

        IEnumerator Start()
        {
            SetUpEventListeners();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            if (scrollBar != null)
            {
                scrollBar.value = 1;
                
            }
        }

        public void SetMainTexture(Sprite sprite,Color color) //(Sprite sprite)
        {
            cardBackground.sprite = sprite;
            cardBackground.color = color;
        }
        public void SetMainText(string newText, Color newTextColor)
        {


			mainTextArea.text = newText;
            mainTextArea.color = newTextColor;
         
            scrollBar.value = 1;
            mainTextArea.rectTransform.pivot = new Vector2(0.5f, 1.0f);
			//scrollBarController.ShouldScrollbarBeShown();

        }

		public void InsertImage(Image newImage){
			newImage.transform.SetParent (mainTextArea.transform);
			newImage.type = Image.Type.Simple;
			newImage.preserveAspect = true;

		}

        void SetUpEventListeners()
        {
          //  backButton.on .onClick.AddListener(() => { StartCoroutine(ShowPreviousScreen()); });
            EventTrigger eventTrigger0 = backButton.GetComponent<EventTrigger>();
            CustomUtilities.AddEventTrigger(eventTrigger0, ShowPreviousScreen, EventTriggerType.PointerDown);
        }
        void Update()
        {
            if (!ViewIScreenController.IsTransitionRunning())
            {
                CheckForSwipe();
            }
			if (mainTextArea != null && fontSizeCalculation != null) {
				mainTextArea.resizeTextMinSize = fontSizeCalculation.cachedTextGenerator.fontSizeUsedForBestFit;
				mainTextArea.resizeTextMaxSize = fontSizeCalculation.cachedTextGenerator.fontSizeUsedForBestFit;
			}
        }

        public void AmmestillingHyperlinkClick()
        {
            Application.OpenURL("http://www.sundhedsplejersken.nu/amning/");
        }

        void CheckForSwipe()
        {
            if (Input.GetMouseButtonDown(0))
            {
                couldBeSwipe = true;
                startPos = Input.mousePosition;
                startTime = Time.time;
                cardPanelPositionOnSwipeStart = ((RectTransform)mainUI.GetDeckPanel().transform).anchoredPosition3D;
            }
            if (Input.GetMouseButton(0))
            {
                float swipeTime = Time.time - startTime;
                swipeDist = (Input.mousePosition - new Vector3(startPos.x, startPos.y, 0)).magnitude;
                //Vector3 swipeDirectionVector = (Input.mousePosition - new Vector3(startPos.x, startPos.y, 0)).normalized;

                if (couldBeSwipe && (swipeTime > minSwipeTime) && (swipeDist > minSwipeDist))
                {
                    // It's a swiiiiiiiiiiiipe!
                    var swipeDirection = Mathf.Abs(Input.mousePosition.y - startPos.y);
                    if (swipeDirection >= 20 && !horizontalSwipe)
                    {
                        verticalSwipe = true;
                        horizontalSwipe = false;
                      //  EnableAllCardPanelVerticalScrolling();
                    }

                    if (!verticalSwipe || horizontalSwipe)
                    {
                        horizontalSwipe = true;
                        verticalSwipe = false;
                        
                      //  Debug.Log("Horizontal swipe");
                    }
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (horizontalSwipe)
                {
                    ShowPreviousScreen();
                }
                swipeDist = 0;
                couldBeSwipe = false;
                horizontalSwipe = false;
                verticalSwipe = false;
            }

        }

        void PerformDeckSwipeActions()
        {
            if (horizontalSwipe)
            {
                ((RectTransform)this.transform).anchoredPosition3D = new Vector3(Input.mousePosition.x , ((RectTransform)this.transform).anchoredPosition3D.y, ((RectTransform)this.transform).anchoredPosition3D.z);
            }
        }

        public void ShouldScrollbarBeShown()
        {
            if (mainTextArea.preferredHeight < ((RectTransform)scrollRect.transform).rect.height) 
            {
                Debug.Log("Disable scroll bar");
                scrollBar.gameObject.SetActive(false);
                scrollRect.vertical = false;
                
            }
			mainTextArea.rectTransform.anchoredPosition = new Vector2(mainTextArea.rectTransform.anchoredPosition.x, 0);
        }

        private List<string> SplitText(string baseString)
        {
            Vector2 extents = new Vector2(mainTextArea.rectTransform.rect.width, mainTextArea.rectTransform.rect.height);
            List<string> textList = new List<string>();
            TextGenerator tg = new TextGenerator();
            TextGenerationSettings generationSettings = mainTextArea.GetGenerationSettings(extents);
            string remainingText = baseString;
            bool isDone = false;

            while (!isDone)
            {
                if (remainingText.StartsWith("\n")) //Remove linebreak at the start
                    remainingText = remainingText.Remove(0, 2);

                tg.Populate(remainingText, generationSettings);
                //			Debug.Log ("Populated characters: " + tg.characterCountVisible);

                if (tg.characterCountVisible < remainingText.Length)
                {
                    int splitPoint = SplitAppropriately(remainingText, tg.characterCountVisible);

                    textList.Add(remainingText.Substring(0, splitPoint));
                    remainingText = remainingText.Substring(splitPoint);
                }
                else
                {
                    textList.Add(remainingText);
                    isDone = true;
                }
            }
            int combinedLength = 0;
            foreach (string s in textList)
                combinedLength += s.Length;

            Debug.Log("Splitting done! Original length: " + baseString.Length + ", Split combined: " + combinedLength);

            //		for (int i = 0; i < textList.Count; i++) 
            //		{		
            //			Debug.Log (i+ "_" + textList[i]);
            //		}

            return textList;
        }

        private int SplitAppropriately(string s, int index)
        {
            if (s[index].ToString() == System.Environment.NewLine) //It's a line break
                return index;

            if (index + 1 < s.Length && s[index + 1] == ' ')
                return index;

            //Go back toward a SPACE
            int tryIndex = index;
            while (tryIndex > 0)
            {
                if (s[tryIndex] == ' ')
                    return tryIndex;

                tryIndex--;
            }

            Debug.LogWarning("TextSplitter did not find a SPACE for appropiate splitting!");

            return index;
        }
        public void ShowPreviousScreen()
        {
            StartCoroutine(ShowPreviousScreenTask());
        }


        public IEnumerator ShowPreviousScreenTask(){
            //mainUI.SetScreenInFocus(previousScreen);
            //previousScreen.EnableScreen();
			belongsToCard.MakeCardContentVisible ();
            while (lerpCounter < 1)
            {
                lerpCounter += Time.deltaTime * Constants.transitionLerpSidewaysMultiplier;
                ((RectTransform)gameObject.transform).anchoredPosition3D = new Vector3(Mathf.Lerp(0, mainUI.mainCanvas.pixelRect.width, lerpCounter), ((RectTransform)gameObject.transform).anchoredPosition3D.y, ((RectTransform)gameObject.transform).anchoredPosition3D.z);

                yield return null;
            }
            
            lerpCounter = 0;
            DisableScreen();

        }
        public void DisableScreen()
        {
            Destroy(gameObject);
        }

		/// <summary>
		/// ICard interface implemented methods
		/// </summary>
		/// 
		public const string cardUID = "";
		
		public string GetCardUID()
		{
			return cardUID;
		}
		
		public int GetCardOwnerID()
		{
			return 0;
			
		}
		
		public void SetCardOwnerID(int newCardOwner)
		{
			// cardOwnerID = newCardOwner;
		}
		
		public GameObject GetGameObject()
		{
			return gameObject;
		}
		
		public void MakeCardContentVisible()
		{
			gameObject.SetActive(true);
		}
		
		public void MakeCardContentInvisible()
		{
			gameObject.SetActive(false);
			// listenForAlertChanges = false;
		}
		
		public void ShowFrontSide()
		{
			// listenForAlertChanges = true;
			//StartCoroutine(UpdateAlertArea());
		}
		public void ShowBackSide()
		{
			
		}
		public bool GetRotateCardToReadMore()
		{
			return true;
		}
		public string GetCardName()
		{
			return "";
		}
		public void SetRotateCardToReadMore(bool newReadMoreValue)
		{
			// rotateCardToReadMore = newReadMoreValue;
		}
		public void SetCardMaterial(Material material)
		{
			
		}
		public void RotateCardToReadMore()
		{
			
			
		}
		
		public void RotateCardBackToOriginal()
		{
			
		}
		
		public Image GetCardIcon()
		{
			return null;
		}
		public Text GetCardNameTextComponent()
		{
			return null;
		}

        public Image GetCardImageTexture()
        {
            return null;
        }
    }
}