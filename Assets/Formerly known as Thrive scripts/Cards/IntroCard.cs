﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Thrive
{
    public class IntroCard : MonoBehaviour, ICard
    {
        MainUI mainUI;
        public Image cardIcon;
        public Text cardNameTextComponent;

        public TutorialScreen tutorialScreenPrefab;
        TutorialScreen tutorialScreenInstance;
		public Button createChildButton;
        public Image cardImageTexture;


		void Awake(){
			tutorialScreenInstance = (TutorialScreen)Instantiate(tutorialScreenPrefab, Vector3.zero, Quaternion.identity);

            if (tutorialScreenInstance.isActiveAndEnabled)
            {
                tutorialScreenInstance.DisableScreen();
            }
		}

        // Use this for initialization
        void Start()
        {
            mainUI = FindObjectOfType<MainUI>();
            LayoutElement layout = GetComponent<LayoutElement>();
            layout.preferredHeight = ((RectTransform)mainUI.GetMainContentPanel().transform).rect.height - ((RectTransform)mainUI.GetFooterPanel().transform).rect.height;
            layout.preferredWidth = Screen.width;

			createChildButton.onClick.AddListener(() => { OnClickCreateChild(); });
            //ShowTutorial();
           // StartCoroutine(WhileNotLoggedIn());
        }

        IEnumerator WhileNotLoggedIn()
        {
            while (mainUI.GetFaderScreen().fadeCycleRunning)
            {
                yield return null;
            }
             ShowTutorial();
        }

		void OnClickCreateChild(){
			OptionsScreen optionsScreen = mainUI.GetOptionsScreen();
            optionsScreen.GetInstatiatedCreateKidProfile().ShowThisScreen();
            optionsScreen.DisableScreen();
           // Destroy(optionsScreen.GetGameObject());
		}

        public void ShowTutorial()
        {
			if (mainUI.GetCurrentDeckID () == 0) {
            
				tutorialScreenInstance.ShowThisScreen ();
			}
        }

        public void HideTutorial()
        {
            if (!tutorialScreenInstance)
            {
                tutorialScreenInstance = (TutorialScreen)Instantiate(tutorialScreenPrefab, Vector3.zero, Quaternion.identity);
            }
            if (tutorialScreenInstance.gameObject.activeSelf)
            {
                tutorialScreenInstance.ShowPreviousScreen();
            }
        }

        public TutorialScreen GetTutorialScreen()
        {
            return tutorialScreenInstance;
        }

        /// <summary>
        /// ICard interface implemented methods
        /// </summary>
        /// 
        public const string cardUID = "MIDT_000_INTRO";

        public string GetCardUID()
        {
            return cardUID;
        }

        public int GetCardOwnerID()
        {
            return 0;

        }

        public void SetCardOwnerID(int newCardOwner)
        {
           // cardOwnerID = newCardOwner;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public void MakeCardContentVisible()
        {
              gameObject.SetActive(true);
        }

        public void MakeCardContentInvisible()
        {
              gameObject.SetActive(false);
           // listenForAlertChanges = false;
        }

        public void ShowFrontSide()
        {
           // listenForAlertChanges = true;
            //StartCoroutine(UpdateAlertArea());
        }
        public void ShowBackSide()
        {

        }
        public bool GetRotateCardToReadMore()
        {
            return false;
        }
        public string GetCardName()
        {
            return "";
        }
        public void SetRotateCardToReadMore(bool newReadMoreValue)
        {
           // rotateCardToReadMore = newReadMoreValue;
        }
        public void SetCardMaterial(Material material)
        {

        }
        public void RotateCardToReadMore()
        {

            
        }

        public void RotateCardBackToOriginal()
        {

        }

        public Image GetCardIcon()
        {
            return cardIcon;
        }
        public Text GetCardNameTextComponent()
        {
            return cardNameTextComponent;
        }

        public Image GetCardImageTexture()
        {
            return cardImageTexture;
        }
    }
}
