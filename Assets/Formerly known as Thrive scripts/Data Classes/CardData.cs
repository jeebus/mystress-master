﻿using UnityEngine;
using System.Collections;

namespace Thrive{
public class CardData {

    public string cardName;
    public GameObject cardPrefab;
    public Sprite cardTexture;
    public Color cardColor;
    public Sprite cardIcon;


    public CardData(string newName, CardTypes.GroCards cardType, GameObject newPrefab, Sprite newCardTexture, Color newCardColor, Sprite newCardIcon)
    {
        cardName = newName;
        cardPrefab = newPrefab;
        cardTexture = newCardTexture;
        cardColor = newCardColor;
        newCardIcon = cardIcon;
    }
}
}