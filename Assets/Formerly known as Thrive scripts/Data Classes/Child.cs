﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Thrive
{

    public enum ChildGender
    {
        M , //male
        F , //female
        U //unknown
    }

    public enum ChildUnit
    {
        I, //Imperial
        M   //Metric
    }



    [Serializable]
    public class Child
    {
        string name = "";
        string childRealName = "";
        int childID = 0;
        DateTime childBirthDate;
        string childGender = "";
        int childHeight = 0;
        int childWeight = 0;
        string childMeassurementUnit = "";


        public List<string> availableCards = new List<string>();

        public Child(){

        }

        public Child(string newChildRealName, int newChildID, DateTime newChildbirthDate, string newChildGender, int newChildHeight, int newChildWeight, string newChildMeassurementUnit)
        {
            childRealName = newChildRealName;
            childID = newChildID;
            childBirthDate = newChildbirthDate;
            childGender = newChildGender;
            childHeight = newChildHeight;
            childWeight = newChildWeight;
            childMeassurementUnit = newChildMeassurementUnit;

        }

        public int GetChildID()
        {
            return childID;
        }
        public void SetChildHeight(int height)
        {
            childHeight=height;
        }
        public void SetChildWeight(int weight)
        {
            childWeight = weight;
        }


        public string GetChildRealName()
        {
            return childRealName;
        }

        public string GetBirthDate()
        {

            return childBirthDate.ToString("yyyy'-'MM'-'dd");

        }

        public int GetChildHeight()
        {
            return childHeight;
        }


        public int GetChildWeight()
        {
            return childWeight;
        }

        public string GetChildGender()
        {
            return childGender;
        }

        public string GetChildMeassurementUnit()
        {
            return childMeassurementUnit;
        }

        public string GetName()
        {
            return name;

        }

    }
}