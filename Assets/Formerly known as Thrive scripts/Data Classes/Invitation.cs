﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Thrive
{
    public class Invitation
    {
        string invitationHash;
        string invitationDate;

        string invitationFromID;
        string invitationFromRealName;
        string invitationFromUserName;

        string invitationToID;
        string invitationToRealName;
        string invitationToUserName;

        string invitationStatus;

        public Invitation(string hash, string date, string fromID, string fromRealName, string fromUserName, string toID, string toRealName, string toUserName, string currentInvitationStatus)
        {
            invitationHash = hash;
            invitationDate = date;
            invitationFromID = fromID;
            invitationFromRealName = fromRealName;
            invitationFromUserName = fromUserName;
            invitationToID = toID;
            invitationToRealName = toRealName;
            invitationToUserName = toUserName;
            invitationStatus = currentInvitationStatus;
        }

        public string GetInvitationHash()
        {
            return invitationHash;
        }

        public string GetInvitationDate()
        {
            return invitationDate;
        }

        public string GetInvitationFromID()
        {
            return invitationFromID;
        }

        public string GetInvitationFromRealName()
        {
            return invitationFromRealName;
        }

        public string GetInvitationFromUserName()
        {
            return invitationFromUserName;
        }

        public string GetInvitationToID()
        {
            return invitationToID;
        }

        public string GetInvitationToRealName()
        {
            return invitationToRealName;
        }

        public string GetInvitationToUserName()
        {
            return invitationToUserName;
        }
        public string GetInvitationStatusType()
        {
            return invitationStatus;
        }

    }
}
