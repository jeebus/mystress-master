﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


namespace Thrive
{
    [Serializable]
    public class User 
    {

        public enum Role
        {
            Owner,
            Child,
            todo
        }

        public Role userRole;

        string name = "";
        string realName = "";
        string email = "";
        int ID = 0;
        DateTime childBirthDate;
        string childGender = "";
        int childHeight = 0;
        int childWeight = 0;
        string childMeassurementUnit = "";


        public User(string newUserName, Role newUserRole)
        {
            name = newUserName;
            userRole = newUserRole;
        }

        public User(string newUserName, string newUserRealName, int newID, string userEmail, Role newUserRole)
        {
            name = newUserName;
            userRole = newUserRole;
            email = userEmail;
            ID = newID;
            realName = newUserRealName;
        }

        public int GetUserID()
        {
            return ID;
        }
        public string GetUserRealName()
        {
            return realName;
        }

        public string GetBirthDate()
        {
            return childBirthDate.ToString("yyyy'-'MM'-'dd");
        }

        public int GetChildHeight()
        {
            return childHeight;
        }


        public int GetChildWeight()
        {
            return childWeight;
        }

        public string GetChildGender()
        {
            return childGender;
        }

        public string GetChildMeassurementUnit()
        {
            return childMeassurementUnit;
        }

        public string GetName()
        {
            return name;

        }
        public string GetUserEmail()
        {
            return email;

        }
    }
}