﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Thrive
{
    public class Deck
    {
        public CardPanel deckCardPanel;
        public List<CardUIPlaceHolder> cards = new List<CardUIPlaceHolder>();
        int deckID;

        public void SetDeckID(int ID){
            deckID = ID;

        }

        public int GetDeckID()
        {
           return deckID;
        }
    }
}
