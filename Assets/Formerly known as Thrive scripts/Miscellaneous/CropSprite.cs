﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Thrive
{
    public class CropSprite : MonoBehaviour
    {
        WebCamTexture webcamTexture = null;

        public Image spriteToCrop;
        public Image spriteDisplayPortrait;


        public Button cropButton;

        void Start()
        {
            #if !UNITY_EDITOR && UNITY_ANDROID|| UNITY_IPHONE
             // Checks how many and which cameras are available on the device
        for (int cameraIndex = 0; cameraIndex < WebCamTexture.devices.Length; cameraIndex++)
        {
            // We want the back camera
            if (!WebCamTexture.devices[cameraIndex].isFrontFacing)
            {
                webcamTexture = new WebCamTexture(cameraIndex, Screen.width, Screen.height);
                // Here we flip the GuiTexture by applying a localScale transformation
                // works only in Landscape mode
                spriteDisplayPortrait.transform.localScale = new Vector3(1,1,-1);
            }
        }
           // webcamTexture = new WebCamTexture();
            webcamTexture.Play();
#endif

            cropButton.onClick.AddListener(() => { TakeSnapshot(); });
        }

        IEnumerator WaitForUserPermission()
        {
            yield return Application.RequestUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone);
            if (Application.HasUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone))
            {
                // we got permission. Set up webcam and microphone here.
            }
            else
            {
                // no permission. Show error here.
            }
        }
        void DownScaleImage(Sprite spriteToScale)
        {
            Texture2D spriteTexture = spriteToScale.texture;
            Rect spriteRect = spriteToScale.textureRect;
            spriteTexture.Resize(64, 64);
            spriteTexture.Apply();
            Sprite downscaleSprite = Sprite.Create(spriteTexture, spriteRect, new Vector2(0, 1));
            spriteDisplayPortrait.sprite = downscaleSprite;

        }

        void TakeSnapshot()
        {
            Texture2D snap = new Texture2D(webcamTexture.width, webcamTexture.height);
            snap.SetPixels(webcamTexture.GetPixels());
            snap.Apply();
            Rect rect = new Rect(0, 0, snap.width, snap.height);
            spriteDisplayPortrait.sprite = Sprite.Create(snap, rect,new Vector2(0.5f,0.5f));
          //  DownScaleImage(spriteDisplayPortrait.sprite);
          //  System.IO.File.WriteAllBytes(_SavePath + _CaptureCounter.ToString() + ".png", snap.EncodeToPNG());

        }

    }
}