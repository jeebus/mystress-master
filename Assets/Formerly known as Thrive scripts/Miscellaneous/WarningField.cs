﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Thrive
{
    public class WarningField : MonoBehaviour
    {

        public CanvasGroup warningGroup;
        float fadeLerpCounter = 0;

        // Use this for initialization
        void Start()
        {

        }

        public void ShowText()
        {

            StartCoroutine(FadeIn());
        }

        public void HideText()
        {
            StartCoroutine(FadeOut());
        }


        IEnumerator FadeOut()
        {
            
            while(fadeLerpCounter >= 0){
                warningGroup.alpha = fadeLerpCounter;
                fadeLerpCounter -= Time.deltaTime;
                yield return null;

            }
            warningGroup.alpha = 0;
            fadeLerpCounter = 0;
            yield return null;
        }

        IEnumerator FadeIn()
        {
            
            while (fadeLerpCounter <= 1)
            {
                warningGroup.alpha = fadeLerpCounter;
                fadeLerpCounter += Time.deltaTime;
                yield return null;
            }
             
            fadeLerpCounter = 1;
            warningGroup.alpha = 1;
            yield return null;
        }
    }

}