﻿using UnityEngine;
using System.Collections;

namespace Thrive
{


    public static class Constants
    {
        public const int userPasswordMinimumLength = 4;

        public const float transitionLerpSidewaysMultiplier = 3.0f;

    }
}