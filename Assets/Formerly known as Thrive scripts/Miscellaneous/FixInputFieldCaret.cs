﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class FixInputFieldCaret : MonoBehaviour, ISelectHandler
{

    public void OnSelect(BaseEventData eventData)
    {
        InputField ipFld = gameObject.GetComponent<InputField>();
        if (ipFld != null)
        {
            RectTransform caretTransform = (RectTransform)transform.Find(gameObject.name + " Input Caret");
            //if (caretTransform != null )//&& textTransform != null)
            //{
            caretTransform.pivot = new Vector2(0.5f,0.65f);

			//caretTransform.anchorMax = new Vector2(caretTransform.anchorMax.x,0.6f);
			//caretTransform.anchorMin = new Vector2(caretTransform.anchorMin.x,0.6f);
//			Debug.Log("Caret pivot updated");
           // }
        }
    }
	
}