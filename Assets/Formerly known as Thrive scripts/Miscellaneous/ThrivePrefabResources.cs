﻿using UnityEngine;
using System.Collections;

namespace Thrive
{
    public static class ThrivePrefabResources
    {
        public static LoginScreen loginScreenPrefab;
        public static AccountCreationScreen createAccountScreenPrefab;
        public static FadeInOutScreen faderScreenPrefab;
        public static CompletedFlowScreen completedFlowScreenPrefab;
        public static OptionsScreen optionsPrefab;
        public static TermsAgreementScreen termsAndAgreementScreenPrefab;
        public static PrivacyStatementScreen privacyStatementScreenPrefab;
        public static RecoverAccountScreen recoverAccountScreenPrefab;
        public static FAQScreen faqScreenPrefab;
        public static PasswordResetScreen passwordResetScreenPrefab;
        public static LoadingScreen loadingScreenPrefab;

        public static GameObject cardPanelPrefab;
        public static BlankBlackScreen blankBlackScreenPrefab;
     
        //Card prefabs
        public static CardUIPlaceHolder cardPlaceHolderPrefab;
        public static GameObject cardHeaderPanelPrefab;

        public static void LoadThrivePrefabs()
        {
            loginScreenPrefab = Resources.Load<LoginScreen>("Prefabs/Login Screen");
            createAccountScreenPrefab = Resources.Load<AccountCreationScreen>("Prefabs/Create account screen");
            faderScreenPrefab = Resources.Load<FadeInOutScreen>("Prefabs/Fader Screen");
            completedFlowScreenPrefab = Resources.Load<CompletedFlowScreen>("Prefabs/Completed flow screen");
            optionsPrefab = Resources.Load<OptionsScreen>("Prefabs/Options screen");
            faqScreenPrefab = Resources.Load<FAQScreen>("Prefabs/FAQ screen");
            termsAndAgreementScreenPrefab = Resources.Load<TermsAgreementScreen>("Prefabs/Terms and agreement screen");
            privacyStatementScreenPrefab = Resources.Load<PrivacyStatementScreen>("Prefabs/Privacy screen");
            recoverAccountScreenPrefab = Resources.Load<RecoverAccountScreen>("Prefabs/Recover account screen");
            passwordResetScreenPrefab = Resources.Load<PasswordResetScreen>("Prefabs/PasswordResetScreen");
            loadingScreenPrefab = Resources.Load<LoadingScreen>("Prefabs/LoadingScreen");

            cardPanelPrefab = Resources.Load<GameObject>("Prefabs/Cardpanel");
            blankBlackScreenPrefab = Resources.Load<BlankBlackScreen>("Prefabs/BlankBlackScreen");
            cardHeaderPanelPrefab = Resources.Load<GameObject>("Prefabs/CardPanelHeaderGroup");

            cardPlaceHolderPrefab = Resources.Load<CardUIPlaceHolder>("Prefabs/Cards/CardPlaceholder");

        }
    }
}
