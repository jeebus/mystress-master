﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

namespace Thrive
{
    public static class CustomUtilities
    {
        static float fadeMultiplier = 3;
        public static IEnumerator UpdateTextOnComponent(Text textComponent,string newString)
        {
            float lerpCounter = 0;
            while (lerpCounter < 1)
            {
                lerpCounter += Time.deltaTime * fadeMultiplier;
                textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, 1 - lerpCounter);
                yield return null;
            }
            lerpCounter = 0;
            textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, 0);
            textComponent.text = newString;
            while (lerpCounter < 1)
            {
                lerpCounter += Time.deltaTime * fadeMultiplier;
                textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, lerpCounter);
                yield return null;
            }
            textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, 1);
        }

        // Note that Color32 and Color implictly convert to each other. You may pass a Color object to this method without first casting it.
        public static string ColorToHex(Color32 color)
        {
            string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
            return hex;
        }

        public static Color HexToColor(string hex)
        {
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            return new Color32(r, g, b, 255);
        }


        public static void AddEventTrigger(EventTrigger eventTrigger, UnityAction action, EventTriggerType triggerType)
        {
            // Create a nee TriggerEvent and add a listener
            EventTrigger.TriggerEvent trigger = new EventTrigger.TriggerEvent();
            trigger.AddListener((eventData) => action()); // you can capture and pass the event data to the listener

            // Create and initialise EventTrigger.Entry using the created TriggerEvent
            EventTrigger.Entry entry = new EventTrigger.Entry() { callback = trigger, eventID = triggerType };

            // Add the EventTrigger.Entry to delegates list on the EventTrigger
            eventTrigger.triggers.Add(entry);
        }

        public static void AddEventTrigger(EventTrigger eventTrigger, UnityAction<BaseEventData> action, EventTriggerType triggerType)
        {
            // Create a nee TriggerEvent and add a listener
            EventTrigger.TriggerEvent trigger = new EventTrigger.TriggerEvent();
            trigger.AddListener((eventData) => action(eventData)); // you can capture and pass the event data to the listener

            // Create and initialise EventTrigger.Entry using the created TriggerEvent
            EventTrigger.Entry entry = new EventTrigger.Entry() { callback = trigger, eventID = triggerType };

            // Add the EventTrigger.Entry to delegates list on the EventTrigger
            eventTrigger.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();// 
            eventTrigger.triggers.Add(entry);
        }
    }

}