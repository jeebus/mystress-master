﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Thrive
{
    public class TextMinimumSize : MonoBehaviour
    {
        public RectTransform parentRectTransform;

        // Use this for initialization
        void Start()
        {
            LayoutElement layout = gameObject.GetComponent<LayoutElement>();
            layout.preferredHeight = parentRectTransform.rect.height;
            //layout.preferredWidth = parentRectTransform.rect.width;
        }

    }
}