﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Thrive
{
    public class ScrollAreaContentAranger : MonoBehaviour {


        public RectTransform content;
        RectTransform scrollArea;
        VerticalLayoutGroup layout;
        public bool addOne;

	    // Use this for initialization
	    void Awake () {
            scrollArea = ((RectTransform)transform);
            layout = content.GetComponent<VerticalLayoutGroup>();
			layout.enabled = false;
	    }

        public IEnumerator UpdateSizeAndArrangement()
        {
            if (content.transform.childCount > 0)
            {
                while (((RectTransform)content.transform.GetChild(0)).rect.height == 0 || content.transform.childCount == 0)
                {
                    Debug.Log("Waiting to update scroll area size");
                    yield return null;
                }
                if (addOne)
                {
                    layout.padding.bottom = (int)Mathf.Max(0, scrollArea.rect.height - ((content.transform.childCount + 1) * ((RectTransform)content.transform.GetChild(0)).rect.height));
                }
                else
                {
                    layout.padding.bottom = (int)Mathf.Max(0, scrollArea.rect.height - ((content.transform.childCount) * ((RectTransform)content.transform.GetChild(0)).rect.height));
                }
                layout.enabled = true;
            }
        }
		public void Enable(){
			layout.enabled = true;
		}
    }
}