﻿using UnityEngine;
using System.Collections;

namespace Thrive
{
    public static class SpriteResources
    {

        public static Sprite backGroundSquareSprite;
        public static Sprite backGroundSprite;
        public static Sprite cardSprite;
        public static Sprite diaperCardSprite;
     //   public static Sprite jaundiceCardSprite;
       // public static Sprite motherCardSprite;
       // public static Sprite bottleCardSprite;
       // public static Sprite breastCardSprite;
        public static Sprite headerSprite;
        public static Sprite switcherIndicator;
        public static Sprite switcherIndicatorEmpty;
        public static Sprite cardNameIconSprite;
        public static Sprite userNameIconSprite;
        public static Sprite horizontalBorderSprite;
        public static Sprite verifiedInputFailedIcon;
        public static Sprite verifiedInputPassedIcon;
        public static Sprite panelFieldSprite;
        public static Sprite panelInsetFieldSprite;
        public static Sprite inputFieldSprite;
        public static Sprite inputFieldSelectedSprite;
        public static Sprite inputFieldSelectedInvalidSprite;
        public static Sprite inputFieldSelectedInvalidSpriteBlack;

        /*
        public static Sprite diaperIcon;
        public static Sprite jaundiceIcon;
        public static Sprite bottleIcon;
        public static Sprite motherIcon;
        public static Sprite breastIcon;

         * */
        public static void LoadInitialSprites()
        {
            backGroundSquareSprite = Resources.Load<Sprite>("Sprites/shared_background_square");
            backGroundSprite = Resources.Load<Sprite>("Sprites/shared_background");
            cardSprite = Resources.Load<Sprite>("Sprites/Shared/maincard_body");

            diaperCardSprite = Resources.Load<Sprite>("Sprites/diapers_cardtexture");
            /*
          jaundiceCardSprite = Resources.Load<Sprite>("Sprites/diapers_cardtexture");
          
          motherCardSprite = Resources.Load<Sprite>("Sprites/005_mor/card_005-bg@1x");
          bottleCardSprite = Resources.Load<Sprite>("Sprites/003_bottle/card_0023");
          breastCardSprite = Resources.Load<Sprite>("Sprites/002_breast/sprites/card_0023");
          */
            headerSprite = Resources.Load<Sprite>("Sprites/shared_header");

            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Shared/switcher-dot");
            if (sprites.Length > 0)
            {
                switcherIndicator = sprites[0];
                if (sprites.Length > 1)
                {
                    switcherIndicatorEmpty = sprites[1];
                }
            }
            cardNameIconSprite = Resources.Load<Sprite>("Sprites/Oval 11");
            userNameIconSprite = Resources.Load<Sprite>("Sprites/head");
            horizontalBorderSprite = Resources.Load<Sprite>("Sprites/h-line");
            verifiedInputFailedIcon = Resources.Load<Sprite>("Sprites/Icon/icn_alert_redwhite@1x");
            verifiedInputPassedIcon = Resources.Load<Sprite>("Sprites/Icon/icn_check_green@1x");
            panelFieldSprite = Resources.Load<Sprite>("Sprites/Shared/labelbg");
            panelInsetFieldSprite = Resources.Load<Sprite>("Sprites/Shared/fwbtn-inset");
            inputFieldSprite = Resources.Load<Sprite>("Sprites/Shared/inputfield-bg-normal");
            inputFieldSelectedSprite = Resources.Load<Sprite>("Sprites/Shared/inputfield-bg-focus");
            inputFieldSelectedInvalidSprite = Resources.Load<Sprite>("Sprites/Shared/inputfield-bg-error");
            inputFieldSelectedInvalidSpriteBlack = Resources.Load<Sprite>("Sprites/Icon/icn_alert_blackwhite@1x");

            /*
            diaperIcon = Resources.Load<Sprite>("Sprites/CardDiapers/assets/diapers_pee_lrg_sel@1x");
            jaundiceIcon = Resources.Load<Sprite>("Sprites/004_gulstot/assets/gulsot_card_icon@1x");
            bottleIcon = Resources.Load<Sprite>("Sprites/003_bottle/cardicon_003");
            motherIcon = Resources.Load<Sprite>("Sprites/005_mor/cardicon_005");
            breastIcon = Resources.Load<Sprite>("Sprites/002_breast/sprites/cardicon_002");
             */
        }
    }
}
