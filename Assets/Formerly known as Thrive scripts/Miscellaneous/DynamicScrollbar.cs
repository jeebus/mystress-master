﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Thrive
{
	public class DynamicScrollbar : MonoBehaviour {

		public ScrollRect scrollRect;
		public Text mainTextArea ;

		// Use this for initialization
		void Awake () {
			if(scrollRect == null){
			scrollRect = GetComponent<ScrollRect>();
			}
			StartCoroutine (YieldBeforeAction());
		}

		void OnEnable(){
			StartCoroutine (YieldBeforeAction());
		}

        public IEnumerator YieldBeforeAction()
        {
			yield return new WaitForSeconds (0.1f);
          //  yield return new WaitForEndOfFrame();
           // yield return new WaitForEndOfFrame();
            //yield return new WaitForEndOfFrame();
            
            ShouldScrollbarBeShown ();
				
		}


		public void ShouldScrollbarBeShown()
		{
//			Debug.Log("mainTextArea.preferredHeight " +mainTextArea.rectTransform.rect.height + " (RectTransform)scrollRect.transform).rect.height " + ((RectTransform)scrollRect.transform).rect.height);
			//Debug.Log ("Scrollbar size " +scrollRect.verticalScrollbar.size );

			if (mainTextArea.rectTransform.rect.height < ((RectTransform)scrollRect.transform).rect.height) {
		//		Debug.Log ("Disable scroll bar");
				scrollRect.verticalScrollbar.gameObject.SetActive (false);
				scrollRect.vertical = false;
				mainTextArea.rectTransform.anchoredPosition = new Vector2(mainTextArea.rectTransform.anchoredPosition.x, 0);
			} else {
	//			Debug.Log ("Enable scroll bar");
				scrollRect.verticalScrollbar.gameObject.SetActive (true);
				scrollRect.vertical = true;
				mainTextArea.rectTransform.anchoredPosition = new Vector2(mainTextArea.rectTransform.anchoredPosition.x, 0);
			}
		}
	}
}