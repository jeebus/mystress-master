﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Thrive
{
    public class AvailableLanguageList : MonoBehaviour
    {
        public Text currentLanguageText;

        List<SmartLocalization.SmartCultureInfo> availableLanguages;
        public Button forwardButton;
        public Button backwardsButton;

        int positionInListCounter = 0;
        float fadeMultiplier = 3;

        // Use this for initialization
        void Start()
        {
            availableLanguages = SmartLocalization.LanguageManager.Instance.GetSupportedLanguages();

            forwardButton.onClick.AddListener(() => { OnForwardButtonClick(); });
            backwardsButton.onClick.AddListener(() => { OnBackwardButtonClick(); });
            
            currentLanguageText.text = SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.nativeName;

            //Set available language list counter too the default language index
            for (int i = 0; i < availableLanguages.Count; i++)
            {
                SmartLocalization.SmartCultureInfo item = availableLanguages[i];
                if (item.nativeName == SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.nativeName)
                {
                    positionInListCounter = i;
                }
            }
        }

        void OnForwardButtonClick()
        {
            positionInListCounter++;
            if (positionInListCounter > availableLanguages.Count-1)
            {
                positionInListCounter = 0;
            }
            StartCoroutine(CustomUtilities.UpdateTextOnComponent(currentLanguageText, availableLanguages[positionInListCounter].nativeName));


            if (SmartLocalization.LanguageManager.Instance.IsLanguageSupported(availableLanguages[positionInListCounter].languageCode))
            {
                SmartLocalization.LanguageManager.Instance.ChangeLanguage(availableLanguages[positionInListCounter].languageCode);
            }
            else
            {
                Debug.Log("The language: " + currentLanguageText.text + " is not supported");
            }
        }

        void OnBackwardButtonClick()
        {
            positionInListCounter--;
            if (positionInListCounter < 0)
            {
                positionInListCounter = availableLanguages.Count - 1;
            }
            StartCoroutine(CustomUtilities.UpdateTextOnComponent(currentLanguageText, availableLanguages[positionInListCounter].nativeName));

            if (SmartLocalization.LanguageManager.Instance.IsLanguageSupported(availableLanguages[positionInListCounter].languageCode))
            {
                SmartLocalization.LanguageManager.Instance.ChangeLanguage(availableLanguages[positionInListCounter].languageCode);
            }
            else
            {
                Debug.Log("The language: " + currentLanguageText.text + " is not supported");
            }
        }

    }
}