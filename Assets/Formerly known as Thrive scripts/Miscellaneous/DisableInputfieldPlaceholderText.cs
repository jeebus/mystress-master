﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

namespace Thrive
{
	public class DisableInputfieldPlaceholderText : MonoBehaviour
	{
		EventTrigger eventTrigger = null;
		

		RectTransform placeholderText;
		
		// Use this for initialization
		void Start()
		{
			InputField ipFld = gameObject.GetComponent<InputField>();
			if (ipFld != null) {
				placeholderText = ipFld.placeholder.rectTransform;
			}
			AddEventTrigger(EnablePlaceholder, EventTriggerType.Deselect);
			AddEventTrigger(DisablePlaceholder, EventTriggerType.Select);

		
		}
		
		
		private void AddEventTrigger(UnityAction action, EventTriggerType triggerType)
		{
			eventTrigger = gameObject.AddComponent<EventTrigger>();

			// Create a nee TriggerEvent and add a listener
			EventTrigger.TriggerEvent trigger = new EventTrigger.TriggerEvent();
			trigger.AddListener((eventData) => action()); // you can capture and pass the event data to the listener
			
			// Create and initialise EventTrigger.Entry using the created TriggerEvent
			EventTrigger.Entry entry = new EventTrigger.Entry() { callback = trigger, eventID = triggerType };
			
			// Add the EventTrigger.Entry to delegates list on the EventTrigger
			eventTrigger.triggers.Add(entry);
		}
		
		private void AddEventTrigger(UnityAction<BaseEventData> action, EventTriggerType triggerType)
		{
			eventTrigger = gameObject.AddComponent<EventTrigger>();

			// Create a nee TriggerEvent and add a listener
			EventTrigger.TriggerEvent trigger = new EventTrigger.TriggerEvent();
			trigger.AddListener((eventData) => action(eventData)); // you can capture and pass the event data to the listener
			
			// Create and initialise EventTrigger.Entry using the created TriggerEvent
			EventTrigger.Entry entry = new EventTrigger.Entry() { callback = trigger, eventID = triggerType };
			
			// Add the EventTrigger.Entry to delegates list on the EventTrigger
			eventTrigger.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();// 
			eventTrigger.triggers.Add(entry);
		}
		
		
		private void DisablePlaceholder(BaseEventData data)
		{
			placeholderText.gameObject.SetActive(false);
//			Debug.Log ("Disable placeholder");
		}
		private void EnablePlaceholder(BaseEventData data)
		{
			placeholderText.gameObject.SetActive(true);
//			Debug.Log ("Enable placeholder");
		}

	}
}