﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;


namespace Thrive{

    public class MainUI : MonoBehaviour
    {
        public Canvas mainCanvas;
        public Canvas presentCardCanvas;
		public Canvas currentScreenCanvas;

        public bool isMyStressApp;

        public RectTransform cameraTarget;

        IScreen screenInFocus;

		public AnimationCurve cardPresentationCurve;
		public AnimationCurve deckSwipeCurve;
		public AnimationCurve screenRightToLeftPresentationCurve;

        public AvailableCardsHolder cardHolder;

        public CardData[] cardDataArray;

        //Cardscreen, has an instance in editor
        public CardMainUIScreen instantiatedCardScreen;

        //Instantiated objects
        LoginScreen instantiatedLoginScreen;
        AccountCreationScreen instantiatedAccountCreationScreen;
        PrivacyStatementScreen instantiatedPrivacyStatementScreen;
        TermsAgreementScreen instantiatedTermsAndAgreementScreen;
        RecoverAccountScreen instantiatedRecoverAccountScreen;
        FAQScreen instantiatedFaqScreen;
        FadeInOutScreen instantiatedFadeScreen;
        CompletedFlowScreen instantiatedFlowScreen;
        OptionsScreen instantiatedOptions;
//        ProfileScreen instantiatedProfileScreen;
        PasswordResetScreen instantiatedPasswordResetScreen;
        LoadingScreen instantiatedLoadingScreen;
        BlankBlackScreen blankScreen;
        IntroCard introCardInstance;

        //Main screen components
        CardPanelHeader header;
        GameObject headerText;
        GameObject mainContent;
        GameObject footer;
        GameObject footerMaximizeDeckButton;
        GameObject cardPanel;
        GameObject deckPanel;
		RectTransform deckPanelRectTransform;

        //Switcher indicators for multiple decks
        List<Image> swithcerIndicators = new List<Image>();

        //Font types for text content on main screen
        public Font mainFont;
        public Font boldFont;

        //Font colors in hex
        public readonly string labelTextColor = "DCDEE0";
        public readonly string prettyPurpleColor = "8385D9";
        public readonly string greenValidButtonColor = "5fe075";
        public readonly string yellowPendingButtonColor = "feb705";
        public readonly string redInvalidButtonColor = "ec5d57";
        public readonly string toggleButtonOffColor = "464a50";


        //UI component sizes
        public readonly float mainContentAnchorMax =  0.8890001f;
        public readonly int cardMargin = 6;
        public float minimizedDeckSpacing;
        public readonly float headerSmallScreenPercentage = 0.05f;
        public readonly float headerLargeScreenPercentage = 0.085f;
        static int listItemsOnScreen = 5;
        public float cardEnlargedPreferredHeight;
        public float cardMinimizedPreferredHeight;



        //Time multipliers for various lerp functions
        const float minMaxDeckSpeed = 3.0f;
        const int presentTimeLerpMultiplier = 5;
        const int presentSizeLerpMultiplier = 2;
        const float deckSideSwipeSpeedMultiplier = 5;

        //The current large/presented card
        public CardUIPlaceHolder currentEnlargedCard;

        //The current deck that is centered on canvas 
        public Deck currentUsedDeck;

        //Static positions for enlarged/minimized cards
        public float mainContentCardYPosition;
        public float mainContentMinimizedDeckYPosition;
        public List<Deck> decks = new List<Deck>();
        public ArrayList cards;
        public List<Vector3> cardsPositionInMaximizedDeck;

        //Text
        public TextAsset privacyText;
        public TextAsset termsAndConditionText;

        Vector3 cardPanelPositionOnSwipeStart;
        Vector3 deckPanelCenterToLerpTo;
        int currentDeckID = 0;

        //Counters
        public float minMaxCounter = 0;
        CardController cardManager;


        //Booleans
        public bool horizontalSwipe = false;
        public bool verticalSwipe = false;
        public bool lerpingCardPanelsSideways = false;
        bool introCardAdded = false;
        bool testScenario = false;
        public bool minimizeHeaderNow = false;
        public bool maximizeHeaderNow = false;
        public bool minimizeFooterNow = false;
        public bool maximizeFooterNow = false;
        public bool minimizingDeckNow = false;
        public bool maximizingDeckNow = false;
        public bool presentCard = false;
        public bool unPresentCard = false;
        public bool showLoginScreen = true;
        bool isLoggedIn = false;
        public bool serverSynchronization;


        //Font sizes
        int basicFontSize;

        //Toggles
        [SerializeField]
        private float threshHoldFactor;
        
        void Awake()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
            }
            SmartLocalization.LanguageManager.Instance.defaultLanguage = "da";

            SmartLocalization.LanguageManager.Instance.ChangeLanguage(SmartLocalization.LanguageManager.Instance.defaultLanguage);
            StartCoroutine(ExperimentalGenerateAppFontSizes());
        }

        IEnumerator ExperimentalGenerateAppFontSizes()
        {
            RectTransform basicFontSizeCalculator = new GameObject("basicFontSizeCalculator").AddComponent<RectTransform>();
            basicFontSizeCalculator.transform.SetParent(transform);
            basicFontSizeCalculator.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Screen.width / 6);
            basicFontSizeCalculator.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Screen.height / 17);
            Text basicFontSizeCalculatorText = basicFontSizeCalculator.GetOrAddComponent<Text>();
            basicFontSizeCalculatorText.font = mainFont;
            basicFontSizeCalculatorText.resizeTextForBestFit = true;
            basicFontSizeCalculatorText.text = "A";
            yield return new WaitForEndOfFrame();
            basicFontSize =(int) (basicFontSizeCalculatorText.cachedTextGenerator.fontSizeUsedForBestFit * presentCardCanvas.scaleFactor);
            Debug.Log("BASIC FONT SIZE IS:" + basicFontSize);
            Destroy(basicFontSizeCalculator.gameObject);
        }

        public int GetBasicFontSize()
        {
            return basicFontSize;
        }

     
     // Use this for initialization
     void Start()
     {
         SpriteResources.LoadInitialSprites();
         ThrivePrefabResources.LoadThrivePrefabs();
         Screens.Init();


         /*  
    //Create card data list
    cardDataArray = new CardData[] {    new CardData("Intro",CardTypes.GroCards.MIDT_000_INTRO, ThrivePrefabResources.introCardPrefab.gameObject, SpriteResources.diaperCardSprite, Color.white, SpriteResources.diaperIcon)};
         ,
                                        new CardData("BLESKIFT",CardTypes.GroCards.MIDT_001_DIAPERS, ThrivePrefabResources.diaperCardPrefab.gameObject, SpriteResources.diaperCardSprite, Color.white, SpriteResources.diaperIcon),
                                         new CardData("AMNING",CardTypes.GroCards.MIDT_002_BREAST, ThrivePrefabResources.breastCardPrefab.gameObject, SpriteResources.breastCardSprite, Color.white, SpriteResources.diaperIcon),
                                         new CardData("MÆLK TIL BARNET",CardTypes.GroCards.MIDT_003_BOTTLE, ThrivePrefabResources.breastCardPrefab.gameObject, SpriteResources.bottleCardSprite, Color.white, SpriteResources.diaperIcon),
                                        new CardData("GULSOT",CardTypes.GroCards.MIDT_004_GULSOT, ThrivePrefabResources.jaundiceCardPrefab.gameObject, SpriteResources.jaundiceCardSprite, new Color(1.0f, 90.0f / 255.0f, 90.0f / 255.0f), SpriteResources.diaperIcon),
                                        new CardData("KROPPEN EFTER FØDSEL",CardTypes.GroCards.MIDT_005_MOR, ThrivePrefabResources.motherCardPrefab.gameObject, SpriteResources.motherCardSprite, new Color(225.0f/255.0f, 222.0f / 255.0f, 240.0f / 255.0f), SpriteResources.motherIcon) };

      */
       
    //PlayerPrefs.DeleteAll();
    if (!testScenario)
    {
        //Force 60 fps for iOS
        Application.targetFrameRate = 60;
        Input.simulateMouseWithTouches = true;

        //Create deck screen
        cards = new ArrayList();
        decks = new List<Deck>();
        InstantiateBaseUI();
        CreateDeckPanel();
         
        deckPanelCenterToLerpTo = GetDeckPanel().transform.position;

        //Instantiate loginscreen
        if (ThrivePrefabResources.loginScreenPrefab.GetComponent<LoginScreen>() != null)
        {
            instantiatedLoginScreen = (LoginScreen)Instantiate(ThrivePrefabResources.loginScreenPrefab, Vector3.zero, Quaternion.identity);
            instantiatedLoginScreen.transform.SetParent(currentScreenCanvas.transform, false);
           
        }
        //Instantiate fade screen
        if (ThrivePrefabResources.faderScreenPrefab.GetComponent<FadeInOutScreen>() != null)
        {
            instantiatedFadeScreen = (FadeInOutScreen)Instantiate(ThrivePrefabResources.faderScreenPrefab, Vector3.zero, Quaternion.identity);
            instantiatedFadeScreen.transform.SetParent(currentScreenCanvas.transform, false);
            instantiatedFadeScreen.DisableScreen();
        }

        //Should we show login screen?
        if (showLoginScreen)
        {
            HideCardScreen();
        }


        cardEnlargedPreferredHeight = ((RectTransform)mainContent.transform).rect.height - ((RectTransform)footer.transform).rect.height * 1.4f;
        cardMinimizedPreferredHeight = cardEnlargedPreferredHeight / listItemsOnScreen;
        mainContentCardYPosition = -Screen.height * 0.42f; //fantastic magic number!!
        mainContentMinimizedDeckYPosition = -Screen.height * 0.935f;//fantastic magic number!!
        minimizedDeckSpacing = cardMinimizedPreferredHeight * 0.11f;//fantastic magic number!!

       // header.foldingPanels.mainContentPanel = ((RectTransform)GetMainContentPanel().transform);

        ViewIScreenController.SetMainUI(this);
        cardMinimizedPreferredHeight= cardMinimizedPreferredHeight* 1.354f;
    }
       
         //Set default language
         //SmartLocalization.LanguageManager.Instance.ChangeLanguage("da");
        // availableLanguages = SmartLocalization.LanguageManager.Instance.GetSupportedLanguages();
        // StartCoroutine(DelaySetDansk());
         maxDistanceBasedOfResolution = Screen.width / threshHoldFactor;

         FindObjectOfType<EventSystem>().pixelDragThreshold = (int)maxDistanceBasedOfResolution;

         Debug.Log("Event system drag threshold " + maxDistanceBasedOfResolution + "    Start() method of MainUI is done.");
     }

     
        IEnumerator DelaySetDansk()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
        }

        void CreateAvailableCards(int deckNr)
        {
            Debug.Log("Create available cards");
            if (cardHolder.availableCardList.Count == 1)
            {
                GameObject prefab = ((GameObject)Instantiate(cardHolder.availableCardList[0], Vector3.zero, Quaternion.identity));

                MonoBehaviour[] list = prefab.GetComponents<MonoBehaviour>();
                foreach (MonoBehaviour mb in list)
                {
                    if (mb is ICard)
                    {
                        ICard icard = (ICard)mb;
                        mb.transform.SetParent(presentCardCanvas.transform.GetChild(0), false);
                    }
                }
            //    ICard singleCard = ((GameObject)Instantiate(cardHolder.availableCardList[0], Vector3.zero, Quaternion.identity)).GetComponent<ICard>();
             //   singleCard.GetGameObject().transform.SetParent(presentCardCanvas.transform.GetChild(0), false);

                mainContent.SetActive(false);

            }
            else
            {
                for (int i = 0; i < cardHolder.availableCardList.Count; i++)
                {
                    Debug.Log("Create available cards, index: " + i);
                    CardUIPlaceHolder cardScript = ((GameObject)Instantiate(ThrivePrefabResources.cardPlaceHolderPrefab.gameObject, Vector3.zero, Quaternion.identity)).GetComponent<CardUIPlaceHolder>();
                    GameObject prefab = ((GameObject)Instantiate(cardHolder.availableCardList[i], Vector3.zero, Quaternion.identity));

                    MonoBehaviour[] list = prefab.GetComponents<MonoBehaviour>();
                    foreach (MonoBehaviour mb in list)
                    {
                        if (mb is ICard)
                        {
                            cardScript.actualCard = (ICard)mb;
                          
                        }
                    }

                 //   cardScript.actualCard = ((GameObject)Instantiate(cardHolder.availableCardList[i], Vector3.zero, Quaternion.identity)).GetComponent<ICard>();
                    if (cardScript.actualCard != null)
                    {
                        Debug.Log("Loaded Actual card not null");
                    }
                    else
                    {
                        Debug.Log("Loaded Actual card is null");
                    }
                    int cardCount = decks[deckNr].cards.Count;


                    cardScript.placeholderText.font = boldFont;
                    cardScript.placeholderText.alignment = TextAnchor.MiddleLeft;
                    cardScript.placeholderText.text = cardScript.actualCard.GetCardNameTextComponent().text;

                    cardScript.cardID = cardCount;



                    cardScript.placeholderGraphicsContent.GetComponent<Image>().sprite = cardScript.actualCard.GetCardImageTexture().sprite;
                    cardScript.placeholderGraphicsContent.GetComponent<Image>().color = cardScript.actualCard.GetCardImageTexture().color;


                    cardScript.placeholderIcon.sprite = cardScript.actualCard.GetCardIcon().sprite;

                    cardScript.gameObject.transform.SetParent(decks[deckNr].deckCardPanel.panelInstance.transform, false);
                    cardScript.actualCard.GetGameObject().transform.SetParent(mainContent.transform, false);


                    decks[deckNr].cards.Add(cardScript);
                    StartCoroutine(WaitWithSettingCardParent(cardScript));
                }
            }

            Debug.Log("Start fading screen");
            GetFaderScreen().RunFadeCycle();
        }

        /*

        public void AddCard(int deckNr, CardTypes.GroCards cardTypeToAdd)
        {
            //On parent/main panel
            if (deckNr == 0 && !introCardAdded)//(decks[deckNr].deckCardPanel.user.userRole == User.Role.Owner && !introCardAdded)
            {
                ICard newCard = (ICard)Instantiate(ThrivePrefabResources.introCardPrefab, Vector3.zero, Quaternion.identity);
                newCard.GetGameObject().transform.SetParent(decks[0].deckCardPanel.panelInstance.transform, false);
                introCardAdded = true;
                introCardInstance = (IntroCard)newCard;
              //  ((ICard)newCard.GetComponent<>).SetCardOwner(UserStats.connectedChildren[deckNr]);
                return;
            }

            //On child panel
          //  if (decks[deckNr].deckCardPanel.user.userRole == User.Role.Child)
            if (decks.Count > deckNr)
            {

                GameObject newCard = ((CardUIPlaceHolder)Instantiate(ThrivePrefabResources.cardPlaceHolderPrefab, Vector3.zero, Quaternion.identity)).gameObject;
                int cardCount = decks[deckNr].cards.Count;

                CardUIPlaceHolder cardScript = newCard.GetComponent<CardUIPlaceHolder>();

                cardScript.cardID = cardCount;

                cardScript.placeholderText.font = boldFont;
                cardScript.placeholderText.alignment = TextAnchor.MiddleLeft;
                cardScript.placeholderText.text = cardDataArray[(int)cardTypeToAdd].cardName;

                cardScript.placeholderGraphicsContent.GetComponent<Image>().sprite = cardDataArray[(int)cardTypeToAdd].cardTexture;
                cardScript.placeholderGraphicsContent.GetComponent<Image>().color = cardDataArray[(int)cardTypeToAdd].cardColor;
    //            cardScript.actualCard = (ICard)Instantiate(cardDataArray[cardCount % cardDataArray.Length].cardPrefab);

                if (cardTypeToAdd == CardTypes.GroCards.MIDT_000_INTRO)
                {
                    cardScript.actualCard = (ICard)Instantiate(ThrivePrefabResources.introCardPrefab, Vector3.zero, Quaternion.identity);
                   // cardScript.placeholderIcon.sprite = SpriteResources.diaperIcon;
                //    Debug.Log("Added card diaper");
                }
                /*
                if (cardTypeToAdd == CardTypes.GroCards.MIDT_001_DIAPERS)
                {
                    cardScript.actualCard = (ICard)Instantiate(ThrivePrefabResources.diaperCardPrefab, Vector3.zero, Quaternion.identity);
                    cardScript.placeholderIcon.sprite = SpriteResources.diaperIcon;
              //      Debug.Log("Added card diaper");
                }

                if (cardTypeToAdd == CardTypes.GroCards.MIDT_002_BREAST)
                {
                    cardScript.actualCard = (ICard)Instantiate(ThrivePrefabResources.breastCardPrefab, Vector3.zero, Quaternion.identity);
                    cardScript.placeholderIcon.sprite = SpriteResources.breastIcon;
                   // Debug.Log("Added card breast");
                }

                if (cardTypeToAdd == CardTypes.GroCards.MIDT_003_BOTTLE)
                {
                    cardScript.actualCard = (ICard)Instantiate(ThrivePrefabResources.bottleCardPrefab, Vector3.zero, Quaternion.identity);
                    cardScript.placeholderIcon.sprite = SpriteResources.bottleIcon;
                    //Debug.Log("Added card bottle");
                }

                if (cardTypeToAdd == CardTypes.GroCards.MIDT_004_GULSOT)
                {
                    cardScript.actualCard = (ICard)Instantiate(ThrivePrefabResources.jaundiceCardPrefab, Vector3.zero, Quaternion.identity);
                    cardScript.placeholderIcon.sprite = SpriteResources.jaundiceIcon;
                   // Debug.Log("Added card jaundice");
                }
                if (cardTypeToAdd == CardTypes.GroCards.MIDT_005_MOR)
                {
                    cardScript.actualCard = (ICard)Instantiate(ThrivePrefabResources.motherCardPrefab, Vector3.zero, Quaternion.identity);
                    cardScript.placeholderIcon.sprite = SpriteResources.motherIcon;
                  //  Debug.Log("Added card mommy");
                }

               
               // ((RectTransform)cardScript.actualCard.GetGameObject().transform).position = new Vector3(-Screen.width * cardOffsetMagicNumber, ((RectTransform)cardScript.actualCard.GetGameObject().transform).anchoredPosition3D.y + ((RectTransform)footer.transform).rect.height, ((RectTransform)cardScript.actualCard.GetGameObject().transform).anchoredPosition3D.z);
                newCard.transform.SetParent(decks[deckNr].deckCardPanel.panelInstance.transform, false);
                cardScript.actualCard.GetGameObject().transform.SetParent(mainContent.transform, false);
                

              //  cardScript.actualCard.SetCardOwnerID(UserStats.connectedChildren[deckNr-1].GetChildID());


                decks[deckNr].cards.Add(cardScript);
                StartCoroutine(WaitWithSettingCardParent(cardScript));
            }
        }
          * */
        IEnumerator WaitWithSettingCardParent(CardUIPlaceHolder cardToSetParentOn)
        {
            cardToSetParentOn.actualCard.GetGameObject().transform.SetParent(cardToSetParentOn.transform, false);
            cardToSetParentOn.actualCard.GetGameObject().transform.SetAsFirstSibling();
            ((RectTransform)cardToSetParentOn.actualCard.GetGameObject().transform).anchorMin = new Vector2(0.5f, 0.5f);
            ((RectTransform)cardToSetParentOn.actualCard.GetGameObject().transform).anchorMax = new Vector2(0.5f, 0.5f);
            ((RectTransform)cardToSetParentOn.actualCard.GetGameObject().transform).sizeDelta = new Vector2(Screen.width ,cardEnlargedPreferredHeight);

           // cardToSetParentOn.placeholderGraphicsContent.transform.SetParent(cardToSetParentOn.transform,false);
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            cardToSetParentOn.actualCard.MakeCardContentInvisible();

            yield return null;
        }

        public void OnLogin()
        {
            GetFaderScreen().EnableScreen();
            //  CreateOfflineContent();
            CreateContent();
            isLoggedIn = true;
       
            GetFaderScreen().RunFadeCycle();

            mainCanvas.renderMode = RenderMode.WorldSpace;
            ViewIScreenController.SetCurrentScreen(new VisitedScreens(instantiatedCardScreen,TransitionType.Fade));
        }

		public void AttemptToPushStats(){
		//	StartCoroutine(UserStats.AttemptToPushStoredStatsToServer());
        }

        public void CreateContentOnRestoreFromSavedData()
        {
            Debug.Log("CreateContentOnRestoreFromSavedData() call");
            CreateContent();
            GetLoadingScreen().ShowPreviousScreen();
         //   loadingIndicator.DeactivateLoadingIcon();

            isLoggedIn = true;
        //    invitationManager.StartCheckingForInvites();
          //  StartCoroutine(InitiateFaderScreen());
            GetFaderScreen().RunFadeCycle();
        }



        public void HideCardScreen()
        {
            //instantiatedCardScreen.SetActive(false);
           // mainContent.SetActive(false);
        }

        public void ShowCardPanel()
        {
           // mainCanvas.transform.GetChild(0).gameObject.SetActive(true);
           // instantiatedCardScreen.SetActive(true);
            //header.userNameText.text = UserStats.userRealName;// .userName;
        }

        void Update()
        {
            if (isLoggedIn && !isMyStressApp)
            {
                if (!minimizingDeckNow && !maximizingDeckNow && !testScenario)
                {
                    //Depending on a card being show, perform certain swiping actions;
                    if (!presentCard && !ViewIScreenController.IsTransitionRunning() && !ViewIScreenController.IsCardTransitionRunning())
                    {

                        MouseUpdate();

                        // CheckForLerpBackFromSideSwipe();
                        PerformDeckSwipeActions();

                    }
                }
            }
        }


	    void InstantiateBaseUI () {
            mainCanvas = gameObject.GetComponent<Canvas>();

            if (mainContent == null){
                CreateMainContent();
            }
			if (header == null)
			{
				CreateHeader();
               
			}
            if(footer == null){
                CreateFooter();
                
            }
            if (!isMyStressApp)
            {
                CreateFooterDeckMinimizingButton();
            }
            else
            {
                footer.SetActive(false);
            }
	    }

//        int incommingMessagePort = 34523;

        


       /*
        
        private void OnMessageReceived(WebSocket webSocket, string message)
        {
	        Debug.Log("Text Message received from server: " + message);
        }

        private void OnWebSocketOpen(WebSocket webSocket)
        {
            Debug.Log("WebSocket Open!");
        }

        
        private void OnBinaryMessageReceived(WebSocket webSocket, byte[] message)
        {
	        Debug.Log("Binary Message received from server. Length: " + message.Length);
        }
        
        private void OnWebSocketClosed(WebSocket webSocket, UInt16 code, string message)
        {
	        Debug.Log("WebSocket Closed!");
        }
*/

        public void CreateContent()
        {
            if (!isMyStressApp)
            {
                Debug.Log("Create non-MyStress content");
                CreateAllDecks();
                CreateSwitcher();
                UpdateSwitcher();
              //  StartCoroutine(CreateStartCards());
            }
            else
            {
                Debug.Log("Create MyStress content");
                CreateSingleDeck();
                CreateSwitcher();
                UpdateSwitcher();
                CreateAvailableCards(0);
            }
          
        }


        void CreateHeader()
        {
            GameObject newheader = (GameObject)Instantiate(ThrivePrefabResources.cardHeaderPanelPrefab, Vector3.zero, Quaternion.identity);
            newheader.transform.SetParent(GetCameraTarget().transform, false);
            header = (CardPanelHeader) newheader.transform.GetChild(0).GetComponent<CardPanelHeader>();
        }

        void CreateMainContent()
        {
            
            mainContent = new GameObject("MainPanel", typeof(RectTransform));
            mainContent.AddComponent<CanvasRenderer>();
			//mainContent.AddComponent<Canvas>();
			//mainContent.layer = 5;
          //  Image image = mainContent.AddComponent<Image>();
           // image.sprite = backGroundSquareSprite;
           // image.type = Image.Type.Simple;
          //  mainContent.AddComponent<Mask>();

            mainContent.transform.SetParent(mainCanvas.transform, false);
            ((RectTransform)mainContent.transform).sizeDelta = Vector2.zero;
            ((RectTransform)mainContent.transform).anchorMin = Vector2.zero;
            ((RectTransform)mainContent.transform).anchorMax = new Vector2(1, mainContentAnchorMax);
        }

        void CreateDeckPanel()
        {
            deckPanel = new GameObject("DeckPanel", typeof(RectTransform));
			deckPanelRectTransform = ((RectTransform)deckPanel.transform);
            deckPanel.AddComponent<CanvasRenderer>();
            ((RectTransform)deckPanel.transform).sizeDelta = Vector2.zero;
            ((RectTransform)deckPanel.transform).anchorMin = new Vector2(0, 0.0377033f);
            ((RectTransform)deckPanel.transform).anchorMax = new Vector2(1, 1);
            
        
            deckPanel.transform.SetParent(mainContent.transform,false);
        }

        void CreateCardPanel()
        {
            cardPanel =   new GameObject("CardPanel", typeof(RectTransform));
            cardPanel.tag = "CardPanel";
            cardPanel.AddComponent<CanvasRenderer>();
            GetCardPanel().transform.SetParent(mainContent.transform,false);

        }

        void CreateFooterDeckMinimizingButton()
        {
            //invis button for footer area to maximize deck after a card has been presented
            footerMaximizeDeckButton = new GameObject("FooterDeckButton", typeof(RectTransform));
            footerMaximizeDeckButton.AddComponent<Button>().onClick.AddListener(() => { OnMinimzedDeckClick(); });
            footerMaximizeDeckButton.AddComponent<CanvasRenderer>();
            footerMaximizeDeckButton.AddComponent<LayoutElement>().ignoreLayout = true;
            footerMaximizeDeckButton.transform.SetParent(GetCameraTarget().transform, false);
            footerMaximizeDeckButton.AddComponent<Image>().sprite = SpriteResources.backGroundSquareSprite;

            footerMaximizeDeckButton.GetComponent<Image>().color = Color.clear;

            Canvas footerMaximizeDeckButtonCanvas = footerMaximizeDeckButton.AddComponent<Canvas>();
            footerMaximizeDeckButtonCanvas.overrideSorting = true;
            footerMaximizeDeckButtonCanvas.sortingOrder = 2;
            footerMaximizeDeckButton.AddComponent<GraphicRaycaster>();
            ((RectTransform)footerMaximizeDeckButton.transform).sizeDelta = Vector2.zero;
            ((RectTransform)footerMaximizeDeckButton.transform).anchorMin = new Vector2(0, 0);
            ((RectTransform)footerMaximizeDeckButton.transform).anchorMax = new Vector2(1, 0.059f);
        }

        void CreateFooter()
        {
            
            

            //the actual footer
            footer = new GameObject("FooterPanel", typeof(RectTransform));
            footer.AddComponent<CanvasRenderer>();
            Image footerImage = footer.AddComponent<Image>();
            footerImage.sprite = SpriteResources.backGroundSprite;
            footerImage.type = Image.Type.Simple;
            footer.transform.SetParent(GetCameraTarget().transform, false);
            ((RectTransform)footer.transform).sizeDelta = Vector2.zero;
            ((RectTransform)footer.transform).anchorMin = new Vector2(0, 0);
            ((RectTransform)footer.transform).anchorMax = new Vector2(1, 0.041f);

            

        
            GridLayoutGroup switcherLayout = footer.AddComponent<GridLayoutGroup>();
            switcherLayout.childAlignment = TextAnchor.MiddleCenter;
            switcherLayout.spacing = new Vector2(10, 0);
            switcherLayout.cellSize = new Vector2(((RectTransform)footer.transform).rect.height / 3, ((RectTransform)footer.transform).rect.height / 3);
        }

        void CreateSwitcher()
        {
            for (int i = 0; i < decks.Count; i++)
            {
                GameObject switcherIcon = new GameObject("SwitcherIcon");
                switcherIcon.AddComponent<CanvasRenderer>();
                Image switcherImage = switcherIcon.AddComponent<Image>();
                switcherImage.sprite = SpriteResources.switcherIndicator;
                switcherImage.type = Image.Type.Simple;
                switcherImage.preserveAspect = true;
                //((RectTransform)switcherIcon.transform).sizeDelta = new Vector2(((RectTransform)footer.transform).rect.height / 4, ((RectTransform)footer.transform).rect.height / 4);
               // ((RectTransform)switcherIcon.transform).position = new Vector3(i - (decks.Count / 2 - 1) * ((RectTransform)switcherIcon.transform).rect.width, 0, 0);

                ((RectTransform)switcherIcon.transform).sizeDelta = Vector2.zero;
                ((RectTransform)switcherIcon.transform).anchorMin = new Vector2(0.492f, 0.3714902f);
                ((RectTransform)switcherIcon.transform).anchorMax = new Vector2(0.5082388f, 0.6233951f);

                switcherIcon.transform.SetParent(footer.transform, false);

               
                swithcerIndicators.Add(switcherImage);
            }
        }

        void AddToSwitcher(int amountToAdd)
        {
            for (int i = 0; i < amountToAdd; i++)
            {
                GameObject switcherIcon = new GameObject("SwitcherIcon");
                switcherIcon.AddComponent<CanvasRenderer>();
                Image switcherImage = switcherIcon.AddComponent<Image>();
                switcherImage.sprite = SpriteResources.switcherIndicatorEmpty;
                switcherImage.type = Image.Type.Simple;
                switcherImage.preserveAspect = true;

                ((RectTransform)switcherIcon.transform).sizeDelta = Vector2.zero;
                ((RectTransform)switcherIcon.transform).anchorMin = new Vector2(0.492f, 0.3714902f);
                ((RectTransform)switcherIcon.transform).anchorMax = new Vector2(0.5082388f, 0.6233951f);

                switcherIcon.transform.SetParent(footer.transform, false);


                swithcerIndicators.Add(switcherImage);
            }
        }
        void UpdateSwitcher()
        {
            for (int i = 0; i < swithcerIndicators.Count; i++)
            {
                if (currentDeckID == i)
                {
                    swithcerIndicators[i].sprite = SpriteResources.switcherIndicator;
                }
                else
                {
                    swithcerIndicators[i].sprite = SpriteResources.switcherIndicatorEmpty;
                }
            }
        }

        void UpdateCardPanelUsername()
        {

            if (GetCurrentDeckID() > 0)
            {
                if (GetCurrentDeckID() - 1 < 0)
                {
                    header.userNameText.text = "Placeholder";//UserStats.connectedChildren[GetCurrentDeckID() - 1].GetChildRealName().ToString();// +" (" + UserStats.connectedChildren[GetCurrentDeckID() - 1].GetChildID() + ")";
                }
            }
            else
            {
                header.userNameText.text = "Placeholder";// UserStats.userRealName;// .userName;
            }
        }


        Vector3 swipeVelocity;
        Vector2 touchStartPosition;
        float swipeSpeed;
        float touchStartTime;
        float maxDistanceBasedOfResolution;
        float deckChangeScreenFraction = 0.5f;
        float swipeSpeedMultiplier = 0.0001f;

        void PerformDeckSwipeActions()
        {
            if (horizontalSwipe)
            {
                Vector3 deltaDistance = cardPanelPositionOnSwipeStart - new Vector3(touchStartPosition.x, touchStartPosition.y, 0);
                GetCameraTarget().anchoredPosition3D = new Vector3(-Input.mousePosition.x + deltaDistance.x, 0,0);
                //GetDeckPanelRectTransform().anchoredPosition3D = new Vector3(Input.mousePosition.x + deltaDistance.x, GetDeckPanelRectTransform().anchoredPosition3D.y, GetDeckPanelRectTransform().anchoredPosition3D.z);
                Camera.main.transform.position = new Vector3(GetCameraTarget().position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
                lerpingCardPanelsSideways = true;
            }
        }

        void MouseUpdate()
        {
            if (Input.GetMouseButtonUp(0))
            {
                horizontalSwipe = false;
                verticalSwipe = false;
                lerpingCardPanelsSideways = true;
             //   EnableAllCardPanelVerticalScrolling();

                Vector2 endPosition = -Input.mousePosition;
                Vector2 delta = endPosition - touchStartPosition;
                float dist = Mathf.Sqrt(Mathf.Pow(delta.x, 2) + Mathf.Pow(delta.y, 2));
                float angle = Mathf.Atan(delta.y / delta.x) * (180.0f / Mathf.PI);
                if (angle < 0) angle = angle * -1;
                float duration = Time.time - touchStartTime;
                float speed = dist / duration;
                swipeSpeed = speed;
                // Left to right swipe
                /*
                if (touchStartPosition.y < endPosition.y)
                {
                    if (angle < 0) angle = angle * 1.0f;
                    Debug.Log("Distance: " + dist + " Angle: " + angle + " Speed: " + speed);

                    if (dist > maxDistanceBasedOfResolution && angle < 10 && speed > 1000)
                    {
                        // Do something related to the swipe
                        //  GetDeckPanelRectTransform().anchoredPosition3D = new Vector3(Input.mousePosition.x + deltaDistance.x, GetDeckPanelRectTransform().anchoredPosition3D.y, GetDeckPanelRectTransform().anchoredPosition3D.z);

                    }
                }
                */
                LerpToClosestDeckPanelOnEndedSideSwipe();
                StartCoroutine(CheckForLerpBackFromSideSwipe());
            }

            if (Input.GetMouseButtonDown(0))
            {
                swipeSpeed = 0;
                touchStartPosition = -Input.mousePosition;
                cardPanelPositionOnSwipeStart = GetCameraTarget().anchoredPosition3D;
                touchStartTime = Time.time;
                DisableAllCardPanelVerticalScrolling();
            }

            if (Input.GetMouseButton(0))
            {

                Vector2 endPosition = -Input.mousePosition;
                Vector2 delta = endPosition - touchStartPosition;
                float angle = Mathf.Atan(delta.y / delta.x) * (180.0f / Mathf.PI);
                if (angle < 0) angle = angle * -1;
                float dist = Mathf.Sqrt(Mathf.Pow(delta.x, 2) + Mathf.Pow(delta.y, 2));
               // float swipeTime = Time.time - touchStartTime;

                if (dist >= maxDistanceBasedOfResolution)
                {
                    if (angle < 35 && !verticalSwipe)
                    {
                        horizontalSwipe = true;
                        verticalSwipe = false;
                        DisableAllCardPanelVerticalScrolling();
//                        Debug.Log("Disable vertical scrolling");
                    }
                    if (angle > 35 && !horizontalSwipe)
                    {
                        horizontalSwipe = false;
                        verticalSwipe = true;
                   //     EnableAllCardPanelVerticalScrolling();
 //                       Debug.Log("Enable vertical scrolling");
                    }
                }

            }

        }
        IEnumerator CheckForLerpBackFromSideSwipe()
        {
            
            Vector3 cachedPosition = GetCameraTarget().anchoredPosition3D; // GetDeckPanelRectTransform().anchoredPosition3D;
            bool hasReachDestination = false;


            while (!horizontalSwipe && !hasReachDestination)
            {
                GetCameraTarget().anchoredPosition3D = Vector3.SmoothDamp(new Vector3(GetCameraTarget().anchoredPosition3D.x, 0, 0), new Vector3(deckPanelCenterToLerpTo.x, 0, 0), ref swipeVelocity, Mathf.Min(0.1f, 0.1f * (1000 / swipeSpeed)));//, 1000.0f,deckSideLerpCounter);
                Camera.main.transform.position = new Vector3(GetCameraTarget().position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
                if (Mathf.Abs(GetCameraTarget().anchoredPosition3D.x - deckPanelCenterToLerpTo.x) < 0.1f)
                {
      //              Debug.Log("Has reached destination");
                    hasReachDestination = true;
                }
                yield return null;
            }
            lerpingCardPanelsSideways = false;
            horizontalSwipe = false;
            verticalSwipe = false;
            swipeSpeed = 0;
        }

        void LerpToClosestDeckPanelOnEndedSideSwipe()
        {
//            Debug.Log("GetCameraTarget().anchoredPosition3D.x: " + -GetCameraTarget().anchoredPosition3D.x + " compared against: " + ((currentDeckID) * new Vector3(mainCanvas.pixelRect.width, 0, 0) - new Vector3((mainCanvas.pixelRect.width) * (deckChangeScreenFraction - (swipeSpeed * swipeSpeedMultiplier)), 0, 0)).x);
            if (GetCameraTarget().anchoredPosition3D.x > ( (currentDeckID) * new Vector3(mainCanvas.pixelRect.width, 0, 0) + new Vector3((mainCanvas.pixelRect.width) * (deckChangeScreenFraction - (swipeSpeed * swipeSpeedMultiplier)), 0, 0)).x)
            {
                if (decks.Count > currentDeckID + 1)
                {
                    deckPanelCenterToLerpTo =  (currentDeckID + 1) * new Vector3(mainCanvas.pixelRect.width, 0, 0);

                    currentDeckID = currentDeckID + 1;


                    if (currentDeckID >= 1)
                    {
                        introCardInstance.HideTutorial();
                    }
                       SetClosestDeckArray();
                      EnableOnlyClosestsPanels();
      //                                  Debug.Log("Lerping forward, current deck ID: " + currentDeckID);
                    currentUsedDeck = decks[currentDeckID];
                }
            }
                
            else if (GetCameraTarget().anchoredPosition3D.x < ((currentDeckID) * new Vector3(mainCanvas.pixelRect.width, 0, 0) - new Vector3(mainCanvas.pixelRect.width * (deckChangeScreenFraction - (swipeSpeed * swipeSpeedMultiplier)), 0, 0)).x)
            {
                if (0 <= currentDeckID - 1)
                {

                    deckPanelCenterToLerpTo =  (currentDeckID - 1) * new Vector3(mainCanvas.pixelRect.width, 0, 0);
                    currentDeckID = currentDeckID - 1;

                    if (currentDeckID == 0)
                    {
                        introCardInstance.ShowTutorial();
                    }


                     SetClosestDeckArray();
                       EnableOnlyClosestsPanels();
     //               Debug.Log("Lerping backward, current deck ID: " + currentDeckID);
                    currentUsedDeck = decks[currentDeckID];
                    cardPanel = currentUsedDeck.deckCardPanel.gameObject;

                }
            }
            
            UpdateCardPanelUsername();
            UpdateSwitcher();
        }

        public void LerpToSpecificDeckPanel(int gotoDeckID)
        {
            currentDeckID = gotoDeckID;
            currentUsedDeck = decks[currentDeckID];
           // LerpToClosestDeckPanelOnEndedSideSwipe();
            SetClosestDeckArray();
            EnableOnlyClosestsPanels();
            deckPanelCenterToLerpTo =  (currentDeckID) * new Vector3(mainCanvas.pixelRect.width, 0, 0);
            StartCoroutine(CheckForLerpBackFromSideSwipe());
            introCardInstance.HideTutorial();
            UpdateCardPanelUsername();
            UpdateSwitcher();
        }


		public void JumpToSpecifcDeckPanel(int gotoDeckID){
			currentDeckID = gotoDeckID;
		//	SetClosestDeckArray();
//			EnableOnlyClosestsPanels();

			((RectTransform)GetDeckPanel().transform).anchoredPosition3D =  ((RectTransform)GetMainContentPanel().transform).anchoredPosition3D - (currentDeckID) * new Vector3(mainCanvas.pixelRect.width, 0, 0);
			introCardInstance.HideTutorial();
		//	UpdateCardPanelUsername();
			UpdateSwitcher();
		}


        public void StepBackInPreviousScreens(int stepAmount)
        {
            StartCoroutine(StepBackInPreviousScreens_Task(stepAmount));
        }

        IEnumerator StepBackInPreviousScreens_Task(int stepAmount)
        {
            for (int i = 0; i < stepAmount; i++)
            {
                Debug.Log("Show previous screen, step " + i);
                StartCoroutine(ViewIScreenController.ShowPreviousScreen());
                while (ViewIScreenController.IsTransitionRunning())
                {
                    Debug.Log("Transition still running");
                    yield return null;

                }
              //  yield return new WaitForSeconds(0.1f);
            }
        }




        public void DisableAllCardPanelVerticalScrolling()
        {
            for (int i = 0; i < decks.Count; i++)
            {
                decks[i].deckCardPanel.DeactivateVerticalScrolling();
            }
        }
        public void EnableAllCardPanelVerticalScrolling()
        {
           
            for (int i = 0; i < decks.Count; i++)
            {
                decks[i].deckCardPanel.ActivateVerticalScrolling();

            }
        }

        public void DisableAllOtherPanels()
        {

            for (int i = 0; i < decks.Count; i++)
            {
                if (i != GetCurrentDeckID())
                {
                    decks[i].deckCardPanel.gameObject.SetActive(false);
                }
            }
        }

        int[] activePanels = new int[3];
        public void SetClosestDeckArray()
        {
            for (int i = 0; i < 3; i++)
            {
                
                if (i == 0)
                {
                    activePanels[i] = GetCurrentDeckID()-1;
                    if (GetCurrentDeckID() - 1 < 0)
                    {
                        activePanels[i] = GetCurrentDeckID();
                    }
                }
                if (i == 1)
                {
                    activePanels[i] = GetCurrentDeckID();
                }
                if (i == 2)
                {
                    activePanels[i] = GetCurrentDeckID()+1;

                    if (GetCurrentDeckID() + 1 >= decks.Count)
                    {
                        activePanels[i] = GetCurrentDeckID();
                    }
                }
              
            }

            //  Debug.Log("ACTIVE PANELS   "+ activePanels[0] + "   " + activePanels[1] + "   " + activePanels[2]);
        }

        public void EnableOnlyClosestsPanels()
        {
            for (int i = 0; i < decks.Count; i++)
            {
  
                decks[i].deckCardPanel.thisCanvas.enabled = false;
                

                for (int j = 0; j < activePanels.Length; j++)
                {
                    if (i == activePanels[j])
                    {
                        decks[i].deckCardPanel.thisCanvas.enabled = true;
                    }
                }
            }
        }


        public void OnMinimzedDeckClick(){
            if (currentEnlargedCard != null)
            {
                if (currentEnlargedCard.actualCard.GetGameObject() != null && !unPresentCard && presentCard)
                {
                    Debug.Log("Maximize deck");
                    presentCard = false;
                    unPresentCard = true;
                    			
                    StartCoroutine(currentEnlargedCard.UnPresentEnlargedCard());
                    maximizingDeckNow = true;
                    StartCoroutine(MaximizeDeck());
                    maximizeFooterNow = true;
                    currentEnlargedCard.cardIsFocused = false;

                    //   EnableAllCardPanelVerticalScrolling();

                    //currentEnlargedCard.ArrangeInOriginalOrder();
                    currentEnlargedCard.EnableCardMainButton();
                    StartCoroutine(MaximizeFooter());
                    Debug.Log("Maximize deck");
                }
            }
        }

       

        public IEnumerator CreateDeckOnServerResponse()
        {
            /*
            while (!UserStats.userContentRecieved)
            {
                Debug.Log("Waiting for create new deck update response");
                yield return null;
            }
            */

            Debug.Log("CREATE DECK FROM INVITE OR CREATE!");
            CreateDecksFromInviteOrCreate();
            yield return null;
        }

        public void CreateDecksFromInviteOrCreate()
        {
            Deck deckInstance = new Deck();

            CardPanel cardPanelInstance = ((GameObject)Instantiate(ThrivePrefabResources.cardPanelPrefab, Vector3.zero, Quaternion.identity)).GetComponent<CardPanel>();
            cardPanelInstance.transform.SetParent(GetDeckPanel().transform, false);

            User newUser = new User("User" + decks.Count, User.Role.Child);
            cardPanelInstance.user = newUser;

            deckInstance.deckCardPanel = cardPanelInstance;
            deckInstance.SetDeckID(decks.Count);
            decks.Add(deckInstance);
            ((RectTransform)cardPanelInstance.transform).anchoredPosition3D += new Vector3((decks.Count-1) * ((RectTransform)mainContent.transform).rect.width, 0, 0);

            AddToSwitcher(1);
          //  UpdateSwitcher();
            /*

            //!!! only works for single invites !!!!
            int lastAdded = UserStats.connectedChildren.Count-1;

            Debug.Log("UserStats.connectedChildren length: " + UserStats.connectedChildren.Count);

            for (int j = 0; j < UserStats.connectedChildren[lastAdded].availableCards.Count; j++)
            {
                Debug.Log("Add card " + (CardTypes.GroCards)Enum.Parse(typeof(CardTypes.GroCards), UserStats.connectedChildren[lastAdded].availableCards[j].ToString()));

                AddCard(lastAdded+1, (CardTypes.GroCards)Enum.Parse(typeof(CardTypes.GroCards), UserStats.connectedChildren[lastAdded].availableCards[j].ToString()));//UserStats.connectedChildren[i].availableCards[j].ToString());
            }
            */
           
        }

        void CreateAllDecks() {

            /*
           // Debug.Log("UserStats.connectedChildren.Count is here: " + UserStats.connectedChildren.Count);
            if (UserStats.connectedChildren.Count > 0)
            {
                for (int i = 0; i < UserStats.connectedChildren.Count + 1; i++)
                {
                    Deck deckInstance = new Deck();

                    CardPanel cardPanelInstance = ((GameObject)Instantiate(ThrivePrefabResources.cardPanelPrefab, Vector3.zero, Quaternion.identity)).GetComponent<CardPanel>();
                    cardPanelInstance.transform.SetParent(GetDeckPanel().transform, false);

                    if (i == 0)
                    {
                        User newUser = new User("User" + i, User.Role.Owner);
                        cardPanelInstance.user = newUser;
                        cardPanelInstance.DeactivateVerticalScrolling();
                    }
                    else
                    {
                        User newUser = new User("User" + i, User.Role.Child);
                        cardPanelInstance.user = newUser;
                    }
                    deckInstance.deckCardPanel = cardPanelInstance;
                    deckInstance.SetDeckID(i + 1);
                    decks.Add(deckInstance);

                    ((RectTransform)cardPanelInstance.transform).anchoredPosition3D += new Vector3(i * ((RectTransform)mainContent.transform).rect.width, 0, 0);

                    if (i == 0)
                    {
                        cardPanel = cardPanelInstance.gameObject;
                        currentUsedDeck = deckInstance;
                    }
                }
            }
            else
            {
                CreateSingleDeck();
            }
             * */
        }

        void CreateSingleDeck()
        {
            Deck deckInstance = new Deck();

            CardPanel cardPanelInstance = ((GameObject)Instantiate(ThrivePrefabResources.cardPanelPrefab, Vector3.zero, Quaternion.identity)).GetComponent<CardPanel>();
            cardPanelInstance.transform.SetParent(GetDeckPanel().transform, false);
            User newUser = new User("User" + 0, User.Role.Owner);
            cardPanelInstance.user = newUser;
            cardPanelInstance.DeactivateVerticalScrolling();
            deckInstance.deckCardPanel = cardPanelInstance;
            deckInstance.SetDeckID(0);
            decks.Add(deckInstance);
            currentUsedDeck = deckInstance;
            Debug.Log("Create single deck, decks.Count: " + decks.Count);
        }
       

        void MinimizeHeader()
        {
            float height = header.GetComponent<LayoutElement>().flexibleHeight;
            if (height == headerSmallScreenPercentage)
            {
                minimizeHeaderNow = false;
            }
            
            header.GetComponent<LayoutElement>().flexibleHeight = Mathf.Lerp(height, headerSmallScreenPercentage,0.1f); 
        }

        void MaximizeHeader()
        {
            float height = header.GetComponent<LayoutElement>().flexibleHeight;
            if (height == headerLargeScreenPercentage)
            {
                maximizeHeaderNow = false;
            }
            header.GetComponent<LayoutElement>().flexibleHeight = Mathf.Lerp(height, headerLargeScreenPercentage, 0.1f); 
        }


        public IEnumerator MinimizeFooter()
        {
            while (minimizeFooterNow && !maximizeFooterNow && minMaxCounter <= 1)
            {
                ((RectTransform)GetFooterPanel().transform).anchoredPosition = new Vector2(((RectTransform)GetFooterPanel().transform).anchoredPosition.x, Mathf.Lerp(0, -((RectTransform)footer.transform).rect.height, minMaxCounter));
                yield return null;
            }
            ((RectTransform)GetFooterPanel().transform).anchoredPosition = new Vector2(((RectTransform)GetFooterPanel().transform).anchoredPosition.x, -((RectTransform)footer.transform).rect.height);
            minimizeFooterNow = false;
        }

        public IEnumerator MaximizeFooter()
        {
            while (!minimizeFooterNow && maximizeFooterNow && minMaxCounter >= 0)
            {
                ((RectTransform)footer.transform).anchoredPosition = new Vector2(((RectTransform)footer.transform).anchoredPosition.x, Mathf.Lerp(-((RectTransform)footer.transform).rect.height, 0, 1 - minMaxCounter));
                yield return null;
            }
            ((RectTransform)GetFooterPanel().transform).anchoredPosition = new Vector2(((RectTransform)GetFooterPanel().transform).anchoredPosition.x, 0);
          
            maximizeFooterNow = false;
        }




        public void StoreCardsPositionInDeckBeforeAnimate(){

            //if ( currentEnlargedCard == null)
            //{
                if ((!maximizingDeckNow && !minimizingDeckNow) )
                {
                    Debug.Log("Store card positions");
                    if (cardsPositionInMaximizedDeck == null)
                    {
                        cardsPositionInMaximizedDeck = new List<Vector3>();
                    }
                    else
                    {
                        cardsPositionInMaximizedDeck = null;
                        cardsPositionInMaximizedDeck = new List<Vector3>();
                    }

                    for (int i = 0; i < currentUsedDeck.cards.Count; i++)
                    {

                        cardsPositionInMaximizedDeck.Add(((RectTransform)currentUsedDeck.cards[i].transform).anchoredPosition);
                        Debug.Log("Elements stored: " + cardsPositionInMaximizedDeck.Count);
                    }
                }
           // }
        }



        //Function that will be called untill deck has been minimized
        public IEnumerator MinimizeDeck()
        {
            while (minimizingDeckNow && !maximizingDeckNow && minMaxCounter <= 1)
            {
                minMaxCounter += Time.deltaTime * minMaxDeckSpeed;
                int visibleCardsInMinimizedDeckCounter = 0;

                for (int i = 0; i < currentUsedDeck.cards.Count; i++)
                {
                    if (!(currentUsedDeck.cards[i]).cardIsFocused)
                    {
						((RectTransform)currentUsedDeck.cards[i].transform).anchoredPosition = Vector2.Lerp(new Vector3(0, cardsPositionInMaximizedDeck[i].y), new Vector2(0, mainContentMinimizedDeckYPosition - (minimizedDeckSpacing * visibleCardsInMinimizedDeckCounter)), cardPresentationCurve.Evaluate(minMaxCounter));
                        visibleCardsInMinimizedDeckCounter++;
                    }
                }
                yield return null;
            }
            minimizingDeckNow = false;
            minMaxCounter = 1;
        }

        

        //Function that will be called until a the deck has been maximized
        public IEnumerator MaximizeDeck()
        {

			while (currentEnlargedCard.placeholderGraphicsContent.color.a < 1) {
				yield return null;
			}

            while (!minimizingDeckNow && maximizingDeckNow && minMaxCounter >= 0)
            {
                minMaxCounter -= Time.deltaTime * minMaxDeckSpeed;

                for (int i = 0; i < currentUsedDeck.cards.Count; i++)
                {
                    RectTransform cardRectTransform = ((RectTransform)(currentUsedDeck.cards[i]).transform);
                        
                    if (!(currentUsedDeck.cards[i]).cardIsFocused && currentEnlargedCard.cardID != (currentUsedDeck.cards[i]).cardID)
                    {
						cardRectTransform.anchoredPosition = Vector2.Lerp(new Vector2(0, mainContentMinimizedDeckYPosition - (minimizedDeckSpacing * i)), new Vector2(0, cardsPositionInMaximizedDeck[i].y), 1 - cardPresentationCurve.Evaluate(minMaxCounter));
                    }
                  
                }
                yield return null;
                
            }
           // currentEnlargedCard.transform.parent.GetComponent<VerticalLayoutGroup>().enabled = true;
            maximizingDeckNow = false;
            minMaxCounter = 0;
      //      scrollAreaRect.movementType = ScrollRect.MovementType.Elastic;
          //  scrollAreaRect.vertical = true;
        }


        //Used to retrieve different heights for header, depending on the device.
        public Vector2 GetHeaderSize(){
            //This is the big header, for IOS
            #if !UNITY_IPHONE
            return new Vector2(0, 0.8890001f);
             #else

            return new Vector2(0, 0.8890001f);
            #endif
        }

        public Vector2 GetMainContentSize(){
            return new Vector2(1, GetHeaderSize().y);
        }

        public FadeInOutScreen GetFaderScreen()
        {
            instantiatedFadeScreen.transform.SetParent(currentScreenCanvas.transform, false);
            return instantiatedFadeScreen;
        }

		public RectTransform GetDeckPanelRectTransform()
		{
			return deckPanelRectTransform;
		}

		public GameObject GetDeckPanel()
        {
            return deckPanel;
        }

        public RectTransform GetCameraTarget()
        {
            return cameraTarget;
        }

        public GameObject GetCardPanel()
        {
            return cardPanel;
        }
        public CardPanelHeader GetHeaderPanel()
        {
            return header;
        }
        public GameObject GetFooterPanel()
        {
            return footer;
        }

        public GameObject GetMainContentPanel()
        {
            return mainContent;
        }

        public LoginScreen GetLoginScreen()
        {
            if (instantiatedLoginScreen == null)
            {
                instantiatedLoginScreen = (LoginScreen)Instantiate(ThrivePrefabResources.loginScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedLoginScreen.transform).anchoredPosition = new Vector2(Screen.width, 0);
                instantiatedLoginScreen.transform.SetParent(currentScreenCanvas.transform, false);
            }
            return instantiatedLoginScreen;
        }

        public AccountCreationScreen GetCreateAccountScreen()
        {
            if (instantiatedAccountCreationScreen == null)
            {
                instantiatedAccountCreationScreen = (AccountCreationScreen)Instantiate(ThrivePrefabResources.createAccountScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedAccountCreationScreen.transform).anchoredPosition = new Vector2(Screen.width,0);
                instantiatedAccountCreationScreen.transform.SetParent(currentScreenCanvas.transform, false);
            }
            return instantiatedAccountCreationScreen;
        }


        public TermsAgreementScreen GetTermsAgreementScreen()
        {
            if (instantiatedTermsAndAgreementScreen == null)
            {
                instantiatedTermsAndAgreementScreen = (TermsAgreementScreen)Instantiate(ThrivePrefabResources.termsAndAgreementScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedTermsAndAgreementScreen.transform).anchoredPosition = new Vector2(Screen.width, 0);
                instantiatedTermsAndAgreementScreen.transform.SetParent(currentScreenCanvas.transform, false);
            }
            return instantiatedTermsAndAgreementScreen;
        }


        public PrivacyStatementScreen GetPrivacyStatementScreen()
        {
            if (instantiatedPrivacyStatementScreen == null)
            {
                instantiatedPrivacyStatementScreen = (PrivacyStatementScreen)Instantiate(ThrivePrefabResources.privacyStatementScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedPrivacyStatementScreen.transform).anchoredPosition = new Vector2(Screen.width, 0);
                instantiatedPrivacyStatementScreen.transform.SetParent(currentScreenCanvas.transform, false);
            }
            return instantiatedPrivacyStatementScreen;
        }

        public RecoverAccountScreen GetRecoverAccountScreen()
        {
            if (instantiatedRecoverAccountScreen== null) {
                instantiatedRecoverAccountScreen = (RecoverAccountScreen)Instantiate(ThrivePrefabResources.recoverAccountScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedRecoverAccountScreen.transform).anchoredPosition = new Vector2(Screen.width, 0);
                instantiatedRecoverAccountScreen.transform.SetParent(currentScreenCanvas.transform, false); 
            }
            return instantiatedRecoverAccountScreen;
        }

        public FAQScreen GetFAQScreen()
        {
            if (instantiatedFaqScreen == null)
            {
                instantiatedFaqScreen = (FAQScreen)Instantiate(ThrivePrefabResources.faqScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedFaqScreen.transform).anchoredPosition = new Vector2(Screen.width, 0);
                instantiatedFaqScreen.transform.SetParent(currentScreenCanvas.transform, false);
            }
            return instantiatedFaqScreen;
        }

        public CompletedFlowScreen GetCompletedFlowScreen()
        {
            if (instantiatedFlowScreen == null)
            {
                instantiatedFlowScreen = (CompletedFlowScreen)Instantiate(ThrivePrefabResources.completedFlowScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedFlowScreen.transform).anchoredPosition = new Vector2(Screen.width, 0);
                instantiatedFlowScreen.transform.SetParent(currentScreenCanvas.transform, false);
            }
            return instantiatedFlowScreen;
        }

        public OptionsScreen GetOptionsScreen()
        {
            if (instantiatedOptions == null)
            {
                instantiatedOptions = (OptionsScreen)Instantiate(ThrivePrefabResources.optionsPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedOptions.transform).anchoredPosition = new Vector2(Screen.width, 0);
                instantiatedOptions.transform.SetParent(currentScreenCanvas.transform, false);
            }
            return instantiatedOptions;
        }
        /*
        public ProfileScreen GetProfileScreen()
        {
            if (instantiatedProfileScreen == null)
            {
                instantiatedProfileScreen = (ProfileScreen)Instantiate(ThrivePrefabResources.profileScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedProfileScreen.transform).anchoredPosition = new Vector2(Screen.width, 0);
                
            }
            instantiatedProfileScreen.transform.SetParent(transform, false);
            return instantiatedProfileScreen;
        }
        */
        public PasswordResetScreen GetPasswordResetScreen()
        {
            if (instantiatedPasswordResetScreen == null)
            {
                instantiatedPasswordResetScreen = (PasswordResetScreen)Instantiate(ThrivePrefabResources.passwordResetScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedPasswordResetScreen.transform).anchoredPosition = new Vector2(Screen.width, 0);
                instantiatedPasswordResetScreen.transform.SetParent(currentScreenCanvas.transform, false);
            }
            return instantiatedPasswordResetScreen;
        }

        public LoadingScreen GetLoadingScreen()
        {
            if (instantiatedLoadingScreen == null)
            {
                instantiatedLoadingScreen = (LoadingScreen)Instantiate(ThrivePrefabResources.loadingScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)instantiatedLoadingScreen.transform).anchoredPosition = new Vector2(Screen.width, 0);
				instantiatedLoadingScreen.transform.SetParent( currentScreenCanvas.transform, false);
            }
            return instantiatedLoadingScreen;
        }


        public BlankBlackScreen GetBlankScreen()
        {
            if (blankScreen == null)
            {
                blankScreen = (BlankBlackScreen)Instantiate(ThrivePrefabResources.blankBlackScreenPrefab, Vector3.zero, Quaternion.identity);
                ((RectTransform)blankScreen.transform).anchoredPosition = new Vector2(Screen.width, 0);
                blankScreen.transform.SetParent(presentCardCanvas.transform, false);
            }
            return blankScreen;
        }



        public TutorialScreen GetTutorialScreen()
        {
            return introCardInstance.GetTutorialScreen();
        }
        /*

        public InvitationManager GetInvitationManager()
        {

            return invitationManager;
        }*/

        public int GetCurrentDeckID()
        {
            return currentDeckID;
        }

        public bool GetIsLoggedIn()
        {
            return isLoggedIn;
        }

		public void StartChildCoroutine(IEnumerator coroutineMethod)
		{
			StartCoroutine(coroutineMethod);
		}
    }
}
